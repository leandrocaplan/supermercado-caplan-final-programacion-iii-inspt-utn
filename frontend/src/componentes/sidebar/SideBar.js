import React from "react";

import {
	faMoneyBill,
	faCartPlus,
	faCoins,
	faShoppingBag,
	faUser

} from "@fortawesome/free-solid-svg-icons";
import { NavItem, NavLink, Nav } from "reactstrap";
import classNames from "classnames";
import { Link } from "react-router-dom";

import SubMenu from "./SubMenu";

const SideBar = ({ isOpen, toggle }) => (
	<div className={classNames("sidebar", { "is-open": isOpen })}>
		<div className="sidebar-header">
		
			<span color="info" onClick={toggle} style={{ color: "#fff" }}>

			</span>
			
			<h3>Supermercado Caplan</h3>
		</div>
		
		<div className="side-menu">
			<Nav vertical className="list-unstyled pb-3">
				<h1>Menú Principal</h1>

				<SubMenu title="Ventas" icon={faCartPlus} items={submenus[0]} />

				{
					JSON.parse(sessionStorage.getItem('user')).categoria=='administrador' ?
					<>
						<SubMenu title="Compras" icon={faMoneyBill} items={submenus[1]}/>
						<SubMenu title="Productos" icon={faShoppingBag} items={submenus[2]}/>
						<SubMenu title="Usuarios" icon={faUser} items={submenus[3]}/>
						<SubMenu title="Cuentas"  icon={faCoins} items={submenus[4]}/>

						<NavItem>
							<NavLink tag={Link} to={"/resetear"}>
								Resetear
							</NavLink>
						</NavItem>

					</>
					:<></>
				}

				<NavItem>
					<NavLink tag={Link} to={"/logout"}>
						Cerrar sesión
					</NavLink>
				</NavItem>
			</Nav>
		</div>
	</div>
);


const submenus = [
  [
    {
      title: "Nueva venta",
      target: "/ventas/nuevo",
    },
    {
      title: "Lista de ventas",
      target: "/ventas/lista",
    },
        {
      title: "Registro de detalles",
      target: "/ventas/registro",
    },
  ],

  [
    {
      title: "Nueva compra",
      target: "/compras/nuevo",
    },
    {
      title: "Lista de compras",
      target: "/compras/lista",
    },
    {
      title: "Registro de detalles",
      target: "/compras/registro",
    },
   ],
   [
    {
      title: "Nuevo producto",
      target: "/productos/nuevo",
    },
    {
      title: "Registro de productos",
      target: "/productos/registro",
    },

   ],
   [
    {
      title: "Nuevo usuario",
      target: "/usuarios/nuevo",
    },
    {
      title: "Registro de usuarios",
      target: "/usuarios/registro",
    },
   ],
   [
    {
      title: "Ingresar o retirar",
      target: "/cuentas/ingresarRetirar",
    },
    {
      title: "Consulta de valores",
      target: "/cuentas/consultar",
    },
   ],
 ];

export default SideBar;
