import React, { Component, useState } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';

import DatePicker from "react-datepicker";
import DateTimePicker from 'react-datetime-picker'

import "react-datepicker/dist/react-datepicker.css";


//Este flag le permite al programador ver en el
//navegador, toda la información util necesario
//para la depuración, mostrada en el renderizado del componente

//Cuando un usuario común debe utilizar la aplicación,
//está por defecto desactivada.

//Cuando un programador está trabajando con el código,
//ya sea el que lo desarrolló inicialmente o bien
//algun otro, simplemente debe establecer 
//este flag en true, y podrá ver en el navegador toda
//la información necesaria para agilizar el proceso de debug
const modoDebug=false;


const mostrarResultantes= true;

//Esta funcion nos resulta util para debugear,
//controlando el stock de cada producto
//y su stock original del lado del frontend
//Solo la utilizo cuando tenga activado el modo debug
const Producto = function(props) { 

	return(
		<tr>
			<td>{Number(props.producto._id)} &nbsp;</td>
			<td>{props.producto.descripcion}&nbsp;</td>
			<td>{props.producto.stock}&nbsp;</td>
			<td>{props.producto.stockResultante}</td>
			<td>{props.producto.stockOriginal}&nbsp;</td>
		</tr>
	);
}

//Esta función, en este componente, me sirve solamente a efectos
//de comparar las fechas, descartando el horario
const formatearFecha =function (fecha) {
	try{
		const fechaObj=new Date(fecha);
		return (
			fechaObj.getDate().toString().padStart(2, "0") + '/'+ 
			(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+ 
			fechaObj.getFullYear()
		);
	}
	catch(error){
		alert(error)
	}
}

const tipoPagoACuenta= {
	'efectivo':'caja',
	'debito':'cuentaCorriente',
	'credito':'creditoAFavor'
};


const tipoPagoAResultante= {
	'efectivo':'resultanteCaja',
	'debito':'resultanteCorriente',
	'credito':'resultanteCredito'
};

//NOTA: En todos los importes monetarios, debo siempre tener dos cifras decimales correspondientes a los centavos.
//Para dicho propósito, a cada operacion en la que esté involucrada
//algun cálculo de un importe, le aplico la función '.toFixed(2)', de manera que evito que el sistema
//muchas mas cifras decimales de las que necesito, a la vez redondeanda mantisa al valor mas cercano correspondiente.

export default class VenderProducto extends Component {
	//Cada venta tiene un encabezado y un detalle
	//Los campos del encabezado son:
	//-Fecha de la venta
	//-Código de la venta
	//-Vendedor
	
	//Estos tres campos son los que identifican a la venta completa
	
	//Luego, el detalle consiste en un array de objetos
	//Cada elemento del array tiene los siguientes campos:
	
	//-Producto (en la API solo guardaremos el ID del producto correposndiente, 
	//pero para operar en el frontend, necesitamos la información del producto completo)
	
	//-Cantidad vendida
	
	//-Tipo de pago (cada tipo de pago está asociado a una cuenta contable distinta)
	
	//-Subtotal
	
	
	//Ademas, la venta completa y cada elemento del detalle tendrá asignado un ID,
	//el cual es generado automáticamente por la API, y no lo utilizamos en este componente del frontend.
	constructor(props) {

		//En el constuctor, definimos los campos que tendrá estado y las inicializamos

		super(props);

		this.state = {

			//Campos a almacenar en el array que mando a la API
			codVenta:1,
			userVendedor: '',
			fechaHora: new Date(),
			fechaHoraActual: true,

			//Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
			vendedor: {},
			productos:[],
			vendedores:[],
			
			//Este campo lo utilizamos para generar automáticamente el código de venta
			codigosExistentes:[],
			
			//Totales por forma de pago
			efectivo: Number(0).toFixed(2),
			debito:Number(0).toFixed(2),
			credito:Number(0).toFixed(2),
			
			//Total general
			total:Number(0).toFixed(2),
			
			//Me guardo la categoria del usuario logueado (vendedor o administrador)
			categoriaUsuario:JSON.parse(sessionStorage.getItem('user')).categoria,
			
			cuentas:{},
			
			//Resultantes de cada cuenta contable
			resultanteCaja:0,
			resultanteCorriente:0,
			resultanteCredito:0,
			
			//Este campo nos resulta útil para agilizar la venta,
			//ya que lo mas probable es que el cliente realize
			//toda la venta con el mismo tipo de pago, por lo
			//cual no es conveniente ir modificando, uno por uno,
			//los tipos de pago asociados a cada elemento del detalle
			tipoPagoPorDefecto:'efectivo',

			isSubmitted:false,
			resData:'',

			//Inicializamos el array correspondiente al detalle con un solo objeto
			detalle:
			[ 
				{
					//Campos a almacenar en el array que mando a la API
					idProducto:'',
					cantidad:1,
					precioActual:0,
					unidadActual:'',
					tipoPago:'efectivo',
					subtotal:0,
					
					 //Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
					producto:{},
				}
			]

		}
		console.log('Ejecuto el constructor');
		console.log(this.state.producto);
	}

        /*
        Nota: el valor del stock resultante de la operación será siempre:
        El valor del stock que el producto tenía previamente - la cantidad vendida en la operación
        */



	//En componentDidMount, hacemos el llamado a la API para que nos traiga los valores
	//necesarios para inicializar el estado y poder generar la información de la venta.
	componentDidMount() {

		console.log(this.state.producto);
		console.log(this.state.vendedores);

		//Al cargar el componente,
		//llamo a la API para que me traiga la lista de vendedores y productos.
		//Este 'get' me trae solamente usuarios registrados como vendedores
		//Tambien me trae la lista de todas las ventas, pero no las utilizaremos
		axios.get('http://localhost:5000/ventas/asociar')
		.then(respuesta => {
			try{
			
				//Para inicializar los campos correspondientes a la venta
				//debo tener al menos un vendedor y un producto almacenados en la base de datos.
				//De lo contrario, no puedo realizar ninguna venta.
				if (respuesta.data.usuarios.length > 0 && respuesta.data.productos.length > 0) {
					
					//Me genero una constante con la cual inicializaré el detalle, correspondiente a un array de un solo objeto
					const detalleInicial=[
						{
							//En el campo corresponidete al ID del producto,
							//me guardo el ID del primer producto devuelto por la base de datos
							idProducto:respuesta.data.productos[0]._id,
							
							//Inicialmente, la cantidad vendida del producto es 1
							cantidad:1,
							
							precioActual:respuesta.data.productos[0].precioVenta,
							unidadActual:respuesta.data.productos[0].unidad,
							
							//Inicialmente, el tipo de pago es en efectivo
							tipoPago:'efectivo',
							
							//Inicialmente, el subtotal del elemento
							//del detalle es equivalente al precio de venta del producto asignado
							//al mismo (ya que su cantidad vendida inicialmente es 1)
							subtotal:respuesta.data.productos[0].precioVenta.toFixed(2),

							 //El producto completo asociado al primer elemento del detalle es: el primer producto devuelto por la base de datos,
							 //al que le añadimos el campo "stockResultante", cuyo valor inicial es el stock
							 //actual del producto menos 1 (ya que 1 es la cantidad vendida inicialmente)
							producto:Object.assign(respuesta.data.productos[0], {stockResultante: Number(respuesta.data.productos[0].stock)-1}),
							
						}
					];
					
					
					this.setState({
						//Me guardo la lista de vendedores devietla por la API
						vendedores: respuesta.data.usuarios,
						
						//En la lista de productos que me guardo en el frontend,
						//me guardo la lista de productos devuelta por la API a la cual
						//le añado un campo: el stock resultante de cada producto.
						productos: respuesta.data.productos.map(function (producto,indice)
								{
									//Genero una variable 'productoConResultante',
									//que inicialmente tiene el mismo contenido que cada
									//elemento de la lista de productos devuelta por la API.
									let productoConResultante=producto;
									
									productoConResultante.stock=Number(producto.stock).toFixed(2);
									//Luego, a la variable generada anteriormente le agrego el campo
									//'stockResultante', cuyo valor inicial se lo asigno con este 'if'
									if(indice==0)
									{	
										//Inicialmente, el stock resultante del primer producto devuelto por
										//la API sera: su stock actual menos 1, ya que inicialmente,
										//en el primer elemento del detalle inicializado estoy vendiendo
										//una unidad de dicho producto.
										productoConResultante.stockResultante=(Number(producto.stock)-1).toFixed(2);
									}
									else
									{
										//Luego, inicialmente, el stock resultante de los demas productos
										//será equivalente a su stock actual
										productoConResultante.stockResultante=Number(producto.stock).toFixed(2);
									}
									//Una vez asignado el campo 'stockResultante' con el valor correspondiente,
									//devuelvo la variable con dicho campo agregado.
									return productoConResultante;
								}
							),
							
						//Inicialmente, el total de la venta pagada en efectivo
						//equivale al subtotal del primer elemento del detalle inicializado previamente
						//(precio de venta del primer producto de la base de datos devuelto por la API)
						efectivo:respuesta.data.productos[0].precioVenta.toFixed(2),
						
						//Inicialmente, el total general de la venta, tambien
						//equivale al subtotal del primer elemento del detalle inicializado previamente
						//(precio de venta del primer producto de la base de datos devuelto por la API)
						total:respuesta.data.productos[0].precioVenta.toFixed(2),
						
						//Me guardo el estado actual de las cuentas que tengo en la base de datos
						cuentas: {
									//Me aseguro de tener siempre dos cifras decimales
									caja:respuesta.data.cuentas.caja.toFixed(2),
									cuentaCorriente:respuesta.data.cuentas.cuentaCorriente.toFixed(2),
									creditoAFavor:respuesta.data.cuentas.creditoAFavor.toFixed(2),
								 },
						
						//Necesito guardarme el listado de ventas para luego generar automáticamente
						//el código de venta correspondiente, para evitar que se pise con el de
						//una venta ya existente.
						ventas: respuesta.data.ventas,
						
						//Debo inicializar los resultantes de cada cuenta contable, cuyo valor
						//equivaldrá a lo que me queda en cada cuenta luego de hacer la operación.
						
						//Inicialmente, el resultante de caja, equivaldrá al valor actual de caja,
						//mas el subtotal del primer elemento del detalle inicializado previamente
						//(precio de venta del primer producto de la base de datos devuelto por la API).
						resultanteCaja: Number(respuesta.data.cuentas.caja +  respuesta.data.productos[0].precioVenta).toFixed(2),
						
						//Luego, inicialmente, el resultante de la cuenta corriente y el crédito a favor,
						//serán iguales a su valor actual
						resultanteCorriente:Number(respuesta.data.cuentas.cuentaCorriente).toFixed(2),
						resultanteCredito:Number(respuesta.data.cuentas.creditoAFavor).toFixed(2),
						
						//Al detalle de venta, lo inicializamos con la const 'detalleInicial', definida mas arriba
						detalle:detalleInicial
					});
								  
			   
					//Obtengo el máximo codigo de venta asignado al actual vendedor y la fecha actual,
					//para luego sumarle 1 al código de venta de la venta actual.
					//De otra forma, en vez de generar una venta nueva, probablemente
					//se agregarían elementos al detalle de una venta existente
					//a menos que el usuario configure a mano un codigo de venta aún inexistente
					
					this.setState(function(estadoAnterior) {
						let estadoNuevo=estadoAnterior;
						let codVentaMax=1;
						
						//Si existe una venta con el ID que recibí por parámetro, me la guardo en
						//la const 'ventaExistente'
						const ventaExistente=respuesta.data.ventas.find(venta=> venta._id==this.props.match.params.idVenta);
						//Primero asigno el usuario y la fecha antes de generar
						//automáticamente el código de venta
						
						
						if(
							//Si no recibo parámetros, estoy por defecto, generando una venta nueva
							(!this.props.match.params.idVenta) ||
							//Si el vendedor al cual tenía asignada la venta existente ya
							//no existe en la base de datos, genero una venta nueva
							(!respuesta.data.usuarios.some(vendedor=> vendedor.username==ventaExistente.vendedor))
						)
						{	
							//Si el usuario actual es un administrador, puedo asignar el vendedor
							//que yo elija a la venta realizada
							if(estadoAnterior.categoriaUsuario =='administrador') {
									//Si no recibo parámetros, por defecto, el vendedor es el primero
									//de la lista que me devuelve la API
									estadoNuevo.userVendedor = respuesta.data.usuarios[0].username;
									estadoNuevo.vendedor= respuesta.data.usuarios[0];
						
							}

							//Si el usuario actual es un vendedor, la venta quedará asociada al mismo
							else {

									estadoNuevo.userVendedor= JSON.parse(sessionStorage.getItem('user')).username;
									estadoNuevo.vendedor = JSON.parse(sessionStorage.getItem('user'));
							}
							estadoNuevo.fechaHora=new Date();
							estadoNuevo.fechaHoraActual=true;
						}
						//Si recibo parámetros, estoy por defecto, agregando elementos a una venta existente
						//Me cargo los datos correspondientes a la misma
						else{
							estadoNuevo.userVendedor=ventaExistente.vendedor;
							estadoNuevo.vendedor=respuesta.data.usuarios.find(vendedor=> vendedor.username==ventaExistente.vendedor);
							estadoNuevo.codVenta=ventaExistente.codVenta;
							estadoNuevo.fechaHora=new Date(ventaExistente.fechaHora);
							estadoNuevo.fechaHoraActual=false;
						}
						
						//Genero la lista de códigos de venta existentes correspondientes
						//al vendedor actual y a la fecha de hoy
						const listaCodigosVentas=this.state.ventas.filter(
							function(venta) {
								if(
									(venta.vendedor==estadoNuevo.userVendedor)
									&&
									(formatearFecha(venta.fechaHora) == formatearFecha(estadoNuevo.fechaHora))
								)

									return venta;
							}
						)
						.map(
							function(venta)
							{
								return venta.codVenta;
							}
						);


						const maximo=Math.max(...listaCodigosVentas);
						console.log(listaCodigosVentas);
						console.log(maximo);
						
						estadoNuevo.codigosExistentes=listaCodigosVentas;
						
						//Si no recibí ningun parámetro (estoy generando una venta nueva)
						if(!this.props.match.params.idVenta){
							
							//Si tengo algun código existente en la 
							//lista que generé, el codigo actual
							//será el máximo que tengo en la lista mas 1
							if(maximo>=0){
								estadoNuevo.codVenta=maximo+1;
							}
							//Si no tengo ninguno, el código
							//de la venta actual será 1
							else{
								estadoNuevo.codVenta=1;
								}								
						}

						return estadoNuevo;
					});
			
				}		
			}
			catch(error){
				alert(error)
			}
		})
		.catch((error) => {
			  alert(error);
			}) 

		console.log('Ejecuto el componentDidMount');
	}


	//Esta funcion la utilizo solamente para ejecutar
	//console.log para el propósito de debugear
	componentDidUpdate() {
	   
	
			console.log(this.state.codVenta);
			console.log(this.state.codigosExistentes);
			
	}


	//Genero el listado del array de productos actual a modo de debug
	listaProductos() {    
		return this.state.productos.map(productoActual => {
			return <Producto producto={productoActual}   />;
		})
	}

/*
----------------------------------------------------
Sección de 'onChange'

Al hacer cualquier modificación en algun campo del formulario,
la variable 'isSubmitted' la reseteamos en 'false'
----------------------------------------------------

*/


	onChangeFechaHoraActual = (e) => {
		try{
			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codVentaMax=1;
				console.log(estadoAnterior.userVendedor);
				
				//Genero la lista de códigos de venta de acuerdo a la fecha de hoy
				const listaCodigosVentas =
				this.state.ventas
					.filter(
						function(venta) {
							if(
									//Filtro las ventas de acuedo al vendedor que tengo en el estado
									(venta.vendedor==estadoAnterior.userVendedor)
									&&
									(
										//Si estoy activando el flag 'fechaHoraActual",
										//el resultado del filter me devolverá las ventas
										//correspondientes al dia de hoy
										 (e.target.checked && formatearFecha(venta.fechaHora) == formatearFecha(new Date())) ||
										 
										 //Si lo estoy desactivando,
										 //el resultado del filter me devolverá las ventas
										 //correspondientes al estado de fechaHora que tengo actualmente
										 (!e.target.checked && formatearFecha(venta.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
										
									)
								)

								return venta;
								}
							)
						.map(
							function(venta)
							{
								//Me quedo solamente con el campo correspondiente al código, del array filtrado previamente
								return venta.codVenta;
							}
						);
						
				//Genero el código de venta con el mismo
				//algoritmo que utiilze en el componentDidMount
				const maximo=Math.max(...listaCodigosVentas);
				console.log(listaCodigosVentas);
				console.log(maximo);
				
				estadoNuevo.codigosExistentes=listaCodigosVentas;
				if(maximo>=0){
					estadoNuevo.codVenta=maximo+1;
				}
				else{
					estadoNuevo.codVenta=1;
					}
				return estadoNuevo;
			});


			//Actualizo el estado del flag 'fechaHoraActual'
			this.setState({
				fechaHoraActual: e.target.checked,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error)
		}
	}
	onChangeFechaHora = (e) => {
		try{

			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codVentaMax=1;
				console.log(estadoAnterior.userVendedor);

				const listaCodigosVentas=this.state.ventas.filter(
					//Si la opción 'fechaHoraActual' está activada,
					//sería redundante corroborarlo en esta función,
					//ya que el propio renderizado condicional del 
					//componente no me permitirá acceder al
					//DateTimePicker que dispara el evento correspondiente
					//a la misma
					function(venta) {
						if(
							//El filter me devuelve todas las ventas correspondientes
							//al vendedor actual del estado y a la fecha seleccionada
							(venta.vendedor==estadoAnterior.userVendedor)
							&&
							(formatearFecha(venta.fechaHora) == formatearFecha(e))
						)

							return venta;
						}
					)
					.map(
						function(venta)
					{
						//Me quedo solamente con el campo correspondiente al código, del array filtrado previamente
						return venta.codVenta;
					}
				);

				//Genero el código de venta con el mismo
				//algoritmo que utiilze en el componentDidMount
				const maximo=Math.max(...listaCodigosVentas);
				console.log(listaCodigosVentas);
				console.log(maximo);
				
				estadoNuevo.codigosExistentes=listaCodigosVentas;
				if(maximo>=0){
					estadoNuevo.codVenta=maximo+1;
				}
				else{
					estadoNuevo.codVenta=1;
					}
				return estadoNuevo;
			});		
			
			//Actualizo el estado de fechaHora
			this.setState({
				fechaHora: e,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error)
		}
	}
	//Al modificar el codigo de venta en el form, actualizo el estado del codigo de venta
	onChangeCodVenta = (e) => {
		try{
			this.setState({
				codVenta: Number(e.target.value),
				isSubmitted:false
			})
		}
		
		catch(error){
			alert(error)
		}
	}

	//Al modificar el vendedor en el form, actualizo el username del vendedor
	//y el vendedor completo asociado a ese username
	onChangeVendedor = (e) => {
		try{


			//Obtengo el máximo codigo de venta asignado al actual vendedor y la fecha actual,
			//para luego sumarle 1 al código de venta de la venta actual.
			//De otra forma, en vez de generar una venta nueva, probablemente
			//se agregarían elementos al detalle de una venta existente
			//a menos que el usuario configure a mano un codigo de venta aún inexistente
			
			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codVentaMax=1;
				console.log(estadoAnterior.userVendedor);
				
				const listaCodigosVentas=this.state.ventas.filter(
				function(venta) {
					if(
						//Filtro todas las ventas correspondientes al vendedor al que estoy actualizando
						(venta.vendedor==e.target.value)
						&&
						(

							//Si el flag 'fechaHoraActual' está activado,el resultado del filter me devolverá las ventas
							//existentes del vendedor correspondientes al dia de hoy
							 (estadoAnterior.fechaHoraActual && formatearFecha(venta.fechaHora) == formatearFecha(new Date())) ||
							 
							 //Si está desactivado,
							 //el resultado del filter me devolverá las ventas existentes del vendedor
							 //correspondientes al estado de fechaHora que tengo actualmente
							 (!estadoAnterior.fechaHoraActual && formatearFecha(venta.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
						)
					)

						return venta;
					}
				)
				.map(
					function(venta)
				{
					//Me quedo solamente con el campo correspondiente al código, del array filtrado previamente
					return venta.codVenta;
				}
				);

				//Genero el código de venta con el mismo
				//algoritmo que utiilze en el componentDidMount
				const maximo=Math.max(...listaCodigosVentas);
				console.log(listaCodigosVentas);
				console.log(maximo);

				estadoNuevo.codigosExistentes=listaCodigosVentas;
				if(maximo>=0){
					estadoNuevo.codVenta=maximo+1;
				}
				else{
					estadoNuevo.codVenta=1;
					}
				return estadoNuevo;
			});

			//Actualizo en el estado los datos del vendedor
			this.setState({
				userVendedor: e.target.value,
				vendedor: this.state.vendedores.find(p => p.username===e.target.value),
				isSubmitted:false
			})
			console.log(this.state.userVendedor);
		}
		catch(error){
			alert(error)
		}
	}		
	
	//Uso un solo método para actualizar cada campo del detalle	
	onChangeDetalle =  (evento, indiceReg) => {
		try{
			
			//Declaro una variable 'detalle', que contiene el valor anterior del detalle
			let detalle = [...this.state.detalle];
			
			//Declaro una constante 'campo', en la que referencio el campo
			//del elemento del detalle que estoy modificando al ejecutar el evento.
			const campo=evento.target.name;
			
			//Declaro una constante 'valor', que contendrá el valor del campo del
			//elemento del detalle que estoy modificando al ejecutar el evento.
			const valor = evento.target.value;
			

			
			//En una constante 'prodAnterior', me guardo la información
			//del producto completo con los valores que tenía antes de modificar
			//el elemento del detalle.
			const prodAnterior = detalle[indiceReg]['producto'];
			

			//En cada elemento del detalle, necesito tener siempre el mismo
			//stock resultante de acuerdo al producto correspondiente


			if(campo=='cantidad') {
			
				//Al cambiar la cantidad, actualizo el stock resultante del producto asociado al registro
				
				//El stock que tiene el producto antes de realizar la venta,
				//menos la suma de todas las cantidades de los demas elementos 
				//del detalle con dicho producto asignado, 
				//menos la cantidad correspondiente al elemento que estoy modificando
				
					
				const stockResultanteAct =
					
					//Parto del stock actual del producto
					(
						Number(detalle[indiceReg]['producto'].stock)
						
						-
						
						//Luego, necesito restarle la cantidad vendida asociada a todos los otros registros
						//cuyo producto asociado sea el mismo que el registro que estoy actualizando
						
						Number(
						//Recorro todos los elementos del detalle
						this.state.detalle
							.filter(function (elemDetalle, indiceFilter){
								if
									(
										//El registro del detalle donde estoy parado
										//tiene asignado el mismo producto que el
										//registro del detalle que estoy actualizando
										(elemDetalle.idProducto==detalle[indiceReg]['idProducto'] )
										
										&&
										
										//El registro del detalle donde estoy parado
										//es diferente al que estoy actualizando
										(indiceFilter != indiceReg) 
									)
									return elemDetalle;
								})
								
							//Luego, sumo todas las cantidades de los elementos devueltos por el filter
							.reduce(
								function(acumulador,elemDetalle) {
									return acumulador + Number(elemDetalle.cantidad);
										},0)		
								)
								//Finalmente, le resto el valor nuevo correspondiente
								//a la cantidad en el registro que estoy actualizando
								-
								Number(valor)
							).toFixed(2);
							
						
					//Una vez que obtuve el stock resultante actualizado, 
									
					this.setState(function(estadoAnterior){
						let estadoNuevo=estadoAnterior;
						
						//Actualizo la lista de productos con el nuevo stock	
						estadoNuevo.productos=estadoAnterior.productos.map(
							function(producto){
								//Cuando encuentre, en la lista de productos del frontend,
								//el producto correspondiente al del registro que estoy actualizando
								if(producto._id==detalle[indiceReg]['idProducto']){
										
										//Actualizo el stock resultante en el producto
										producto.stockResultante=stockResultanteAct;
										
										//Al producto recien actualizado, lo actualizo en el elemento del detalle correspondiente
										detalle[indiceReg]['producto'] = producto;
										detalle[indiceReg]['subtotal'] = 
											(
												Number(producto.precioVenta)*Number(valor)
											).toFixed(2);
									}
								return producto;	
								}
						);
						
						return estadoNuevo;
						});
			}
		
			//Si el campo que estoy modificando corresponde al producto, entro a este if
			if(campo=='idProducto' ) {
			

				/*
				 * Algoritmo utilizado para calcular los resultantes de stock, al actualizar 
				 * el campo correspondiente al producto de un registro
				 * Stock original:
				Naranja : 20
				Manzana : 30

				Detalle anterior
				0->Naranja:10
				1->Manzana:4
				2->Naranja:5

				Res naranja anterior: 20 - (10+5) = 20 - 15  =	 5
				Res manzana anterior: 30 - (4)    = 30 - (4) =	26

				Cambio el registro con índice 2, de Naranja a Manzana

				Detalle actualizado
				0->Naranja:10
				1->Manzana:4
				2->Manzana:5

				Res naranja nuevo: 20 - (10)  = 20 - 10  =	 10
				Res manzana nuevo: 30 - (4+5) = 30 - 9   =	 21

				Estoy parado en el registro 2
				cant = 5

				Calculo el resultante de naranjas
				Naranja-->Producto anterior
				Manzana-->Producto actualizado

				Primero actualizo el producto anterior (naranja)

				Recorro todos los elementos del detalle (excepto el 2, donde estoy parado)
				que tengan asignada una naranja, y hago la suma de las cantidades

				Al stock original de naranjas le resto dicha suma
				No necesito el valor de cantidad del registro donde
				estoy parado, ya que al modificarle el producto,
				no contendrá ninguna información que me afecte al
				stock resultante de las naranjas

				Luego, actualizo el producto actualizado (manzana)
				Recorro todos los elementos del detalle (excepto el 2, donde estoy parado)
				que tengan asignada una manzana, y hago la suma de las cantidades
				Luego, a esa suma, le sumo ademas el valor de cantidad del registro 2,
				(donde estoy parado)

				Finalmente, al stock original de manzanas le resto dicha suma

				 * */
	 
			
			
			
			//Al cambiar el producto, debo deducir sumarle la cantidad al producto anterior
			//y deducirle la misma al nuevo	
			
			//Me guardo el producto que tenia antes de actualizar el registro
			//Deprecacion: ya lo tenia fuera del if
			const prodAnt=detalle[indiceReg]['producto'];
			
			//Me guardo el producto que estoy actualizando en el registro
			const prodAct=this.state.productos.find(p=> p._id==valor);
			
			
			//Actualizo los campos "precioActual" y "unidadActual"
			//de acuerdo a los valores de precio y unidad
			//del producto al que estoy actualizando el elemento
			//del detalle
			detalle[indiceReg]['precioActual']=Number(prodAct.precioVenta);
			detalle[indiceReg]['unidadActual']=prodAct.unidad;

			//Si el producto al que estoy modificando el elemento del detalle
			//se vende por unidad, necesariamente su cantidad debe ser un
			//valor entero, por lo que, si anteriormente tenía un valor
			//no entero, lo redondeo al valor entero hacia arriba mas cercano
			//(lo redondeo hacia arriba ya que de otra manera me podría quedar
			//un cero en campo de cantidad, el cual no es un valor válido)
			
			//Hago entonces, una asignación condicional
			const cantidadRedondeada = prodAct.unidad=='u' 
				? Math.ceil(Number(detalle[indiceReg]['cantidad']))
				: Number(detalle[indiceReg]['cantidad']);
			
			//Le asigno el valor redondeado hacia arriba al campo de cantidad
			if(prodAct.unidad=='u'){
				detalle[indiceReg]['cantidad'] = cantidadRedondeada;			
			}
			const stockResultanteProdAnt =
					(
						//Parto del stock actual del producto
						Number(prodAnt.stock)
						-
						
						//Luego, necesito restarle la cantidad vendida asociada a todos los otros registros
						//cuyo producto asociado sea el mismo que el registro que estoy actualizando
						
						Number(
							//Recorro todos los elementos del detalle
							this.state.detalle
							.filter(function (elemDetalle, indiceFilter){
								if
									(
									//El registro del detalle donde estoy parado
									//tiene asignado el mismo producto que el
									//registro del detalle que estoy actualizando
									(elemDetalle.idProducto==prodAnt._id )
									
									&&
									
									//El registro del detalle donde estoy parado
									//es diferente al que estoy actualizando
									(indiceFilter != indiceReg) 
									)
									return elemDetalle;
								})
								
							//Luego, sumo todas las cantidades de los elementos devueltos por el filter
							.reduce(
								function(acumulador,elemDetalle) {
								
									return acumulador + Number(elemDetalle.cantidad);

										},0)
										
							)
						).toFixed(2);
			
			
					const stockResultanteProdAct =
					(
						//Parto del stock actual del producto
						Number(prodAct.stock)
						-
						
						//Luego, necesito restarle la cantidad vendida asociada a todos los otros registros
						//cuyo producto asociado sea el mismo que el registro que estoy actualizando
						
						Number(
							//Recorro todos los elementos del detalle
							this.state.detalle
								.filter(function (elemDetalle, indiceFilter){
									if
										(
										//El registro del detalle donde estoy parado
										//tiene asignado el mismo producto que el
										//registro del detalle que estoy actualizando
										(elemDetalle.idProducto==prodAct._id )
										
										&&
										
										//El registro del detalle donde estoy parado
										//es diferente al que estoy actualizando
										(indiceFilter != indiceReg) 
										)
										return elemDetalle;
									})
									
								//Luego, sumo todas las cantidades de los elementos devueltos por el filter
								.reduce(
									function(acumulador,elemDetalle) {
									
										return acumulador + Number(elemDetalle.cantidad);

											},0) + cantidadRedondeada 
								)
							).toFixed(2);
				
				
					this.setState(function(estadoAnterior){
						let estadoNuevo=estadoAnterior;
						
						//Actualizo la lista de productos con el nuevo stock	
						estadoNuevo.productos=estadoAnterior.productos.map(
							function(producto){
								//Cuando encuentre, en la lista de productos del frontend,
								//el producto correspondiente al del registro que estoy actualizando
								if(producto._id==prodAct._id){
										
										//Actualizo el stock resultante en el producto
										producto.stockResultante=stockResultanteProdAct;
										
										//Al producto recien actualizado, lo actualizo en el elemento del detalle correspondiente
										detalle[indiceReg]['producto'] = producto;
										detalle[indiceReg]['subtotal'] = 
											(
												Number(producto.precioVenta)*cantidadRedondeada
											).toFixed(2);
									}
									if(producto._id==prodAnt._id){
										
										//Actualizo el stock resultante en el producto
										producto.stockResultante=stockResultanteProdAnt;
										

									}
								return producto;	
								}
						);
						
						return estadoNuevo;
						});
			
			}
			
			//Al modificar el tipo de pago de un producto,
			//actualizo la forma de pago por defecto

			if(campo=='tipoPago' ) {
				this.setState({
					tipoPagoPorDefecto:valor
				});
			}
			detalle[indiceReg][campo] = valor;
			
			this.setState(function(estadoAnterior){
				let estadoNuevo=estadoAnterior;
				
				console.log(detalle);
				
				//Actualizo los totales de acuerdo al tipo de pago
				estadoNuevo.efectivo=detalle
					.filter(elementoDetalle => elementoDetalle.tipoPago=='efectivo')
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);
					
				estadoNuevo.debito=detalle
					.filter(elementoDetalle => elementoDetalle.tipoPago=='debito')
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);
								
				estadoNuevo.credito=detalle
					.filter(elementoDetalle => elementoDetalle.tipoPago=='credito')
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);

				//Actualizo el total general
				estadoNuevo.total=detalle
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);				

				//Una vez que actualizé los totales por cada tipo de pago,
				//actualizo el resultante de cada cuenta contable de acuerdo a esos valores
				estadoNuevo.resultanteCaja=
					(Number(estadoAnterior.cuentas.caja) + Number(estadoNuevo.efectivo)).toFixed(2);
				
				estadoNuevo.resultanteCorriente=
					(Number(estadoAnterior.cuentas.cuentaCorriente) + Number(estadoNuevo.debito)).toFixed(2);
				
				estadoNuevo.resultanteCredito=
					(Number(estadoAnterior.cuentas.creditoAFavor) + Number(estadoNuevo.credito)).toFixed(2);
				
				
				//Finalmente, actualizo el detalle con todas las
				//modificaciones realizadas en esta función
				estadoNuevo.detalle=detalle;
				
				return estadoNuevo;
			})


		}
		catch(error){
			alert(error)
		}
}

/*
----------------------------------------------------
	Sección de manejadores de eventos asociados a botones
	(manejador de evento del submit del form)
----------------------------------------------------

*/

	//Entro a esta función cada vez que debo agregar un registro
	//al detalle, el cual también se agregará en el form dinámico
	agregarElemento = () => {
		try{
			//Me guardo el ID del primer producto que me devolvió
			//la API desde la base de datos, ya que,
			//inicialmente, el nuevo registro del detalle
			//estará asignado a dicho producto
			const idPrimero=this.state.productos[0]._id;
			
			//Obtengo la cantidad total vendida de dicho producto,
			//ya existente en cada registro del detalle, previo
			//a añadir el nuevo registro
			const cantAnterior= this.state.detalle
				.filter(elemDetalle=>elemDetalle.idProducto==idPrimero)
				.reduce(
					function(acumulador,elemDetalle) {
						return acumulador+Number(elemDetalle.cantidad);
				},0).toFixed(2);
			
			
			//El nuevo reultante de stock del producto agregado será:
			//El stock que tenía originalmente,
			//menos la cantidad vendida anterior que calculé antes,
			//menos 1 (correspondiente a la cantidad con la que inicializo el nuevo registro)
									
			const nuevoResultante= (this.state.productos[0].stock - cantAnterior -1).toFixed(2);

			this.setState(
				function(estadoAnterior)
				{
					let estadoNuevo=estadoAnterior;	
					
					//Debo ahora, actualizar el stock resultante de toda
					//la lista de productos con el nuevo resultante
					
					//Me guardo en 'productos' la lista de productos que tenia en el estado anterior
					let productos=estadoAnterior.productos;
					
					//El stock resultante del primer producto de la lista,
					//lo piso con el nuevo resultante calculado anteriormente
					productos[0].stockResultante = nuevoResultante;
					
					//Me guardo en una variable el primer producto
					//de la lista, ya con el stock resultante actualizado
					let producto=productos[0];
					

					//Actualizo el detalle
					//El detalle actualizado será igual a:
					estadoNuevo.detalle=				
						//El detalle que tenía anteriormente, al cual le concateno un objeto
						//con la información que tendrá el nuevo registro
						estadoAnterior.detalle.concat(
							//La información del nuevo registro será:	
							{	
								//El ID asignado al producto, correspondiente al
								//primer elemento de la lista de productos
								idProducto:idPrimero,
								
								//La cantidad vendida (inicialmente 1)
								cantidad:1,
								
								//Actualizo los valores de precio actual
								//y unidad actual, con el precio
								//y unidad de dicho producto
								precioActual:Number(producto.precioVenta),
								unidadActual:producto.unidad,
								
								//El tipo  de pago será el que tengo en el campo
								//'formaPagoPorDefecto', correspondiente
								//a la última forma de pago que seleccioné
								//previamente en algún elemento del detalle
								tipoPago: estadoAnterior.tipoPagoPorDefecto,
								
								//El subtotal, inicialmente equivalente al
								//precio de venta del producto vendido
								subtotal:Number(producto.precioVenta).toFixed(2),
								
								//El producto completo asignado al detalle
								//(ya lo teniamos guardado previamente en una variable,
								//de forma que, convenientemente, la utilizamos aquí)
								producto:producto
							}
						)
					
					//Luego, actualizamos la lista competa de productos,
					//con el nuevo valor de stock resultante actualizado
					//en el primer producto de la lista	
					estadoNuevo.productos=productos;
					
					//Actualizo el total de ventas realizado en el tipo de pago por defecto
					//(le sumo el subtotal del nuevo registro, inicialmente equivalente al precio de venta del primer producto)
					estadoNuevo[estadoAnterior.tipoPagoPorDefecto] = 
						(
							Number(estadoAnterior[estadoAnterior.tipoPagoPorDefecto]) + 
							Number(producto.precioVenta)
						).toFixed(2);
					
					//Actualizo el resultante la cuenta asociada al tipo de pago por defecto
					//(le sumo el subtotal del nuevo registro, inicialmente equivalente al precio de venta del primer producto)
					estadoNuevo[tipoPagoAResultante[estadoAnterior.tipoPagoPorDefecto]]=
						(
							Number(estadoAnterior[tipoPagoAResultante[estadoAnterior.tipoPagoPorDefecto]]) + 
							Number(producto.precioVenta)
						).toFixed(2);
					
					//Actualizo el total general de la venta
					////(le sumo el subtotal del nuevo registro, inicialmente equivalente al precio de venta del primer producto)
					estadoNuevo.total= 
						(
							Number(estadoAnterior.total) + 
							Number(producto.precioVenta)
						).toFixed(2);
					
					//Devuelvo el estado con todos sus valores actualizados
					
					estadoNuevo.isSubmitted=false;
					return estadoNuevo;
				}
			);
		}
		catch(error){
			alert(error)
		}
	}
	
	//Entro a esta función cada vez que quiero remover un elemento
	//al detalle, el cual también se eliminará en el form dinámico
	removerElemento = (elemento,indice) => {
		try{

			//Utilizando destructuring, me guardo:
			
			let {detalle,tipoPago,subtotal}
			 = 
				{
					//En 'detalle', el estado actual del detalle previo a la actualización
					detalle:[...this.state.detalle],
					
					//En 'tipoPago', me guardo el tipo de pago asociado al
					//registro del detalle que estoy eliminando
					tipoPago:this.state.detalle[indice].tipoPago,
					
					//En 'subtotal', me guardo el subtotal correspondiente al
					//registro del detalle que estoy eliminando
					subtotal:this.state.detalle[indice].subtotal,
				};
				
			
			//Me guardo en 'producto', el producto asociado al
			//registro del detalle que estoy eliminando
			
			//Podría incluirlo en el destructuring
			const producto=detalle[indice].producto;
			
			//El nuevo stock resultante de dicho producto será:
			//El stock resultante que tenía antes de la eliminación, mas la cantidad
			//correspondiente que tenia el detalle que estoy eliminando
			const nuevoResultante = (Number(producto.stockResultante) + Number(detalle[indice].cantidad)).toFixed(2);
			
			//Elimino el registro del detalle con splice
			detalle.splice(indice, 1);
			
			//Busco el índice del producto a actualizar el stock resultante
			this.setState(function(estadoAnterior){
				let estadoNuevo=estadoAnterior;
				
				//Debo ahora, actualizar el stock resultante de toda
				//la lista de productos con el nuevo resultante
				
				//Me guardo en 'productos' la lista de productos
				//que tengo previamente a la actualización
				let productos=estadoAnterior.productos;
				//Busco el indice del producto a actualizar en la lista de productos
				const indiceAActualizar=productos.findIndex(p=>p._id==producto._id);
				
				console.log(indiceAActualizar);
				
				//Al producto en la lista a actualizar, le asigno
				//el nuevo resultante calculado anteriormente
				productos[indiceAActualizar].stockResultante=nuevoResultante;
				
				//Luego, actualizamos la lista competa de productos,
				//con el nuevo valor de stock resultante actualizado
				//en el producto correspondiente de la lista	
				estadoNuevo.productos=productos;
				
				//Actualizamos el detalle con el registro eliminado
				estadoNuevo.detalle=detalle;
				
				//Actualizo el total de ventas correspondiente al tipo de pago
				//correspondiente al detalle que estoy eliminando
				//Su nuevo valor corresponderá a su estado anterior,
				//menos el subtotal que tenía dicho registro.
				estadoNuevo[tipoPago]=(Number(estadoAnterior[tipoPago]) - Number(subtotal)).toFixed(2);
				
				
				//Actualizo resultante de la cuenta contable asociada
				//al tipo de pago
				//correspondiente al detalle que estoy eliminando
				//Su nuevo valor corresponderá a su estado anterior,
				//menos el subtotal que tenía dicho registro.
				estadoNuevo[tipoPagoAResultante[tipoPago]]=(Number(estadoAnterior[tipoPagoAResultante[tipoPago]]) - Number(subtotal)).toFixed(2);
				
				//Actualizo el total de ventas
				//Su nuevo valor corresponderá a su estado anterior,
				//menos el subtotal que tenía dicho registro.
				estadoNuevo.total=(Number(estadoAnterior.total)- Number(subtotal)).toFixed(2);
				estadoNuevo.isSubmitted=false;
				return estadoNuevo;
			})

		}
			
		catch(error){
			alert(error)
		}
	}

	//Hago la validación, controlando que ningún producto tenga un stock resultante negativo
	validarStock = () => {
		try{
			let flag = true ;
			this.state.detalle.forEach(
				function(elementoDetalle) {
					if(Number(elementoDetalle.producto.stockResultante)<0)
						flag = false;
					}
			)
			return flag;
		}
		catch(error){
			alert(error)
			}
	}
	
	onSubmit = (e) => {
		try{

			//Esta linea evita que al hacer el submit, salgamos de la vista actual

			e.preventDefault();

			console.log(this.validarStock());

			//Si la validación de stock es correcta,
			//y tenemos al menos un elemento en el detalle
			if(this.validarStock() && this.state.detalle.length>0)   {


				/*
				 * Me genero un array en donde todos los elementos tengan un campo comun
				 * para toda la venta (codigo de venta y nombre de usuario del vendedor),
				 * y luego, los elementos asociados al detalle de cada venta.
				 */

				const codVenta=this.state.codVenta;
				const userVendedor=this.state.userVendedor;
				
				//Asigno los elementos del detalle que voy a mandar a la API
				const detalle=this.state.detalle.map (
					function(elemento) {
						const detalleDevuelto= {
							producto: elemento.idProducto,
							cantidad: elemento.cantidad,
							precioActual: elemento.precioActual,
							unidadActual: elemento.unidadActual,
							tipoPago: elemento.tipoPago,
							subtotal: elemento.subtotal
						}
						return detalleDevuelto;
					}
				);
				
				//Si fechaHora actual vale 'true', genero una nueva fecha y hora correspondiente al
				//momento en el que genero la venta.
				//Si no, le mando a la API la fecha y hora que seleccioné en el formulario
				const fechaHora = (this.state.fechaHoraActual) ? new Date() : this.state.fechaHora;
			   
				//const fechaHora=new Date();

				const venta=
				{
					codVenta:codVenta,
					userVendedor: userVendedor,
					detalle: detalle,
					fechaHora: fechaHora
				};


				console.log(venta);
				//Hacemos un post a la API con la const cargada previamente
				axios.post('http://localhost:5000/ventas/agregar', venta)
				.then(res => {


					//Si el POST fue exitoso, actualizo mi lista de productos
					//en el frontend, de acuerdo a las cantidades vendidas de
					//cada producto
					
					
					this.setState(function(estadoAnterior){
						let estadoNuevo=estadoAnterior;
						const codVenta=estadoAnterior.codVenta;	
						
						//Actualizo la lista de productos con su stock resultante
						estadoNuevo.productos=estadoAnterior.productos.map(function (producto){
							producto.stock=producto.stockResultante;
							return producto;
						});
						
						//Como inicialmente la cantidad vendida
						//de un producto es por defecto 1,
						//e inicialmente, dicho producto es el primero
						//de la lista,inicialmente, el stock resultante
						//del primer producto de la lista actualizada
						//será el resultante de la venta anterior
						//mas una unidad
						estadoNuevo.productos[0].stockResultante--;
						
						//Actualizo las cuentas con su resultante
						estadoNuevo.cuentas={
							caja:estadoAnterior.resultanteCaja,
							cuentaCorriente:estadoAnterior.resultanteCorriente,
							creditoAFavor:estadoAnterior.resultanteCredito
						};
						
						//Actualizo los resultantes de cada cuenta contable
						estadoNuevo.resultanteCaja=Number(estadoAnterior.cuentas.caja) + Number(estadoAnterior.productos[0].precioVenta);
						estadoNuevo.resultanteCorriente=estadoAnterior.cuentas.cuentaCorriente;
						estadoNuevo.resultanteCredito=estadoAnterior.cuentas.creditoAFavor;
						
						//Inicializo el detalle de la venta subsiguiente
						estadoNuevo.detalle=[{

								//Campos a almacenar en el array que mando a la API
								idProducto:estadoAnterior.productos[0]._id,
								cantidad:1,
								precioActual:Number(estadoAnterior.productos[0].precioVenta),
								unidadActual:estadoAnterior.productos[0].unidad,
								tipoPago:'efectivo',
								subtotal:estadoAnterior.productos[0].precioVenta.toFixed(2),

								//Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
				
								//Como ya actualize previamente el stock resultante
								//mas arriba, me conviene directamente asignar el
								//producto asociado al detalle con el valor
								//que tiene en estadoNuevo
								producto:estadoNuevo.productos[0]


							}];
							
						
						//Le concateno al array de códigos de venta existentes,
						//el correspondiente a la venta actual
						estadoNuevo.codigosExistentes=estadoAnterior.codigosExistentes.concat(estadoAnterior.codVenta);
						
						//El código de venta de la venta subsiguiente, será una unidad
						//mayor al máximo código que tengo en mi array de códigos existenes,
						//de forma que me aseguro que la misma, por defecto, sea nueva y
						//no añada elementos a una existente
						estadoNuevo.codVenta= Math.max(...estadoNuevo.codigosExistentes) + 1;
						
						//Le concateno a mi array de ventas existentes
						//la que estoy realizando actuamente, de manera que, sin
						//necesidad de volver a hacer un llamado a la
						//API, pueda seguir generando correctamente
						//los códigos de venta con los algoritmos implementados
						estadoNuevo.ventas=estadoAnterior.ventas.concat(
							{
								codVenta:codVenta,
								vendedor:estadoAnterior.userVendedor,
								detalle:estadoAnterior.detalle,
								fechaHora:estadoAnterior.fechaHora.toISOString()
							}
						);
						
						estadoNuevo.tipoPagoPorDefecto="efectivo";
						//Seteamos la variable 'isSubmitted' en true,
						//de manera que al hacer el render condicional, nos muestre
						//en la vista el response del post
						estadoNuevo.isSubmitted=true;
						
						//Nos guardamos el response del post para mostrarlo en la vista
						estadoNuevo.resData=res.data;
						
						console.log("res.data: " +res.data);					
						
						return estadoNuevo;
					});
					
				})
				//Si tuvimos un error en el POST, entramos al catch, para poder
				//mostrar en la vista el mismo
				.catch(res => {
					this.setState({isSubmitted:true});
					if(res.code=="ERR_BAD_REQUEST") {
						this.setState({resData:res.response.data});
					}
					else {
						this.setState({resData:res.message});

					}
					console.log(res);
				});


				//Cierro validacion de stock
			}
			//Cierro POST de venta
		}
		catch(error){
			alert(error)
		}
	}
	render() {
		try{
		return (
			<div className="center-realizar-venta">
				<center>
					<h3>Nueva venta</h3>
					<br/>


					{(this.state.vendedores.length>0 && this.state.productos.length>0)
						
						? 


					<Form onSubmit={this.onSubmit}>

						<FormGroup>

							<Row className="encabezado">

								<Col sm="0">
									<Label className="label-fecha-hora-actuales">
										Registrar fecha y hora actuales:
									</Label>

								</Col>

								<Col sm="0">
									<Input
										type="checkbox"
										className="checkbox-fecha-hora-actuales"
										checked={this.state.fechaHoraActual} 
										onChange={this.onChangeFechaHoraActual}
									/>
								</Col>

								{
								
									!this.state.fechaHoraActual &&
									<>

									<Col sm="0">
										<Label className="label-fecha-hora">
											Fecha y hora:
										</Label>
									</Col>

									<Col sm="0" >
										<DateTimePicker
											className="inp-fecha-hora"
											value={this.state.fechaHora}
											onChange={this.onChangeFechaHora}
											format="dd/MM/yyyy hh:mm:ss"
											showTimeSelect
										/>
									</Col>	
									</>	
								}

								<Col sm="0" >
									<Label className="label-cod-venta">
										 Código de venta: 
									</Label>
								</Col>


								<Col sm="0" >

									<Input 
										type="number"
										min="1"
										step="1" 
										className="inp-cod-venta"
										value={this.state.codVenta}
										onChange={this.onChangeCodVenta}
									/>

								</Col>




								{
									this.state.categoriaUsuario=='administrador'
									
									?
									
									<>

									<Col sm="0">
										<Label className="label-vendedor">
											Vendedor:
										</Label>
									</Col>

									<Col sm="0" size="sm"> 

										<Input
											size="sm"
											type = "select"
											required
											className="inp-vendedor"
											value={this.state.userVendedor}
											onChange={this.onChangeVendedor}
										>
											{
												this.state.vendedores.map(function(vendedor) {
													return <option key={vendedor.username} value={vendedor.username}>
														{vendedor.nombre} {vendedor.apellido}
														</option>;
												})
											}
										</Input>
										</Col>
									</>
									:
									<div></div>
								}

							</Row>


							<Row className="encabezado">
								<Col sm="0">
									<Label>
										{
										//En la lista de códigos generada, 
										//verifico si el código de venta actual
										//del estado existe
										this.state.codigosExistentes.some(function(codigo){
										return codigo==this.state.codVenta
										},this)
										
										?
										//Si existe, es una venta existente
										<>
											<h6>Venta existente</h6>
											<h7>Los elementos del detalle de venta se agregarán a le misma</h7>
										</>
										//Si no, es una compra nueva
										:
										
										<h6>Venta nueva</h6>
										}
									</Label>

								</Col>
							</Row>


						</FormGroup>


						<FormGroup>
						<br/>  <h5> Detalle:</h5> 
						</FormGroup>

						{ //Aquí implemento el form dinámico

							this.state.detalle.map(
								function (elemento, indice) {
									console.log(elemento);
									console.log(indice);

									return(

										<FormGroup className="formgroup-detalle">
										
											<Row className="fila-detalle">
												<Col sm="0">

												<Label className="label-producto">
													Producto:
												</Label>

												</Col>

												<Col sm="0" >
													<Input
														name="idProducto"
														size="sm"
														bsSize="sm"
														type="select" 
														required
														className="inp-producto"
														value={elemento.idProducto}
														onChange={evento =>  this.onChangeDetalle(evento, indice)}
													>

														{
															this.state.productos.map(function(producto) {
																return <option key={producto._id} value={producto._id}>
																	{producto.descripcion}
																</option>;
															})
														}

													</Input>
												</Col>
												
												
												<Col sm="0">
												<Label className="label-cantidad">
													Cantidad:
												</Label>
												</Col>

												{elemento.producto.unidad=='u' ?
												<Col sm="0">
													<Input
														name="cantidad"
														size="sm"
														type="number"
														min="1"
														step="1" 
														className="inp-cantidad"
														value={elemento.cantidad}
														onChange={evento =>  this.onChangeDetalle(evento, indice)}
													/>

												</Col>	
												
												:
												<Col sm="0">
													<Input
														name="cantidad"
														size="sm"
														type="number"
														min="0.01"
														step="0.01" 
														className="inp-cantidad"
														value={elemento.cantidad}
														onChange={evento =>  this.onChangeDetalle(evento, indice)}
													/>

												</Col>
												
												}
							
												<Col sm="0">
													<Label className="label-unidad">
														&nbsp;{elemento.producto.unidad}
													</Label>
												</Col>
							
												<Col sm="0">
													<Label className="label-precio-unitario">
														{'$/' + elemento.producto.unidad+ ' : $' + elemento.producto.precioVenta} 
													</Label>
												</Col>
												
												<Col sm="0">


												<Label  className="label-subtotal">
														{'Subtotal: $ ' + elemento.subtotal} 
													</Label>
												</Col>

												<Col sm="0">
												
													<Label  className="label-tipo-pago">
														Tipo de pago: 
													</Label>
													
												</Col>

												<Col sm="0">
													<Input 
													type = "select" 
													required 
													name="tipoPago"
													className="inp-tipo-pago"
													value={elemento.tipoPago}
													onChange={evento =>  this.onChangeDetalle(evento, indice)}
													>
														<option value="efectivo">Efectivo</option>
														<option value="debito">Tarjeta de débito</option>
														<option value="credito">Tarjeta de crédito</option>
													</Input>
												</Col>


												<Col sm="0">
													<Button
														className="boton-remover"  
														onClick={evento =>  this.removerElemento(evento, indice)}
													>
														Remover
													</Button>
												</Col>


											</Row>

											<Row className="fila-stock">

												<Col sm="0">
													<Label className="label-margen-izq"/>
												</Col>

												<Col sm="0">
													<Label className="label-stock-actual">
														<>
															Stock Actual:   
															{	
																//Formateo el stock de acuerdo a la unidad del producto correspondiente
																(elemento.producto.unidad=='u') ?
																(' '+ Number(elemento.producto.stock).toFixed(0) + ' ' + elemento.producto.unidad):
																(' '+ Number(elemento.producto.stock).toFixed(2) + ' ' + elemento.producto.unidad)
															} 
														</>
													</Label>
												</Col>

												<Col sm="0">
													<Label className="label-stock-resultante">

														{
															Number(elemento.producto.stockResultante)>=0 
															
															?
															<>
															Stock Resultante : 
															{
																//Formateo el stock resultante de acuerdo a la unidad del producto correspondiente	
																(elemento.producto.unidad=='u') ?
																(' '+ Number(elemento.producto.stockResultante).toFixed(0) + ' ' + elemento.producto.unidad):
																(' '+ Number(elemento.producto.stockResultante).toFixed(2) + ' ' + elemento.producto.unidad)
															} 
															</>
															: 
															<>No hay suficiente stock</>
														}

													</Label>
												</Col>
												
												<Col sm="0">
														<Label className="label-margen-der"/>
													</Col>


											</Row>

										</FormGroup>


									)
							},this)
						}



						<div className="div-resultantes">
													
							<Row className="fila-resultantes-cuentas">
								{
									mostrarResultantes
										?
											<>
												<Col sm="0">
													<Label className="label-resultantes-cuentas">
														Caja:
													</Label>
												</Col>


												<Col sm="0">
													<Label className="label-resultantes-cuentas">
														{' $ ' + Number(this.state.cuentas.caja).toFixed(2)}
													</Label>
												</Col>
											</>
										
										:
											<>
												<Col sm="0">
													<Label className="label-espaciado-resultantes"/>
												</Col>
											</>
								}

								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Efectivo:
									</Label>
								</Col>


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{' $ ' + Number(this.state.efectivo).toFixed(2)}
									</Label>
								</Col>

								
								{
								
									mostrarResultantes
										?
										<>
											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													Resultante:
												</Label>
											</Col>


											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													{' $ ' + Number(this.state.resultanteCaja).toFixed(2)}
												</Label>
											</Col>
										</>
									
									:
										<>
											<Col sm="0">
												<Label className="label-espaciado-resultantes"/>

											</Col>
										</>
							}
								


							</Row>

							<Row className="fila-resultantes-cuentas">


								
								
								{
								mostrarResultantes
									?
										<>
											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													Cuenta Corriente:
												</Label>
											</Col>


											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													{' $ ' + Number(this.state.cuentas.cuentaCorriente).toFixed(2)}
												</Label>
											</Col>
										</>
									
									:
										<>
											<Col sm="0">
												<Label className="label-espaciado-resultantes"/>
											</Col>
										</>
								}


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Tarjeta de débito:
									</Label>
								</Col>

								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{' $ ' + Number(this.state.debito).toFixed(2)}
									</Label>
								</Col>

								{
								mostrarResultantes
									?
										<>
											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													Resultante:
												</Label>
											</Col>


											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													{' $ ' + Number(this.state.resultanteCorriente).toFixed(2)}
												</Label>
											</Col>
										</>
									
									:
										<>
											<Col sm="0">
												<Label className="label-espaciado-resultantes"/>
											</Col>
										</>
								}
								
								
								


							</Row>

							<Row className="fila-resultantes-cuentas">
								
								
								{
								mostrarResultantes
									?
										<>
											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													Crédito a favor:
												</Label>
											</Col>


											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													{' $ ' + Number(this.state.cuentas.creditoAFavor).toFixed(2)}
												</Label>
											</Col>
										</>
									
									:
										<>
											<Col sm="0">
												<Label className="label-espaciado-resultantes"/>
											</Col>
										</>
								}
								



								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Tarjeta de crédito:
									</Label>
								</Col>

								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{' $ ' + Number(this.state.credito).toFixed(2)}
									</Label>
								</Col>

								{
								mostrarResultantes
									?
										<>
											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													Resultante:
												</Label>
											</Col>


											<Col sm="0">
												<Label className="label-resultantes-cuentas">
													{' $ ' + Number(this.state.resultanteCredito).toFixed(2)}
												</Label>
											</Col>            
										</>
									
									:
										<>
											<Col sm="0">
												<Label className="label-espaciado-resultantes"/>
											</Col>
										</>
								}

								

							</Row>

							<Row className="fila-resultantes-cuentas"/>

							<Row className="fila-resultantes-cuentas">

								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Total:
									</Label>
								</Col>


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{' $ ' + Number(this.state.total).toFixed(2)}
									</Label>
								</Col>


							</Row>
						</div>

						<Row className="fila-botones-submit">
							<Col sm="0">
							<Button className="boton" 
								onClick={this.agregarElemento} 
								>Agregar Producto </Button>
							</Col>
							
							
							<Col sm="0">
								<Label className="label-margen"/>
							</Col>


							<Col sm="0">


							{this.validarStock() ?
							<Button className="boton" type="submit">Realizar venta </Button>
							:
							<Label className="label-stock-insuficiente">
								<h6>Tenemos un stock insuficiente</h6>
								<h6>en algun producto</h6>
							</Label>
							}
							</Col>
						</Row>



					</Form> 
					
					:	
					<h4>No hay vendedores o productos registrados</h4>

					}


					<Label className="label-respuesta-api">
						{this.state.isSubmitted && this.state.resData}
					</Label>
					
				</center>
			{modoDebug && this.listaProductos() }
			</div>
			
		)
	}
	catch(error){
		alert(error)
	}
}
}
