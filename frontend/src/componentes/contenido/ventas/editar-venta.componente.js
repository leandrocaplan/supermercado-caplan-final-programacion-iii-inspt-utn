import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';

//Este flag le permite al programador ver en el
//navegador, toda la información util necesario
//para la depuración, mostrada en el renderizado del componente

//Cuando un usuario común debe utilizar la aplicación,
//está por defecto desactivada.

//Cuando un programador está trabajando con el código,
//ya sea el que lo desarrolló inicialmente o bien
//algun otro, simplemente debe establecer 
//este flag en true, y podrá ver en el navegador toda
//la información necesaria para agilizar el proceso de debug
const modoDebug=false;

//Esta funcion nos resulta util para debugear,
//controlando el stock de cada producto
//y su stock original del lado del frontend
//Solo la utilizo cuando tenga activado el modo debug
const Producto = function(props) { 
	return(
		<tr>
			<td>{Number(props.producto._id)} &nbsp;</td>
			<td>{props.producto.descripcion}&nbsp;</td>
			<td>{props.producto.stock}</td>
			<td>{props.producto.stockOriginal}</td>
		</tr>
	  );
}


//Defino las constantes 'tipoPagoAResultante' y
//'tipoPagoACuenta', las cuales contendrán un objeto
//que me facilitará la asociación entre un determinado
//tipo de pago y la cuenta contable asociada al mismo.

//Asocio cada tipo de pago con cada cuenta contable
const tipoPagoACuenta= {
	'efectivo':'caja',
	'debito':'cuentaCorriente',
	'credito':'creditoAFavor'
};

//Asocio cada tipo de pago con el
//resultante de cada cuenta contable
const tipoPagoAResultante= {
	'efectivo':'resultanteCaja',
	'debito':'resultanteCorriente',
	'credito':'resultanteCredito'
};

//NOTA: Luego de hacer cualquier operación numérica, al resultado de la misma
//le aplicamos siempre la función 'toFixed(2)', de manera que el resultado nos quede
//redondeado con dos cifras decimales

/* *
 * ¿Porqué necesitamos utilizar el campo 'stockOriginal'?
 * 
 * Ejemplo:
 * 
 * Tengo inicialmente un stock de 100 celulares
 * 
 * Hago una venta de celulares por una cantidad de 3
 * 
 *--> 
 * 
 * Me queda un resultante de 97 en el stock de celulares
 * En el detalle de venta me queda una cantidad de 3
 * 
 * Luego actualizo la cantidad en que figura en el detalle de venta a
 * al valor de 100 unidades, sin tener activada la opción de actualizar stock.
 * 
 * En la lista de productos me sigue quedando 97 en el stock de celulares.
 * 
 * Luego, vuelvo a actualizar la cantidad vendida en el detalle a 3,
 * esta vez con la opción de actualizar stock activada.
 * 
 * En el resultante de stock, me queda:
 * (Stock actual + Cantidad anterior - Cantidad actualizada):
 * 
 * 97 + 100 - 3 = 194
 * 
 * -->
 * 
 * Me queda un resultante de stock mayor al que tuve originalmente, lo cual no debe ser válido
 * 
 * Necesito, por lo tanto,
 * tanto desde el lado del Frontend como del de la API, alguna forma de guardarme los
 * valores de stock que tenía antes de editar la venta.
 * 
 * Ese valor lo tengo entonces, en el campo 'stockOriginal'
 * 
 * Al realizar la validación, entonces, debo corroborar que el resultante de stock
 * no solo sea mayor o igual a 0, sino también menor o igual al valor de 'stockOriginal'
 
* */

export default class EditarVenta extends Component {


	constructor(props) {

	super(props);
		//En el constuctor, definimos los campos del estado y los inicializamos con un valor
		//que se corresponderá con el tipo de datos que contendrá dicho campo
		this.state = {
			
			//Estos campos corresponden a la información que tenía
			//asignada el detalle de venta previo a su actualización
			idProductoAnterior: '',
		  	cantidadAnterior: 0,	
			unidadAnterior:'',
			tipoPagoAnterior: '',
			subtotalAnterior:0,
			
			precioAnterior:0,
			precioActual:0,
		
			//Estos campos corresponden a la información que contendrá
			//el detalle de venta luego de su actualización
			idProducto: '',
			cantidad: 1,
			unidad:'',
			tipoPago: 'efectivo',
			subtotal: 0,
			stockResultante: 0,

			//Estos campos son auxiliares, ya que nos sirven
			//para operar en el frontend, utilizando la información
			//de los productos existentes en nuestra base de datos
			productos: [],
			producto: {
					_id:'',
					unidad:'',
					descripcion:'',
					precioCompra:0,
					precioVenta:0,
					stock:0,
					stockOriginal:0,
				},
			
			//Aquí me guardo el importe actual de cada cuenta contable
			//que tengo actualmente en mi base de datos
			cuentas:{},
			
			//Aquí me guardo el resultante que tendrá cada cuenta contable
			//luego de actualizar el detalle de venta
			resultanteCaja:0,
			resultanteCorriente:0,
			resultanteCredito:0,
			
			//Estos dos flags me indicarán si, al editar el detalle,
			//actualizo o no el stock del producto asociado al detalle
			//y las cuentas contables correspondientes
			actStock:true,
			actCuentas:true,
			
			//Estos dos campos los utilizo para mostrar,
			//cuando corresponda, el resultado devuelto por la API luego
			//de realizar el POST
			isSubmitted: false,
			resData:'',
		}
	}

	componentDidMount() {

		//Al cargar el componente,
		//llamo a la API para que me traiga los datos de la venta a actualizar
		//y la lista de vendedores y productos.
		//Este 'get' me trae solamente usuarios registrados como vendedores
		axios.get('http://localhost:5000/ventas/asociarDetalle/id/' + this.props.match.params.idDetalle + "/" + this.props.match.params.idVenta)
			.then(respuesta => {
				try{ 	
					//Me guardo en el estado del componente la informacion inicial del
					//detalle de venta a actualizar, y el listado de productos
					//que me devuelve la API
					this.setState({
						//En el campo 'productos', me guardo la lista de productos que me devuelve la API
						productos:respuesta.data.productos,
						
						
						//En los campos 'idProductoAnterior', 'cantidadAnterior',
						//'tipoPagoAnterior' y 'subtotalAnterior', me guardo
						//la información del detalle de venta previa a su actualización
						idProductoAnterior: respuesta.data.venta.producto,
						cantidadAnterior: Number(respuesta.data.venta.cantidad),
						tipoPagoAnterior:respuesta.data.venta.tipoPago,
						subtotalAnterior: Number(respuesta.data.venta.subtotal).toFixed(2),
						
						//Inicialmente, los campos 'cantidad' y 'tipoPago'
						//valen lo mismo que lo que valían en el detalle
						//de venta previo a la actualización
						cantidad: Number(respuesta.data.venta.cantidad),
						tipoPago: respuesta.data.venta.tipoPago,
						
						precioAnterior:respuesta.data.venta.precioActual,
						unidadAnterior:respuesta.data.venta.unidadActual,
					   //Inicialmente, las cuentas contables valen lo mismo
					   //a lo que valían previamente a la actualización del detalle
						cuentas: {
								caja:Number(respuesta.data.cuentas.caja).toFixed(2),
								cuentaCorriente:Number(respuesta.data.cuentas.cuentaCorriente).toFixed(2),
								creditoAFavor:Number(respuesta.data.cuentas.creditoAFavor).toFixed(2)
							}
					});
					  
					//Debo inicializar los campos correspondientes al producto y
					//los resultantes de las cuentas, de acuerdo a si el producto
					//que tenía asignado previamente el detalle de venta,
					//sigue existiendo en la base de datos o no.

					//Si el producto inicialmente asociado a mi venta a actualizar sigue existiendo
					//en mi base de datos, me guardo la información de dicho producto

					//Entro a este 'if' si dicho producto sigue existiendo en mi base de datos.
					//Este 'if' funciona debido a que la función 'find', si no encuentra el
					//elemento correspondiente, nos devuelve el valor 'undefined'
					if (typeof (respuesta.data.productos.find(p => p._id==respuesta.data.venta.producto)) != 'undefined') {
						
						//En la variable 'producto', me guardo la información del producto completo
						//asociado al detalle que tengo inicialmente asignado en el detalle
						//de venta previo a su actualización
						let producto=respuesta.data.productos.find(p => p._id==respuesta.data.venta.producto);
						
						//Redondeo el stock del producto a dos cifras decimales
						producto.stock=producto.stock.toFixed(2);
						
						//Redondeo el campo 'stockOriginal' del producto a dos cifras decimales
						producto.stockOriginal=producto.stockOriginal.toFixed(2);
						
						this.setState({

							//Inicialmente, en este caso, el stock resultante de la operacion
							//es el mismo que tengo antes de actualizar la cantidad vendida
							stockResultante: Number(producto.stock),
							
							//En el campo 'producto', me guardo el producto completo
							//inicialmente asociado al elemento del detalle de venta
							producto: producto,
							
							//En el campo 'idProducto', me guardo el ID del producto
							//inicialmente asociado al elemento del detalle
							idProducto: producto._id,
							
							//En el campo 'subtotal', inicialmente tengo:
							//-El precio de venta del producto asociado inicialmente al detalle de venta que estoy actualizando.
							//-por la cantidad asignada inicialmente al detalle de venta que estoy actualizando
							
							//Este valor no siempre coincidirá con el subtotal anterior,
							//ya que es factible que se actualize dicho precio del producto,
							//previamente a la actualización del detalle de venta
							subtotal:
								(
									Number(producto.precioVenta)*Number(respuesta.data.venta.cantidad)
								).toFixed(2),
							
						  });
						  
						  //Ahora, debo inicializar los resultantes de cuentas,
						  //de acuerdo al tipo de de pago que tenía inicialmente
						  //asociado al elemento de detalle de venta:
						  
						  //Para ello, me valgo de los objetos de auxiliares que
						  //definí como constantes mas arriba, antes de declarar 
						  //la clase
						  
						  //Recorro, con un for, el objeto 'tipoPagoAResultante'
						  for(const tipoPago in tipoPagoAResultante){
							  
							  //Cuando el tipo de pago asociado inicialmente
							  //al detalle de venta que estoy actualizando,
							  //se corresponda con el tipo de pago donde
							  //estoy parado en el ciclo 'for':

							  if(respuesta.data.venta.tipoPago==tipoPago){
								  this.setState({
									  //El resultante de la cuenta asociada a ese tipo de pago valdrá:
									  //-El importe que tengo actualmente en dicha cuenta
									  //-menos el subtotal anterior que tenía almacenado previamente en dicho detalle de venta
									  //-mas el nuevo subtotal (calculado en base a los valores indicados mas arriba):

									  [tipoPagoAResultante[tipoPago]]:
										(
											Number(respuesta.data.cuentas[tipoPagoACuenta[tipoPago]]) -
											Number(respuesta.data.venta.subtotal) +
											Number(producto.precioVenta)*Number(respuesta.data.venta.cantidad)
										).toFixed(2)
									  });
							  }
							 
							 //Cuando dentro del for, estoy parado en un tipo de pago
							 //que no se corresponde con el que está asociado inicialmente
							 //el detalle de venta que estoy actualizando
							  else{
								  //El resultante de cuenta asociada a ese tipo de pago,
								  //simplemente valdrá lo mismo que el importe que
								  //tiene actualmente dicha cuenta
								  this.setState({
									  [tipoPagoAResultante[tipoPago]]:
										(
											Number(respuesta.data.cuentas[tipoPagoACuenta[tipoPago]]) 
										).toFixed(2)
									  })  
								}
						}
					}
					
					//Entro a este 'else' si el producto inicialmente asociado a mi venta a actualizar
					//ya no existe en mi base de datos
					else {

						//En la variable 'producto', me guardo la información del producto completo
						//correspondiente al primero que me devuelve la API, existente en la base de datos
						let producto=respuesta.data.productos[0]  || {
								_id:'',
								unidad:'',
								descripcion:'',
								precioCompra:0,
								precioVenta:0,
								stock:0,
								stockOriginal:0,
							};
						
						//Redondeo el stock del producto a dos cifras decimales
						producto.stock=producto.stock.toFixed(2);
						
						//Redondeo el campo 'stockOriginal' del producto a dos cifras decimales
						
						
						producto.stockOriginal=producto.stockOriginal.toFixed(2);
						
						//Si el producto que tengo ahora se vende
						//por unidad, redondeo hacia arriba la cantidad
						
						//Sino, la dejo igual
						const cantidad= (producto.unidad=='u') ?
							Math.ceil(Number(respuesta.data.venta.cantidad)):
							Number(respuesta.data.venta.cantidad)
							
						this.setState({
							cantidad:cantidad,
							//Inicialmente, en este caso, el stock resultante de la operacion
							//es la cantidad de stock que tiene actualmente el producto menos
							//la cantidad asociada a la venta a actualizar
							//(si el producto al cual el detalle de venta tenía asignado
							//originalmente ya no existe en la base de datos, necesariamente
							//será un producto diferente al original, por lo cual el stock resultante será
							//el stock actual menos la cantidad)
							stockResultante: 
								(
									Number(producto.stock) - 
									Number(cantidad)
								).toFixed(2),
							
							//En el campo 'producto', me guardo la infomación del producto completo
							//correspondiente al primero de la lista que me devolvió la API
							producto: producto,
							
							//En el campo 'idProducto', me guardo el ID del producto correspondiente
							//al primero de la lista que me devolvió la API
							idProducto: producto._id,
							
							
							//En el campo 'subtotal', inicialmente tengo:
							//-El precio de venta del primer producto devuelto por la API.
							//-por la cantidad asignada inicialmente al detalle de venta que estoy actualizando
							subtotal:
								(
									Number(producto.precioVenta)*cantidad
								).toFixed(2)
							});
						  
						  
						  //Ahora, debo inicializar los resultantes de cuentas,
						  //de acuerdo al tipo de de pago que tenía inicialmente
						  //asociado al elemento de detalle de venta:
						  
						  //Para ello, me valgo de los objetos de auxiliares que
						  //definí como constantes mas arriba, antes de declarar 
						  //la clase
						  
						  //Recorro, con un for, el objeto 'tipoPagoAResultante'
						  
						  for(const tipoPago in tipoPagoAResultante){
							 
							  //Cuando el tipo de pago asociado inicialmente
							  //al detalle de venta que estoy actualizando,
							  //se corresponda con el tipo de pago donde
							  //estoy parado en el ciclo 'for':
							  if(respuesta.data.venta.tipoPago==tipoPago){
								
								//El resultante de la cuenta asociada a ese tipo de pago valdrá:
								//-El importe que tengo actualmente en dicha cuenta
								//-menos el subtotal anterior que tenía almacenado previamente en dicho detalle de venta
								//-mas el nuevo subtotal (calculado en base a los valores indicados mas arriba):
								  this.setState({
									  [tipoPagoAResultante[tipoPago]]:
										(
											Number(respuesta.data.cuentas[tipoPagoACuenta[tipoPago]]) -
											Number(respuesta.data.venta.subtotal) +
											Number(producto.precioVenta)*Number(respuesta.data.venta.cantidad)
										).toFixed(2)
									  });
							  }
							 //Cuando dentro del for, estoy parado en un tipo de pago
							 //que no se corresponde con el que está asociado inicialmente
							 //el detalle de venta que estoy actualizando
							  else{
								  //El resultante de cuenta asociada a ese tipo de pago,
								  //simplemente valdrá lo mismo que el importe que
								  //tiene actualmente dicha cuenta
								  this.setState({
									  [tipoPagoAResultante[tipoPago]]:
										(
											Number(respuesta.data.cuentas[tipoPagoACuenta[tipoPago]]) 
										).toFixed(2)
									  })  
								}
						}
				}
			
				}
				catch(error){
					alert(error)
				}
			})
			//Si hubo un error en el llamado a la API, entro al catch
			.catch((error) => {
			  alert(error);
		});               	
	}

	listaProductos() { 
		try{   
			return this.state.productos.map(productoActual => {
				return <Producto producto={productoActual} eliminarProducto={this.eliminarProducto} key={productoActual._id}  />;
				}
			)
		}
		catch(error){
			alert(error)
		}
}


/*
----------------------------------------------------
    Sección de 'onChange'

  Al hacer cualquier modificación en algun campo del Formulario,
  la variable 'isSubmitted' la reseteamos en 'false' 
----------------------------------------------------

*/


	//Utilizo una sola función onChange para actualizar cualquier campo del detalle
	onChangeDetalle= (e) => {
		try{

			//En la const 'campo', me guardo el campo del detalle que voy a actualizar
			const campo=e.target.name;

			//En la const 'valor', me guardo el valor con el que voy a actualizar dicho campo
			const valor=e.target.value;

			//En las const 'actCuentas' y 'actStock', me guardo las
			//opciones de actualizar cuenta y actualizar stock
			//Dependiendo de si estan activadas o no, al editar
			//el detalle tambien actualizo las cuentas contables
			//correspondientes y el stock de productos correspondiente
			const actCuentas=this.state.actCuentas;
			const actStock=this.state.actStock

		   //Si el campo que estoy actualizando corresponde al producto,
		   //entro a este if
			if(campo=="idProducto") {
				
				
				//Si el producto al que estoy actualizando se vende por unidad
				const producto=this.state.productos.find(p => p._id===valor)
				
				 const cantidadRedondeada = producto.unidad=='u' 
					? Math.ceil(Number(this.state.cantidad))
					: Number(this.state.cantidad);
					
				if(producto.unidad=='u'){
						this.setState({
							cantidad: cantidadRedondeada,

						})	
					}
				

				//Actualizo el producto completo asociado al elemento del detalle
				//Ademas, actualizo el subtotal, correspondiente al precio de venta
				//del producto que estoy actualizando, por la cantidad que tengo
				//actualmente en el estado
				this.setState({
					producto: producto,
					subtotal: (
								Number(producto.precioVenta)*Number(cantidadRedondeada)
								).toFixed(2)
				})


				//Si tengo activada la opción de actualizar stock,
				//actualizo el stock resultante, de acuerdo a si es el
				//producto actualizado en el elemento del detalle
				//es el mismo que tenia asignado previo a la actualización o no
				if(actStock) {

					//Si el producto asociado a mi elemento de detalle de venta
					//a actualizar es el mismo que tenía
					//antes de actualizarla, el stock resultante de dicho producto
					//luego de la operación será:

					//El stock que tenía previamente dicho producto,
					//mas la cantidad asociada a la venta antes de actualizarla,
					//menos la cantidad de la venta actualizada
					if(valor==this.state.idProductoAnterior) {
						this.setState({
							stockResultante:
								Number(producto.stock) +
								Number(this.state.cantidadAnterior) -
								Number(cantidadRedondeada)
						});
					}

					//Si el producto asociado a mi venta a actualizar es diferente al que tenía
					//antes de actualizarla, el stock resultante de dicho producto
					//luego de la operación será:

					//El stock que tenía previamente
					//menos la cantidad de la venta actualizada
					else {
						this.setState({
							stockResultante:
								Number(producto.stock) -
								Number(cantidadRedondeada)
						});
					}
				
				//Cierro el if correspondiente al que entro en caso de
				//tener que actualizar el stock
				}
				
				//Dado que estamos actualizando solamente un producto, 
				//no necesitamos, como en el componente de realizar venta,
				//actualizar el stock de cada producto existente en la lista
				//de productos que tengo en el frontend
				
				//Si tengo activada la opción de actualizar cuentas,
				//actualizo los resultantes de las cuentas contables,
				//de acuerdo a si la cuenta actualizada en el elemento
				//del detalle es la misma que tenia asignada previamente
				//a la actualización o no
				if(actCuentas) {
					
					//Si la cuenta contable asociada al elemento de detalle de venta
					//a actualizar es la misma que tenía antes de actualizarlo
					if(this.state.tipoPago==this.state.tipoPagoAnterior) {

						this.setState({						
							//El resultante de dicha cuenta luego de la operación será:
							//-El importe que tenía previamente en dicha cuenta,
							//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
							//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)
							[tipoPagoAResultante[this.state.tipoPago]]:
								(
									Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPago]])-
									Number(this.state.subtotalAnterior) +
									Number(producto.precioVenta) * cantidadRedondeada
								).toFixed(2)
						});

					}
					
					//Si la cuenta contable asociada al elemento de detalle de venta
					//a actualizar es diferente a la que tenía antes de actualizarlo
					else {

						this.setState({
							//El resultante de la cuenta asociada al elemento de detalle previo a la actualización será:
							//-El importe que tenía previamente en dicha cuenta
							//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
							[tipoPagoAResultante[this.state.tipoPagoAnterior]]: 
								(
									Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPagoAnterior]]) -
									Number(this.state.subtotalAnterior)
								).toFixed(2)
								,

							//El resultante de la cuenta asociada al elemento de detalle que estoy actualizando será:
							//-El importe que tenía previamente en dicha cuenta
							//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)
							[tipoPagoAResultante[this.state.tipoPago]]:
								(
									Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPago]]) +
									Number(producto.precioVenta) * cantidadRedondeada
								).toFixed(2)
						});

					}
				//Cierro el if correspondiente al que entro en caso de
				//tener que actualizar las cuentas contables
				}
			//Cierro el if correspondiente a actualizar producto
			}
			
			
			//Si el campo que estoy actualizando corresponde a la cantidad,
			//entro a este if
			if(campo=="cantidad") {

				//Primero, actualizo el subtotal, que es equivalente
				//al precio de venta del producto actual por el valor
				//que estoy actualizando
				this.setState({
							subtotal:
								(
									Number(this.state.producto.precioVenta)*Number(valor)
								).toFixed(2)
						});


				//Si tengo activada la opción de actualizar stock,
				//actualizo el stock resultante, de acuerdo a si el
				//producto actualizado en el elemento del detalle
				//es el mismo que tenia asignado previo a la actualización,
				//o es un producto diferente
				if(actStock) {
					//Si el producto asociado a mi venta a actualizar es el mismo que tenía
					//antes de actualizarla, el stock resultante de dicho producto
					//luego de la operación será:

					//-El stock que tenía previamente
					//-mas la cantidad asociada a la venta antes de actualizarla,
					//-menos la cantidad de la venta actualizada
					if(this.state.idProducto==this.state.idProductoAnterior) {

						this.setState({
							stockResultante:
								(
									Number(this.state.producto.stock) +
									Number(this.state.cantidadAnterior) -
									Number(valor)
								).toFixed(2)
						});
					}

					//Si el producto asociado a mi venta a actualizar es diferente al que tenía
					//antes de actualizarla, el stock resultante de dicho producto
					//luego de la operación será:

					//El stock que tenía previamente
					//menos la cantidad de la venta actualizada
					else {
						this.setState({
							stockResultante:
								(
									Number(this.state.producto.stock) -
									Number(valor)
								).toFixed(2)
						});
					}
				
				//Cierro el if correspondiente al que entro en caso de
				//tener que actualizar el stock
				}

				//Si tengo activada la opción de actualizar cuentas,
				//actualizo los resultantes de las cuentas contables,
				//de acuerdo a si la cuenta actualizada en el elemento
				//del detalle es la misma que tenia asignada previamente
				//a la actualización o no
				if(actCuentas) {
					
					//Si la cuenta contable asociada al elemento de detalle de venta
					//a actualizar es la misma que tenía antes de actualizarlo
					if(this.state.tipoPago==this.state.tipoPagoAnterior) {
						
						//El resultante de dicha cuenta luego de la operación será:
						//-El importe que tenía previamente en dicha cuenta,
						//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
						//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)				
						this.setState({
							[tipoPagoAResultante[this.state.tipoPago]]:
								(
									Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPago]])-
									Number(this.state.subtotalAnterior) +
									Number(this.state.producto.precioVenta)*Number(valor)
								).toFixed(2)
						});

					}
					//Si la cuenta contable asociada al elemento de detalle de venta
					//a actualizar es diferente a la que tenía antes de actualizarlo
					else {
						
						this.setState({
							
							//El resultante de la cuenta asociada al elemento de detalle previo a la actualización será:
							//-El importe que tenía previamente en dicha cuenta
							//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
							[tipoPagoAResultante[this.state.tipoPagoAnterior]]:
								(
									Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPagoAnterior]])-
									Number(this.state.subtotalAnterior)
								).toFixed(2),

							//El resultante de la cuenta asociada al elemento de detalle que estoy actualizando será:
							//-El importe que tenía previamente en dicha cuenta
							//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)
							[tipoPagoAResultante[this.state.tipoPago]]:
								(
									Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPago]])+
									Number(this.state.producto.precioVenta)*Number(valor)
								).toFixed(2)
						});

					}
				//Cierro el if correspondiente al que entro en caso de
				//tener que actualizar las cuentas contables
				}
			//Cierro el if correspondiente a actualizar cantidad	
			}
			
			//Si el campo que estoy actualizando corresponde al tipo de pago,
			//y ademas tengo activada la opción de actualizar cuentas,
			//entro a este if
			
			if(campo=="tipoPago" && actCuentas) {
				
				//Me conviene hacer un setState funcional, ya que tengo gran cantidad de
				//actualizaciones de estado que dependerán del estado anterior
				this.setState(function (estadoAnterior){
					let estadoNuevo=estadoAnterior;
					
					//Antes de entrar a los if, me guardo en una const
					//el tipo de pago seleccionado previamente
					const tipoPagoSeleccionadoPreviamente=estadoAnterior.tipoPago;
					
					
					
					if(valor==estadoAnterior.tipoPagoAnterior) {
						
						
						//El resultante de dicha cuenta luego de la operación será:
						//-El importe que tenía previamente en dicha cuenta,
						//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
						//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)

							estadoNuevo[tipoPagoAResultante[valor]] =
								(
									Number(estadoAnterior.cuentas[tipoPagoACuenta[valor]])-
									Number(estadoAnterior.subtotalAnterior) +
									Number(estadoAnterior.subtotal)
								).toFixed(2);
							
							
							//Si en la lista previamente seleccioné un tipo de pago
							//diferente al anterior que tenía asignado el detalle
							//de venta, y ahora selecciono nuevamente el tipo de pago
							//correspondiente al que tenía antes de la actualización,
							//el resultante del tipo de pago que seleccioné
							//previamente volverá a su valor original, equivalente
							//al valor que tenía antes de actualizar el detalle de venta
							estadoNuevo[tipoPagoAResultante[tipoPagoSeleccionadoPreviamente]]=
								(
									Number(estadoAnterior.cuentas[tipoPagoACuenta[tipoPagoSeleccionadoPreviamente]])

								).toFixed(2);
						}
					
					
						//Si la cuenta contable asociada al elemento de detalle de venta
						//a actualizar es diferente a la que tenía antes de actualizarlo
						else {
							
							//El resultante de la cuenta asociada al elemento de detalle previo a la actualización será:
							//-El importe que tenía previamente en dicha cuenta
							//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
							estadoNuevo[tipoPagoAResultante[estadoAnterior.tipoPagoAnterior]]=
								(
									Number(estadoAnterior.cuentas[tipoPagoACuenta[estadoAnterior.tipoPagoAnterior]])-
									Number(estadoAnterior.subtotalAnterior)
								).toFixed(2);
							
							
							//El resultante de la cuenta asociada al elemento de detalle que estoy actualizando será:
							//-El importe que tenía previamente en dicha cuenta
							//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)
				
							estadoNuevo[tipoPagoAResultante[valor]]=
								(
									Number(estadoAnterior.cuentas[tipoPagoACuenta[valor]]) +
									Number(estadoAnterior.subtotal)
								).toFixed(2);
							
							//Si el tipo de pago seleccionado previamente no se corresponde
							//con el tipo de pago asignado inicialmente al detalle a actualizar
							if(tipoPagoSeleccionadoPreviamente != estadoAnterior.tipoPagoAnterior){
								//El resultante del tipo de pago que seleccioné
								//previamente volverá a su valor original, equivalente
								//al valor que tenía antes de actualizar el detalle de venta
								estadoNuevo[tipoPagoAResultante[tipoPagoSeleccionadoPreviamente]]=
									(
										Number(estadoAnterior.cuentas[tipoPagoACuenta[tipoPagoSeleccionadoPreviamente]])

									).toFixed(2);
						
							}
						}
								
					return estadoNuevo;
					});
				

			//Cierro el if correspondiente a actualizar tipo de pago
			}

			//Ejecuto este setState, sea cual sea el campo que estoy actualizando
			this.setState({
				//Asigno el valor actualizado al campo correspondiente
				[campo]:valor,
				
				//Desactivo el flag isSubmitted
				isSubmitted:false
				
			})

		//Cierro el onChangeDetalle
		}
		catch(error){
			alert(error)
		}
	}
	
	//Entro a esta función cada vez que modifico la opción de actualizar stock
	onChangeActStock = (e) => {
		try{

			//Si actStock estaba previamente desactivada, entro a este if
			if(!this.state.actStock) {
				
				//Si el producto asociado a mi venta a actualizar es el mismo que tenía
				//antes de actualizarla, el stock resultante de dicho producto
				//luego de la operación será:

				//-El stock que tenía previamente
				//-mas la cantidad asociada a la venta antes de actualizarla,
				//-menos la cantidad de la venta actualizada
				if(this.state.idProducto==this.state.idProductoAnterior) {
					this.setState({	
						stockResultante:
							(
								Number(this.state.producto.stock) +
								Number(this.state.cantidadAnterior) -
								Number(this.state.cantidad)
							).toFixed(2)
						});
				}

				//Si el producto asociado a mi venta a actualizar es diferente al que tenía
				//antes de actualizarla, el stock resultante de dicho producto
				//luego de la operación será:

				//-El stock que tenía previamente en dicho producto
				//-menos la cantidad de la venta actualizada
				else {
					this.setState({	
						stockResultante:
						(
							Number(this.state.producto.stock) -
							Number(this.state.cantidad)
						).toFixed(2)
					});
				}
		
		//Cierro el if correspondiente al que entro si 'actStock' estaba
		//previamente desactivada
		}

		//Si actStock estaba previamente activada, entro a este else
		else {
			this.setState({
				//El stock resultante será igual al stock actual
				//del producto, ya que no estoy actualizando ahora
				//dicho stock
				stockResultante: 
					(
						Number(this.state.producto.stock)
					).toFixed(2),
			});
		
		//Cierro el else correspondiente al que entro si 'actStock' estaba
		//previamente activada    
		}
		
		//Ejecuto este setState, sea cual sea el valor previo de actStock
		this.setState({
				//Si actStock estaba previamente activada, ahora estará desactivada,
				//y viceversa
				actStock: e.target.checked,
				//Desactivo el flag isSubmitted
				isSubmitted:false
			});
		
		//Cierro el onChangeActStock
		}
		catch(error){
			alert(error)
		}
}


	//Entro a esta función cada vez que modifico la opción de actualizar cuentas
	onChangeActCuentas = (e) => {
		try{
			
			//Si actCuentas estaba previamente desactivada, entro a este if
			if(!this.state.actCuentas) { 
				
				//Si la cuenta contable asociada al elemento de detalle de venta
				//a actualizar es la misma que tenía antes de actualizarlo	
				if(this.state.tipoPago==this.state.tipoPagoAnterior) {
					
					//El resultante de dicha cuenta luego de la operación será:
					//-El importe que tenía previamente en dicha cuenta,
					//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
					//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)				
					this.setState({
						[tipoPagoAResultante[this.state.tipoPago]]:
							(
								Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPago]])-
								Number(this.state.subtotalAnterior) +
								Number(this.state.subtotal)
							).toFixed(2),
					});
				}

				//Si la cuenta contable asociada al elemento de detalle de venta
				//a actualizar es diferente a la que tenía antes de actualizarlo
				else {

					this.setState({
						//El resultante de la cuenta asociada al elemento de detalle previo a la actualización será:
						//-El importe que tenía previamente en dicha cuenta
						//-menos el subtotal que tenia el elemento de detalle de venta antes de actualizarlo,
						[tipoPagoAResultante[this.state.tipoPagoAnterior]]:
							(
								Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPagoAnterior]])-
								Number(this.state.subtotalAnterior)
							).toFixed(2),

						//El resultante de la cuenta asociada al elemento de detalle que estoy actualizando será:
						//-El importe que tenía previamente en dicha cuenta
						//-mas el subtotal actualizado del detalle de venta (precio * cantiadad del producto vendido)
						[tipoPagoAResultante[this.state.tipoPago]]:
							(
								Number(this.state.cuentas[tipoPagoACuenta[this.state.tipoPago]]) +
								Number(this.state.subtotal)
							).toFixed(2)
					});
				}
				
			//Cierro el if correspondiente al que entro si 'actCuentas' estaba
			//previamente desactivada
			}
			
			//Si actCuentas estaba previamente activada, entro a este else
			else{
				
				//El resultante de cada cuenta contable será igual al
				//valor que tenían originalmente, ya que no estoy actualizando
				//dichos valores
				this.setState({
					resultanteCaja:Number(this.state.cuentas.caja).toFixed(2),
					resultanteCorriente:Number(this.state.cuentas.cuentaCorriente).toFixed(2),
					resultanteCredito:Number(this.state.cuentas.creditoAFavor).toFixed(2)
				});
			
			//Cierro el else correspondiente al que entro si 'actCuentas' estaba
			//previamente activada
			}

			//Ejecuto este setState, sea cual sea el valor previo de actStock
			this.setState({
				
				//Si actCuentas estaba previamente activada,
				//ahora estará desactivada, y viceversa
				actCuentas: e.target.checked,
				
				//Desactivo el flag isSubmitted
				isSubmitted:false
			})
			
		//Cierro el onChangeActCuentas
		}
		catch(error){
			alert(error)
		}
}


/*
----------------------------------------------------
    Sección de 'onSubmit' 
    (manejador de evento del submit del Form)
----------------------------------------------------
*/


	//Al hacer el submit, entro al manejador de eventos
	onSubmit = (e) => {
		try {

		//Esta linea evita que al hacer el submit, salgamos de la vista actual
		e.preventDefault();
		
		//Validamos el resultante de stock
		const validacionStock=(
				( !this.state.actStock) ||
				(
					(Number(this.state.stockResultante) >= 0) &&
					(Number(this.state.stockResultante) <= Number(this.state.producto.stockOriginal))
				)
			);
		
		//Validamos el resultante de cuentas
		const validacionCuentas=(
				(!this.state.actCuentas) ||
				((Number(this.state.resultanteCaja) >= 0) && (Number(this.state.resultanteCorriente)>= -50000)) 
			);
			
		//Antes de hacer el POST, debemos realizar la siguiente validación
		if(validacionStock && validacionCuentas) 
		
		//Entro al cuerpo del if si la validación fue correcta
		{
			
			//Declaramos una const que contendrá un objeto
			//con los valores del elemento del 
			//detalle de venta actualizado
			const venta = {
				idProducto: this.state.idProducto,
				cantidad: Number(this.state.cantidad),
				precioActual:Number(this.state.producto.precioVenta),
				unidadActual:this.state.producto.unidad,
				tipoPago: this.state.tipoPago,
				subtotal: Number(this.state.subtotal),
				actStock: this.state.actStock,
				actCuentas: this.state.actCuentas
			};
		 
			//Hacemos un post a la API con la const cargada previamente
			axios.post('http://localhost:5000/ventas/actualizarDetalle/id/' + this.props.match.params.idDetalle + "/" + this.props.match.params.idVenta , venta)
				.then(res =>{ 
					//Mostramos por consola el response del post
					console.log(res.data);


					//Si el POST fue exitoso, actualizamos el estado
						
					//En este caso, utilizamos un setState funcional, ya que
					//necesitamos los valores del estado anterior para actualizarlo.
					//y ademas, actualizar condicionalmete los stocks de cada producto y las
					//cuentas, dependiendo de si están o no activadas las opciones correspondientes
					const actStock=this.state.actStock;
					const actCuentas=this.state.actCuentas;
					
					this.setState(
						function (estadoAnterior){
							let estadoNuevo=estadoAnterior;
													
							//Si la opcion de actualizar stock está activada,
							if(actStock) {

								//Actualizo mi lista de productos con el stock actualizado
								estadoNuevo.productos=estadoAnterior.productos.map(
									function(producto) {

										//Al producto correspondiente al asignado
										//a la venta previa a la actualización,
										//le devolvemos el stock que tenia antes
										//de la actualización
										if(producto._id == estadoAnterior.idProductoAnterior) {
											producto.stock=
												(
													Number(producto.stock) +
													Number(estadoAnterior.cantidadAnterior)
												).toFixed(2);
										}

										//El stock del producto asociado a mi venta actualizada,
										//ahora es igual al stock resultante de la operación.

										//Si es el mismo que el producto inicial,
										//pisa la asignación realizada mas arriba en el map
										if(producto._id === estadoAnterior.idProducto) {
											producto.stock=
												Number(estadoAnterior.stockResultante).toFixed(2);
										}

										return producto;

									}
								);
							}
							
							if(actCuentas){
								estadoNuevo.cuentas=
									{
										caja:estadoAnterior.resultanteCaja,
										cuentaCorriente:estadoAnterior.resultanteCorriente,
										creditoAFavor:estadoAnterior.resultanteCredito
									};
							}
								
							 //Ahora, el ID del producto inicial lo piso con el id
							//del producto asignado en el estado actual
							estadoNuevo.idProductoAnterior=estadoAnterior.idProducto;

							//Luego, la cantidad anterior de la operación,
							//tambien la piso con la catidad asignada
							//al estado actual
							estadoNuevo.cantidadAnterior=Number(estadoAnterior.cantidad);
							
							estadoNuevo.precioAnterior=estadoAnterior.producto.precioVenta;
							estadoNuevo.unidadAnterior=estadoAnterior.producto.unidad;
							
							estadoNuevo.subtotalAnterior=estadoAnterior.subtotal;  
							estadoNuevo.tipoPagoAnterior=estadoAnterior.tipoPago;
							
							//Finalmente, seteamos el flag'isSubmitted' en true,
							//de manera que al hacer el render condicinal, nos muestre
							//en la vista el response del post

							estadoNuevo.isSubmitted=true;
							estadoNuevo.resData=res.data;
							return estadoNuevo;
						}
					);

				})
				.catch(res => {

					this.setState({
							isSubmitted:true
						});

					if(res.code=="ERR_BAD_REQUEST") {
						this.setState({resData:res.response.data});
					}

					else {
						this.setState({resData:res.message});
					}

					console.log(res);
				});

		}

		else {

			if(!validacionStock) {
				if (Number(this.state.stockResultante) < 0) {
					alert(
						"Nos queda un resultante de stock negativo\n\n"
						+ "Intente convalidar el stock de este producto, o editar el elemento desactivando la opción de actualizar stock."
					);
				}
				else if((Number(this.state.stockResultante) > Number(this.state.producto.stockOriginal))) {
				alert(
					"Nos queda un resultante de stock mayor al stock original\n\n"
					+ "Intente convalidar el stock de este producto, o editar el elemento desactivando la opción de actualizar stock."
					);
				}
				
			}
			if(!validacionCuentas) {
				
				
				if (Number(this.state.resultanteCaja) < 0 && this.state.tipoPago==this.state.tipoPagoAnterior) {
					alert(
						"En Caja, nos quedaría un resultante negativo,"
						+ "ya que su monto actual es de $ " + this.state.cuentas.caja + ", "
						+ "el importe que teníamos\npreviamente en este elemento era de $ " + this.state.subtotalAnterior + ",\n"
						+ "el importe actualizado del detalle es de $ " + this.state.subtotal + ",\n"
						+ "y por lo tanto, el resultante sería $ " + this.state.resultanteCaja + ".\n\n"
						+ "Intente convalidar el estado de esta cuenta, o editar el elemento desactivando la opción de actualizar cuentas."
					);
				}
				
				
				if (Number(this.state.resultanteCaja) < 0 && this.state.tipoPago!=this.state.tipoPagoAnterior) {
					alert(
						"En Caja, nos quedaría un resultante negativo,"
						+ "ya que su monto actual es de $ " + this.state.cuentas.caja + ", "
						+ "el importe que teníamos previamente en el detalle era de de $ " + this.state.subtotalAnterior + ", "
						+ "y por lo tanto, el resultante sería $ " + this.state.resultanteCaja + ".\n\n"
						+ "Intente convalidar el estado de esta cuenta, o editar el elemento desactivando la opción de actualizar cuentas."
					);
				}
				
				//((Number(this.state.resultanteCaja) >= 0) && (Number(this.state.resultanteCorriente)>= -50000)) 
				if (Number(this.state.resultanteCorriente) < -50000 && this.state.tipoPago==this.state.tipoPagoAnterior) {
					alert(
						"En Cuenta Corriente, nos quedaría un resultante menor al límite de descubierto,"
						+ "ya que su monto actual es de $ " + this.state.cuentas.cuentaCorriente + ", "
						+ "el importe que teníamos\npreviamente en este elemento era de $ " + this.state.subtotalAnterior + ",\n"
						+ "el importe actualizado del detalle es de $ " + this.state.subtotal + ",\n"
						+ "y por lo tanto, el resultante sería $ " + this.state.resultanteCorriente + ".\n\n"
						+ "Intente convalidar el estado de esta cuenta, o editar el elemento desactivando la opción de actualizar cuentas."
					);
				}
				
				
				if (Number(this.state.resultanteCorriente) < -50000 && this.state.tipoPago!=this.state.tipoPagoAnterior) {
					alert(
						"En Cuenta Corriente, nos quedaría un resultante menor al límite de descubierto,"
						+ "ya que su monto actual es de $ " + this.state.cuentas.cuentaCorriente + ", "
						+ "el importe que teníamos previamente en el detalle era de de $ " + this.state.subtotalAnterior + ", "
						+ "y por lo tanto, el resultante sería $ " + this.state.resultanteCorriente + ".\n\n"
						+ "Intente convalidar el estado de esta cuenta, o editar el elemento desactivando la opción de actualizar cuentas."
					);
				}
			}
			}
		}
		catch(error){
			alert(error)
		}
	}

	
	//Hago el renderizado
	render() {
		try{
			return (
				<div className="center-editar-venta">

				<br/>

				{
					this.state.productos.length==0 
					
					?
					
					<h4>No hay productos registrados</h4>
					:

					<Form onSubmit={this.onSubmit} >


						<Row className="fila-titulo">      
							<Col sm="0">
								<Label className="label-titulo">
									<h3>Editar Detalle</h3>
								</Label>
							</Col>
						</Row>
						
						
						<Row className="fila-editar-venta">

							<Col sm="0">
								<Label className="label-campos-editar-venta">
									Producto:
								</Label>
							</Col>

							<Col sm="0">
								<Input 
									type="select"
									required
									name="idProducto"
									className="inp-editar-venta"
									value={this.state.idProducto}
									onChange={this.onChangeDetalle}
								>
									{
										//Despliego la lista de productos por su descripción
										this.state.productos.map(function(producto) {
											return <option key={producto._id} value={producto._id}>
												{producto.descripcion}
											</option>;
											}
										)
									}
								</Input>
							</Col>

							<Col sm="0">

								{
									//Hago un render condicional, que me indica si el producto
									//que estoy actualizando en el detalle es el mismo que tenía
									//previamente
									
									(this.state.idProducto==this.state.idProductoAnterior) 
									
									?
									
									<Label className="label-misma-asignacion">
										Mismo producto
									</Label>
									
									:
									
									<Label className="label-misma-asignacion">
										Distinto producto
									</Label>
								}
							</Col>
							
							<Col sm="0">

									<Label className="label-editar-precio-unitario">
										{'$/' + this.state.producto.unidad+ ' : $' + this.state.producto.precioVenta} 
									</Label>
								
							</Col>
							

						</Row>


						<Row className="fila-editar-venta">
							<Col sm="0">
								<Label className="label-campos-editar-venta">
									Cantidad: 
								</Label>
							</Col>

							<Col sm="0">
								{
									this.state.producto.unidad=='u'
									?
									<Input 
										type="number"
										name="cantidad"
										step="1"
										min="1"
										className="inp-cant-editar-venta"
										value={this.state.cantidad}
										onChange={this.onChangeDetalle}
									/>
									
									:
									<Input 
										type="number"
										name="cantidad"
										step="0.01"
										min="0.01"
										className="inp-cant-editar-venta"
										value={this.state.cantidad}
										onChange={this.onChangeDetalle}
									/>
								}
							 
							</Col>
							
							<Col sm="0">
								<Label className="label-unidad-editar-venta">
									{this.state.producto.unidad}
								</Label>        
							</Col>


							<Col sm="0">
								<Label className="label-misma-asignacion">
									{
										(this.state.idProducto==this.state.idProductoAnterior) 
										&&
										<>Cantidad anterior: {this.state.cantidadAnterior + ' ' + this.state.unidadAnterior }</>
									}
								</Label>        
							</Col>
							
							<Col sm="0">
								<Label className="label-precio-anterior">
									{
										(this.state.idProducto==this.state.idProductoAnterior) 
										&&
										<>Precio anterior: {" $ " +this.state.precioAnterior + "/" + this.state.unidadAnterior}</>
									}
								</Label>        
							</Col>
							
						</Row>


						<Row className="fila-editar-venta">
						
							<Col sm="0">
								<Label className="label-campos-editar-venta">
									Tipo de pago:
								</Label>
							</Col>

							<Col sm="0">
								<Input
									type="select"
									name="tipoPago"
									required className="inp-editar-venta"
									value={this.state.tipoPago}
									onChange={this.onChangeDetalle}
								>

									<option value="efectivo">Efectivo</option>
									<option value="debito">Tarjeta de débito</option>
									<option value="credito">Tarjeta de crédito</option>
								
								</Input>
							</Col>

							<Col sm="0">

							{
								//Hago un render condicional, que me indica si el tipo de pago
								//que estoy actualizando en el detalle es el mismo que tenía
								//previamente
							
								(this.state.tipoPago==this.state.tipoPagoAnterior) 
								
								?
								
								<Label className="label-misma-asignacion">
									Misma forma de pago
								</Label>
							
								:
								
								<Label className="label-misma-asignacion">
									Distinta forma de pago
								</Label>
							}
							</Col>

						</Row>
						
						<Row className="fila-espaciado" />
						
						<Row className="fila-actualizar-valores">
							
							<Col sm="0">
								<Label className="label-editar-venta">
									Actualizar stock:
								</Label>
							</Col>


							<Col sm="0">
								<Input
									type="checkbox"
									className="checkbox-editar-venta"
									checked={this.state.actStock}
									onChange={this.onChangeActStock}
								/>

							</Col>
							
						</Row>


						<Row className="fila-actualizar-valores">

							<Col sm="0">
								<Label className="label-editar-venta">
									Actualizar cuentas:
								</Label>
							</Col>

							<Col sm="0">
								<Input
									type="checkbox"
									className="checkbox-editar-venta"
									checked={this.state.actCuentas}
									onChange={this.onChangeActCuentas}
								/>

							</Col>
							
						</Row>


						<Row className="fila-editar-venta">

							<Col sm="0">
								<br/>
							</Col>
							
						</Row>  



						<div class="center-resultados">
							<Row className="fila-resultado">
								<Col sm="0">
									<Label className="label-resultado">
										Subtotal anterior:
									</Label>
								</Col>
			
								<Col sm="0"> </Col>

								<Col sm="0">
									<Label className="label-valor-resultado">
										{' $ ' + this.state.subtotalAnterior}
									</Label>
								</Col>
							</Row>


							<Row className="fila-resultado">
								<Col sm="0">
									<Label className="label-resultado">
										Subtotal actualizado:
									</Label>
								</Col>
							
								<Col sm="0">

									<Input 
										type="hidden" 
										className="inp-resultado"
										name="subtotal"
										value=  { this.state.subtotal}
									/>

								</Col>

								<Col sm="0">
									<Label className="label-valor-resultado">
										{' $ ' + this.state.subtotal}
									</Label>
								</Col>
							</Row>

							<Row className="fila-resultado"/>
							
							<Row className="fila-resultado">
								
								<Col sm="0">
									<Label className="label-resultado">
										Stock Actual:
									</Label>
								</Col>
								
								<Col sm="0">
									<Label className="label-valor-resultado">
										{
										this.state.producto.unidad=='u' ?
											Number(this.state.producto.stock).toFixed(0) + ' ' + this.state.producto.unidad:
											Number(this.state.producto.stock).toFixed(2) + ' ' + this.state.producto.unidad
										}
									</Label>
								</Col>
								
							</Row>


							<Row className="fila-resultado">
								
								<Col sm="0">
									<Label className="label-resultado">
										Stock Original:
									</Label>
								</Col>
								
								<Col sm="0">
									<Label className="label-valor-resultado">
										{
											this.state.producto.unidad=='u' ?
												Number(this.state.producto.stockOriginal).toFixed(0) + ' ' + this.state.producto.unidad:
												Number(this.state.producto.stockOriginal).toFixed(2) + ' ' + this.state.producto.unidad
										}
									</Label>
								</Col>
							</Row>


							<Row className="fila-resultado">
								<Col sm="0">
									<Label className="label-resultado">
										Stock Resultante:
									</Label>
								</Col>
							
								<Col sm="0">
									<Label className="label-valor-resultado">
										{
											(this.state.stockResultante>=0)
											
											?
												this.state.producto.unidad=='u' ?
													Number(this.state.stockResultante).toFixed(0) + ' ' + this.state.producto.unidad:
													Number(this.state.stockResultante).toFixed(2) + ' ' + this.state.producto.unidad
											:
											
												<h6>No hay suficiente stock</h6>

										}
									</Label>

								</Col>
							</Row>

						</div>
						
						<div>
						
							<br/>					

							{
								modoDebug &&
								<>
								<Row className="fila-resultantes-cuentas">

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Caja:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + this.state.cuentas.caja}
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Resultante:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + this.state.resultanteCaja}
										</Label>
									</Col>

								</Row>


								<Row className="fila-resultantes-cuentas">

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Cuenta Corriente:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + this.state.cuentas.cuentaCorriente}
										</Label>
									</Col>

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Resultante:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + this.state.resultanteCorriente}
										</Label>
									</Col>

								</Row>



								<Row className="fila-resultantes-cuentas">

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Crédito a favor:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + this.state.cuentas.creditoAFavor}
										</Label>
									</Col>



									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Resultante:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + this.state.resultanteCredito}
										</Label>
									</Col>            

								</Row>
							</>
						}
						</div>
						

						<Row className="fila-resultado">
						
							<Col sm="0"> </Col>
						
							<Col sm="0">
								<Button  type="submit" className="boton">Actualizar detalle</Button>
							</Col>
							
						</Row>
						<Row className="fila-resultado"/>
						<Row className="fila-resultado">
							
							<Col sm="0"> </Col>
							
							<Col sm="0">
								<Label className="label-respuesta-api">	
									{this.state.isSubmitted && this.state.resData}
								</Label>
							</Col>
						</Row>

						{modoDebug && this.listaProductos()}
						{modoDebug && this.state.stockResultante}		
					</Form> 

				}

				</div>
			)
		
		}
		catch(error){
			alert(error)
		}
	//Cierro la función render	
	}
//Cierro la clase correspondiente al componente
}
