import React, { Component  } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Venta = function(props) {

return(
	<tr>
		<td class="text-center">{props.venta.fecha}</td>
		<td class="text-center">{props.venta.hora}</td>
		<td class="text-center">{props.venta.codVenta}</td>
		<td class="text-center">{props.venta.vendedorNomAp}</td>

		<td class="text-center">
			<Link to={"registro/" + encodeURIComponent(props.venta.fecha) +'/' + props.venta.codVenta +'/' + props.venta.vendedor +'/' + props.venta.id  }>Ver detalle</Link> | &nbsp;		
			<Link to={"nuevo/" + props.venta.id  }>Agregar elementos</Link>  | &nbsp;
			<Link to={"editarEnc/" + props.venta.id  } >Editar encabezado</Link> 
		</td>

	</tr>
  );
  

}



export default class ListaVentas extends Component {

	constructor(props) {
		super(props);
		
		this.state = {
			listaVentas: [],
			vendedores: []
		};
	}

	componentDidMount() {
		axios.get('http://localhost:5000/ventas/listaVentas')
			.then(respuesta => {
				try {
					this.setState({
						listaVentas: respuesta.data.arrayVentas,
						vendedores: respuesta.data.vendedores
					})
				}
				catch(error){
					alert(error);
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}

	listaVentas() {
		try{
			return this.state.listaVentas
				.filter(function(venta) {
					//Si el usuario logueado es un vendedor
					if (JSON.parse(sessionStorage.getItem('user')).categoria=='vendedor') {
						//Devuelvo solo las ventas asignadas a ese vendedor
						if(venta.vendedor==JSON.parse(sessionStorage.getItem('user')).username) {
							return venta;
						}
					}
					//Si el usuario logueado no es un vendedor, devuelvo todass las ventas
					else {
						return venta;
					}
				})
				.map(ventaActual => {
					let ventaFormato=ventaActual;
					
					const vendedor=this.state.vendedores.find(v => v.username==ventaActual.vendedor);
					if(vendedor) {
						ventaFormato.vendedorNomAp =vendedor.nombre + ' ' + vendedor.apellido;
					}
					else {
						ventaFormato.vendedorNomAp= "(Usuario dado de baja: " + ventaActual.vendedor + ")";
					}
					return <Venta venta= {ventaFormato}   />;
				})
		}
		
		catch(error){
			alert(error);
		}
	}
	render() {
		try{
			return(
				<div>
	
					<h3>Ventas registradas</h3>

					<br/>

					<h6>Para eliminar una venta, seleccione "Ver detalle" y pulse el boton "Eliminar todos"</h6>
					<h6>situado debajo de la lista de elementos del detalle.</h6>

					<table className="table">
						<thead>
							<tr>
								<th class="text-center">Fecha</th>
								<th class="text-center">Hora</th>
								<th class="text-center">Código</th>
								<th class="text-center">Vendedor</th>
								<th class="text-center">Opciones</th>
							</tr>
						</thead>
						
						<tbody>
							{ this.listaVentas() }
						</tbody>
					</table>
		
				</div>   
			);
		}
		
		catch(error) {
			alert(error)
		}
	}
}
