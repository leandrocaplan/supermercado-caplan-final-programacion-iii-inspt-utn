import React from "react";

import {
	Navbar,
	Nav,
	NavItem,
	NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";

const MenuVentas = () => {

return (
	<div class="center-menu">
		<Navbar
			color="dark"      
			className="custom-navMenu"
			expand="md"
			container="md"
		>

			<Nav className="mc-auto" navbar>

				<NavItem className="custom-menu">
					<NavLink tag={Link} to={"/ventas/nuevo"}>
						<p>Nueva venta</p>
					</NavLink>
				</NavItem>         

				<>&nbsp;&nbsp;&nbsp;</>


				<NavItem className="custom-menu">
					<NavLink tag={Link} to={"/ventas/lista"}>    
						<p>Lista de ventas</p>
					</NavLink>
				</NavItem>

				<>&nbsp;&nbsp;&nbsp;</>

				<NavItem className="custom-menu">
					<NavLink tag={Link} to={"/ventas/registro"}>    
						<p>Registro de detalles</p>
					</NavLink>
				</NavItem>


			</Nav>

		</Navbar>
	</div>
	);
};

export default MenuVentas;
