import React, { Component, useState } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';
import DateTimePicker from 'react-datetime-picker'

const formatearFecha =function (fecha) {
	const fechaObj=new Date(fecha);
    return (
			fechaObj.getDate().toString().padStart(2, "0") + '/'+ 
			(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+ 
			fechaObj.getFullYear()
		)};
		
const formatearTipoPago={
  'efectivo':'Efectivo',
  'debito':'Tarjeta de débito',
  'credito':'Tarjeta de crédito'
};

		
export default class ReasignarDetalleVenta extends Component {

	constructor(props) {
		//En el constuctor, definimos las variables de estado y las inicializamos

		super(props);

		this.state = {

			detalle:{},
			ventas:[],
			ventaAReasignar:{},
			idVentaAReasignar:'',
			nuevaVenta:false,
			ventaExistente:false,
			isSubmitted:false,
			resData:"",
			
			//Me traigo las variables de realizar-venta
			//Campos a almacenar en el array que mando a la API
			codVenta:1,
			userVendedor: '',
			fechaHora: new Date(),
			fechaHoraActual: true,

			//Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
			vendedor: {},
			productos:[],
			vendedores:[],
			codigosExistentes:[],
			idVentaAsociada:'',
			producto:{},
			
			categoriaUsuario:JSON.parse(sessionStorage.getItem('user')).categoria,
			

			isSubmitted:false,
			resData:'',

		}
	}


	/*
	Nota: el valor del stock resultante de la operación será siempre:
	El valor del stock que el producto tenía previamente - la cantidad vendida en la operación
	*/


	//Nota: los atributos codVenta (Código de venta) y userVendedor (nombre de usuario del vendedor), son comunes para toda la venta
	//El resto de los atributos van asociados a cada elemento del detalle de la venta.

	componentDidMount() {
		//Al cargar el componente,
		//llamo a la API para que me traiga la lista de vendedores y productos.
		//Este 'get' me trae solamente usuarios registrados como vendedores
		//Tambien me trae la lista de todas las ventas, pero no las utilizaremos
		axios.get('http://localhost:5000/ventas/detalleId/' + this.props.match.params.idDetalle + "/" + this.props.match.params.idVenta)
			.then(respuesta => {
				try {
					//Me guardo la lista de vendedores
					this.setState({
						detalle:respuesta.data.detalleVentas.detalle,
						idVentaAsociada:this.props.match.params.idVenta,
						ventas:respuesta.data.detalleVentas.ventas
							.filter(function(venta) {
								const categoriaUsuario = JSON.parse(sessionStorage.getItem('user')).categoria;
								const username = JSON.parse(sessionStorage.getItem('user')).username;

								//Si el usuario logueado es un vendedor, filtro solo las ventas
								//asociadas a dicho vendedor
								if(categoriaUsuario=='vendedor' && venta.vendedor==username) {
									return venta;
								}
								else if(categoriaUsuario=='administrador') {
										return venta;
									}
								}),

						vendedores:respuesta.data.vendedores,
						producto:respuesta.data.producto
					});

					//Si el usuario actual es un administrador, puedo asignar el vendedor
					//que yo elija a la venta a modificar
					if(this.state.categoriaUsuario =='administrador') {

						this.setState({
							userVendedor: respuesta.data.vendedores[0].username,
							vendedor: respuesta.data.vendedores[0],
						});
					}

					//Si el usuario actual es un vendedor, la venta quedará asociada al mismo
					else {
						this.setState({
							userVendedor: JSON.parse(sessionStorage.getItem('user')).username,
							vendedor: JSON.parse(sessionStorage.getItem('user'))
						});
					}

					//Obtengo el máximo codigo de venta asignado al actual vendedor y la fecha actual,
					//para luego sumarle 1 al código de venta de la venta actual.
					//De otra forma, en vez de generar una venta nueva, probablemente
					//se agregarían elementos al detalle de una venta existente
					//a menos que el usuario configure a mano un codigo de venta aún inexistente

					this.setState(
						//En lugar de pasarle un objeto a setState,
						//le paso un callback con el estado anterior
						function(estadoAnterior) {
							//Inicialmente, el estado nuevo es igual al estado anterior
							let estadoNuevo=estadoAnterior;


							let codVentaMax=1;

							//Me genero un array con todos los códigos de venta
							//correspondientes a las ventas de el usuario asignado
							//actualmente y la fecha asignada actualmente
							const codigosExistentes=this.state.ventas.filter(
							function(venta) {
								if(
									(venta.vendedor==estadoAnterior.userVendedor)
									&&
									(formatearFecha(venta.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
								)
									return venta;
							}
													)
							.map(
								function(venta)
								{
									return venta.codVenta;
								}
							);

							//Del array que generé antes, me guardo el máximo valor
							const maximo=Math.max(...codigosExistentes);

							//Debug: muestro el array generado y su máximo valor
							console.log(codigosExistentes);
							console.log(maximo);


							//Deprecación: esta linea no la necesitamos
							//Si el código de venta asignado al estado actual
							//existe en el array generado anteriormente,
							//sabemos que dicha venta existe,
							//por lo que ventaExistente valdrá true.
							//De lo contrario, la venta no existe y ventaExistente}
							//valdrá false
							const ventaExistente= codigosExistentes.some(function(codigo) {
								return codigo==estadoAnterior.codVenta
							});


							//Me guardo en el estado actualizado el
							//array generado con los códigos de venta
							//existentes asignados a dicha fecha y vendedor
							estadoNuevo.codigosExistentes=codigosExistentes;

							//Si existía en el array generado algún código de venta
							//el máximo siempre será mayor o igual a 0
							if(maximo>=0) {
								//El codigo de venta del estado actual valdrá
								//el máximo valor hallado en el mas 1, de manera
								//que me puedo asegurar que, si quiero generar
								//una venta nueva, su código no será el mismo
								//que el de una venta ya existente
								estadoNuevo.codVenta=maximo+1;
							}
							else {

								//Si no existe ninguna venta asignada a cierto vendedor
								//y cierta fecha, la constante máximo valdrá (-Infinity),
								//por lo cual, el código de venta generado será 1
								estadoNuevo.codVenta=1;
							}
							return estadoNuevo;
						}
						
					);


				}
				catch(error) {
					alert(error)
				}
			})
			.catch((error) => {
				alert(error);
			});
	}

	componentDidUpdate() {
		console.log(this.state.ventas);
	}

	/*
	----------------------------------------------------
	Sección de 'onChange'
	Al hacer cualquier modificación en algun campo del formulario,
	la variable 'isSubmitted' la reseteamos en 'false'
	----------------------------------------------------
	*/

	onChangeVentaAReasignar = (e) => {
		try {

			this.setState({
				idVentaAReasignar: e.target.value,
				isSubmitted:false,
				resData:""
			})
			if(e.target.value !="") {
				const ventaAReasignar=this.state.ventas.find(venta => venta._id==e.target.value);
				this.setState({
					ventaAReasignar:ventaAReasignar,
					codVenta:ventaAReasignar.codVenta,
					userVendedor:ventaAReasignar.vendedor,
					fechaHora:new Date(ventaAReasignar.fechaHora),
					fechaHoraActual:false,
					ventaExistente:true

				})

			}

			else {

				//Obtengo el máximo codigo de venta asignado al actual vendedor y la fecha actual,
				//para luego sumarle 1 al código de venta de la venta actual.
				//De otra forma, en vez de generar una venta nueva, probablemente
				//se agregarían elementos al detalle de una venta existente
				//a menos que el usuario configure a mano un codigo de venta aún inexistente

				this.setState(
					//En lugar de pasarle un objeto a setState,
					//le paso un callback con el estado anterior
					function(estadoAnterior) {
						//Inicialmente, el estado nuevo es igual al estado anterior
						let estadoNuevo=estadoAnterior;


						let codVentaMax=1;

						//Me genero un array con todos los códigos de venta
						//correspondientes a las ventas de el usuario asignado
						//actualmente y la fecha asignada actualmente
						const codigosExistentes=this.state.ventas.filter(
						function(venta) {
							if(
								(venta.vendedor==estadoAnterior.userVendedor)
								&&
								(formatearFecha(venta.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
							)

								return venta;
						}
												)
						.map(
							function(venta)
							{
								return venta.codVenta;
							}
						);


						//Del array que generé antes, me guardo el máximo valor
						const maximo=Math.max(...codigosExistentes);

						//Debug: muestro el array generado y su máximo valor
						console.log(codigosExistentes);
						console.log(maximo);


						const ventaExistente= codigosExistentes.some(function(codigo) {
							return codigo==estadoAnterior.codVenta
						});

						//estadoNuevo.ventaExistente=ventaExistente;
						//console.log(ventaExistente)

						//Me guardo en el estado actualizado el
						//array generado con los códigos de venta
						//existentes asignados a dicha fecha y vendedor
						estadoNuevo.codigosExistentes=codigosExistentes;

						//Si existía en el array generado algún código de venta
						//el máximo siempre será mayor o igual a 0
						if(maximo>=0) {
							//El codigo de venta del estado actual valdrá
							//el máximo valor hallado en el mas 1, de manera
							//que me puedo asegurar que, si quiero generar
							//una venta nueva, su código no será el mismo
							//que el de una venta ya existente
							estadoNuevo.codVenta=maximo+1;
						}
						else {

							//Si no existe ninguna venta asignada a cierto vendedor
							//y cierta fecha, la constante máximo valdrá (-Infinity),
							//por lo cual, el código de venta generado será 1
							estadoNuevo.codVenta=1;
						}

						estadoNuevo.ventaExistente=false;
						estadoNuevo.fechaHora= new Date();
						estadoNuevo.fechaHoraActual= true;

						return estadoNuevo;
					}
					
				);
			}

		}
		catch(error) {
			alert(error)
		}
	}

	onChangeFechaHoraActual = (e) => {
		try {

			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codVentaMax=1;
				console.log(estadoAnterior.userVendedor);

				const codigosExistentes=this.state.ventas
					.filter(
						function(venta) {
							if(
								(venta.vendedor==estadoAnterior.userVendedor)
								&&
								(
									(e.target.checked && formatearFecha(venta.fechaHora) == formatearFecha(new Date())) ||
									(!e.target.checked && formatearFecha(venta.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
								)
							)
							return venta;
						}
					)
					.map(
						function(venta)
						{
							return venta.codVenta;
						}
					);


				const maximo=Math.max(...codigosExistentes);
				console.log(codigosExistentes);
				console.log(maximo);

				const ventaExistente= codigosExistentes.some(function(codigo) {
					return codigo==estadoAnterior.codVenta
				});

				//estadoNuevo.ventaExistente=ventaExistente;
				//console.log(ventaExistente);
				estadoNuevo.ventaExistente=false;

				estadoNuevo.codigosExistentes=codigosExistentes;
				if(maximo>=0) {
					estadoNuevo.codVenta=maximo+1;
				}
				else {
					estadoNuevo.codVenta=1;
				}
				return estadoNuevo;
			});

			this.setState({
				fechaHoraActual: e.target.checked,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	onChangeFechaHora = (e) => {
		try {
			//El codigo de venta será el mismo para todos los elementos del array

			console.log(e);

			//Actualizo los codigos existentes cuando cambio la fecha
			this.setState(

				function(estadoAnterior) {
					//Inicialmente, el estado nuevo es igual al estado anterior
					let estadoNuevo=estadoAnterior;

					//Me genero un array con todos los códigos de venta
					//correspondientes a las ventas de el usuario asignado
					//actualmente y la fecha asignada actualmente
					const codigosExistentes=this.state.ventas
						.filter(
							function(venta) {
								if(
									(venta.vendedor==estadoAnterior.userVendedor)
									&&
									(formatearFecha(venta.fechaHora) == formatearFecha(e))
								)

								return venta;
							}
						)
						.map(
							function(venta)
							{
								return venta.codVenta;
							}
						);

					const ventaExistente= codigosExistentes.some(function(codigo) {
						return codigo==estadoAnterior.codVenta
					});

					estadoNuevo.ventaExistente=ventaExistente;
					estadoNuevo.codigosExistentes=codigosExistentes;

					return estadoNuevo;
				}
			);


			const ventaAReasignar=this.state.ventas.find(function (venta) {
				if(
					(venta.codVenta==this.state.codVenta) &&
					(venta.vendedor==this.state.userVendedor) &&
					(formatearFecha(venta.fechaHora)== formatearFecha(e))
				)
				{
					console.log("Entro al if");
					return venta;
				}
			},this);


			if(ventaAReasignar) {
				this.setState({
					ventaAReasignar:ventaAReasignar,
					idVentaAReasignar:ventaAReasignar._id,
					ventaExistente:true
				})
			}
			else {
				this.setState({
					ventaAReasignar:{},
					idVentaAReasignar:'',
					ventaExistente:false
				})
			}

			this.setState({
				fechaHora: e,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	//Al modificar el codigo de venta en el form, actualizo el estado del codigo de venta
	onChangeCodVenta = (e) => {

		try {
			const ventaAReasignar=this.state.ventas.find(function (venta) {

				if(

					(venta.codVenta==e.target.value) &&
					(venta.vendedor==this.state.userVendedor) &&
					(formatearFecha(venta.fechaHora) == formatearFecha(this.state.fechaHora))
				)
				{
					console.log("Entro al if");
					return venta;
				}

			},this);


			if(ventaAReasignar) {
				this.setState({
					ventaAReasignar:ventaAReasignar,
					idVentaAReasignar:ventaAReasignar._id,
					ventaExistente:true
				})
			}
			else {
				this.setState({
					ventaAReasignar:{},
					idVentaAReasignar:'',
					ventaExistente:false
				})
			}

			this.setState({
				codVenta: Number(e.target.value),
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}

	//Al modificar el vendedor en el form, actualizo el username del vendedor
	//y el vendedor completo asociado a ese username
	onChangeVendedor = (e) => {

		try {

			//Actualizo los codigos existentes cuando cambio el vendedor
			this.setState(
				function(estadoAnterior) {
					//Inicialmente, el estado nuevo es igual al estado anterior
					let estadoNuevo=estadoAnterior;



					//Me genero un array con todos los códigos de venta
					//correspondientes a las ventas de el usuario asignado
					//actualmente y la fecha asignada actualmente
					const codigosExistentes=this.state.ventas.filter(
					function(venta) {
						if(
							(venta.vendedor==e.target.value)
							&&
							(
								//Si el flag fechahoraActual está activado,
								//el resultado del filter me devolverá las ventas existentes del vendedor
								//correspondientes al dia de hoy
								(estadoAnterior.fechaHoraActual && formatearFecha(venta.fechaHora) == formatearFecha(new Date())) ||

								//Si está desactivado,
								//el resultado del filter me devolverá las ventas existentes del vendedor
								//correspondientes al estado de fechaHora que tengo actualmente
								(!estadoAnterior.fechaHoraActual && formatearFecha(venta.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
							)
						)

						return venta;
					}
											)
					.map(
						function(venta)
						{
							return venta.codVenta;
						}
					);

					const ventaExistente= codigosExistentes.some(function(codigo) {
						return codigo==estadoAnterior.codVenta
					});

					estadoNuevo.ventaExistente=ventaExistente;

					//Me guardo en el estado actualizado el
					//array generado con los códigos de venta
					//existentes asignados a dicha fecha y vendedor
					estadoNuevo.codigosExistentes=codigosExistentes;
					
					return estadoNuevo;
				}
			);


			const ventaAReasignar=this.state.ventas.find(function (venta) {
				if(
					(venta.codVenta==this.state.codVenta) &&
					(venta.vendedor==e.target.value) &&
					(formatearFecha(venta.fechaHora)== formatearFecha(this.state.fechaHora))
				)
				{
					return venta;
				}

			},this);


			if(ventaAReasignar) {
				this.setState({
					ventaAReasignar:ventaAReasignar,
					idVentaAReasignar:ventaAReasignar._id,
					ventaExistente:true
				})
			}
			else {
				this.setState({
					ventaAReasignar:{},
					idVentaAReasignar:'',
					ventaExistente:false
				})

			}
			this.setState({
				userVendedor: e.target.value,
				vendedor: this.state.vendedores.find(p => p.username===e.target.value),
				isSubmitted:false
			})
			console.log(this.state.userVendedor);
		}
		catch(error) {
			alert(error)
		}
	}

	onSubmit = (e) => {
		try {

			//Esta linea evita que al hacer el submit, salgamos de la vista actual
			e.preventDefault();

			console.log(this.state.idVentaAsociada);
			console.log(this.state.idVentaAReasignar);
			if(this.state.idVentaAsociada!=this.state.idVentaAReasignar) {

				const idVentaAsociada=this.state.idVentaAsociada;
				const idVentaAReasignar=this.state.idVentaAReasignar;
				const idDetalle=this.state.detalle._id;
				const ventaExistente=this.state.ventaExistente;
				const codVenta=this.state.codVenta;
				const userVendedor=this.state.userVendedor;
				const fechaHora=this.state.fechaHora;

				const reasignacion=
				{
					idVentaAsociada:idVentaAsociada,
					idVentaAReasignar:idVentaAReasignar,
					idDetalle:idDetalle,
					ventaExistente:ventaExistente,
					codVenta:codVenta,
					userVendedor:userVendedor,
					fechaHora:fechaHora
				};


				//console.log(reasignacion);
				//Hacemos un post a la API con la const cargada previamente
				axios.post('http://localhost:5000/ventas/reasignar/', reasignacion)
					.then(res => {
						this.setState({
							//Debo actualizar el listado de ventas y el ID de la venta asociada
							isSubmitted:true,
							resData:res.data.mensaje

						});

						this.setState(

							/*
							 * Debo eliminar el elemento del detalle de la venta
							 * a la cual estaba asignado previamente y luego,
							 * reasignarselo a la venta correspondiente
							 *
							 *	Necesito:
							 * 	-Buscar la venta a la cual estaba asignado el detalle
							 * 	-Eliminarle el elemento del detalle que quiero reasignar
							 * 	-Si lo reasigno a una venta nueva, debo generarla con el unico elemento en el detalle, correspondiente al elemento reasignado
							 * 	-Si lo reasigno a una venta existente, debo concatenerle el elemento del detalle que estoy reasignando
							 * 	-Actualizar en el array de ventas, ambas ventas: tanto la venta de la cual desasigné el detalle, como la venta a la cual lo reasigné
							 *  */

							function(estadoAnterior) {

								//Me genero una variable estadoNuevo, inicialmente
								//igual al cuerpo del estado anterior
								let estadoNuevo=estadoAnterior;

								//Me genero un array ventasFiltradas, con los mismos elementos que tenia en
								//el array de ventas original, pero eliminandole la venta
								//a la cual le quiero desasignar el detalle
								let ventasFiltradas=estadoAnterior.ventas.filter(venta => (venta._id!=estadoAnterior.idVentaAsociada));


								//En el array de ventas que tenía en el estado anterior, busco la venta asociada
								//al detalle que quiero desasignar, y me la guardo en ventaOriginal
								let ventaOriginal=estadoAnterior.ventas.find(venta => venta._id==estadoAnterior.idVentaAsociada);


								//En la venta original que me guardé arriba, busco por su ID,
								//el elemento de detalle que quiero desasignar.
								//Me guardo en detalleFiltrado el nuevo detalle sin el elemento
								//que desasigné
								const detalleFiltrado=ventaOriginal.detalle.filter(elemento => elemento._id != estadoAnterior.detalle._id);

								//Si en el detalle de la venta a la cual estoy desasignando
								//el elemento que quiero, nos queda algún otro elemento
								if(detalleFiltrado.length>0) {

									//Al detalle de la venta original le asigno el
									//detalle con el elemento correspondiente desasignado
									ventaOriginal.detalle=detalleFiltrado;

									//Al array de ventas filtradas, le concateno la
									//venta original, ahora con el detalle actualizado
									//(sin el elemento que desasigné)
									ventasFiltradas=ventasFiltradas.concat(ventaOriginal);
								}

								//Si en el detalle de la venta a la cual estoy desasignando
								//el elemento que quiero, no nos queda ningun otro elemento,
								//simplemente no hacemos nada mas y eliminamos de la lista
								//la venta, ya que la misma no contendrá ningun elemento
								//en su detalle y resultará superfluo tenerla en el array


								//Una vez desasignado el elemento del detalle de la venta
								//a la cual lo tenía asignado originalmente, debo reasignarlo,
								//ya sea a una venta existente o a una nueva


								//Declaro una variable donde tendré mi nuevo array de ventas
								let ventasNuevo;

								//Declaro una variable la cual contendrá la venta a la cual
								//quiero reasignarle el elemento del detalle.
								//Debe estar fuera del scope del if/else, ya que
								//luego deberé utilizarla para actualizar el ID de la venta
								//asociada actualmente en el estado
								let ventaReasignada;

								//Si estoy reasignandolo a una venta ya existente
								if(estadoAnterior.ventaExistente) {


									//En el array de ventas que tenía en el estado anterior, busco la venta completa
									//a la cual quiero reasignar el detalle,
									//y me la guardo en la variable ventaReasignada

									ventaReasignada=estadoAnterior.ventas.find(venta => venta._id==idVentaAReasignar);

									//Luego, al detalle de dicha venta le concateno el elemento del detalle
									//que quiero reasignar
									ventaReasignada.detalle=ventaReasignada.detalle.concat(estadoAnterior.detalle);


									//Al array que generé antes con la venta correspondiente con el detalle
									//desasingado, le elimino la venta correspondiente a la que quiero
									//reasignar el elemento del detalle, y me guardo el resultado
									//en ventasNuevo.
									ventasNuevo=ventasFiltradas.filter(venta => (venta._id!=estadoAnterior.idVentaAReasignar));

									//Luego, a ventasNuevo le agregamos la venta ya con el elemento
									//del detalle asignado
									ventasNuevo=ventasNuevo.concat(ventaReasignada);

								}

								//Si estoy asignandolo a una nueva venta
								else {

									//Me genero una nueva venta, con un ID que aún desconozco,
									//(ya que me lo generará automáticamente la API),
									//y un array en el detalle con un único elemento,
									//correspondiente al elemento reasignado
									//Me guardo la nueva venta en ventaReasignada
									ventaReasignada= {
										_id:res.data.id,
										codVenta:estadoAnterior.codVenta,
										vendedor:estadoAnterior.userVendedor,
										detalle:[estadoAnterior.detalle],
										fechaHora:new Date(estadoAnterior.fechaHora)
									};
									estadoNuevo.idVentaAReasignar=res.data.id;
									estadoNuevo.codigosExistentes=estadoAnterior.codigosExistentes.concat(estadoAnterior.codVenta);

									//Finalmente, al array que contiene la lista
									//de ventas actualizada, le asigno todas las
									//ventas contenidas en el array filtrado,
									//agregandole la venta generada arriba
									ventasNuevo=ventasFiltradas.concat(ventaReasignada);

								}

								//Al estado que estoy actualizando
								//en la propiedad 'ventas', le asigno
								//el array que generé con las ventas actualizadas
								estadoNuevo.ventas=ventasNuevo;

								//Luego, la propiedad idVentaAsociada,
								//la actualizo con el ID de la venta a la cual
								//reasigné el detalle
								estadoNuevo.idVentaAsociada=ventaReasignada._id;



								return estadoNuevo;
							}
							
						);
					})
					.catch(res => {
						this.setState({isSubmitted:true});
						if(res.code=="ERR_BAD_REQUEST") {
							this.setState({resData:res.response.data});
						}
						else {
							this.setState({resData:res.message});

						}
						console.log(res);
					});
				//Cierro POST de venta
			}
			else {
				alert("La venta a la cual quiero reasignar el detalle es la misma a la cual el detalle ya está asignado");
			}
		}
		catch(error) {
			alert(error)
		}
	}
	
	render() {
		try{
			return (

				<div className="center-realizar-venta">
					<center>
						<h3>Información del detalle:</h3>
						
							<Row className="fila-resultantes-cuentas">
								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										ID del detalle:
									</Label>
								</Col>


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{this.state.detalle._id}
									</Label>
								</Col>
							</Row>

							<Row className="fila-resultantes-cuentas">
								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										ID de venta asociada:
									</Label>
								</Col>


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{this.state.idVentaAsociada}
									</Label>
								</Col>
							</Row>



							<Row className="fila-resultantes-cuentas">
								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Producto:
									</Label>
								</Col>


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{
											(this.state.producto) ?
												this.state.producto.descripcion:
												"(Producto dado de baja :" + Number(this.state.detalle.producto) +")"
										}
									</Label>
								</Col>
							</Row>

							<Row className="fila-resultantes-cuentas">
								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Precio unitario:
									</Label>
								</Col>

								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{" $ "+ this.state.detalle.precioActual + "/" + this.state.detalle.unidadActual}
									</Label>
								</Col>
							</Row>

							<Row className="fila-resultantes-cuentas">
								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Cantidad:
									</Label>
								</Col>

								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{this.state.detalle.cantidad + " " + this.state.detalle.unidadActual}
									</Label>
								</Col>
							</Row>

							<Row className="fila-resultantes-cuentas">
								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Tipo de pago:
									</Label>
								</Col>


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{formatearTipoPago[this.state.detalle.tipoPago]}
									</Label>
								</Col>
							</Row>

							<Row className="fila-resultantes-cuentas">
								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										Subtotal:
									</Label>
								</Col>


								<Col sm="0">
									<Label className="label-resultantes-cuentas">
										{"$ " +this.state.detalle.subtotal}
									</Label>
								</Col>
							</Row>


							<br/>
							<Form onSubmit={this.onSubmit} >

								<Input 
									type="select"
									className="inp-reasignar"

									value={this.state.idVentaAReasignar}
									onChange={this.onChangeVentaAReasignar}>
										{

											[
												<option key="" value="">
												Venta Nueva
												</option>
											].concat(this.state.ventas.map(function(venta) {
											const vendedorFormat=
												this.state.vendedores.some(v=>v.username==venta.vendedor) ?
													this.state.vendedores.find(v=>v.username==venta.vendedor):
													{
														nombre: "(Usuario dado de baja: ",
														apellido: venta.vendedor + ")"	
													} ;
											return <option key= {venta._id} value= {venta._id}>
												{
												"Codigo: " + venta.codVenta + 
												"\tVendedor: " + vendedorFormat.nombre + "  "+ vendedorFormat.apellido +
												"\tFecha: " + formatearFecha(venta.fechaHora)}
											</option>;
											},this)
											)
										}
								</Input>


								<Row className="fila-espaciado"/>
								<Row className="encabezado">

									<Col sm="0">
										<Label className="label-fecha-hora-actuales">
											Registrar fecha y hora actuales:
										</Label>
									</Col>

									<Col sm="0">
										<Input
											type="checkbox"
											className="checkbox-fecha-hora-actuales"
											defaultChecked={this.state.fechaHoraActual} 
											value={this.state.fechaHoraActual}
											checked={this.state.fechaHoraActual}
											selected={this.state.fechaHoraActual}
										onChange={this.onChangeFechaHoraActual}
										/>
									</Col>

									{!this.state.fechaHoraActual &&
									<>

										<Col sm="0">
											<Label className="label-fecha-hora">
												Fecha y hora:
											</Label>
										</Col>

										<Col sm="0" >
									
											<DateTimePicker
												className="inp-fecha-hora"
												value={this.state.fechaHora}
												onChange={this.onChangeFechaHora}
												format="dd/MM/yyyy hh:mm:ss"
												showTimeSelect
											/>
										</Col>	
									</>	
									}

										<Col sm="0" >
											<Label className="label-cod-venta" >
												 Código de venta:
											</Label>
										</Col>


										<Col sm="0" >

											<Input 
												type="number"
												min="1"
												step="1" 
												className="inp-cod-venta"
												value={this.state.codVenta}
												onChange={this.onChangeCodVenta}
												/>


										</Col>

										{this.state.categoriaUsuario=='administrador' &&
										<>

											<Col sm="0">

												<Label className="label-vendedor">
													Vendedor:
												</Label>
											</Col>

											<Col sm="0" size="sm"> 

											<Input
												size="sm"
												type = "select"
												required
												className="inp-vendedor"
												value={this.state.userVendedor}
												onChange={this.onChangeVendedor}>
												{
													this.state.vendedores.map(function(vendedor) {
														return<option key={vendedor.username} value={vendedor.username}>
															{vendedor.nombre} {vendedor.apellido}
															</option>;
													}
													)
												}
											</Input>
											</Col>
										</>
	
									}
								</Row>
								
								<Row className="encabezado">
									<Col sm="0">
										<Label >

										</Label>

									</Col>
								</Row>


								<Row className="encabezado">
									<Col sm="0">
										<Label className="label-reasignar">
											ID de venta a reasignar:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-reasignar">
											{this.state.idVentaAReasignar!='' 
												?
												this.state.idVentaAReasignar
												: 
												<>(Venta nueva)</>}

										</Label>
									</Col>
								</Row>


								{
									this.state.codigosExistentes.some(function(codigo){
										return codigo==this.state.codVenta
										},this)
										?
										<h6>Venta existente</h6>
										:
										<h6>Venta nueva</h6>
								}

								<Button className="boton" type="submit">Reasignar Venta</Button>
							</Form>
							<Label className="label-respuesta-api">
								{this.state.isSubmitted && this.state.resData}
							</Label>
					</center>
				</div>
			)
		}
		catch(error){
			alert(error)
		}
	}
}	
