import React, { Component} from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';


export default class EditarProducto extends Component {

	constructor(props) {

		super(props);

		this.state = {
			_id:'',
			descripcion:'',
			unidad:'',
			unidadOriginal:'',
			precioCompra:0,
			precioVenta:0,
			stock:0,    
			producto: '',
			vendedor: '',
			cantidad: 0,
			tipoPago: '',
			total: 0,
			productos: [],
			vendedores: [],

			isSubmitted:false
		}

		console.log('Ejecuto el constructor');
		console.log(this.state.producto);
	}


	componentDidMount() {

		axios.get('http://localhost:5000/productos/' + this.props.match.params.id)
		  .then(respuesta => {
				try {
				   this.setState({
						_id: respuesta.data._id,
						descripcion: respuesta.data.descripcion,
						unidad: respuesta.data.unidad,
						unidadOriginal:respuesta.data.unidad,
						precioCompra: respuesta.data.precioCompra,
						precioVenta: respuesta.data.precioVenta,
						stock:respuesta.data.stock
					});
			 
				}
				catch(error) {
					alert(error)
				}
			 })
			.catch((error) => {
			  alert(error);
			})           
	 }


	onChangeDescripcion = (e) => {
		try{
			this.setState({
			  descripcion: e.target.value,
			  isSubmitted:false
			})
		}
		catch(error){
			alert(error);
		}
	}
	
	onChangeUnidad = (e) => {
		try {
			if(e.target.value == 'u') {
				this.setState({
					stock: Math.ceil(this.state.stock)
				})
			}
			this.setState({
				unidad: e.target.value,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	onChangePrecioCompra = (e) => {
		try{
			this.setState({
				precioCompra: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error)
		}
	}
	
	onChangePrecioVenta = (e) => {
		try{
			this.setState({
			  precioVenta: e.target.value,
			  isSubmitted:false
			})
		}
		
		catch(error){
			alert(error)
		}
	}
	onChangeStock = (e) => {
		try{
			this.setState({
				stock: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error)
		}
	}
	
	onSubmit = (e) => {
		try{
			e.preventDefault();

			this.setState({isSubmitted: true});
			const producto = {

				descripcion:this.state.descripcion,
				unidad:this.state.unidad,
				precioCompra:this.state.precioCompra,
				precioVenta: this.state.precioVenta,
				stock:this.state.stock
			}
			
			let modificarUnidad=true;
			let asegurarPrecios=true;
			
			if(this.state.unidad != this.state.unidadOriginal){
				modificarUnidad=window.confirm("Al modificar la unidad de un producto, puedo obtener resultados inesperados en el sistema\n¿Está seguro de que desea proseguir?");
			}	
			
			if(Number(this.state.precioCompra) > Number(this.state.precioVenta)){
				asegurarPrecios=window.confirm("El precio de compra es mayor al precio de venta\nEsto no es sensato desde el punto de vista económico\n¿Está seguro de que desea proseguir?");
			}	
							
			if(modificarUnidad && asegurarPrecios) {
					console.log(producto);
					//console.log(JSON.stringify(user));
					axios.post('http://localhost:5000/productos/actualizar/' + this.props.match.params.id, producto)
					.then(res => {
						console.log(res.data);
						this.setState({
							unidadOriginal:this.state.unidad,
							resData: res.data,
							isSubmitted:true
						})
					})
				.catch((error) => {
					alert(error);
				});			
			}
		}
		catch(error){
			alert(error);
		}
	}
	
	render() {
		try{
			return (
				<center>
					<div class="center-nuevo-prod-usu">
						<h3>Editar Producto</h3>
						<h6>No podemos editar el ID del producto</h6>
						<h6>Si queremos modificarlo, debemos darlo de baja y generar uno nuevo</h6>
						<br/>
						<Form onSubmit={this.onSubmit} className="form-nuevo-prod-usu">


							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										ID:  
									</Label>
								</Col>

								<Col sm="0">
								<Label className="label-nuevo-prod-usu">

									<h6>{Number(this.state._id)}</h6>

								</Label>
								</Col>
							</Row>


							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
								<Label className="label-nuevo-prod-usu">
									Descripción: 
								</Label>
								</Col>

								<Col sm="0">
									<Input  type="text"
									required
									className="inp-nuevo-prod-usu"
									value={this.state.descripcion}
									onChange={this.onChangeDescripcion}
									/>
								</Col>     
							</Row>

							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
								<Label className="label-nuevo-prod-usu">
									Unidad: 
								</Label>
								</Col>

								<Col sm="0">
								<Input  
									type="select"
									required
									className="inp-nuevo-prod-usu"
									value={this.state.unidad}
									onChange={this.onChangeUnidad}
								>
									<option value="u">u</option>
									<option value="kg">kg</option>
								</Input>                
								</Col>     
							</Row>


							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Precio de compra: 
									</Label>
								</Col>

								<Col sm="0">
									<Input 
									type="number"
									min="0"
									step="0.01"
									className="inp-nuevo-prod-usu"
									value={this.state.precioCompra}
									onChange={this.onChangePrecioCompra}
									/>
								</Col>               
							</Row>

							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Precio de venta:
									</Label> 
								</Col>

								<Col sm="0">
									<Input 
										type="number"
										min="0"
										step="0.01"
										className="inp-nuevo-prod-usu"
										value={this.state.precioVenta}
										onChange={this.onChangePrecioVenta}
									/>  
								</Col>              
							</Row>

							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Stock:
									</Label>
								</Col>
												
								{
								this.state.unidad=='u'
								?
									<Col sm="0">
										<Input 
											type="number"
											min="0"
											step="1"
											className="inp-nuevo-prod-usu"
											value={this.state.stock}
											onChange={this.onChangeStock}
										/>  
									</Col>
								:
									<Col sm="0">
										<Input 
											type="number"
											min="0"
											step="0.01"
											className="inp-nuevo-prod-usu"
											value={this.state.stock}
											onChange={this.onChangeStock}
										/>  
									</Col>
								}
							</Row>


							<br/>

							<Col sm="0">
								<Button  type="submit"  className="boton">Editar Producto</Button>
							</Col>

					</Form>
					<Label className="label-respuesta-api">
						{this.state.isSubmitted && this.state.resData}
					</Label>
				</div>
			</center>
			)
		}
		catch(error){
			alert(error)
		}
	}
}
