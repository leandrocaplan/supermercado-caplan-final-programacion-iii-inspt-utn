import React, { Component,useEffect, useState  } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import { Label} from 'reactstrap';

const Producto = function(props) { 

	return(
	<tr>
		<td class="text-center">{Number(props.producto._id)}</td>
		<td class="text-center">{props.producto.descripcion}</td>
		<td class="text-center">{props.producto.unidad}</td>
		<td class="text-center">{"$ "+ Number(props.producto.precioCompra).toFixed(2)}</td>
		<td class="text-center">{"$ "+Number(props.producto.precioVenta).toFixed(2)}</td>
		<td class="text-center">{
			props.producto.unidad=="u" ?
				Number(props.producto.stock).toFixed(0) :
				Number(props.producto.stock).toFixed(2)
			}</td>
		<td class="text-center">{
			props.producto.unidad=="u" ?
				Number(props.producto.stockOriginal).toFixed(0):
				Number(props.producto.stockOriginal).toFixed(2)
			}</td>
		<td class="text-center">
			<Link to={"editar/"+props.producto._id}>Editar</Link> | <a href="#" onClick={() => { props.eliminarProducto(props.producto._id) }}>Eliminar</a>
		</td>   
	</tr>
	);
}

export default class RegistroProductos extends Component {

	constructor(props) {

		super(props);
		this.state = {
			productos: [], 
			resData:'',
			isSubmitted:false
		};
	}

	componentDidMount() {

		axios.get('http://localhost:5000/productos/')
			.then(respuesta => {
				try {
					this.setState({
						productos: respuesta.data
					})
				}
				catch(error){
					alert(error)
				}
			})
			.catch(res => {
					if(res.code=="ERR_BAD_REQUEST") {
						this.setState({
							resData:res.response.data}
							);
					}

					else {
						this.setState({resData:res.message});
					}

				console.log(res);
			});
	}
  
	eliminarProducto = (id) => {
		try{
			axios.delete('http://localhost:5000/productos/'+id)
			  .then(response => { 
					this.setState({
						productos: this.state.productos.filter(productos => productos._id !== id),
						isSubmitted:true,
						resData:response.data			
					});
				  })
				  .catch(res => {
					this.setState({
						isSubmitted:true
					});

					if(res.code=="ERR_BAD_REQUEST") {
						this.setState({resData:res.response.data});
					}

					else {
						this.setState({resData:res.message});
					}

					console.log(res);
				});

		}
		
		catch(error){
			alert(error);
		}
	}

	listaProductos() {    
		try{
			return this.state.productos.map(productoActual => {
			  return <Producto producto={productoActual} eliminarProducto={this.eliminarProducto} key={productoActual._id}  />;
			})
		}
		catch(error){
			alert(error);
		}
	}
	
	render() {
		try{
			return(
				<div>
					<h3>Productos Registrados</h3>
					<table className="table">
					
						<thead>
							<tr>
								<th class="text-center">ID</th>
								<th class="text-center">Descripción</th>
								<th class="text-center">Unidad</th>
								<th class="text-center">Precio de compra</th>
								<th class="text-center">Precio de Venta</th>
								<th class="text-center">Stock Actual</th>
								<th class="text-center">Stock Original</th>
								<th class="text-center">Opciones</th>
							</tr>
						</thead>
						
						<tbody>
							{ this.listaProductos() }
						</tbody>
						
					</table>
					<center>
						<Label className="label-respuesta-api">
							{this.state.isSubmitted && this.state.resData}
						</Label>
					</center>
				</div>   
			  );
		}
		catch(error){
			alert(error);
		}
	}
}
