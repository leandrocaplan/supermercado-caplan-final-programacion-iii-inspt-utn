import React from "react";
import {
	Navbar,
	Nav,
	NavItem,
	NavLink,
} from "reactstrap";

import { Link } from "react-router-dom";

const MenuProductos  = () => {

	return (
	<div class="center-menu">
		<Navbar
			color="dark"      
			className="custom-navMenu"
			expand="md"
			container="md"
		>
			<Nav className="mc-auto" navbar>

				<NavItem className="custom-menu">
					<NavLink tag={Link} to={"/productos/nuevo"}>
						<p>Nuevo producto</p>
					</NavLink>
				</NavItem>         

				<>&nbsp;&nbsp;&nbsp;</>

				<NavItem className="custom-menu">
					<NavLink tag={Link} to={"/productos/registro"}>    
						<p>Registro de productos</p>
					</NavLink>
				</NavItem>

			</Nav>

		</Navbar>
	</div>
	);
};

export default MenuProductos;
