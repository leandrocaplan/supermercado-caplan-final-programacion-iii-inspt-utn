import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';

export default class NuevoProducto extends Component {
	constructor(props) {
		try{

			super(props);

			this.state = {
				_id:0,
				idExistentes:[],
				descripcion:'Descripcion',
				unidad:'u',
				precioCompra:1,
				precioVenta:1,
				stock:0,    
				isSubmitted:false
			}
		}
		catch(error){
			alert(error)
		}
	}	
	
	componentDidMount() {

		axios.get('http://localhost:5000/productos/generar/generarId')
		  .then(respuesta => {
			  try {
				   this.setState({
						_id: respuesta.data.idPred,
						idExistentes:respuesta.data.arrayIdExistentes
				   });
			   }
				catch(error){
					alert(error)
				}
			 })
			.catch((error) => {
			  alert(error);
			})           

	}
	
	onChangeId= (e) => {
		try{
			this.setState({
				_id: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error);
		}
	}
	
	onChangeDescripcion = (e) => {
		try{
			this.setState({
				descripcion: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error);
		}
	}
	
	onChangeUnidad = (e) => {
	try{
		if(e.target.value == 'u'){
			 this.setState({
					stock: Math.ceil(this.state.stock)
				})
			}
		this.setState({
			unidad: e.target.value,
			isSubmitted:false
		})
	}
	
	catch(error){
			alert(error);
		}
	}
	
	onChangePrecioCompra = (e) => {
		try{
			this.setState({
				precioCompra: e.target.value,
				isSubmitted:false
			})
		}
		
		catch(error) {
			alert(error);
		}
	}
	onChangePrecioVenta = (e) => {	
		try{

			this.setState({
				precioVenta: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
				alert(error);
		}
	}
	onChangeStock = (e) => {
		try{
			this.setState({
				stock: e.target.value,
				isSubmitted:false
			})
		}
		
		catch(error){
			alert(error);
		}
	}
	
	onSubmit = (e) => {
		try{

			e.preventDefault();

			
			let asegurarPrecios=true;
			let noExiste=true;
			
			if(Number(this.state.precioCompra) > Number(this.state.precioVenta)) {
				asegurarPrecios=window.confirm("El precio de compra es mayor al precio de venta\nEsto no es sensato desde el punto de vista económico\n¿Está seguro de que desea proseguir?");
			}
				
			if(this.state.idExistentes.some(id => id==this.state._id)){
				alert("Ya existe un elemento con ese ID");
				noExiste=false;
			}
			
			if(asegurarPrecios && noExiste){
				
				const producto = {
					_id: String(this.state._id),
					descripcion: this.state.descripcion,
					unidad: this.state.unidad,
					precioCompra: this.state.precioCompra,
					precioVenta: this.state.precioVenta,
					stock: this.state.stock
				}
				
				axios.post('http://localhost:5000/productos/agregar', producto)
					.then(res => {
						this.setState({
							isSubmitted: true,
							resData:res.data,
							idExistentes:this.state.idExistentes.concat(this.state._id),
							_id:Number(this.state._id)+1,
							descripcion:'Descripcion',
							unidad:'u',
							precioCompra:1,
							precioVenta:1,
							stock:0
						});

					})
					.catch(res => {

						this.setState({isSubmitted:true});

						if(res.code=="ERR_BAD_REQUEST") {
							this.setState({resData:res.response.data});
						}
						else {
							this.setState({resData:res.message});			
						}
					});
				}

		}
			
		catch(error){
			alert(error);
		}
	}
	render() {
		try{

			return (
				<center>
					<div class="center-nuevo-prod-usu">

						<h3>Nuevo Producto</h3>
						<br/>

						<Form onSubmit={this.onSubmit} className="form-nuevo-prod-usu">


							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										ID: 
									</Label>
								</Col>

								<Col sm="0">
									<Input  type="number"
										required
										
										min="1"
										step="1"
										className="inp-nuevo-prod-usu"
										value={this.state._id}
										onChange={this.onChangeId}
									/>
								</Col>     
							</Row>

							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Descripción: 
									</Label>
								</Col>

								<Col sm="0">
									<Input  type="text"
									required
									className="inp-nuevo-prod-usu"
									value={this.state.descripcion}
									onChange={this.onChangeDescripcion}
									/>
								</Col>     
							</Row>
							
							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Unidad: 
									</Label>
								</Col>

								<Col sm="0">
									<Input  
										type="select"
										required
										className="inp-nuevo-prod-usu"
										value={this.state.unidad}
										onChange={this.onChangeUnidad}
										>
											<option value="u">u</option>
											<option value="kg">kg</option>
									</Input>                
								</Col>     
							</Row>


							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Precio de compra: 
									</Label>
								</Col>

								<Col sm="0">
									<Input 
									type="number"
									min="0.01"
									step="0.01"
									className="inp-nuevo-prod-usu"
									value={this.state.precioCompra}
									onChange={this.onChangePrecioCompra}
									/>
								</Col>               
							</Row>

							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Precio de venta:
									</Label> 
								</Col>

								<Col sm="0">
									<Input 
										type="number"
										min="0.01"
										step="0.01"
										className="inp-nuevo-prod-usu"
										value={this.state.precioVenta}
										onChange={this.onChangePrecioVenta}
									/>  
								</Col>              
							</Row>

							<Row className="fila-nuevo-prod-usu">
								<Col sm="0">
									<Label className="label-nuevo-prod-usu">
										Stock inicial:
									</Label>
								</Col>
								
								
								{
									this.state.unidad=='u'
									?
									<Col sm="0">
										<Input 
											type="number"
											min="0"
											step="1"
											className="inp-nuevo-prod-usu"
											value={this.state.stock}
											onChange={this.onChangeStock}
										/>  
									</Col>
									:
									<Col sm="0">
										<Input 
											type="number"
											min="0"
											step="0.01"
											className="inp-nuevo-prod-usu"
											value={this.state.stock}
											onChange={this.onChangeStock}
										/>  
									</Col>
								}
							</Row>


							<br/>

							<Col sm="0">
								<Button  type="submit" className="boton">Agregar Producto</Button>
							</Col>
						

						</Form>
						<Label className="label-respuesta-api">
							{this.state.isSubmitted && this.state.resData}
						</Label>
					</div>
				</center>

			)
			
		}
		catch(error){
			alert(error)
		}
	}
}

