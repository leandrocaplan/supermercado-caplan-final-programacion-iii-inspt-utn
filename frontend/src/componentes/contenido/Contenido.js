import React from "react";
import classNames from "classnames";
import { Container } from "reactstrap";
import { Switch, Route } from "react-router-dom";
import axios from 'axios';

//Aquí hago la importación de los componentes
//de acuerdo a su enrutamiento local

//Componentes del módulo de ventas
import RegistroVentas from "./ventas/registro-ventas.componente";
import ListaVentas from "./ventas/lista-ventas.componente";
import EditarVenta from "./ventas/editar-venta.componente";
import VenderProducto from "./ventas/realizar-venta.componente";
import MenuVentas from "./ventas/menu-ventas.componente";
import ReasignarDetalleVenta from"./ventas/reasignar-detalle.componente"
import EditarEncabezadoVenta from"./ventas/editar-encabezado.componente"


//Componentes del módulo de compras
import RegistroCompras from "./compras/registro-compras.componente";
import ListaCompras from "./compras/lista-compras.componente";
import EditarCompra from "./compras/editar-compra.componente";
import ComprarProducto from "./compras/realizar-compra.componente";
import MenuCompras from "./compras/menu-compras.componente";
import ReasignarDetalleCompra from"./compras/reasignar-detalle.componente"
import EditarEncabezadoCompra from"./compras/editar-encabezado.componente"


//Componentes del módulo de productos
import RegistroProductos from "./productos/registro-productos.componente";
import EditarProducto from "./productos/editar-producto.componente";
import NuevoProducto from "./productos/nuevo-producto.componente";
import MenuProductos from "./productos/menu-productos.componente";

//Componentes del módulo de usuarios
import RegistroUsuarios from "./usuarios/registro-usuarios.componente";
import EditarUsuario from "./usuarios/editar-usuario.componente";
import NuevoUsuario from "./usuarios/nuevo-usuario.componente";
import MenuUsuarios from "./usuarios/menu-usuarios.componente";

//Componentes del módulo de cuentas
import ConsultarCuentas from "./cuentas/consultar-cuentas.componente";
import IngresarRetirar from "./cuentas/ingresar-retirar-cuentas.componente";
import MenuCuentas from "./cuentas/menu-cuentas.componente";


import Logout from "./logout.componente";
import Resetear from "./resetear.componente";

import Topbar from "./Topbar";

let usuario = JSON.parse(sessionStorage.getItem('user')) || {"username":"pepe"};
console.log(usuario);

const Contenido = ({ sidebarIsOpen, toggleSidebar }) => (

	<Container
		fluid
		className={classNames("content", { "is-open": sidebarIsOpen })}
	>
		<Topbar toggleSidebar={toggleSidebar} />
		<br></br>

		<Switch>
			<Route path="/" exact component={
				() => 
					<p>
					¡¡Bienvenido/a 
						{
						' '+  JSON.parse(sessionStorage.getItem('user')).nombre + ' '
							+ JSON.parse(sessionStorage.getItem('user')).apellido
						}
					!!
					</p>
				}
			/>

			//Aquí, hago el enrutamiento de cada uno de los componentes, asignandole
			//la URL correspondiente a cada uno de ellos tomando como
			//base la URL raíz del sistema.

			<Route path="/ventas/" exact component={MenuVentas} />
			<Route path="/ventas/lista/" exact component={ListaVentas} />
			<Route path="/ventas/registro/"  exact component={RegistroVentas} />
			<Route path="/ventas/registro/:fecha/:codigo/:vendedor/:id"  exact component={RegistroVentas} />
			<Route path="/ventas/editar/:idDetalle/:idVenta" exact component={EditarVenta} />
			<Route path="/ventas/reasignar/:idDetalle/:idVenta" exact component={ReasignarDetalleVenta} />
			<Route path="/ventas/nuevo" exact component={VenderProducto} />
			<Route path="/ventas/nuevo/:idVenta" exact component={VenderProducto} />
			<Route path="/ventas/editarEnc/:idVenta" exact component={EditarEncabezadoVenta} />

			<Route path="/compras/"  exact component={MenuCompras} />
			<Route path="/compras/lista/"  exact component={ListaCompras} />
			<Route path="/compras/registro/"  exact component={RegistroCompras} />
			<Route path="/compras/registro/:fecha/:codigo/:comprador/:id"  exact component={RegistroCompras} />
			<Route path="/compras/editar/:idDetalle/:idCompra" exact component={EditarCompra} />
			<Route path="/compras/reasignar/:idDetalle/:idCompra" exact component={ReasignarDetalleCompra} />
			<Route path="/compras/nuevo" exact component={ComprarProducto} />
			<Route path="/compras/nuevo/:idCompra" exact component={ComprarProducto} />
			<Route path="/compras/editarEnc/:idCompra" exact component={EditarEncabezadoCompra} />

			<Route path="/usuarios/"  exact component={MenuUsuarios} />
			<Route path="/usuarios/registro" exact component={RegistroUsuarios} />
			<Route path="/usuarios/editar/:id" exact component={EditarUsuario} />
			<Route path="/usuarios/nuevo" exact component={NuevoUsuario} />

			<Route path="/productos/"  exact component={MenuProductos} />
			<Route path="/productos/registro" exact component={RegistroProductos} />
			<Route path="/productos/editar/:id" exact component={EditarProducto} />
			<Route path="/productos/nuevo" exact component={NuevoProducto} />

			<Route path="/cuentas/" exact component={MenuCuentas} />
			<Route path="/cuentas/consultar" exact component={ConsultarCuentas} />
			<Route path="/cuentas/ingresarRetirar" exact component={IngresarRetirar} />

			<Route path="/resetear" exact component={Resetear} />

			<Route path="/logout" exact component={Logout} />
		</Switch>
	</Container>
);

export default Contenido;
