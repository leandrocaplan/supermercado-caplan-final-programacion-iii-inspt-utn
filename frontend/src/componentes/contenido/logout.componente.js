import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

export default class Logout extends Component {
 
	constructor(props) {
		super(props);
	}


	async redirigir() {
		sessionStorage.clear();
		window.location = await "/";
	}
	render() {
		return (
			<>
				{this.redirigir()} 
			</>
		)
	}
}
