import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';
import DatePicker from "react-datepicker";


//Este flag le permite al programador ver en el
//navegador, toda la información util necesario
//para la depuración, mostrada en el renderizado del componente

//Cuando un usuario común debe utilizar la aplicación,
//está por defecto desactivada.

//Cuando un programador está trabajando con el código,
//ya sea el que lo desarrolló inicialmente o bien
//algun otro, simplemente debe establecer 
//este flag en true, y podrá ver en el navegador toda
//la información necesaria para agilizar el proceso de debug
const modoDebug=false;

//Esta funcion nos resulta util para debugear,
//controlando el stock de cada producto
//y su stock original del lado del frontend
//Solo la utilizo cuando tenga activado el modo debug
const Producto = function(props) { 

return(
	<tr>
		<td>{Number(props.producto._id)} &nbsp;</td>
		<td>{props.producto.descripcion}&nbsp;</td>
		<td>{props.producto.stock}</td>
		<td>{props.producto.stockOriginal}</td>
	</tr>
  );
}


//Me formatea los datos de una fecha, recibiendo un string
//con la fecha correspondiente en formato ISO 8601, o bien un objeto Date,
//devolviendo un string con la fecha en formato "dd/mm/aaaa"

//A la vez, descartamos el horario asociado a la fecha recibida,
//lo que nos puede ser util tambien para comparar dos fechas sin tomar
//en cuenta el horario

const formatearFecha =function (fecha) {
    try {
        const fechaObj=new Date(fecha);
        return (
			fechaObj.getDate().toString().padStart(2, "0") + '/'+
			(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+
			fechaObj.getFullYear()
		);
    }
    catch(error) {
        alert(error)
    }
}


//Recibo una fecha y hora en formato ISO y devuelvo la
//hora como un string de la forma "hh:mm:ss"
const formatearHora =function (fecha) {
    try {
        const fechaObj=new Date(fecha);
        return (
			fechaObj.getHours().toString().padStart(2, "0") + ':'+
			(fechaObj.getMinutes()).toString().padStart(2, "0") + ':'+
			fechaObj.getSeconds().toString().padStart(2, "0")
		);
    }
    catch(error) {
        alert(error)
    }
}
//Realiza una operación complementaria a la funcion anterior, recibiendo un
//string de la forma "dd/mm/aaaa", y devolviendo un objeto tipo
//Date con el valor de la fecha correspondiente.

//Nos sirve para operar sobre la fecha como un objeto Date,
//y para descartar el horario asignado a ella, ya que
//de otra forma, el mismo nos traería errores a la hora de hacer el
//ordenamiento,donde no queremos que el horario sea un criterio para el mismo.

const desformatearFecha =function (fecha) {
    try {
        const partes = fecha.split("/");
        return new Date(
		   parseInt(partes[2], 10),
		   parseInt(partes[1], 10) - 1,
		   parseInt(partes[0], 10)
	   );
    }
    catch(error) {
        alert(error)
    }
}


//Este componente es el de mas bajo orden utilizado en este archivo
//Emite, uno por uno, los detalles de cada registro del detalle, pasado por parámetro
//Ademas, recibe por parámetro la funcion 'eliminarDetalle', la cual va asociada a cada elemento del
//detalle emitido, y permite eliminarla tanto en el frontend como en la base de datos, luego de realizar
//la validación correspondiente a la eliminación
const RegistroDetalleFormateado = function(props) { 

return(
  <tr>
    <td class="text-center">{props.compra.fecha}</td>
    <td class="text-center">{props.compra.hora}</td>
    <td class="text-center">{props.compra.compradorNomAp}</td>
    <td class="text-center">{props.compra.codCompra}</td>
    <td class="text-center">{props.compra.proveedor}</td>
    <td class="text-center">{props.compra.productoDesc}</td>
    <td class="text-center">{props.compra.precio}</td>
    <td class="text-center">{props.compra.cantidad}</td>
    <td class="text-center">{props.compra.unidadActual}</td>
    <td class="text-center">{props.compra.formaPagoFormat}</td>
    <td class="text-center">$ {props.compra.subtotal.toFixed(2)}</td>
    <td>
		<Link to={"/compras/editar/"+props.compra._id + "/" +props.compra.idCompraAsociada}>Editar</Link> | &nbsp;
		<Link to={"/compras/reasignar/"+props.compra._id + "/" +props.compra.idCompraAsociada}>Reasignar</Link> | &nbsp;
		<a href="#" onClick={() => { props.eliminarDetalle(props.compra._id, props.compra.idCompraAsociada) }}>Eliminar </a>
    </td>   
  </tr>
  );
}

//Este componente es de orden intermedio
//Filtra los registros del detalle de cada compra,
//de acuerdo a los parámetros recibidos, y los emite formateados
const ListaDevuelta = function(props) {

    const codigoFiltrado=Number(props.parametro.codigoFiltrado);
    const productoFiltrado=Number(props.parametro.productoFiltrado);
    const fechaFiltrada=props.parametro.fechaFiltrada;
    const todasLasFechas=props.parametro.todasLasFechas;
    const compradorFiltrado=props.parametro.compradorFiltrado;
    const formaPagoFiltrado=props.parametro.formaPagoFiltrado;
	
	console.log(productoFiltrado);
	

    const detallesFormateados
    = 
    props.parametro.detallesCompras
		//Filtro los elementos del detalle devueltos de acuerdo a los parámetros recibidos
		.filter(
			function(elemDetalle) {
				
				const productosExistentes=props.parametro.productos.map(p => p._id);
				const compradoresExistentes=props.parametro.compradores.map(v => v.username);
				console.log(productosExistentes);
				console.log(productoFiltrado);
				
				//Condiciones para el filtrado de detalles de compra
				if(				
					//Si el usuario logueado es un administrador, puede ver las compras de todos
					//los compradores, pudiendo filtrarlos a partir del parámetro que le peao
					//a ListaDevuelta (string vacío para ver todos)

					(
						(compradorFiltrado=="" || elemDetalle.comprador==compradorFiltrado) ||
						(compradorFiltrado=="-1" && (!compradoresExistentes.some(elem => elem==elemDetalle.comprador)))	
					)
					
					&&


					//Si el código de compra pasado a ListaDevuelta es 0, devuelvo todos
					//los detalles de compra sin importar su código.
					//Si es diferente a 0, filtro los detalles por su código asociado
					(codigoFiltrado==0|| elemDetalle.codCompra==codigoFiltrado) &&

					//Si el producto pasado a ListaDevuelta es un string vacío,
					//muestro todos los detalles de compra sin importar su prodcto asociado
					//Si no es un string vacío, devuelvo los detalles asociados al producto
					//pasado por parámetro a ListaDevuelta.
					//Si el parametro recibido es -1, muestro solo los productos dados de baja
					//en la base de datos
					(
						(productoFiltrado=="" || elemDetalle.producto==productoFiltrado) ||
						(productoFiltrado=="-1" && (!productosExistentes.some(elem => elem==elemDetalle.producto)))		
					)
					
					&&


					//Si el parámetro todasLasFechas pasado a ListaDevuelta vale true (verdadero),
					//muestro todos los detalles de la compra sin importar su fecha.
					//Si no, muestro todos los detalles registrados en la fecha pasada por parámetro
					(todasLasFechas || formatearFecha(elemDetalle.fechaHora) == formatearFecha(fechaFiltrada) ) &&



					//Si el tipo de pado pasado a ListaDevuelta es un string vacío,
					//muestro todos los detalles de compra sin importar su tipo de pago.
					//Si no, muestro todos los detalles de compra realizados con el tipo
					//de pago pasado por parámetro.
					(formaPagoFiltrado=="" || elemDetalle.formaPago==formaPagoFiltrado)
					
				)
				{
					return elemDetalle;
				}
			}
		)
		//Asocio cada elemento del detalle con su correspondiente nombre y apellido del comprador
		//y la descripción del producto correspondiente
		
		//Si no encuentra alguno de los dos, porque estan dados de baja en la base de datos,
		//lo indica mostrando el correspondiente nombre de usuario,en caso del comprador
		//o ID en caso del producto
		.map (
			function(elemDetalle) {

				let detalleFormateado=elemDetalle;
				const producto=props.parametro.productos.find(v => v._id==elemDetalle.producto);
				const comprador=props.parametro.compradores.find(u => u.username==elemDetalle.comprador);

				if(comprador) {
					detalleFormateado.compradorNomAp = comprador.nombre + ' ' + comprador.apellido;
				}
				else {
					detalleFormateado.compradorNomAp='(Usuario dado de baja: '+ elemDetalle.comprador + ')';
				}

				if(producto)  {

					detalleFormateado.productoDesc= producto.descripcion;				
				}
				else {

					detalleFormateado.productoDesc='(Producto dado de baja: ' + Number(elemDetalle.producto) + ')';
				}
				
				detalleFormateado.precio="$ "+ Number(elemDetalle.precioActual).toFixed(2);


				if(elemDetalle.formaPago=='caja') detalleFormateado.formaPagoFormat = 'Caja';
				if(elemDetalle.formaPago=='creditoAFavor') detalleFormateado.formaPagoFormat = 'Crédito A Favor';
				if(elemDetalle.formaPago=='cuentaCorriente') detalleFormateado.formaPagoFormat = 'Cuenta Corriente';

				//detalleFormateado.fecha=new Date(elemDetalle.createdAt).getDate() + '/'+ (new Date(elemDetalle.createdAt).getMonth()+1) + '/'+ new Date(elemDetalle.createdAt).getFullYear();
				detalleFormateado.fecha=formatearFecha(elemDetalle.fechaHora);
				detalleFormateado.hora=formatearHora(elemDetalle.fechaHora);
				//formatearFecha(elemDetalle.fechaHora)
				return detalleFormateado;
			}
		)
		
		//Ordeno los elementos devueltos de acuerdo a su fecha, comprador, y código de compra, en ese orden
		.sort(
			function(a, b) {

				return desformatearFecha(a.fecha) - desformatearFecha(b.fecha) || a.compradorNomAp.localeCompare(b.compradorNomAp) || a.codCompra - b.codCompra ;
			}

    );


	//Obtengo el total del importe de compra de los elementos filtrados
    const obtenerTotal= function() {
        return detallesFormateados.reduce((total, detalleAct) => total + detalleAct.subtotal, 0).toFixed(2);

    };
	
	
	//Al componente de orden inferior 'RegistroDetalleFormateado' le paso, uno por uno, los elementos
	//filtrados y formateados, como tambien le paso por parámetro la función 'eliminarDetalle'
    const listaDetalles= function() {
        return detallesFormateados.map(compraActual => {
            return <RegistroDetalleFormateado compra= {compraActual} eliminarDetalle= {props.eliminarDetalle}  />;
        })

    }
	
	//Hago el renderizado correspondiente, donde incluyo:
	
	//En la parte superior,el botón "Eliminar todos", que ejecuta la función 'eliminarDetalles',
	//la cual  luego de hacer la validación correspondiente, me elimina
	//tanto en el frontend como en la base de datos, todos los elementos filtrados previamente
    
    //Luego, renderizo en una tabla, los encabezados correspondientes a cada campo del elemento del detalle
    
    //Posteriormente, en la misma tabla, muestro, uno por uno, los datos de cada elemento de detalle,
    //devueltos mas arriba con la función 'listaDetalles'
    
    //Finalmente, abajo de todo, muestro el total de los importes de compra
    //que obtuve mas arriba con la función 'obtenerTotal'
    return(
			<div>

				<h3>Detalles de compras registrados</h3>
				<br/>
				<h6>Nota: Los precios mostrados en la lista corresponden a los registrados en el momento de la compra</h6>
				<h6>Es posible que los mismos hayan sido actualizados posteriormente</h6>
				<br/>
				<table className="table">
					<thead>
						<tr>
							<th class="text-center">Fecha</th>
							<th class="text-center">Hora</th>
							<th class="text-center">Comprador</th>
							<th class="text-center">Código</th>
							<th class="text-center">Proveedor</th>
							<th class="text-center">Producto</th>
							<th class="text-center">Precio unitario</th>
							<th class="text-center">Cantidad</th>
							<th class="text-center">Unidad</th>
							<th class="text-center">Tipo de Pago</th>
							<th class="text-center">Subtotal</th>
							<th class="text-center">Opciones</th>
						</tr>
					</thead>

					<tbody>
						{listaDetalles()}
					</tbody>
				</table>
				

				<p class="center"> Total de los importes de los elementos filtrados: $ {obtenerTotal()}</p>
				
				<center>
					<Button className="boton"
						onClick={() => { props.eliminarDetalles(detallesFormateados) }}>
						Eliminar todos
					</Button>
				</center>	

			</div>
          );
}
//Este es nuestro componente padre

//Nos trae desde la API, la lista completa de todos los elementos
//del detalle de todas las compras existentes en la base de datos
//Tambien, recibe la lista de todos los compraddores (administradores)
//y productos existentes

//Luego, me permite elegir los campos correspondientes, tanto
//para mostrar como para poder editar o eliminar cada uno de
//dichos elementos, de acuerdo al criterio que yo elija

//Puede recibir tres parámetros, correspondientes a la fecha,
//usuario comprador, y código de compra, de manera
//que nos resulte mas práctico emitir la información
//de una compra en particular de acuerdo a estos tres
//campos que son únicos para cada compra, ya sea para
//consultar, editar, o eliminar.
export default class RegistroCompras extends Component {


	constructor(props) {

		super(props);
		this.state = {
			detallesCompras: [], 
			compradores: [],
			productos: [], 
			detallesFormateados:[],
			comprador:{},
			cuentas:{
				caja:0,
				cuentaCorriente:0,
				creditoAFavor:0
			},
			codigoFiltrado:0,
			fechaFiltrada: new Date(),
			todasLasFechas: true,
			productoFiltrado: "0",
			compradorFiltrado: '',
			formaPagoFiltrado: '',

			userVendedor:"",
			codigo: 0,
			resData:'',
			actStock:true,
			actCuentas:true                 

		};
		//La funcion bind nos permite operar con el objeto this en 
		//las funciones eliminarDetalles y validarEliminacion,
		//en los componentes hijos, a los cuales se las
		//paso por parámetro
		this.eliminarDetalles = this.eliminarDetalles.bind(this);
		this.validarEliminacion = this.validarEliminacion.bind(this);

	}

	//En el componentDidMount, me traigo los datos desde la API,
	//e inicializo los campos a filrar con los valores del encabezado
	//de una determinada compra si es que los recibo
	componentDidMount() {
	
		//Muestro los parámetros recibidos por URL
		console.log( decodeURIComponent(this.props.match.params.fecha));
		console.log(this.props.match.params.codigo);
		console.log(this.props.match.params.comprador);
		
		//Hago el llamado a la API
		axios.get('http://localhost:5000/compras/asociarAplanar/' + (this.props.match.params.id || ''))
			.then(respuesta=> {
				try{
					this.setState({
						//Me guardo en el estado todos los datos que me
						//devuelve la API
						detallesCompras: respuesta.data.detallesCompras,
						compradores: respuesta.data.usuarios,                              
						productos: respuesta.data.productos,
						cuentas: respuesta.data.cuentas,
					})
				}
				catch(error){
					alert(error)
				}
			})
			.catch((error) => {
				alert(error);
			});
		
		//Seteo los campos de fecha, codigo de compra, y comprador filtrados,
		//de acuerdo a si recibí dichos campos por parámetro o no
		//(si no los recibí, los dejo vacios o en 0)
		
		if(this.props.match.params.fecha){
			try{
				this.setState({
					todasLasFechas: false,          
					fechaFiltrada: desformatearFecha(decodeURIComponent(this.props.match.params.fecha))
				})
			}
			catch(error){
				alert(error)
			}
		}
		
		if(this.props.match.params.codigo){
		
			this.setState({
				codigoFiltrado:this.props.match.params.codigo

			})               
		}
		
		if(this.props.match.params.comprador){
			this.setState({
				compradorFiltrado:this.props.match.params.comprador

			})               
		}
	}

	//Utilizo este método para debugear cuando se actualiza el estado
	componentDidUpdate(){
	//console.log(this.state.detallesCompras);
	//console.log(this.state.compradores);
		console.log(this.state.fechaFiltrada);

	}

	//Esta funcion me sirve para debugear, controlando el stock actual y original de
	//la lista de productos en el frontend, utilizando el componente 'Producto'
	//definido arriba de todo
	listaProductos() {
		try{
			return this.state.productos.map(productoActual => {
			  return <Producto producto={productoActual} eliminarProducto={this.eliminarProducto} key={productoActual._id}  />;
			})
		 }
		 
		catch(error){
			alert(error)
		}
	}
	//Esta funcion la utilizo para eliminar un solo elemento del detalle
	//Debo pasarla como parámetro a los componentes hijos, pero la defino
	//en el componente padre.
	//Recibe como parámetros los ID del elemento de detalle a eliminar
	//y el de la compra asociada a dicho elemento
	eliminarDetalle = (idDetalle,idCompra) => {
		try{
			
			//Me guardo primero, en una const, los datos completos
			//del elemento de detalle a eliminar
			const detalleEliminado=this.state.detallesCompras.find(v => v._id==idDetalle);
			
			//Me guardo en const los flags de actualizar
			//stock y actualizar cuentas
			
			const actStock=this.state.actStock;
			const actCuentas=this.state.actCuentas;
			
			//Me guardo los datos completos del producto a eliminar
			const producto=this.state.productos.find(p => p._id==detalleEliminado.producto);
			
			
			const validacionStock =(
				//O bien la opción de actualizar stock está desactivada
				(!actStock) ||
				//O bien el producto ya no existe en mi base de datos
				(!producto) ||
				//O bien al eliminar el elemento no me queda un resultante
				//de stock negativo en dicho producto
				
				(Number(producto.stock) - Number(detalleEliminado.cantidad) >= 0)
				
			);
			//Validación
			if(validacionStock)
			
			//Si la validación fue exitosa, entro al cuerpo del if
			{
				//Le mando a la API, con un método DELETE, los IDs del detalle y de la compra asociada,
				//como también los flags de actualizar stock y actualizar cuentas
				axios.delete('http://localhost:5000/compras/eliminarDetalle/' + idDetalle + '/' + idCompra + '/' + actStock + '/' + actCuentas)
				//Si no obtuvimos errores en la peticion a la API fue exitosa, entramos al then
				.then(response => {
					
					
					this.setState({
						//Actualizo la lista de detalles de compras, eliminando aquella
						//que eliminé en la API, utilizando un filter
						detallesCompras: this.state.detallesCompras.filter(
							function(elemento) {
								if(elemento._id != idDetalle) {
									return elemento;
								}
							}),
						
						//Me guardo la respuesta del metodo DELETE, para luego mostrarlo en el render
						resData: response.data
					})

					//Si la opcion de actualizar cuentas está activada, actualizo la cuenta
					//correspondiente al producto que eliminé
					if(actCuentas) {
						//Me guardo en una variable el estado anterior de las cuentas
						let cuentas= {...this.state.cuentas};
						
						//A la cuenta asociada dicho elemento,
						//le repongo el importe correspondiente
						//al elemento del detalle
						cuentas[detalleEliminado.formaPago] =
							Number(cuentas[detalleEliminado.formaPago]) +
							Number(detalleEliminado.subtotal)
						
						//Actualizo el estado con el nuevo valor de las cuentas	
						this.setState({
							cuentas:cuentas
						})
					}

					//Si la opcion de actualizar stock está activada, actualizo la lista
					//de productos con el stock del producto afectado actualizado
					if(actStock) {
						
						//Con un map, en la const 'productos' me guardo el listado de productos con
						//el producto asociado al detalle de compra que eliminé actualizado (deduciendo de su
						//stock la cantidad correspondiente de dicho elemento del detalle)
						const productos=this.state.productos.map(
							function (producto) {
								if(producto._id==detalleEliminado.producto) {
									producto.stock-=detalleEliminado.cantidad;
								}
								return producto;
							}
						);
						
						//Actualizo el estado con el array
						//de productos que obtuve en dicha variable
						this.setState({
							productos:productos
						})
					}


				})
				.catch(res => {

					if(res.code=="ERR_BAD_REQUEST") {
						this.setState({resData:res.response.data});
					}
					else {
						this.setState({resData:res.message});

					}
					console.log(res);
				});


			}
			//Si la validación falló, muestro un mensaje que lo indica
			else 
			{					
				//Formateo las cantidades de acuerdo a la unidad del producto asociado
				const stockFormateado = 
					producto.unidad=='u' ? 
					producto.stock.toFixed(0) :
					producto.stock.toFixed(2);
								
				const cantidadFormateada=
					producto.unidad=='u' ? 
					Number(detalleEliminado.cantidad).toFixed(0) :
					Number(detalleEliminado.cantidad).toFixed(2);
					
				
				const resultanteFormateado=
					producto.unidad=='u' ? 
					(Number(producto.stock) - Number(detalleEliminado.cantidad)).toFixed(0) :
					(Number(producto.stock) - Number(detalleEliminado.cantidad)).toFixed(2);
				
				alert(
						"Al eliminar este elemento del detalle, nos queda un\n" 
						+ "resultante de stock inválido:\n\n" 
						+ "\nEn el producto \"" + producto.descripcion + "\"," 
						+ "el stock actual que tengo es de " 
						+ stockFormateado + " " + producto.unidad + ",\n" 
						+ "y la cantidad que estoy intentando deducir es de " 
						+ cantidadFormateada + " " + producto.unidad + ".\n" 
						+ "Por lo tanto, me generaría un resultante de stock de "  
						+ resultanteFormateado + " " + producto.unidad +  ",\n" 
						+ "el cual es negativo.\n"
						+ "Intente convalidar el stock de este producto, o eliminar el elemento desactivando la opción de actualizar stock." 
				);
			}

		}
		catch(error) {
			alert(error);
		}
	}
	//Esta función la utilizo para validar la eliminación de
	//todo un conjunto de detalles.
	//Si hay un solo elemento de detalle que no cumple con la
	//validación, no elimino ninguno de los
	//elementos de detalle afectados
	validarEliminacion =  function(detallesAEliminar) {
		try{
			
			//Debo validar los stocks solamente, ya que las cuentas no tienen límite superior
			//a la hora de ser actualizadas de acuerdo a su eliminación
			console.log("Entro a validar detalle");	
			console.log(detallesAEliminar);
			let resultadoValidacionStock=true;
			let erroresValidacionStock=[];
			
			if(this.state.actStock) {
				//Hay que generar primero, un array con cada elemento del detalle asociado a cada producto
				//ya que el detalle puede llegar a tener varios elementos asociados
				//al mismo producto, con cantidades distintas
				//Necesito el total de cada cantidad asociada a cada producto
				
				const productoCantidad = {};
				for (let elemDetalle of detallesAEliminar) {
					productoCantidad[elemDetalle.producto] = Number(productoCantidad[elemDetalle.producto]) || 0;
					productoCantidad[elemDetalle.producto] += Number(elemDetalle.cantidad);
				}

				const arrayProductosCantidades = [];
				for (let producto in productoCantidad) {
					const cantidad = productoCantidad[producto];
					arrayProductosCantidades.push({
						producto,
						cantidad
					});
				}
			
				console.log(arrayProductosCantidades);
				
				//Una vez generado el array, valido que la cantidad de cada producto vendido
				//no me genere un stock resultante negativo en ningun producto
				for (const elemProdCant of arrayProductosCantidades) {

					const producto =  Number(elemProdCant.producto);
					const cantidad =  Number(elemProdCant.cantidad);
					//const cantidad=90;
					let productoActualizado= this.state.productos.find(p => p._id==producto);
					
					//Si el producto que estoy validando sigue existiendo en mi base de datos,
					//y en dicho dicho productos, me queda un resultante negativo
					// el flag de validación de stock valdrá false		
					
					if(productoActualizado && Number(productoActualizado.stock) - Number(cantidad) < 0) {
						resultadoValidacionStock=false;
		
							const stockFormateado = 
								productoActualizado.unidad=='u' ? 
								productoActualizado.stock.toFixed(0) :
								productoActualizado.stock.toFixed(2);
								
							const cantidadFormateada=
								productoActualizado.unidad=='u' ? 
								cantidad.toFixed(0) :
								cantidad.toFixed(2);
								
							const resultanteFormateado=
								productoActualizado.unidad=='u' ? 
								(Number(productoActualizado.stock) - Number(cantidad)).toFixed(0) :
								(Number(productoActualizado.stock) - Number(cantidad)).toFixed(2);
								
							erroresValidacionStock =	
								erroresValidacionStock.concat(
									"\nEn el producto \"" + productoActualizado.descripcion + "\", " +
									"el stock actual que tengo es de " + 
									stockFormateado + " " + productoActualizado.unidad + ",\n" +
									"la cantidad que estoy intentando deducir es de " + 
									cantidadFormateada + " " + productoActualizado.unidad + ",\n" +

									"Por lo tanto, me generaría un resultante de stock de " + 
									resultanteFormateado + " " + productoActualizado.unidad +  ",\n" +
									"el cual es negativo.\n"
								);
					}
				}
			}
			
			
			//Devolvemos el resultado de la validacion
			if(!resultadoValidacionStock){
				alert(
					"Al eliminar estos elementos del detalle,\ntengo errores de validación en los siguientes productos:\n\n" +
					erroresValidacionStock.map(e=>e) +
					"\n\nIntente convalidar el stock de dichos productos, o eliminar los elementos desactivando la opción de actualizar stock." 
				);
			}
			return (resultadoValidacionStock); 
			
		}
		catch(error) {
			alert(error)
		}
	}
    //Recibe como argumento la lista de detalles que quiero eliminar, la cual obtengo
    //desde el componente hijo 'ListaDevuelta', de acuerdo al conjunto
    //de elementos que me devolvió el filtrado, y los elimino, tanto
    //en la base de datos a traves de la API como en el frontend
	eliminarDetalles= function(detallesAEliminar) {
		try{

			//En dos const me guardo los valores de los flags
			//de actualizar stock y actualizar cuentas
			const actStock=this.state.actStock;
			const actCuentas=this.state.actCuentas;

			//Con la función que definí arriba, realizo la validación
			//de todos los elementos que quiero eliminar.
			//Si la validación fue exitosa, entro al cuerpo del if
			if(this.validarEliminacion(detallesAEliminar)) {
				
				//Me genero un objeto, que luego mando a la API, con los siguientes campos:
				const objetoDetalles= {
					//El campo 'listaDetalles', contendrá un array con todos los IDs de los
					//elementos de los detalles que quiero eliminar, y el ID
					//de la compra asociada al mismo
					listaDetalles:
						detallesAEliminar.map(
						function(detalle) {
							return {
								idDetalle:detalle._id,
								idCompra: detalle.idCompraAsociada
							}
						}),
						
						//En los campos 'actStock' y 'actCuentas', me guardo el valor de los flags
						//correspondientes a las opciones de actualizar stock y cuentas
						actStock: this.state.actStock,
						actCuentas: this.state.actCuentas
				};
				
				//Me genero ademas, un array con los IDs de los elementos de los detalles
				//a eliminar,para que luego me resulte mas cómodo realizar el filter correspondiente
				
				const arrayId =	detallesAEliminar.map(
					function(detalle) {
						return detalle._id;
					}
				);

				//Hago un POST a la API con la constante objetoDetalles que generé mas arriba
				axios.post('http://localhost:5000/compras/eliminarDetalles', objetoDetalles)
				//Si la eliminación fue exitosa, entro al then
					.then(response => {
						
						//Actualizo la lista de detalles de compras en el frontend, eliminando aquellos
						//que eliminé en la API, utilizando un filter, y el array
						//'arrayId' que generé mas arriba (puedo prescindir del mismo, pero lo dejamos
						//ya que nos resulta práctico)
						this.setState({
							detallesCompras: this.state.detallesCompras.filter(
								function(elemento) {
									if(!arrayId.some(
										function (idAEliminar) {
												return idAEliminar==elemento._id;
											}
											)
										)
										{
											return elemento;
										}
								}
							),
							resData: response.data
						})


						//Si la opcion de actualizar cuentas está activada, actualizo la cuenta
						//correspondiente al producto que eliminé
						if(actCuentas) {
							//Me guardo en una variable el estado anterior de las cuentas
							let cuentas= {...this.state.cuentas};
							
							//Recorro, elemento por elemento, los detalles que estoy
							//eliminando, y por cada uno de ellos actualizo la cuenta
							//contable asociada a cada uno, reponiendole el importe
							//correspondiente
							for(const detalleEliminado of detallesAEliminar) {
								cuentas[detalleEliminado.formaPago] = 
									Number(cuentas[detalleEliminado.formaPago]) +
									Number(detalleEliminado.subtotal)
							}
							//Luego, actualizo las cuentas en mi estado
							this.setState({
								cuentas:cuentas
							})
						}

						//Si la opcion de actualizar stock está activada, actualizo la lista
						//de productos, reponiendole el stock del producto que eliminé
						if(actStock) {
							//Me guardo en una variable el estado anterior de la lista de productos
							let productos=[...this.state.productos];
							
							//Recorro con un for la lista de detalles que estoy eliminando
							for(const detalleEliminado of detallesAEliminar) {
								//Por cada uno de los elementos sobre los que itero,
								//con un map, en la const 'productos' me guardo el listado de productos con
								//el producto asociado al detalle de compra que eliminé actualizado (deduciendo de su
								//stock la cantidad correspondiente de dicho elemento del detalle)
								productos=productos.map(
									function(productoActualizado) {
										if(productoActualizado._id==detalleEliminado.producto) {
											productoActualizado.stock-=detalleEliminado.cantidad;
										}

									return productoActualizado;
								});
							}
							//Actualizo el estado con el array
							//de productos que obtuve en dicha variable
							this.setState({
								productos:productos
							})

						}
					})
						
					.catch(res => {

						if(res.code=="ERR_BAD_REQUEST") {
							this.setState({resData:res.response.data});
						}
						else {
							this.setState({resData:res.message});

						}
						console.log(res);
					});
			}
		
		}
		catch(error){
			alert(error)
		}
	}



/*
----------------------------------------------------
    Sección de 'onChange'

  Al hacer cualquier modificación en algun campo del formulario,
  la variable 'isSubmitted' la reseteamos en 'false' 
----------------------------------------------------

*/


	onChangeTodasLasFechas = (e) => {

		try {
			this.setState({
				todasLasFechas: e.target.checked,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	onChangeFechaFiltrada = (e) => {
		try {
			this.setState({
				fechaFiltrada: e,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}

	//Al modificar el codigo de compra en el form, actualizo el estado del codigo de compra

	onChangeCodigoFiltrado = (e) => {
		try {
			this.setState({
				codigoFiltrado: Number(e.target.value),
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	//Al modificar el producto en el form, actualizo el id del producto
	//y el producto completo asociado a ese id
	onChangeProductoFiltrado = (e) => {
		try {
			this.setState({
				productoFiltrado: e.target.value,
				producto: this.state.productos.find(p => p._id===e.target.value),
				isSubmitted:false
			})

		}
		catch(error) {
			alert(error)
		}
	}
	//Al modificar el comprador en el form, actualizo el username del comprador
	//y el comprador completo asociado a ese username
	onChangeVendedorFiltrado = (e) => {
		try {
			this.setState({
				compradorFiltrado: e.target.value,
				//comprador: this.state.compradores.find(p => p.username===e.target.value),
				isSubmitted:false
			})
			//  //console.log(this.state.userVendedor);
		}
		catch(error) {
			alert(error)
		}
	}



	//Al modificar el tipo de pago en el form, actualizo el estado del tipo de pago

	onChangeTipoPagoFiltrado = (e) => {
		try {
			this.setState({
				formaPagoFiltrado: e.target.value,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	onChangeActStock = (e) => {
		try {

			this.setState({
				actStock: e.target.checked,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}


	onChangeActCuenta = (e) => {
		try{
			this.setState({
				actCuentas: e.target.checked,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}


	//Una vez que definí las funciones correspondientes, hago el render.
	//En el render hago:

	//-Un form donde asigno los valores con los que quiero filtrar los elementos del detalle
	// (dejando un campo vacío o en 0 para obtener todos)

	//-La llamada al componente hijo 'ListaDevuelta', a la que le paso como parámetro tanto el estado
	// del componente padre como las funciones 'eliminarDetalle' y 'eliminarDetalles'

	//-Muestro los valores actuales de cada cuenta contable

	//Muestro ademas, la lista actual de productos con su stock correspondiente, a modo de debug
		
	render() {
		try{
			return(
				<div> 
					<Form>

						<FormGroup>
						<Row className="filtro-venta">					
								
							<Col sm="0">
								<Label className="label-filtro-todas-las-fechas">
									<h7>Ver todas las fechas:</h7>
								</Label>

							</Col>
							
							<Col sm="0">
								<Input
									type="checkbox"
									className="checkbox-registro"
									checked={this.state.todasLasFechas}
									onChange={this.onChangeTodasLasFechas}
								/>
							</Col>
							
							{!this.state.todasLasFechas &&
								<>					
									<Col sm="0">
										<Label className="label-filtro-fecha">
											<h7>Fecha:</h7>
										</Label>

									</Col>


									<Col sm="0">
											<DatePicker
											className="inp-filtro-fecha"
												selected={this.state.fechaFiltrada}
												onChange={this.onChangeFechaFiltrada}
												dateFormat="dd/MM/yyyy"

											/>
									</Col>
								</>
							}
									
							<Col sm="0">
								<Label className="label-filtro-cod-venta">
									<h7>Codigo de compra (0 para ver todos):</h7>
								</Label>

							</Col>

							<Col sm="0">
								<Input 
								type="number"
								step="1" 
								className="inp-filtro-cod-venta"
								value={this.state.codigoFiltrado}
								onChange={this.onChangeCodigoFiltrado}
								/>
							</Col>
							
							<Col sm="0">
								<Label className="label-filtro-vendedor">
									<h7>Vendedor: </h7>
								</Label>
							</Col>

							<Col sm="0">
								<Input type="select"
								className="inp-filtro-vendedor"
								value={this.state.compradorFiltrado}
								
								onChange={this.onChangeVendedorFiltrado}>
								
									{	
										[
											<option key='' value='' selected=''>
												Todos
											</option>
										]
										.concat(
											<option key="-1" value="-1">
												Dados de baja
											</option>
										)
										.concat(
											this.state.compradores.map(function(comprador) {
												return <option key={comprador.username} value={comprador.username}>
															{comprador.nombre} {comprador.apellido}
														</option>;
											}
										)
									)
								}
									
								</Input>
							</Col>

						</Row>
						
						<Row className="fila-filtro-producto-pago">
								<Col sm="0">
									<Label className="label-filtro-producto">
										<h7>Producto:</h7>
									</Label>
								</Col>

								<Col sm="0">
									<Input type="select"

									className="inp-filtro-producto"
									value={this.state.productoFiltrado}
									onChange={this.onChangeProductoFiltrado}>
										
										{	
											[
												<option key="0" value="0">
													Todos
												</option>
											]
											.concat(
												<option key="-1" value="-1">
													Dados de baja
												</option>
											)
											.concat(
												this.state.productos.map(function(producto) {
													return <option key={producto._id} value={producto._id}>
																{producto.descripcion}
															</option>;
												}
											)
										)
									}
									</Input>
								</Col>



								<Col sm="0">
									<Label className="label-filtro-tipo-pago">
										<h7>Tipo de pago: </h7>
									</Label>
								</Col>

								<Col sm="0">
									<Input type="select" 
									className="inp-filtro-tipo-pago"
									value={this.state.formaPagoFiltrado}
									onChange={this.onChangeTipoPagoFiltrado}
									>
										<option selected = '' value='' >Todos</option>
										<option value="caja">Caja</option>
										<option value="cuentaCorriente">Cuenta corriente</option>
										<option value="creditoAFavor">Credito A Favor</option>
									</Input>
								</Col>



							</Row>
							
							<br/>
							<Row className="fila-mensaje-opciones">
								<Col sm="0">
									<Label className="label-actualizar">
										<h6>Al eliminar uno o varios elementos de detalle</h6>
										<h6>tengo la opcion de actualizar el stock del producto</h6>
										<h6>y/o la cuenta contable asociada a los mismos:</h6>
									</Label>
								</Col>
							</Row>
							
							<Row className="fila-detalles-actualizar">
								<Col sm="0">
									<Label className="label-detalles-act-stock">
										<h7>Actualizar stock:</h7>
									</Label>
								</Col>

								<Col sm="0">
									<Input type="checkbox"
										className="checkbox-registro"
										checked={this.state.actStock}
										onChange={this.onChangeActStock}
									/>
								
								</Col>


								<Col sm="0">
									<Label className="label-detalles-act-cuentas">
										<h7>Actualizar cuentas: </h7>
									</Label>
								</Col>

								<Col sm="0">
									<Input type="checkbox"
										className="checkbox-registro"
										checked={this.state.actCuentas}
										onChange={this.onChangeActCuenta}
									/>
								</Col>

							</Row>
							
						</FormGroup>

					</Form>
					<br/>

					
					<ListaDevuelta parametro={this.state} eliminarDetalle={this.eliminarDetalle} eliminarDetalles={this.eliminarDetalles}/>

					
					<center>
						<Label className="label-respuesta-api">
							{this.state.resData}
						</Label>
					</center>
					
					{modoDebug &&						
					<div>		
					<Row className="fila-resultantes-cuentas">

						<Col sm="0">
							<Label className="label-resultantes-cuentas">
								Caja:
							</Label>
						</Col>


						<Col sm="0">
							<Label className="label-resultantes-cuentas">
								{' $ ' + Number(this.state.cuentas.caja).toFixed(2)}
							</Label>
						</Col>


					</Row>

					<Row className="fila-resultantes-cuentas">

						<Col sm="0">
							<Label className="label-resultantes-cuentas">
								Cuenta Corriente:
							</Label>
						</Col>

						<Col sm="0">
							<Label className="label-resultantes-cuentas">
								{' $ ' + Number(this.state.cuentas.cuentaCorriente).toFixed(2)}
							</Label>
						</Col>

					</Row>

					<Row className="fila-resultantes-cuentas">


						<Col sm="0">
							<Label className="label-resultantes-cuentas">
								Crédito a favor:
							</Label>
						</Col>


						<Col sm="0">
							<Label className="label-resultantes-cuentas">
								{' $ ' + Number(this.state.cuentas.creditoAFavor).toFixed(2)}
							</Label>
						</Col>

					</Row>
					</div>
				}
				{modoDebug && this.listaProductos() }		
					
				</div>
			  );
		}
			
		catch(error){
			alert(error)
		}
	}
}
