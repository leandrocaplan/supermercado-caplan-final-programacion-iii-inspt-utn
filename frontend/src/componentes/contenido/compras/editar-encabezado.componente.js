import React, { Component, useState } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';

import DatePicker from "react-datepicker";
import DateTimePicker from 'react-datetime-picker'

import "react-datepicker/dist/react-datepicker.css";


const formatearFecha =function (fecha) {
	
	try{
		const fechaObj=new Date(fecha);
		return (
			fechaObj.getDate().toString().padStart(2, "0") + '/'+ 
			(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+ 
			fechaObj.getFullYear()
		);
	}
	catch(error){
		alert(error)
	}
}

const formatearHora =function (fecha) {
    try {
        const fechaObj=new Date(fecha);
        return (
			fechaObj.getHours().toString().padStart(2, "0") + ':'+
			(fechaObj.getMinutes()).toString().padStart(2, "0") + ':'+
			fechaObj.getSeconds().toString().padStart(2, "0")
		);
    }
    catch(error) {
        alert(error)
    }
}
//Realiza una operación complementaria a la funcion anterior, recibiendo un
//string de la forma "dd/mm/aaaa", y devolviendo un objeto tipo
//Date con el valor de la fecha correspondiente.

//Nos sirve para operar sobre la fecha como un objeto Date,
//y para descartar el horario asignado a ella, ya que
//de otra forma, el mismo nos traería errores a la hora de hacer el
//ordenamiento,donde no queremos que el horario sea un criterio para el mismo.

const desformatearFecha =function (fecha) {
    try {
        const partes = fecha.split("/");
		return new Date(
			parseInt(partes[2], 10),
			parseInt(partes[1], 10) - 1,
			parseInt(partes[0], 10)
		);
    }
	catch(error) {
		alert(error)    
    }
}


export default class EditarEncabezadoCompra extends Component {
	//Cada compra tiene un encabezado y un detalle
	//Los campos del encabezado son:
	//-Fecha de la compra
	//-Código de la compra
	//-Comprador
	//-Proveedor
	

	//Ademas, la compra completa y cada elemento del detalle tendrá asignado un ID,
	//el cual es generado automáticamente por la API, y no lo utilizamos en este componente del frontend.
	constructor(props) {

		//En el constuctor, definimos los campos que tendrá estado y las inicializamos

		super(props);

		this.state = {

			//Campos a almacenar en el array que mando a la API
			codCompra:1,
			userComprador: '',
			comprador: {},
			proveedor:'',
			hora:'',
			
			fechaHora: new Date(),
			
			codCompraOriginal:1,
			userCompradorOriginal: '',
			proveedorOriginal:'',
			compradorOriginal: {},
			fechaHoraOriginal: new Date(),
			compradores: [],
			horaOriginal:'',
			
			codigosExistentes:[],
			isSubmitted:false,
			resData:'',
			categoriaUsuario:JSON.parse(sessionStorage.getItem('user')).categoria,
		}

	}

	
	//En componentDidMount, hacemos el llamado a la API para que nos traiga los valores
	//necesarios para inicializar el estado y poder generar la información de la compra.
	componentDidMount() {
	
		console.log(this.state.producto);
		console.log(this.state.compradores);

		axios.get('http://localhost:5000/compras/listaCompras/' +  this.props.match.params.idCompra)
		.then(respuesta => {

			try{
				//Para inicializar los campos correspondientes a la compra
				//debo tener al menos un comprador y un producto almacenados en la base de datos.
				//De lo contrario, no puedo realizar ninguna compra.
				if (respuesta.data.compradores.length > 0) {
					const comprador=respuesta.data.compradores.find(v=> v.username==respuesta.data.compra.comprador) || respuesta.data.compradores[0] ;
					this.setState({
						compradores:respuesta.data.compradores,
						comprador:comprador,
						compradorOriginal:
							respuesta.data.compradores.find(v=> v.username==respuesta.data.compra.comprador) ||
							{
								username:'',
								nombre:'',
								apellido:'',
								categoria:''
							},
						userComprador:comprador.username,
						userCompradorOriginal:respuesta.data.compra.comprador,
						codCompra:respuesta.data.compra.codCompra,
						codCompraOriginal:respuesta.data.compra.codCompra,
						fechaHora:new Date(respuesta.data.compra.fechaHora),
						fechaHoraOriginal:new Date(respuesta.data.compra.fechaHora),
						hora: formatearHora(respuesta.data.compra.fechaHora),
						horaOriginal: formatearHora(respuesta.data.compra.fechaHora),
						
						proveedor:respuesta.data.compra.proveedor,
						proveedorOriginal:respuesta.data.compra.proveedor,
						arrayCompras:respuesta.data.arrayCompras,
						codigosExistentes:respuesta.data.arrayCompras
							.filter(function(compra){
								if(compra.comprador==comprador.username && compra.fecha== formatearFecha(respuesta.data.compra.fechaHora)){
									return compra
									}
								})
							.map(
								function(compra){
									return compra.codCompra;
								}
						)
					})
				}
			}
			catch(error) {
				alert(error)
			}
				
		})
		.catch((error) => {
			alert(error);
		});

	}

	//Genero información de debug al actualizar el estado del componente
	componentDidUpdate() {
		console.log(this.state.fechaHora);
		console.log(this.state.hora);
		console.log(this.state.horaOriginal);
		console.log(this.state.codigosExistentes);
	
		console.log(this.state.arrayCompras);
		console.log(this.state.fechaHora);
		console.log(this.state.fechaHoraOriginal);
	}



/*
----------------------------------------------------
Sección de 'onChange'

Al hacer cualquier modificación en algun campo del formulario,
la variable 'isSubmitted' la reseteamos en 'false'
----------------------------------------------------

*/


	onChangeFechaHora = (e) => {
		try{

			this.setState({
				fechaHora: e,
				hora:(formatearHora(e)),
				//Cuando cambio la fecha, actualizo el array de códigos existentes
				//(no tomo en cuenta la hora)
				codigosExistentes:this.state.arrayCompras
					.filter(function(compra){
						if(compra.comprador==this.state.userComprador && compra.fecha==formatearFecha(e)){
							return compra
							}
						},this)
					.map(
					function(compra){
							return compra.codCompra;
						},this),
				isSubmitted:false
			})
		}
			
		catch(error){
			alert(error)
		}
	}
	//Al modificar el codigo de compra en el form, actualizo el estado del codigo de compra
	onChangeCodCompra = (e) => {
		try{
			this.setState({
				codCompra: Number(e.target.value),
				isSubmitted:false
			})
		}		
		catch(error){
			alert(error);
		}
	}
	
	onChangeProveedor = (e) => {
		try {

			this.setState({
				proveedor: e.target.value,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error);
		}
	}
	//Al modificar el comprador en el form, actualizo el username del comprador
	//y el comprador completo asociado a ese username
	onChangeComprador = (e) => {
		try{
		
			this.setState({
				userComprador: e.target.value,
				comprador: this.state.compradores.find(p => p.username===e.target.value),
				
				//Actualizo los códigos existentes, de acuerdo
				//al comprador al que estoy asignando
				codigosExistentes:this.state.arrayCompras
							.filter(function(compra){
								if(compra.comprador==e.target.value && compra.fecha==formatearFecha(this.state.fechaHora)){
									return compra
									}
								},this)
							.map(
							function(compra){
									return compra.codCompra;
								},this)
						,
				isSubmitted:false
			})
			console.log(this.state.userComprador);
		}
			
		catch(error){
			alert(error);
		}
	}	
	
	onSubmit = (e) => {
		try {
			//Esta linea evita que al hacer el submit, salgamos de la vista actual

			e.preventDefault();

			//Validamos que el stock resultante de la operación no sea negativo
			//y ademas, el detalle tenga algun elemento

			const codCompra=this.state.codCompra;
			const userComprador=this.state.userComprador;
			const fechaHora = this.state.fechaHora;
			const hora = this.state.hora;
			const proveedor = this.state.proveedor;
			
			//Generamos una const con los datos de la compra
			//actualizada para mandar a la API
			const compra =
			{
				codCompra:codCompra,
				userComprador: userComprador,
				proveedor:proveedor,
				fechaHora: fechaHora
			};


			console.log(compra);
			//Hacemos un post a la API con la const cargada previamente
			axios.post('http://localhost:5000/compras/editarEncabezado/' + this.props.match.params.idCompra, compra)
			.then(res => {
				this.setState({
						codCompraOriginal:this.state.codCompra,
						userCompradorOriginal:this.state.userComprador,
						compradorOriginal:this.state.comprador,
						fechaHoraOriginal:this.state.fechaHora,
						proveedorOriginal:this.state.proveedor,
						horaOriginal:this.state.hora,
						arrayCompras:this.state.arrayCompras
							.filter(function (compra){
								//Eliminamos de nuestro array la compra con el
								//encabezado que teníamos originalmente	 
								if(
									!(
										(compra.codCompra == this.state.codCompraOriginal) &&
										(compra.comprador == this.state.userCompradorOriginal) &&
										(compra.fecha == formatearFecha(this.state.fechaHoraOriginal))
									)
								)
								{
									return compra;
								}
							},this)
							//Le concatenamos la compra con el encabezado actualizado
							.concat({
									codCompra:this.state.codCompra,
									comprador:this.state.userComprador,
									proveedor:this.state.proveedor,
									fecha: formatearFecha(this.state.fechaHora),
									hora: this.state.hora
								}),
						//Al array de codigos ezistentes, le eliminamos el
						//original y le concatenamos el actualizado
						codigosExistentes:this.state.codigosExistentes
							.filter(c =>c !=this.state.codCompraOriginal)
							.concat(this.state.codCompra),
						isSubmitted:true,
						resData:res.data
					});
				
			})
			//Si tuvimos un error en el POST, entramos al catch, para poder
			//mostrar en la vista el mismo
			.catch(res => {
				this.setState({isSubmitted:true});
				if(res.code=="ERR_BAD_REQUEST") {
					this.setState({resData:res.response.data});
				}
				else {
					this.setState({resData:res.message});

				}
				console.log(res);
			});

		//Cierro POST de compra
		}
		catch(error) {
			alert(error);
		}
	}
	
	render() {
		try{
			return (
				<Form onSubmit={this.onSubmit} >


					<Row className="encabezado">

						<Col sm="0">
							<Label className="label-fecha-hora">
								Fecha y hora:
							</Label>
						</Col>

						<Col sm="0" >
							<DateTimePicker
								className="inp-fecha-hora"
								value={this.state.fechaHora}
								onChange={this.onChangeFechaHora}
								format="dd/MM/yyyy hh:mm:ss"
								showTimeSelect
							/>
						</Col>	
						
						<Col sm="0" >
							<Label  className="label-cod-venta" >
								Código de compra:  
							</Label>
						</Col>

						<Col sm="0" >
							<Input 
							type="number"
							min="1"
							step="1" 
							className="inp-cod-venta"
							value={this.state.codCompra}
							onChange={this.onChangeCodCompra}
							/>

						</Col>

						<Col sm="0">
							<Label className="label-vendedor">
								Comprador:
							</Label>
						</Col>

						<Col sm="0" size="sm"> 

							<Input
							size="sm"
							type = "select"
							required
							className="inp-vendedor"
							value={this.state.userComprador}
							onChange={this.onChangeComprador}>
								{
									this.state.compradores.map(function(comprador) {
										return <option key={comprador.username} value={comprador.username}>
											{comprador.nombre} {comprador.apellido}
										</option>;
									})
								}
							</Input>
						</Col>

						<Col sm="0">
							<Label className="label-proveedor">
								Proveedor:
							</Label>
						</Col>


						<Col sm="0" >

							<Input 
							type="text"
							className="inp-proveedor"
							value={this.state.proveedor}
							onChange={this.onChangeProveedor}
							/>

						</Col>

					</Row>
					
					{/*Fila de espaciado*/}
					<Row className="encabezado"/>
					


					<Row className="encabezado">
						<Col sm="0">
							{
								(
									(this.state.codCompra==this.state.codCompraOriginal)&
									(formatearFecha(this.state.fechaHora)==formatearFecha(this.state.fechaHoraOriginal))&&
									(this.state.userComprador == this.state.userCompradorOriginal) 
								) 

								? 
										(
											this.state.proveedor==this.state.proveedorOriginal && 
											this.state.hora==this.state.horaOriginal
										) 
									
										?
											<Label className="label-resultado-editar-encabezado">
												Los valores del encabezado son iguales a los actuales
											</Label>
											:

											<Button className="boton" type="submit">
												Editar encabezado
											</Button>
							
										:

										(this.state.codigosExistentes.some(
											function(codigo){
												return codigo==this.state.codCompra
											},this)
										) 

									?

									<Label className="label-resultado-editar-encabezado">
										Ya existe una compra con los mismos campos que identifican al encabezado
									</Label>

									:
								
									<Button className="boton" type="submit">
										Editar encabezado
									</Button>
							}
						</Col>
					</Row>
					<Row className="encabezado">

						<Col sm="0"> </Col>

						<Col sm="0">
							<Label className="label-respuesta-api">
								{this.state.isSubmitted && this.state.resData}
							</Label>
						</Col>
						
					</Row>

				</Form>
			)
		}
		catch(error){
			alert(error);
		}
	}
}
