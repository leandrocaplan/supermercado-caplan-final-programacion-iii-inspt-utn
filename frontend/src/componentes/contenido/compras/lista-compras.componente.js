import React, { Component  } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Compra = function(props) { 


return(
	<tr>
		<td class="text-center">{props.compra.fecha}</td>
		<td class="text-center">{props.compra.hora}</td>
		<td class="text-center">{props.compra.codCompra}</td>
		<td class="text-center">{props.compra.compradorNomAp}</td>
		<td class="text-center">{props.compra.proveedor}</td>

		<td class="text-center">
			<Link to={"registro/" + encodeURIComponent(props.compra.fecha) +'/' + props.compra.codCompra +'/' + props.compra.comprador +'/' + props.compra.id  }>Ver detalle</Link> | &nbsp;		
			<Link to={"nuevo/" + props.compra.id}>Agregar elementos</Link> | &nbsp;
			<Link  to={"editarEnc/" + props.compra.id }>Editar encabezado</Link> 
		</td>

	</tr>
  );
}

	

export default class ListaCompras extends Component {

	constructor(props) {

		super(props);
		this.state = {
			listaCompras:[],
		};
	}


	componentDidMount() {
		axios.get('http://localhost:5000/compras/listaCompras')
		.then(respuesta => {
			try{
				this.setState({
					listaCompras: respuesta.data.arrayCompras,
					compradores:respuesta.data.compradores
				})
			}
			catch(error){
					alert(error)
				}
		})
		.catch((error) => {
			console.log(error);
		});
	}

	listaCompras() {
		try{
			return this.state.listaCompras
				.map(compraActual => {
					let compraFormato=compraActual;
					const comprador=this.state.compradores.find(v => v.username==compraActual.comprador);
					if(comprador){
						compraFormato.compradorNomAp =comprador.nombre + ' ' + comprador.apellido;
					}
					else{
						compraFormato.compradorNomAp= "(Usuario dado de baja: " + compraActual.comprador + ")";
						}
					return <Compra compra= {compraActual}/>;
				})
		}
		
		catch(error) {
			alert(error)
		}
	}
	  
	render() {
		try{
			return(
				<div>
					<h3>Compras registradas</h3>

					<br/>

					<h6>Para eliminar una compra, seleccione "Ver detalle" y pulse el boton "Eliminar todos"</h6>
					<h6>situado debajo de la lista de elementos del detalle.</h6>

					<table className="table">
						<thead>
							<tr>
								<th class="text-center">Fecha</th>
								<th class="text-center">Hora</th>
								<th class="text-center">Código</th>
								<th class="text-center">Comprador</th>
								<th class="text-center">Proveedor</th>
								<th class="text-center">Opciones</th>
							</tr>
						</thead>
						<tbody>
							{ this.listaCompras() }
						</tbody>
					</table>
				</div>   
			  );
		}	
		catch(error) {
			alert(error);
		}
	}

}
