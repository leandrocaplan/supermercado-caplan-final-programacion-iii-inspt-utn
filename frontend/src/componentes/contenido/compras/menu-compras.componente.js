import React from "react";
import {
	Navbar,
	Nav,
	NavItem,
	NavLink,
} from "reactstrap";

import { Link } from "react-router-dom";

const MenuCompras  = () =>  {

	return (
		<div class="center-menu">
			<Navbar
				color="dark"      
				className="custom-navMenu"
				expand="md"
				container="md"
				>

				<Nav className="mc-auto" navbar>

					<NavItem className="custom-menu">
						<NavLink tag={Link} to={"/compras/nuevo"}>
							<p>Nueva compra</p>
						</NavLink>				
					</NavItem>         


					<>&nbsp;&nbsp;&nbsp;</>


					<NavItem className="custom-menu">
						<NavLink tag={Link} to={"/compras/lista"}>    
							<p>Lista de compras</p>
						</NavLink>
					</NavItem>

					
					<>&nbsp;&nbsp;&nbsp;</>


					<NavItem className="custom-menu">
						<NavLink tag={Link} to={"/compras/registro"}>    
							<p>Registro de detalles</p>
						</NavLink>
					</NavItem>

				</Nav>

			</Navbar>
		</div>
	);
};

export default MenuCompras;
