import React, { Component, useState } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';
import DateTimePicker from 'react-datetime-picker'


const formatearFecha =function (fecha) {
    try {
		const fechaObj=new Date(fecha);
		return (
			fechaObj.getDate().toString().padStart(2, "0") + '/'+
			(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+
			fechaObj.getFullYear()
		);
    }
    catch(error) {
        alert(error)
    }
}

const formatearCuenta= {
	'caja':'Caja',
	'cuentaCorriente' :'Cuenta Corriente',
	'creditoAFavor' : 'Credito A Favor'
};
		
export default class ReasignarDetalleCompra extends Component {

	constructor(props) {

		//En el constuctor, definimos las variables de estado y las inicializamos

		super(props);

		this.state = {

			detalle:{},
			compras:[],
			compraAReasignar:{},
			idCompraAReasignar:'',
			nuevaCompra:false,
			compraExistente:false,
			isSubmitted:false,
			resData:"",
			
			//Me traigo las variables de realizar-compra
			//Campos a almacenar en el array que mando a la API
			codCompra:1,
			userComprador: '',
			proveedor:'Proveedor',
			fechaHora: new Date(),
			fechaHoraActual: true,

			//Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
			comprador: {},
			productos:[],
			compradores:[],
			codigosExistentes:[],
			idCompraAsociada:'',
			producto:{},
			categoriaUsuario:JSON.parse(sessionStorage.getItem('user')).categoria,				

			isSubmitted:false,
			resData:'',

		}
	}



	/*
	Nota: el valor del stock resultante de la operación será siempre:
	El valor del stock que el producto tenía previamente - la cantidad vendida en la operación
	*/


	//Nota: los atributos codCompra (Código de compra) y userComprador (nombre de usuario del comprador), son comunes para toda la compra
	//El resto de los atributos van asociados a cada elemento del detalle de la compra.

	componentDidMount() {

		//Al cargar el componente,
		//llamo a la API para que me traiga la lista de compradores y productos.
		//Este 'get' me trae solamente usuarios registrados como compradores
		//Tambien me trae la lista de todas las compras, pero no las utilizaremos
		axios.get('http://localhost:5000/compras/detalleId/' + this.props.match.params.idDetalle + "/" + this.props.match.params.idCompra)
			.then(respuesta => {

				try{

					//Me guardo la lista de compradores
					this.setState({
							detalle:respuesta.data.detalleCompras.detalle,
							idCompraAsociada:this.props.match.params.idCompra,
							compras:respuesta.data.detalleCompras.compras,
							compradores:respuesta.data.compradores,
							userComprador: respuesta.data.compradores[0].username,
							comprador: respuesta.data.compradores[0],
							producto:respuesta.data.producto
					});
				   
						//Obtengo el máximo codigo de compra asignado al actual comprador y la fecha actual,
						//para luego sumarle 1 al código de compra de la compra actual.
						//De otra forma, en vez de generar una compra nueva, probablemente
						//se agregarían elementos al detalle de una compra existente
						//a menos que el usuario configure a mano un codigo de compra aún inexistente
						
					this.setState(
						//En lugar de pasarle un objeto a setState,
						//le paso un callback con el estado anterior
						function(estadoAnterior) {
							//Inicialmente, el estado nuevo es igual al estado anterior
							let estadoNuevo=estadoAnterior;
							
							
							let codCompraMax=1;

							//Me genero un array con todos los códigos de compra
							//correspondientes a las compras de el usuario asignado
							//actualmente y la fecha asignada actualmente
							const codigosExistentes=this.state.compras
								.filter(
									function(compra) {
										if(
											(compra.comprador==estadoAnterior.userComprador)
											&&
											(formatearFecha(compra.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
										)

											return compra;
									}
								)
								.map(
									function(compra)
									{
										return compra.codCompra;
									}
								);

							
							//Del array que generé antes, me guardo el máximo valor
							const maximo=Math.max(...codigosExistentes);
							
							//Debug: muestro el array generado y su máximo valor
							console.log(codigosExistentes);
							console.log(maximo);
							
							
							//Deprecación: esta linea no la necesitamos
							//Si el código de compra asignado al estado actual
							//existe en el array generado anteriormente,
							//sabemos que dicha compra existe,
							//por lo que compraExistente valdrá true.
							//De lo contrario, la compra no existe y compraExistente}
							//valdrá false
							const compraExistente= codigosExistentes.some(function(codigo){
								return codigo==estadoAnterior.codCompra
								});
								
							//estadoNuevo.compraExistente=compraExistente;
							//console.log(compraExistente)	
							
							//Me guardo en el estado actualizado el
							//array generado con los códigos de compra
							//existentes asignados a dicha fecha y comprador
							estadoNuevo.codigosExistentes=codigosExistentes;
							
							//Si existía en el array generado algún código de compra
							//el máximo siempre será mayor o igual a 0
							if(maximo>=0){
								//El codigo de compra del estado actual valdrá
								//el máximo valor hallado en el mas 1, de manera
								//que me puedo asegurar que, si quiero generar
								//una compra nueva, su código no será el mismo
								//que el de una compra ya existente
								estadoNuevo.codCompra=maximo+1;
							}
							else{
								
								//Si no existe ninguna compra asignada a cierto comprador
								//y cierta fecha, la constante máximo valdrá (-Infinity),
								//por lo cual, el código de compra generado será 1
								estadoNuevo.codCompra=1;
								}
							return estadoNuevo;
						}
					);
			
				}
				catch(error){
					alert(error)
				}
			})
			.catch((error) => {
				console.log(error);
			});

		console.log('Ejecuto el componentDidMount');
	}


	componentDidUpdate() {
		console.log(this.state.compras);   
	}


	onChangeCompraAReasignar = (e) => {
		try {
			console.log(this.state);
			this.setState({
				idCompraAReasignar: e.target.value,
				isSubmitted:false,
				resData:""
			})
			if(e.target.value !="") {
				const compraAReasignar=this.state.compras.find(compra => compra._id==e.target.value);
				this.setState({
					compraAReasignar:compraAReasignar,
					codCompra:compraAReasignar.codCompra,
					userComprador:compraAReasignar.comprador,
					proveedor:compraAReasignar.proveedor,
					fechaHora:new Date(compraAReasignar.fechaHora),
					fechaHoraActual:false,
					compraExistente:true
				})
			}

			else {

				//Obtengo el máximo codigo de compra asignado al actual comprador y la fecha actual,
				//para luego sumarle 1 al código de compra de la compra actual.
				//De otra forma, en vez de generar una compra nueva, probablemente
				//se agregarían elementos al detalle de una compra existente
				//a menos que el usuario configure a mano un codigo de compra aún inexistente

				this.setState(
					//Utilizo un setState funcional
					function(estadoAnterior) {
						//Inicialmente, el estado nuevo es igual al estado anterior
						let estadoNuevo=estadoAnterior;


						let codCompraMax=1;

						//Me genero un array con todos los códigos de compra
						//correspondientes a las compras de el usuario asignado
						//actualmente y la fecha asignada actualmente
						const codigosExistentes=this.state.compras.filter(
						function(compra) {
							if(
								(compra.comprador==estadoAnterior.userComprador)
								&&
								(formatearFecha(compra.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
							)

								return compra;
						}
												)
						.map(
							function(compra)
						{
							return compra.codCompra;
						}
						);


						//Del array que generé antes, me guardo el máximo valor
						const maximo=Math.max(...codigosExistentes);

						//Debug: muestro el array generado y su máximo valor
						console.log(codigosExistentes);
						console.log(maximo);


						//Deprecación: esta linea no la necesitamos
						//Si el código de compra asignado al estado actual
						//existe en el array generado anteriormente,
						//sabemos que dicha compra existe,
						//por lo que compraExistente valdrá true.
						//De lo contrario, la compra no existe y compraExistente}
						//valdrá false
						const compraExistente= codigosExistentes.some(function(codigo) {
							return codigo==estadoAnterior.codCompra
						});

						//estadoNuevo.compraExistente=compraExistente;
						//console.log(compraExistente)

						//Me guardo en el estado actualizado el
						//array generado con los códigos de compra
						//existentes asignados a dicha fecha y comprador
						estadoNuevo.codigosExistentes=codigosExistentes;

						//Si existía en el array generado algún código de compra
						//el máximo siempre será mayor o igual a 0
						if(maximo>=0) {
							//El codigo de compra del estado actual valdrá
							//el máximo valor hallado en el mas 1, de manera
							//que me puedo asegurar que, si quiero generar
							//una compra nueva, su código no será el mismo
							//que el de una compra ya existente
							estadoNuevo.codCompra=maximo+1;
						}
						else {

							//Si no existe ninguna compra asignada a cierto comprador
							//y cierta fecha, la constante máximo valdrá (-Infinity),
							//por lo cual, el código de compra generado será 1
							estadoNuevo.codCompra=1;
						}

						estadoNuevo.compraExistente=false;
						estadoNuevo.fechaHora= new Date();
						estadoNuevo.fechaHoraActual= true;


						return estadoNuevo;
					}
					
				);
			}

		}
		catch(error) {
			alert(error)
		}
	}

/*
----------------------------------------------------
    Sección de 'onChange'

  Al hacer cualquier modificación en algun campo del formulario,
  la variable 'isSubmitted' la reseteamos en 'false'
----------------------------------------------------

*/

	onChangeFechaHoraActual = (e) => {
		try {


			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codCompraMax=1;
				console.log(estadoAnterior.userComprador);

				const codigosExistentes=this.state.compras
					.filter(
						function(compra) {
							if(
								(compra.comprador==estadoAnterior.userComprador)
								&&
								(
									(e.target.checked && formatearFecha(compra.fechaHora) == formatearFecha(new Date()) ||
									(!e.target.checked && formatearFecha(compra.fechaHora) == formatearFecha(estadoAnterior.fechaHora) )
									)
								)
							)
							return compra;
						}
					)
					.map(
						function(compra)
						{
							return compra.codCompra;
						}
					);


				const maximo=Math.max(...codigosExistentes);
				console.log(codigosExistentes);
				console.log(maximo);

				const compraExistente= codigosExistentes.some(function(codigo) {
					return codigo==estadoAnterior.codCompra
				});


				estadoNuevo.compraExistente=false;

				estadoNuevo.codigosExistentes=codigosExistentes;
				if(maximo>=0) {
					estadoNuevo.codCompra=maximo+1;
				}
				else {
					estadoNuevo.codCompra=1;
				}
				return estadoNuevo;
			});

			//  console.log(e);
			//  console.log(e.target.checked);
			this.setState({
				fechaHoraActual: e.target.checked,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	
	onChangeFechaHora = (e) => {
		try {


			//Actualizo los codigos existentes cuando cambio la fecha
			this.setState(
				function(estadoAnterior) {
					//Inicialmente, el estado nuevo es igual al estado anterior
					let estadoNuevo=estadoAnterior;

					//Me genero un array con todos los códigos de compra
					//correspondientes a las compras de el usuario asignado
					//actualmente y la fecha asignada actualmente
					const codigosExistentes=this.state.compras
						.filter(
							function(compra) {
								if(
									(compra.comprador==estadoAnterior.userComprador)
									&&
									(formatearFecha(compra.fechaHora) == formatearFecha(e))
								)

									return compra;
							}
						)
						.map(
							function(compra)
								{
									return compra.codCompra;
								}
						);



					const compraExistente= codigosExistentes.some(function(codigo) {
						return codigo==estadoAnterior.codCompra
					});

					estadoNuevo.compraExistente=compraExistente;
					estadoNuevo.codigosExistentes=codigosExistentes;
					return estadoNuevo;
				}
			);

			const compraAReasignar=this.state.compras.find(function (compra) {
				if(
					(compra.codCompra==this.state.codCompra) &&
					(compra.comprador==this.state.userComprador) &&
					(formatearFecha(compra.fechaHora)== formatearFecha(e))
				)
				{
					console.log("Entro al if");
					return compra;
				}

			},this);

			console.log(compraAReasignar);
			if(compraAReasignar) {
				this.setState({
					compraAReasignar:compraAReasignar,
					idCompraAReasignar:compraAReasignar._id,
					proveedor:compraAReasignar.proveedor,
					compraExistente:true
				})
			}
			else {
				this.setState({
					compraAReasignar:{},
					idCompraAReasignar:'',
					proveedor:'Proveedor',
					compraExistente:false
				})

			}

			this.setState({
				fechaHora: e,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error);
		}
	}

	onChangeProveedor = (e) => {
		try {
			this.setState({
				proveedor:e.target.value
			})
		}
		catch(error) {
			alert(error)
		}
	}
	//Al modificar el codigo de compra en el form, actualizo el estado del codigo de compra

	onChangeCodCompra = (e) => {
		try {

			const compraAReasignar=this.state.compras.find(function (compra) {

				if(
					(compra.codCompra==e.target.value) &&
					(compra.comprador==this.state.userComprador) &&
					(formatearFecha(compra.fechaHora) == formatearFecha(this.state.fechaHora))
				)
				{
					console.log("Entro al if");
					return compra;
				}

			},this);

			console.log(compraAReasignar);
			if(compraAReasignar) {
				this.setState({
					compraAReasignar:compraAReasignar,
					idCompraAReasignar:compraAReasignar._id,
					proveedor:compraAReasignar.proveedor,
					compraExistente:true
				})
			}
			else {
				this.setState({
					compraAReasignar:{},
					idCompraAReasignar:'',
					proveedor:'Proveedor',
					compraExistente:false
				})

			}


			this.setState({
				codCompra: Number(e.target.value),
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}

	//Al modificar el comprador en el form, actualizo el username del comprador
	//y el comprador completo asociado a ese username
	onChangeComprador = (e) => {
		try {


			//Actualizo los codigos existentes cuando cambio el comprador
			this.setState(

			function(estadoAnterior) {
				//Inicialmente, el estado nuevo es igual al estado anterior
				let estadoNuevo=estadoAnterior;

				//Me genero un array con todos los códigos de compra
				//correspondientes a las compras de el usuario asignado
				//actualmente y la fecha asignada actualmente
				const codigosExistentes=this.state.compras.filter(
				function(compra) {
					if(
						(compra.comprador==e.target.value)
						&&
						(
							(estadoAnterior.fechaHoraActual && formatearFecha(compra.fechaHora) == formatearFecha(new Date())) ||
							//Si está desactivado,
							//el resultado del filter me devolverá las compras existentes del comprador
							//correspondientes al estado de fechaHora que tengo actualmente
							(!estadoAnterior.fechaHoraActual && formatearFecha(compra.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
						)
					)

						return compra;
				}
										)
				.map(
					function(compra)
					{
						return compra.codCompra;
					}
				);


				const compraExistente= codigosExistentes.some(function(codigo) {
					return codigo==estadoAnterior.codCompra
				});

				estadoNuevo.compraExistente=compraExistente;

				//Me guardo en el estado actualizado el
				//array generado con los códigos de compra
				//existentes asignados a dicha fecha y comprador
				estadoNuevo.codigosExistentes=codigosExistentes;

				return estadoNuevo;
			}
			);

			const compraAReasignar=this.state.compras.find(function (compra) {
				if(
					(compra.codCompra==this.state.codCompra) &&
					(compra.comprador==e.target.value) &&
					(formatearFecha(compra.fechaHora)== formatearFecha(this.state.fechaHora))
				)
				{
					return compra;
				}

			},this);

			console.log(compraAReasignar);
			if(compraAReasignar) {
				this.setState({
					compraAReasignar:compraAReasignar,
					idCompraAReasignar:compraAReasignar._id,
					proveedor:compraAReasignar.proveedor,
					compraExistente:true
				})
			}
			else {
				this.setState({
					compraAReasignar:{},
					idCompraAReasignar:'',
					proveedor:'Proveedor',
					compraExistente:false
				})

			}
			this.setState({
				userComprador: e.target.value,
				comprador: this.state.compradores.find(p => p.username===e.target.value),
				isSubmitted:false
			})
			console.log(this.state.userComprador);
		}
		catch(error) {
			alert(error)
		}
	}

	onSubmit = (e) => {
		try {

			//Esta linea evita que al hacer el submit, salgamos de la vista actual
			e.preventDefault();

			console.log(this.state.idCompraAsociada);
			console.log(this.state.idCompraAReasignar);
			if(this.state.idCompraAsociada!=this.state.idCompraAReasignar) {

				const idCompraAsociada=this.state.idCompraAsociada;
				const idCompraAReasignar=this.state.idCompraAReasignar;
				const idDetalle=this.state.detalle._id;
				const compraExistente=this.state.compraExistente;
				const codCompra=this.state.codCompra;
				const userComprador=this.state.userComprador;
				const fechaHora=this.state.fechaHora;
				const proveedor=this.state.proveedor;

				const reasignacion=
				{
					idCompraAsociada: idCompraAsociada,
					idCompraAReasignar:idCompraAReasignar,
					idDetalle:idDetalle,
					compraExistente:compraExistente,
					codCompra:codCompra,
					userComprador:userComprador,
					proveedor:proveedor,
					fechaHora:fechaHora
				};


				//console.log(reasignacion);
				//Hacemos un post a la API con la const cargada previamente
				axios.post('http://localhost:5000/compras/reasignar/', reasignacion)
					.then(res => {
						this.setState({
								isSubmitted:true,
								resData:res.data.mensaje
						});

						this.setState(

							/*
							 * Debo eliminar el elemento del detalle de la compra
							 * a la cual estaba asignado previamente y luego,
							 * reasignarselo a la compra correspondiente
							 *
							 *	Necesito:
							 * 	-Buscar la compra a la cual estaba asignado el detalle
							 * 	-Eliminarle el elemento del detalle que quiero reasignar
							 * 	-Si lo reasigno a una compra nueva, debo generarla con el unico elemento en el detalle, correspondiente al elemento reasignado
							 * 	-Si lo reasigno a una compra existente, debo concatenerle el elemento del detalle que estoy reasignando
							 * 	-Actualizar en el array de compras, ambas compras: tanto la compra de la cual desasigné el detalle, como la compra a la cual lo reasigné
							 *  */

						function(estadoAnterior) {

							//Me genero una variable estadoNuevo, inicialmente
							//igual al cuerpo del estado anterior
							let estadoNuevo=estadoAnterior;

							//Me genero un array comprasFiltradas, con los mismos elementos que tenia en
							//el array de compras original, pero eliminandole la compra
							//a la cual le quiero desasignar el detalle
							let comprasFiltradas=estadoAnterior.compras.filter(compra => (compra._id!=estadoAnterior.idCompraAsociada));

							//En el array de compras que tenía en el estado anterior, busco la compra asociada
							//al detalle que quiero desasignar, y me la guardo en compraOriginal
							let compraOriginal=estadoAnterior.compras.find(compra => compra._id==estadoAnterior.idCompraAsociada);


							//En la compra original que me guardé arriba, busco por su ID,
							//el elemento de detalle que quiero desasignar.
							//Me guardo en detalleFiltrado el nuevo detalle sin el elemento
							//que desasigné
							const detalleFiltrado=compraOriginal.detalle.filter(elemento => elemento._id != estadoAnterior.detalle._id);

							//Si en el detalle de la compra a la cual estoy desasignando
							//el elemento que quiero, nos queda algún otro elemento
							if(detalleFiltrado.length>0) {

								//Al detalle de la compra original le asigno el
								//detalle con el elemento correspondiente desasignado
								compraOriginal.detalle=detalleFiltrado;

								//Al array de compras filtradas, le concateno la
								//compra original, ahora con el detalle actualizado
								//(sin el elemento que desasigné)
								comprasFiltradas=comprasFiltradas.concat(compraOriginal);
							}

							//Si en el detalle de la compra a la cual estoy desasignando
							//el elemento que quiero, no nos queda ningun otro elemento,
							//simplemente no hacemos nada mas y eliminamos de la lista
							//la compra, ya que la misma no contendrá ningun elemento
							//en su detalle y resultará superfluo tenerla en el array

							//Una vez desasignado el elemento del detalle de la compra
							//a la cual lo tenía asignado originalmente, debo reasignarlo,
							//ya sea a una compra existente o a una nueva

							//Declaro una variable donde tendré mi nuevo array de compras
							let comprasNuevo;

							//Declaro una variable la cual contendrá la compra a la cual
							//quiero reasignarle el elemento del detalle.
							//Debe estar fuera del scope del if/else, ya que
							//luego deberé utilizarla para actualizar el ID de la compra
							//asociada actualmente en el estado
							let compraReasignada;

							//Si estoy reasignandolo a una compra ya existente
							if(estadoAnterior.compraExistente) {

								//En el array de compras que tenía en el estado anterior, busco la compra completa
								//a la cual quiero reasignar el detalle,
								//y me la guardo en la variable compraReasignada

								compraReasignada=estadoAnterior.compras.find(compra => compra._id==idCompraAReasignar);

								//Luego, al detalle de dicha compra le concateno el elemento del detalle
								//que quiero reasignar
								compraReasignada.detalle=compraReasignada.detalle.concat(estadoAnterior.detalle);


								//Al array que generé antes con la compra correspondiente con el detalle
								//desasingado, le elimino la compra correspondiente a la que quiero
								//reasignar el elemento del detalle, y me guardo el resultado
								//en comprasNuevo.
								comprasNuevo=comprasFiltradas.filter(compra => (compra._id!=estadoAnterior.idCompraAReasignar));

								//Luego, a comprasNuevo le agregamos la compra ya con el elemento
								//del detalle asignado
								comprasNuevo=comprasNuevo.concat(compraReasignada);

							}

							//Si estoy asignandolo a una nueva compra
							else {

								//Me genero una nueva compra, con el ID
								//generado por la API,
								//y un array en el detalle con un único elemento,
								//correspondiente al elemento reasignado
								//Me guardo la nueva compra en compraReasignada
								compraReasignada= {
									_id: res.data.id,
									codCompra: estadoAnterior.codCompra,
									comprador: estadoAnterior.userComprador,
									proveedor: estadoAnterior.proveedor,
									detalle: [estadoAnterior.detalle],
									fechaHora:new Date(estadoAnterior.fechaHora)
								};
								estadoNuevo.idCompraAReasignar=res.data.id;
								estadoNuevo.codigosExistentes=estadoAnterior.codigosExistentes.concat(estadoAnterior.codCompra);

								//Finalmente, al array que contiene la lista
								//de compras actualizada, le asigno todas las
								//compras contenidas en el array filtrado,
								//agregandole la compra generada arriba
								comprasNuevo=comprasFiltradas.concat(compraReasignada);
							}

							//Al estado que estoy actualizando
							//en la propiedad 'compras', le asigno
							//el array que generé con las compras actualizadas
							estadoNuevo.compras=comprasNuevo;

							//Luego, la propiedad idCompraAsociada,
							//la actualizo con el ID de la compra a la cual
							//reasigné el detalle
							estadoNuevo.idCompraAsociada=compraReasignada._id;



							return estadoNuevo;
						}
						);
					})
					.catch(res => {
						this.setState({isSubmitted:true});
						if(res.code=="ERR_BAD_REQUEST") {
							this.setState({resData:res.response.data});
						}
						else {
							this.setState({resData:res.message});

						}
						console.log(res);
					});


				//Cierro POST de compra
			}
			else {
				alert("La compra a la cual quiero reasignar el detalle es la misma a la cual el detalle ya está asignado");
			}
		}
		catch(error) {
			alert(error)
		}
	}

	render() {
		try{
		  return (
		  
				<div className="center-realizar-venta">
					<center>
						<h3>Información del detalle:</h3>

						<Row className="fila-resultantes-cuentas">
							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									ID del detalle:
								</Label>
							</Col>


							<Col sm="0">
							<Label className="label-resultantes-cuentas">
							{this.state.detalle._id}
							</Label>
							</Col>
						</Row>


						<Row className="fila-resultantes-cuentas">
							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									ID de compra asociada:
								</Label>
							</Col>


							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									{this.state.idCompraAsociada}
								</Label>
							</Col>
						</Row>



						<Row className="fila-resultantes-cuentas">
							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									Producto:
								</Label>
							</Col>


							<Col sm="0">
								<Label className="label-resultantes-cuentas">
										{
											(this.state.producto) ?
												this.state.producto.descripcion:
												"(Producto dado de baja :" + Number(this.state.detalle.producto) +")"
										}
								</Label>
							</Col>
						</Row>
						
						<Row className="fila-resultantes-cuentas">
							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									Precio unitario:
								</Label>
							</Col>

							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									{" $ "+ this.state.detalle.precioActual + "/" + this.state.detalle.unidadActual}
								</Label>
							</Col>
						</Row>
							
						<Row className="fila-resultantes-cuentas">
							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									Cantidad:
								</Label>
							</Col>

							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									{this.state.detalle.cantidad + " " + this.state.detalle.unidadActual}
								</Label>
							</Col>
						</Row>

						<Row className="fila-resultantes-cuentas">
							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									Tipo de pago:
								</Label>
							</Col>

							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									{formatearCuenta[this.state.detalle.formaPago]}
								</Label>
							</Col>
						</Row>

						<Row className="fila-resultantes-cuentas">
							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									Subtotal:
								</Label>
							</Col>


							<Col sm="0">
								<Label className="label-resultantes-cuentas">
									{"$ " + this.state.detalle.subtotal}
								</Label>
							</Col>
						</Row>


						<br/>
						
						<Form onSubmit={this.onSubmit} >

							<Input 
								type="select"
								className="inp-reasignar"
								value={this.state.idCompraAReasignar}
								onChange={this.onChangeCompraAReasignar}>
								{

									[
									<option key="" value="">
									Compra Nueva
									</option>
									].concat(this.state.compras.map(function(compra) {
									const compradorFormat=
										this.state.compradores.some(v=>v.username==compra.comprador) ?
											this.state.compradores.find(v=>v.username==compra.comprador):
											{
												nombre: "(Usuario dado de baja: ",
												apellido: compra.comprador + ")"	
											} ;
											return  <option key= {compra._id} value= {compra._id}>
													{
														"Codigo: " + compra.codCompra + 
														"\tComprador: " + compradorFormat.nombre + "  "+ compradorFormat.apellido +
														"\tFecha: " + formatearFecha(compra.fechaHora)
													}
												</option>;
									},this)
									)
							}
							</Input>


							<Row  className="fila-espaciado"/>
						
							<Row  className="encabezado">


								<Col sm="0">
									<Label className="label-fecha-hora-actuales">
										Registrar fecha y hora actuales:
									</Label>

								</Col>

								<Col sm="0">
									<Input
										type="checkbox"
										className="checkbox-fecha-hora-actuales"
										defaultChecked={this.state.fechaHoraActual} 
										value={this.state.fechaHoraActual}
										checked={this.state.fechaHoraActual}
										selected={this.state.fechaHoraActual}
										onChange={this.onChangeFechaHoraActual}
									/>
							</Col>

							{!this.state.fechaHoraActual &&
							<>

							<Col sm="0">
								<Label className="label-fecha-hora">
									Fecha y hora:
								</Label>

							</Col>

							<Col sm="0" >
								<DateTimePicker
									className="inp-fecha-hora"
									value={this.state.fechaHora}
									onChange={this.onChangeFechaHora}
									format="dd/MM/yyyy hh:mm:ss"
									showTimeSelect
								/>
							</Col>	
							</>	
						}

							<Col sm="0" >
								<Label className="label-cod-venta">
									Código de compra:  
								</Label>
							</Col>


							<Col sm="0" >

								<Input 
									type="number"
									min="1"
									step="1" 
									className="inp-cod-venta"
									value={this.state.codCompra}
									onChange={this.onChangeCodCompra}
								/>

							</Col>


							<Col sm="0">

								<Label className="label-vendedor">
									Comprador:
								</Label>
							</Col>

							<Col sm="0"	size="sm"> 
								<Input
									size="sm"
									type = "select"
									required
									className="inp-vendedor"
									value={this.state.userComprador}
									onChange={this.onChangeComprador}>
									{
										this.state.compradores.map(function(comprador) {
											return <option key={comprador.username} value={comprador.username}>
														{comprador.nombre} {comprador.apellido}
													</option>;
										})
									}
								</Input>
							</Col>

							<Col sm="0" >
								<Label  className="label-proveedor" >
									Proveedor:
								</Label>
							</Col>



							<Col sm="0">
							{
							
								this.state.compraExistente
								
								?
								
									<Label className="label-proveedor-existente">
										{this.state.proveedor}
									</Label>
								:	
									<Input 
										type="text"
										className="inp-proveedor"
										value={this.state.proveedor}
										onChange={this.onChangeProveedor}
									/>
							}

							</Col>
						</Row>
						
						<Row className="encabezado"/>


						<Row className="encabezado">
							<Col sm="0">
								<Label className="label-reasignar">
									ID Compra a reasignar:
								</Label>
							</Col>


							<Col sm="0">
								<Label className="label-reasignar">
									{
										this.state.idCompraAReasignar!='' ?
										this.state.idCompraAReasignar
										: <>(Compra nueva)</>
									}
								</Label>
							</Col>
						</Row>

							{
								this.state.codigosExistentes.some(function(codigo){
									return codigo==this.state.codCompra
								},this)
								
								?
									<h6>Compra existente</h6>
								
								:
								
									<h6>Compra nueva</h6>
							}

							<Button className="boton" type="submit">Reasignar Compra</Button>
						</Form>
						
						<Label className="label-respuesta-api">
							{this.state.isSubmitted && this.state.resData}
						</Label>
					</center>
				</div>
			)
		}
		catch(error){
			alert(error)
		}
	}
}
