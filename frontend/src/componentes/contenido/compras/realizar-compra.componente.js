import React, { Component, useState } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';

import DatePicker from "react-datepicker";
import DateTimePicker from 'react-datetime-picker'

import "react-datepicker/dist/react-datepicker.css";


//Este flag le permite al programador ver en el
//navegador, toda la información util necesario
//para la depuración, mostrada en el renderizado del componente

//Cuando un usuario común debe utilizar la aplicación,
//está por defecto desactivada.

//Cuando un programador está trabajando con el código,
//ya sea el que lo desarrolló inicialmente o bien
//algun otro, simplemente debe establecer 
//este flag en true, y podrá ver en el navegador toda
//la información necesaria para agilizar el proceso de debug
const modoDebug=false;

const mostrarResultantes=true;
//Esta funcion nos resulta util para debugear,
//controlando el stock de cada producto
//y su stock original del lado del frontend
//Solo la utilizo cuando tenga activado el modo debug
const Producto = function(props) { 
	
	return(
	  <tr>
		<td>{Number(props.producto._id)} &nbsp;</td>
		<td>{props.producto.descripcion}&nbsp;</td>
		<td>{props.producto.stock}&nbsp;</td>
		<td>{props.producto.stockResultante}</td>
		<td>{props.producto.stockOriginal}&nbsp;</td>
	  </tr>
	  );
}

	

//Esta función, en este componente, me sirve solamente a efectos
//de comparar las fechas, descartando el horario
const formatearFecha =function (fecha) {	
	try{
		const fechaObj=new Date(fecha);
		return (
				fechaObj.getDate().toString().padStart(2, "0") + '/'+ 
				(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+ 
				fechaObj.getFullYear()
			);
	}
	catch(error){
		alert(error)
	}
}


const formaPagoAResultante= {
	'caja':'resultanteCaja',
	'cuentaCorriente':'resultanteCorriente',
	'creditoAFavor':'resultanteCredito'
};

//NOTA: En todos los importes monetarios, debo siempre tener dos cifras decimales correspondientes a los centavos.
//Para dicho propósito, a cada operacion en la que esté involucrada
//algun cálculo de un importe, le aplico la función '.toFixed(2)', de manera que evito que el sistema
//muchas mas cifras decimales de las que necesito, a la vez redondeanda mantisa al valor mas cercano correspondiente.

export default class ComprarProducto extends Component {
		/*
		Cada compra tiene un encabezado y un detalle
		Los campos del encabezado son:
		-Fecha de la compra
		-Código de la Compra
		-Comprador
		-Proveedor
		
		Los tres primeros campos son los que identifican a la venta completa
		
		Luego, el detalle consiste en un array de objetos
		Cada elemento del array tiene los siguientes campos:
		
		-Producto (en la API solo guardaremos el ID del producto correposndiente, 
		pero para operar en el frontend, necesitamos la información del producto completo)
		
		-Cantidad comprada
		
		-Forma de pago (correspondiente a cada cuenta contable distinta)
		
		-Subtotal
		
		
		Ademas, la venta completa y cada elemento del detalle tendrá asignado un ID,
		el cual es generado automáticamente por la API, y no lo utilizamos en este componente del frontend.
		* */
	constructor(props) {	
		//En el constuctor, definimos las variables de estado y las inicializamos

		super(props);

		this.state = {

			
			//Campos a almacenar en el array que mando a la API
			codCompra:1,
			userComprador: '',
			proveedor:'Proveedor',
			fechaHora: new Date(),
			fechaHoraActual: true,

			//Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
			comprador: {},
			productos:[],
			compradores:[],
			
			//Este campo lo utilizamos para generar automáticamente el código de venta
			codigosExistentes:[],
			compras:[],
			
			//Totales por forma de pago
			caja: Number(0).toFixed(2),
			cuentaCorriente:Number(0).toFixed(2),
			creditoAFavor:Number(0).toFixed(2),
			
			//Total general
			total:Number(0).toFixed(2),
			categoriaUsuario:JSON.parse(sessionStorage.getItem('user')).categoria,
			
			cuentas:{},
			//Resultantes de cada cuenta contable
			resultanteCaja:0,
			resultanteCorriente:0,
			resultanteCredito:0,
			
			//Este campo nos resulta útil para agilizar la venta,
			//ya que lo mas probable es que el cliente realize
			//toda la compra con el mismo tipo de pago, por lo
			//cual no es conveniente ir modificando, uno por uno,
			//los tipos de pago asociados a cada elemento del detalle
			formaPagoPorDefecto:'caja',

			isSubmitted:false,
			resData:'',

			//Inicializamos el array correspondiente al detalle con un solo objeto
			detalle:
				[ 
					{
						//Campos a almacenar en el array que mando a la API
						idProducto:'',
						cantidad:1,
						precioActual:0,
						unidadActual:'',
						formaPago:'caja',
						subtotal:0,

						 //Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
						producto:{}
					}
				]
		}
		console.log('Ejecuto el constructor');
		console.log(this.state.producto);
	}



	/*
	Nota: el valor del stock resultante de la operación será siempre:
	El valor del stock que el producto tenía previamente + la cantidad comprada en la operación
	*/


	//Nota: los atributos codCompra (Código de compra) y userComprador (nombre de usuario del comprador), son comunes para toda la compra
	//El resto de los atributos van asociados a cada elemento del detalle de la compra.


	//En componentDidMount, hacemos el llamado a la API para que nos traiga los valores
	//necesarios para inicializar el estado y poder generar la información de la venta.
	componentDidMount() {	
		console.log(this.state.producto);
		console.log(this.state.compradores);

		//Al cargar el componente,
		//llamo a la API para que me traiga la lista de compradores y productos.
		//Este 'get' me trae solamente usuarios registrados como compradores
		//Tambien me trae la lista de todas las compras, pero no las utilizaremos
		axios.get('http://localhost:5000/compras/asociar')
		.then(respuesta => {

			try{
			//Si en la lista de compradores hay al menos un comprador registrado
			if (respuesta.data.usuarios.length > 0 && respuesta.data.productos.length > 0) {

			//Me genero una constante con la cual inicializaré el detalle, correspondiente a un array de un solo objeto
			const detalleInicial=[
				{
					//En el campo corresponidete al ID del producto,
					//me guardo el ID del primer producto devuelto por la base de datos
					idProducto:respuesta.data.productos[0]._id,
					
					//Inicialmente, la cantidad comprada del producto es 1
					cantidad:1,
					
					//Inicialmente, el tipo de pago es en caja
					formaPago:'caja',
					
					
					precioActual:respuesta.data.productos[0].precioCompra,
					unidadActual:respuesta.data.productos[0].unidad,

					//Inicialmente, el subtotal del elemento
					//del detalle es equivalente al precio de venta del producto asignado
					//al mismo (ya que su cantidad vendida inicialmente es 1)
					subtotal:respuesta.data.productos[0].precioCompra.toFixed(2),

					 //El producto completo asociado al primer elemento del detalle es: el primer producto devuelto por la base de datos,
					 //al que le añadimos el campo "stockResultante", cuyo valor inicial es el stock
					 //actual del producto menos 1 (ya que 1 es la cantidad vendida inicialmente)
					producto:Object.assign(respuesta.data.productos[0], {stockResultante: Number(respuesta.data.productos[0].stock)+1}),
					
				}
			];
			
			
			this.setState({
				//Me guardo la lista de vendedores devietla por la API
				compradores: respuesta.data.usuarios,
				compras:respuesta.data.compras,
				//En la lista de productos que me guardo en el frontend,
				//me guardo la lista de productos devuelta por la API a la cual
				//le añado un campo: el stock resultante de cada producto.
				productos: respuesta.data.productos.map(function (producto,indice)
						{
							//Genero una variable 'productoConResultante',
							//que inicialmente tiene el mismpo contenido que cada
							//elemento de la lista de productos devuelta por la API.
							let productoConResultante=producto;
							
							productoConResultante.stock=Number(producto.stock).toFixed(2);
							//Luego, a la variable generada anteriormente le agrego el campo
							//'stockResultante', cuyo valor inicial se lo asigno con este 'if'
							if(indice==0)
							{	
								//Inicialmente, el stock resultante del primer producto devuelto por
								//la API sera: su stock actual menos 1, ya que inicialmente,
								//en el primer elemento del detalle inicializado estoy vendiendo
								//una unidad de dicho producto.
								productoConResultante.stockResultante=(Number(producto.stock)+1).toFixed(2);
							}
							else
							{
								//Luego, inicialmente, el stock resultante de los demas productos
								//será equivalente a su stock actual
								productoConResultante.stockResultante=Number(producto.stock).toFixed(2);
							}
							//Una vez asignado el campo 'stockResultante' con el valor correspondiente,
							//devuelvo la variable con dicho campo agregado.
							return productoConResultante;
						}
					),
					
				//Inicialmente, el total de la venta pagada en caja
				//equivale al subtotal del primer elemento del detalle inicializado previamente
				//(precio de venta del primer producto de la base de datos devuelto por la API)
				caja:respuesta.data.productos[0].precioCompra.toFixed(2),
				
				//Inicialmente, el total general de la venta, tambien
				//equivale al subtotal del primer elemento del detalle inicializado previamente
				//(precio de venta del primer producto de la base de datos devuelto por la API)
				total:respuesta.data.productos[0].precioCompra.toFixed(2),
				
				//Me guardo el estado actual de las cuentas que tengo en la base de datos
				cuentas: {
							//Me aseguro de tener siempre dos cifras decimales
							caja:respuesta.data.cuentas.caja.toFixed(2),
							cuentaCorriente:respuesta.data.cuentas.cuentaCorriente.toFixed(2),
							creditoAFavor:respuesta.data.cuentas.creditoAFavor.toFixed(2),
						 },
				
				//Necesito guardarme el listado de ventas para luego generar automáticamente
				//el código de venta correspondiente, para evitar que se pise con el de
				//una venta ya existente.
				compras: respuesta.data.compras,
				
				//Debo inicializar los resultantes de cada cuenta contable, cuyo valor
				//equivaldrá a lo que me queda en cada cuenta luego de hacer la operación.
				
				//Inicialmente, el resultante de caja, equivaldrá al valor actual de caja,
				//mas el subtotal del primer elemento del detalle inicializado previamente
				//(precio de venta del primer producto de la base de datos devuelto por la API).
				resultanteCaja: Number(respuesta.data.cuentas.caja -  respuesta.data.productos[0].precioCompra).toFixed(2),
				
				//Luego, inicialmente, el resultante de la cuenta corriente y el crédito a favor,
				//serán iguales a su valor actual
				resultanteCorriente:Number(respuesta.data.cuentas.cuentaCorriente).toFixed(2),
				resultanteCredito:Number(respuesta.data.cuentas.creditoAFavor).toFixed(2),
				
				//Al detalle de venta, lo inicializamos con la const 'detalleInicial', definida mas arriba
				detalle:detalleInicial,



			});


			//Si en la lista de productos hay al menos un producto registrado,
			//me guardo la lista de productos, e inicializo los valores correspondientes
			//con los datos del primer producto de la base de datos
			
					 
			//Obtengo el máximo codigo de compra asignado al actual comprador y la fecha actual,
			//para luego sumarle 1 al código de compra de la compra actual.
			//De otra forma, en vez de generar una compra nueva, probablemente
			//se agregarían elementos al detalle de una compra existente
			//a menos que el usuario configure a mano un codigo de compra aún inexistente
			
			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codCompraMax=1;


				
				const compraExistente=respuesta.data.compras.find(compra=> compra._id==this.props.match.params.idCompra);
				//Primero asigno el usuario y la fecha antes de generar
				//automáticamente el código de compra
				
				
				if(
					//Si no recibo parámetros, estoy por defecto, generando una compra nueva
					(!this.props.match.params.idCompra) ||
					//Si el vendedor al cual tenía asignada la venta existente ya
					//no existe en la base de datos, genero una venta nueva
					(!respuesta.data.usuarios.some(comprador=> comprador.username==compraExistente.comprador))
				)
				{
					
					//Si no recibo parámetros, por defecto, el comprador es el usuario actualmente logueado
					estadoNuevo.userComprador= JSON.parse(sessionStorage.getItem('user')).username;
					estadoNuevo.comprador = JSON.parse(sessionStorage.getItem('user'));
					estadoNuevo.fechaHora=new Date();
					estadoNuevo.fechaHoraActual=true;
					
				}
				
				
				//Si recibo parámetros, estoy por defecto, agregando elementos a una compra existente
				//Me cargo los datos correspondientes a la misma
				else{
						
						estadoNuevo.userComprador=compraExistente.comprador;
						estadoNuevo.comprador=respuesta.data.usuarios.find(comprador=> comprador.username==compraExistente.comprador);
						estadoNuevo.codCompra=compraExistente.codCompra;
						estadoNuevo.fechaHora=new Date(compraExistente.fechaHora);
						estadoNuevo.fechaHoraActual=false;
					}
				
				//Genero la lista de códigos de compra existentes correspondientes
				//al comprador actual y a la fecha de hoy
				const listaCodigosCompras=this.state.compras.filter(
					function(compra) {
						if(
							(compra.comprador==estadoNuevo.userComprador)
							&&
							(formatearFecha(compra.fechaHora) == formatearFecha(estadoNuevo.fechaHora))
						)

							return compra;
					}
				)
				.map(
					function(compra)
				{
					return compra.codCompra;
				}
				);


				const maximo=Math.max(...listaCodigosCompras);
				console.log(listaCodigosCompras);
				console.log(maximo);
				
				estadoNuevo.codigosExistentes=listaCodigosCompras;
				
				//Si no recibí ningun parámetro (estoy generando una compra nueva)

				if(!this.props.match.params.idCompra){
					
					//Si tengo algun código existente en la 
					//lista que generé, el codigo actual
					//será el máximo que tengo en la lista mas 1
					if(maximo>=0){
						estadoNuevo.codCompra=maximo+1;
					}
					
					//Si no tengo ninguno, el código
					//de la venta actual será 1
					else{
						estadoNuevo.codCompra=1;
						}								
				}

				return estadoNuevo;
			});


			}
			}
			catch(error){
				alert(error)
			}
		})
		.catch((error) => {
			alert(error);
		});

		console.log('Ejecuto el componentDidMount');
	}

	//Utilizo el componentDidUpdate solo
	//para proposito de debug
	componentDidUpdate() {
		console.log("Hola");
		console.log(this.state.compras);
	}

	//Genero el listado del array de productos actual a modo de debug
	listaProductos() {    	
		try{
			return this.state.productos.map(productoActual => {
			  return <Producto producto={productoActual}   />;
			})
		}
		catch(error){
			alert(error)
		}
	}

/*
----------------------------------------------------
	Sección de 'onChange'

  Al hacer cualquier modificación en algun campo del formulario,
  la variable 'isSubmitted' la reseteamos en 'false'
----------------------------------------------------

*/

	onChangeFechaHoraActual = (e) => {	
		try{

		//El codigo de compra será el mismo para todos los elementos del array
		
			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codCompraMax=1;
				console.log(estadoAnterior.userComprador);
				
				//Genero la lista de códigos de venta de acuerdo a la fecha de hoy
				const listaCodCompra=this.state.compras.filter(
				function(compra) {
					if(
						//Filtro las compras de acuedo al comprador que tengo en el estado
						(compra.comprador==estadoAnterior.userComprador)
						&&
						(
							//Si estoy activando el flag 'fechaHoraActual",
							//el resultado del filter me devolverá las compras
							//correspondientes al dia de hoy
							(e.target.checked && formatearFecha(compra.fechaHora) == formatearFecha(new Date())) ||
							
							//Si lo estoy desactivando,
							//el resultado del filter me devolverá las ventas
							//correspondientes al estado de fechaHora que tengo actualmente
							(!e.target.checked && formatearFecha(compra.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
						
						)
					)

						return compra;
					}
				)
				.map(
					function(compra)
				{
					//Me quedo solamente con el campo correspondiente al código, del array filtrado previamente
					return compra.codCompra;
				}
				);

				//Genero el compra de venta con el mismo
				//algoritmo que utiilze en el componentDidMount
				const maximo=Math.max(...listaCodCompra);
				console.log(listaCodCompra);
				console.log(maximo);
				
				estadoNuevo.codigosExistentes=listaCodCompra;
				if(maximo>=0){
					estadoNuevo.codCompra=maximo+1;
				}
				else{
					estadoNuevo.codCompra=1;
					}
				return estadoNuevo;
			});


			//Actualizo el estado del flag 'fechaHoraActual'
			this.setState({
				fechaHoraActual: e.target.checked,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error);
		}
	}
	
	onChangeFechaHora = (e) => {	
		try{
			
			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codCompraMax=1;
				console.log(estadoAnterior.userComprador);

				const listaCodCompra=this.state.compras
					.filter(
						//Si la opción 'fechaHoraActual' está activada,
						//sería redundante corroborarlo en esta función,
						//ya que el propio renderizado condicional del 
						//componente no me permitirá acceder al
						//DateTimePicker que dispara el evento correspondiente
						function(compra) {
							if(
								//El filter me devuelve todas las compras correspondientes
								//al comprador actual del estado y a la fecha seleccionada
								(compra.comprador==estadoAnterior.userComprador)
								&&
								(formatearFecha(compra.fechaHora) == formatearFecha(e))
							)

							return compra;
						}
					)
					//Me quedo solo con el campo de
					//código de compra
					.map(
						function(compra)
						{
							return compra.codCompra;
						}
				);
				//Genero el código de compra con el mismo
				//algoritmo que utiilze en el componentDidMount
				const maximo=Math.max(...listaCodCompra);
				console.log(listaCodCompra);
				console.log(maximo);
				
				estadoNuevo.codigosExistentes=listaCodCompra;
				if(maximo>=0){
					estadoNuevo.codCompra=maximo+1;
				}
				else{
					estadoNuevo.codCompra=1;
					}
				return estadoNuevo;
			});
			
			//Actualizo el estado de fechaHora
			this.setState({
				fechaHora: e,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error);
		}
		
	}
	
	//Al modificar el codigo de compra en el form, actualizo el estado del codigo de compra
	onChangeCodCompra = (e) => {	
		try{
			this.setState({
				codCompra: Number(e.target.value),
				isSubmitted:false
			})
		}
		catch(error){
			alert(error)
		}
	}
	
	//Al modificar el comprador en el form, actualizo el username del comprador
	//y el comprador completo asociado a ese username
	onChangeComprador = (e) => {	
		try {

			//Obtengo el máximo codigo de compra asignado al actual comprador y la fecha actual,
			//para luego sumarle 1 al código de compra de la compra actual.
			//De otra forma, en vez de generar una compra nueva, probablemente
			//se agregarían elementos al detalle de una compra existente
			//a menos que el usuario configure a mano un codigo de compra aún inexistente
			
			this.setState(function(estadoAnterior) {
				let estadoNuevo=estadoAnterior;
				let codCompraMax=1;
				console.log(estadoAnterior.userComprador);

				const listaCodCompra=this.state.compras
					.filter(
						function(compra) {
							if(
							//Filtro todas las compras correspondientes al comprador al que estoy actualizando
								(compra.comprador==e.target.value)
								&&
								(
									//Si el flag 'fechaHoraActual' está activado,el resultado del filter me devolverá las compras
									//existentes del vendedor correspondientes al dia de hoy
									(estadoAnterior.fechaHoraActual && formatearFecha(compra.fechaHora) == formatearFecha(new Date())) ||
									 
									 //Si está desactivado,
									 //el resultado del filter me devolverá las compras existentes del vendedor
									 //correspondientes al estado de fechaHora que tengo actualmente
									 (!estadoAnterior.fechaHoraActual && formatearFecha(compra.fechaHora) == formatearFecha(estadoAnterior.fechaHora))
								)
							)
							return compra;
						}
					)
					.map(
						function(compra)
						{
							//Me quedo solamente con el campo correspondiente al código, del array filtrado previamente
							return compra.codCompra;
						}
					);

				//Genero el código de compra con el mismo
				//algoritmo que utiilze en el componentDidMount
				const maximo=Math.max(...listaCodCompra);
				console.log(listaCodCompra);
				console.log(maximo);

				estadoNuevo.codigosExistentes=listaCodCompra;
				if(maximo>=0){
					estadoNuevo.codCompra=maximo+1;
				}
				else{
					estadoNuevo.codCompra=1;
					}
				return estadoNuevo;
			});

			//Actualizo en el estado los datos del comprador
			this.setState({
				userComprador: e.target.value,
				comprador: this.state.compradores.find(p => p.username===e.target.value),
				isSubmitted:false
			})
			console.log(this.state.userComprador);
		}
			
		catch(error) {
			alert(error)
		}
	}	
							
	//Actualizo en el estado los datos del proveedor		
	onChangeProveedor = (e) => {	
		try{
			this.setState({
				proveedor: e.target.value,
				isSubmitted:false
			})

		}
		
		catch(error){
			alert(error)
		}
	}
	
	//Uso un solo método para actualizar cada campo del detalle	
	onChangeDetalle =  (evento, indiceReg) => {	
		try{
			
			//Declaro una variable 'detalle', que contiene el valor antirior del detalle
			let detalle = [...this.state.detalle];
			
			//Declaro una constante 'campo', en la que referencio el campo
			//del elemento del detalle que estoy modificando al ejecutar el evento.
			const campo=evento.target.name;
			
			//Declaro una constante 'valor', que contendrá el valor del campo del
			//elemento del detalle que estoy modificando al ejecutar el evento.
			const valor = evento.target.value;
			

			//Informacion de debug
			console.log(campo);
			console.log(indiceReg);

			//En una constante 'prodAnterior', me guardo la información
			//del producto completo con los valores que tenía antes de modificar
			//el elemento del detalle.
			const prodAnterior = detalle[indiceReg]['producto'];
			

			//En cada elemento del detalle, necesito tener siempre el mismo
			//stock resultante de acuerdo al producto correspondiente


			if(campo=='cantidad') {
			
				//Al cambiar la cantidad, actualizo el stock resultante del producto asociado al registro
				
				//El stock que tiene el producto antes de realizar la venta,
				//menos la suma de todas las cantidades de los demas elementos 
				//del detalle con dicho producto asignado, 
				//menos la cantidad correspondiente al elemento que estoy modificando
				
				
					
				const stockResultanteAct =
					
					//Parto del stock actual del producto
					(
						Number(detalle[indiceReg]['producto'].stock)
						
						+
						
						//Luego, necesito sumarle la cantidad comprada asociada a todos los otros registros
						//cuyo producto asociado sea el mismo que el registro que estoy actualizando
						

						Number(
							//Recorro todos los elementos del detalle
							this.state.detalle
								.filter(function (elemDetalle, indiceFilter){
									if
										(
										//El registro del detalle donde estoy parado
										//tiene asignado el mismo producto que el
										//registro del detalle que estoy actualizando
										(elemDetalle.idProducto==detalle[indiceReg]['idProducto'] )
																		
										&&

										//El registro del detalle donde estoy parado
										//es diferente al que estoy actualizando
										(indiceFilter != indiceReg) 
										)
										return elemDetalle;
									})
									
								//Luego, sumo todas las cantidades de los elementos devueltos por el filter
								.reduce(
									function(acumulador,elemDetalle) {
									
										return acumulador + Number(elemDetalle.cantidad);

											},0)
							)
							
							+
							//Finalmente, le sumo el valor nuevo correspondiente
							//a la cantidad en el registro que estoy actualizando
						Number(valor)
					).toFixed(2);
						
						
					//Una vez que obtuve el stock resultante actualizado, 
					
					
					this.setState(function(estadoAnterior){
						let estadoNuevo=estadoAnterior;
						
						//Actualizo la lista de productos con el nuevo stock	
						estadoNuevo.productos=estadoAnterior.productos.map(
							function(producto){
								//Cuando encuentre, en la lista de productos del frontend,
								//el producto correspondiente al del registro que estoy actualizando
								if(producto._id==detalle[indiceReg]['idProducto']){
										
										//Actualizo el stock resultante en el producto
										producto.stockResultante=stockResultanteAct;
										
										//Al producto recien actualizado, lo actualizo en el elemento del detalle correspondiente
										detalle[indiceReg]['producto'] = producto;
										detalle[indiceReg]['subtotal'] = (Number(producto.precioCompra)*Number(valor)).toFixed(2);
										
								}
								return producto;	
							}

						);
						estadoNuevo.isSubmitted=false;
						return estadoNuevo;
						});
			}
		

			//Si el campo que estoy modificando corresponde al producto, entro a este if
			if(campo=='idProducto' ) {
			

				//Al cambiar el producto, debo deducir sumarle la cantidad al producto anterior
				//y deducirle la misma al nuevo	
				
				//Me guardo el producto que tenia antes de actualizar el registro
				const prodAnt=detalle[indiceReg]['producto'];
				
				//Me guardo el producto que estoy actualizando en el registro
				const prodAct=this.state.productos.find(p=> p._id==valor);
				
				
				//Actualizo los campos "precioActual" y "unidadActual"
				//de acuerdo a los valores de precio y unidad
				//del producto al que estoy actualizando el elemento
				//del detalle
				detalle[indiceReg]['precioActual']=Number(prodAct.precioCompra);
				detalle[indiceReg]['unidadActual']=prodAct.unidad;
				
				//Si el producto al que estoy modificando el elemento del detalle
				//se vende por unidad, necesariamente su cantidad debe ser un
				//valor entero, por lo que, si anteriormente tenía un valor
				//no entero, lo redondeo al valor entero hacia arriba mas cercano
				//(lo redondeo hacia arriba ya que de otra manera me podría quedar
				//un cero en campo de cantidad, el cual no es un valor válido)
				
				//Hago entonces, una asignación condicional
				const cantidadRedondeada = prodAct.unidad=='u' 
					? Math.ceil(Number(detalle[indiceReg]['cantidad']))
					: Number(detalle[indiceReg]['cantidad']);
				
				//Le asigno el valor redondeado hacia arriba al campo de cantidad
				//si es que el producto se vende por unidad
				if(prodAct.unidad=='u'){
					detalle[indiceReg]['cantidad'] = cantidadRedondeada;			
				}
					
				const stockResultanteProdAnt =
					(
						//Parto del stock actual del producto
						Number(prodAnt.stock)
						+
						
						//Luego, necesito sumarle la cantidad comprada asociada a todos los otros registros
						//cuyo producto asociado sea el mismo que el registro que estoy actualizando
						

						Number(
							//Recorro todos los elementos del detalle
							this.state.detalle
								.filter(function (elemDetalle, indiceFilter){
									if
										(
										//El registro del detalle donde estoy parado
										//tiene asignado el mismo producto que el
										//registro del detalle que estoy actualizando
										(elemDetalle.idProducto==prodAnt._id )
										
										&&
										
										//El registro del detalle donde estoy parado
										//es diferente al que estoy actualizando
										(indiceFilter != indiceReg) 
										)
										return elemDetalle;
									})
									
								//Luego, sumo todas las cantidades de los elementos devueltos por el filter
								.reduce(
									function(acumulador,elemDetalle) {
									
										return acumulador + Number(elemDetalle.cantidad);

											},0)
										
							)
					).toFixed(2);
			
			
					const stockResultanteProdAct =
					(
						//Parto del stock actual del producto
						Number(prodAct.stock)
						
						+
						
						//Luego, necesito sumarle la cantidad comprada asociada a todos los otros registros
						//cuyo producto asociado sea el mismo que el registro que estoy actualizando
						

						Number(
							//Recorro todos los elementos del detalle
							this.state.detalle
							.filter(function (elemDetalle, indiceFilter){
								if
									(
									//El registro del detalle donde estoy parado
									//tiene asignado el mismo producto que el
									//registro del detalle que estoy actualizando
									(elemDetalle.idProducto==prodAct._id )
									
									&&
									
									//El registro del detalle donde estoy parado
									//es diferente al que estoy actualizando
									(indiceFilter != indiceReg) 
									)
									return elemDetalle;
								})
								
							//Luego, sumo todas las cantidades de los elementos devueltos por el filter
							.reduce(
								function(acumulador,elemDetalle) {
								
											return acumulador + Number(elemDetalle.cantidad);

										},0) + cantidadRedondeada
							)
						).toFixed(2);
				
				
					this.setState(function(estadoAnterior){
						let estadoNuevo=estadoAnterior;
						
						//Actualizo la lista de productos con el nuevo stock	
						estadoNuevo.productos=estadoAnterior.productos.map(
							function(producto){
								//Cuando encuentre, en la lista de productos del frontend,
								//el producto correspondiente al del registro que estoy actualizando
								if(producto._id==prodAct._id){
										
										//Actualizo el stock resultante en el producto
										producto.stockResultante=stockResultanteProdAct;
										
										//Al producto recien actualizado, lo actualizo en el elemento del detalle correspondiente
										detalle[indiceReg]['producto'] = producto;
										detalle[indiceReg]['subtotal'] =
											(
												Number(producto.precioCompra)*cantidadRedondeada
											).toFixed(2);
									}
									if(producto._id==prodAnt._id){
										
										//Actualizo el stock resultante en el producto
										producto.stockResultante=stockResultanteProdAnt;
										

									}
								return producto;	
								}
						);
						estadoNuevo.isSubmitted=false;
						return estadoNuevo;
						});
			
			}
			
			//Al modificar la forma de pago de un producto,
			//actualizo la forma de pago por defecto

			if(campo=='formaPago') {
				this.setState({
					formaPagoPorDefecto:valor
				});
			}
			detalle[indiceReg][campo] = valor;
			
			this.setState(function(estadoAnterior){
				let estadoNuevo=estadoAnterior;
				
				console.log(detalle);
				
				//Actualizo los totales de acuerdo al tipo de pago
				estadoNuevo.caja=detalle
					.filter(elementoDetalle => elementoDetalle.formaPago=='caja')
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);
								
				estadoNuevo.cuentaCorriente=detalle
					.filter(elementoDetalle => elementoDetalle.formaPago=='cuentaCorriente')
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);
								
				estadoNuevo.creditoAFavor=detalle
					.filter(elementoDetalle => elementoDetalle.formaPago=='creditoAFavor')
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);
				
				//Actualizo el total general
				estadoNuevo.total=detalle
					.reduce(
						function(acumulador,elemDetalle) {
							return	acumulador+Number(elemDetalle.subtotal);
						}
					,0).toFixed(2);				
				
				//Una vez que actualizé los totales por cada tipo de pago,
				//actualizo el resultante de cada cuenta contable de acuerdo a esos valores
				estadoNuevo.resultanteCaja=
					(
						Number(estadoAnterior.cuentas.caja) - Number(estadoNuevo.caja)
					).toFixed(2);
				
				estadoNuevo.resultanteCorriente=
					(
						Number(estadoAnterior.cuentas.cuentaCorriente) - Number(estadoNuevo.cuentaCorriente)
					).toFixed(2);
				
				estadoNuevo.resultanteCredito=
					(
						Number(estadoAnterior.cuentas.creditoAFavor) -Number(estadoNuevo.creditoAFavor)
					).toFixed(2);
				
				
				//Finalmente, actualizo el detalle con todas las
				//modificaciones realizadas en esta función
				estadoNuevo.isSubmitted=false;
				estadoNuevo.detalle=detalle;
				
				return estadoNuevo;
			})


			}
		
		catch(error){
			alert(error)
		}
	}

/*
----------------------------------------------------
	Sección de manejadores de eventos asociados a botones
----------------------------------------------------
*/


	agregarElemento = () => {	
		try{
			console.log(this.state.detalle);
			console.log("Hola");
			
			//Me guardo el ID del primer producto que me devolvió
			//la API desde la base de datos, ya que,
			//inicialmente, el nuevo registro del detalle
			//estará asignado a dicho producto
			const idPrimero=this.state.productos[0]._id;
			
			//Obtengo la cantidad comrpada vendida de dicho producto,
			//ya existente en cada registro del detalle previamente
			//a añadir el nuevo registro
			const cantAnterior= this.state.detalle
									.filter(elemDetalle=>elemDetalle.idProducto==idPrimero)
									.reduce(
										function(acumulador,elemDetalle) {
											return acumulador+Number(elemDetalle.cantidad);
									},0).toFixed(2);
			
			
			//El nuevo reultante de stock del producto agregado será:
			//El stock que tenía originalmente,
			//menos la cantidad vendida anterior que calculé antes,
			//menos 1 (correspondiente a la cantidad con la que inicializo el nuevo registro)
									
			const nuevoResultante= Number(this.state.productos[0].stock) +  Number(cantAnterior) + Number(1);
			
		
			this.setState(
				function(estadoAnterior){
					let estadoNuevo=estadoAnterior;	
					
					//Debo ahora, actualizar el stock resultante de toda
					//la lista de productos con el nuevo resultante
					
					//Me guardo en 'productos' la lista de productos que tenia en el estado anterior
					let productos=estadoAnterior.productos;
					
					//El stock resultante del primer producto de la lista,
					//lo piso con el nuevo resultante calculado anteriormente
					productos[0].stockResultante = nuevoResultante;
					
					//Me guardo en una variable el primer producto
					//de la lista, ya con el stock resultante actualizado
					let producto=productos[0];
					
					//Actualizo el detalle
					//El detalle actualizado será igual a:
					estadoNuevo.detalle=				
						//El detalle que tenía anteriormente, al cual le concateno un objeto
						//con la información que tendrá el nuevo registro
						estadoAnterior.detalle.concat(
							//La información del nuevo registro será:	
							{	
								//El ID asignado al producto, correspondiente al
								//primer elemento de la lista de productos
								idProducto:idPrimero,
								
								//La cantidad vendida (inicialmente 1)
								cantidad:1,
								
								
								
								//Actualizo los valores de precio actual
								//y unidad actual, con el precio
								//y unidad de dicho producto
								precioActual:Number(producto.precioCompra),
								unidadActual:producto.unidad,
						
								//La forma de pago será la que tengo en el campo
								//'formaPagoPorDefecto', correspondiente
								//a la última forma de pago que seleccioné
								//previamente en algún elemento del detalle
								formaPago:estadoAnterior.formaPagoPorDefecto,
								
								//El subtotal, inicialmente equivalente al
								//precio de venta del producto vendido
								subtotal:Number(producto.precioCompra).toFixed(2),
								
								//El producto completo asignado al detalle
								//(ya lo teniamos guardado previamente en una variable,
								//de forma que, convenientemente, la utilizamos aquí)
								producto:producto
							}
						)
					
					//Luego, actualizamos la lista competa de productos,
					//con el nuevo valor de stock resultante actualizado
					//en el primer producto de la lista	
					estadoNuevo.productos=productos;
					
					//Actualizo el total de ventas realizado en la forma de pago por defecto
					//(le sumo el subtotal del nuevo registro, inicialmente equivalente al precio de venta del primer producto)
					estadoNuevo[estadoAnterior.formaPagoPorDefecto]= 
						(
							Number(estadoAnterior[estadoAnterior.formaPagoPorDefecto]) + 
							Number(producto.precioCompra)
						).toFixed(2);
					
					//Actualizo el resultante de la forma de pago por defecto
					//(le sumo el subtotal del nuevo registro, inicialmente equivalente al precio de venta del primer producto)
					estadoNuevo[formaPagoAResultante[estadoAnterior.formaPagoPorDefecto]]=
						(
							Number(estadoAnterior[formaPagoAResultante[estadoAnterior.formaPagoPorDefecto]]) -
							Number(producto.precioCompra)
						).toFixed(2);
					
					//Actualizo el total general de la venta
					////(le sumo el subtotal del nuevo registro, inicialmente equivalente al precio de venta del primer producto)
					estadoNuevo.total= 
						(
							Number(estadoAnterior.total) + 
							Number(producto.precioCompra)
						).toFixed(2);
					
					estadoNuevo.isSubmitted=false;
					//Devuelvo el estado con todos sus valores actualizados
					return estadoNuevo;
				});		
		}
		
		catch(error) {
			alert(error)
		}
	}
	
	//Entro a esta función cada vez que quiero remover un elemento
	//al detalle, el cual también se eliminará en el form dinámico
	removerElemento = (elemento,indice) => {	
		try{

			//Utilizando destructuring, me guardo:
			let {detalle,formaPago,subtotal}
			 = 
				{
					//En 'detalle', el estado actual del detalle previo a la actualización
					detalle:[...this.state.detalle],
					
					//En 'formaPago', me guardo el tipo de pago asociado al
					//registro del detalle que estoy eliminando
					formaPago:this.state.detalle[indice].formaPago,
					
					//En 'subtotal', me guardo el subtotal correspondiente al
					//registro del detalle que estoy eliminando
					subtotal:this.state.detalle[indice].subtotal,
				};
				
			
			//Me guardo en 'producto', el producto asociado al
			//registro del detalle que estoy eliminando
			const producto=detalle[indice].producto;
			
			//El nuevo stock resultante de dicho producto será:
			//El stock resultante que tenía antes de la eliminación, mas la cantidad
			//correspondiente que tenia el detalle que estoy eliminando
			const nuevoResultante = (Number(producto.stockResultante) - Number(detalle[indice].cantidad)).toFixed(2);
			
			//Elimino el registro del detalle con splice
			detalle.splice(indice, 1);
			
			//Busco el índice del producto a actualizar el stock resultante
			this.setState(function(estadoAnterior){
				let estadoNuevo=estadoAnterior;
				
				//Debo ahora, actualizar el stock resultante de toda
				//la lista de productos con el nuevo resultante
				
				//Me guardo en 'productos' la lista de productos
				//que tengo previamente a la actualización
				let productos=estadoAnterior.productos;
				//Busco el indice del producto a actualizar en la lista de productos
				const indiceAActualizar=productos.findIndex(p=>p._id==producto._id);
				
				console.log(indiceAActualizar);
				
				//Al producto en la lista a actualizar, le asigno
				//el nuevo resultante calculado anteriormente
				productos[indiceAActualizar].stockResultante=nuevoResultante;
				
				//Luego, actualizamos la lista competa de productos,
				//con el nuevo valor de stock resultante actualizado
				//en el producto correspondiente de la lista	
				estadoNuevo.productos=productos;
				
				//Actualizamos el detalle con el registro eliminado
				estadoNuevo.detalle=detalle;
				
				//Actualizo el total de ventas correspondiente al tipo de pago
				//correspondiente al detalle que estoy eliminando
				//Su nuevo valor corresponderá a su estado anterior,
				//menos el subtotal que tenía dicho registro.
				estadoNuevo[formaPago]=Number(Number(estadoAnterior[formaPago]) - Number(subtotal)).toFixed(2);
				
				
				//Actualizo resultante de la cuenta contable asociada
				//al tipo de pago
				//correspondiente al detalle que estoy eliminando
				//Su nuevo valor corresponderá a su estado anterior,
				//menos el subtotal que tenía dicho registro.
				estadoNuevo[formaPagoAResultante[formaPago]]=(Number(estadoAnterior[formaPagoAResultante[formaPago]]) + Number(subtotal)).toFixed(2);
				
				//Actualizo el total de ventas
				//Su nuevo valor corresponderá a su estado anterior,
				//menos el subtotal que tenía dicho registro.
				estadoNuevo.total=(Number(estadoAnterior.total)- Number(subtotal)).toFixed(2);
				estadoNuevo.isSubmitted=false;
				return estadoNuevo;
				})

		}
		catch(error){
			alert(error)
		}
	}
	
	//Valido el resultante de las cuentas contables
	validarCuentas = () => {	
		try{
			if (
				//Si los resultantes de caja y cuenta corriente
				//no nos dan un valor por debajo del permitido,
				//la validación es correcta
				(Number(this.state.resultanteCaja)>=0) &&
				(Number(this.state.resultanteCorriente)>=-50000)
			)
			{
				return true;
			}
			else 
			{
				return false;
			}
		
		}
		catch(error){
				alert(error)
		}
	}
	
	onSubmit = (e) => {	
		try{

			//Esta linea evita que al hacer el submit, salgamos de la vista actual

			e.preventDefault();

			this.setState({
				isSubmitted:true
			});


			//Si la validación de cuentas contables es correcta,
			//y tenemos al menos un elemento en el detalle
			if(this.validarCuentas() && this.state.detalle.length>0)   {
				/*
				 * Me genero un array en donde todos los elementos tengan un campo comun
				 * para toda la compra (codigo de compra y nombre de usuario del comprador),
				 * y luego, los elementos asociados al detalle de cada compra.
				 */

				const codCompra=this.state.codCompra;
				const userComprador=this.state.userComprador;
				const proveedor=this.state.proveedor;	
				//Asigno los elementos del detalle que voy a mandar a la API
				const detalle=this.state.detalle.map (
				function(elemento) {
					const detalleDevuelto= {
						producto: elemento.idProducto,
						cantidad: elemento.cantidad,
						precioActual: elemento.precioActual,
						unidadActual: elemento.unidadActual,
						formaPago: elemento.formaPago,
						subtotal: elemento.subtotal
						}

						return detalleDevuelto;
					}
				);
				//Si fechaHora actual vale 'true', genero una nueva fecha y hora correspondiente al
				//momento en el que genero la compra.
				//Si no, le mando a la API la fecha y hora que seleccioné en el formulario
				const fechaHora = (this.state.fechaHoraActual) ? new Date() : this.state.fechaHora;
			   
				//const fechaHora=new Date();

				const compra=
				{
					codCompra:codCompra,
					userComprador: userComprador,
					proveedor:proveedor,
					detalle: detalle,
					fechaHora: fechaHora
				};


				console.log(compra);
				//Hacemos un post a la API con la const cargada previamente
				axios.post('http://localhost:5000/compras/agregar', compra)
					.then(res => {

							//Si el POST fue exitoso, actualizo mi lista de productos
							//en el frontend, de acuerdo a las cantidades vendidas de
							//cada producto						

							this.setState(function(estadoAnterior){
								let estadoNuevo=estadoAnterior;
								const codCompra=estadoAnterior.codCompra;	
								//Actualizo la lista de productos con su stock resultante
								estadoNuevo.productos=estadoAnterior.productos.map(function (producto){
									producto.stock=producto.stockResultante;
									return producto;
									});
								
								//Como inicialmente la cantidad comprada
								//de un producto es por defecto 1,
								//e inicialmente, dicho producto es el primero
								//de la lista,inicialmente, el stock resultante
								//del primer producto de la lista actualizada
								//será el resultante de la compra anterior
								//mas una unidad
								estadoNuevo.productos[0].stockResultante++;
								
								//Actualizo las cuentas con su resultante
								estadoNuevo.cuentas={
									caja:estadoAnterior.resultanteCaja,
									cuentaCorriente:estadoAnterior.resultanteCorriente,
									creditoAFavor:estadoAnterior.resultanteCredito
								};
								
								//Actualizo los resultantes de cada cuenta contable
								estadoNuevo.resultanteCaja=Number(estadoAnterior.cuentas.caja) - Number(estadoAnterior.productos[0].precioCompra);
								estadoNuevo.resultanteCorriente=estadoAnterior.cuentas.cuentaCorriente;
								estadoNuevo.resultanteCredito=estadoAnterior.cuentas.creditoAFavor;
								
								//Inicializo el detalle de la venta subsiguiente
								estadoNuevo.detalle=[{

										//Campos a almacenar en el array que mando a la API
										idProducto:estadoAnterior.productos[0]._id,
										cantidad:1,
										precioActual:Number(estadoAnterior.productos[0].precioCompra),
										unidadActual:estadoAnterior.productos[0].unidad,
										formaPago:'caja',
										subtotal:estadoAnterior.productos[0].precioCompra.toFixed(2),

										//Campos auxiliares (no se guardarán en la API, pero nos sirven para operar en el frontend)
																	
										//Como ya actualize previamente el stock resultante
										//mas arriba, me conviene directamente asignar el
										//producto asociado al detalle con el valor
										//que tiene en estadoNuevo
										producto:estadoNuevo.productos[0]
									}];
										
								//Le concateno al array de códigos de compra existentes,
								//el correspondiente a la compra actual
								estadoNuevo.codigosExistentes=estadoAnterior.codigosExistentes.concat(estadoAnterior.codCompra);
								
								//El código de venta de la venta subsiguiente, será una unidad
								//mayor al máximo código que tengo en mi array de códigos existenes,
								//de forma que me aseguro que la misma, por defecto, sea nueva y
								//no añada elementos a una existente
								estadoNuevo.codCompra= Math.max(...estadoNuevo.codigosExistentes) + 1;
								
								
								//Le concateno a mi array de compras existentes
								//la que estoy realizando actuamente, de manera que, sin
								//necesidad de volver a hacer un llamado a la
								//API, pueda seguir generando correctamente
								//los códigos de compra con los algoritmos implementados
								estadoNuevo.compras=estadoAnterior.compras.concat(
									{
										codCompra:codCompra,
										comprador:estadoAnterior.userComprador,
										proveedor:estadoAnterior.proveedor,
										detalle:estadoAnterior.detalle,
										fechaHora:estadoAnterior.fechaHora.toISOString()
									}
								);
								
								estadoNuevo.formaPagoPorDefecto="caja";	
								//Seteamos la variable 'isSubmitted' en true,
								//de manera que al hacer el render condicional, nos muestre
								//en la vista el response del post
								estadoNuevo.isSubmitted=true;
								
								//Nos guardamos el response del post para mostrarlo en la vista
								estadoNuevo.resData=res.data;
								
								console.log("res.data: " +res.data);					
								
								return estadoNuevo;
								});
					})
					//Si tuvimos un error en el POST, entramos al catch, para poder
					//mostrar en la vista el mismo
					.catch(res => {
						this.setState({isSubmitted:true});
						if(res.code=="ERR_BAD_REQUEST") {
							this.setState({resData:res.response.data});
						}
						else {
							this.setState({resData:res.message});

						}
						console.log(res);
					});


				//Cierro validacion de stock
			}
			//Cierro POST de compra
		}
		catch(error){
			alert(error);
		}
		
	}
	
	render() {	
		try{
			return (

				<div className="center-realizar-venta">
					<center>
						<h3>Nueva compra</h3>
						
						<br/>

						{
							(this.state.compradores.length>0 && this.state.productos.length>0)
							
							? 


						<Form onSubmit={this.onSubmit}  >

							<FormGroup>

								<Row  className="encabezado">

									<Col sm="0">
										<Label className="label-fecha-hora-actuales">
											Registrar fecha y hora actuales:
										</Label>

									</Col>

									<Col sm="0">
								
										<Input
											type="checkbox"
											className="checkbox-fecha-hora-actuales"
											checked={this.state.fechaHoraActual}
											onChange={this.onChangeFechaHoraActual}
										/>
									</Col>

								{
									!this.state.fechaHoraActual &&
									<>
										<Col sm="0">
											<Label className="label-fecha-hora">
												Fecha y hora:
											</Label>

										</Col>

										<Col sm="0" >
											<DateTimePicker
												className="inp-fecha-hora"
												value={this.state.fechaHora}
												onChange={this.onChangeFechaHora}
												format="dd/MM/yyyy hh:mm:ss"
												showTimeSelect
											/>
										</Col>	
									</>	
								}

									<Col sm="0" >
										<Label className="label-cod-venta" >
											Código de compra: 
										</Label>
									</Col>


									<Col sm="0" >

										<Input 
											type="number"
											min="1"
											step="1" 
											className="inp-cod-venta"
											value={this.state.codCompra}
											onChange={this.onChangeCodCompra}
										/>

									</Col>


									<Col sm="0">
										<Label className="label-vendedor">
											Comprador:
										</Label>
									</Col>

									<Col sm="0" size="sm"> 

										<Input
											size="sm"
											type = "select"
											required
											className="inp-vendedor"
											value={this.state.userComprador}
											onChange={this.onChangeComprador}
										>
										
											{
												this.state.compradores.map(function(comprador) {
													return <option key={comprador.username} value={comprador.username}>
														{comprador.nombre} {comprador.apellido}
													</option>;
												})
											}
										</Input>
									</Col>


									<Col sm="0">
										<Label className="label-proveedor">
											Proveedor:
										</Label>
									</Col>

									<Col sm="0" size="sm"> 
									
										{
											//Si estoy en una compra existente
											
											this.state.codigosExistentes.some(function(codigo){
												return codigo==this.state.codCompra
											},this)
										
										?
										
											//Muestro el proveedor
										<Label className="label-proveedor-existente">
											{
												
												this.state.compras.find(
													function(compra){
														console.log(compra);
														return (
																compra.codCompra==this.state.codCompra && 
																compra.comprador==this.state.userComprador && 
																formatearFecha(compra.fechaHora)==formatearFecha(this.state.fechaHora)
																)
														},this).proveedor || "Error"
													}
													

										</Label>
										:
											
										//Si estoy en una compra nueve, lo ingreso
										<Input
											size="sm"
											type = "text"
											ref="otraCosa"
											required
											className="inp-proveedor"
											value={this.state.proveedor}
											onChange={this.onChangeProveedor}
										/>
									}
									</Col>

								</Row>

								<Row className="encabezado">
									<Col sm="0">
									<Label>
										{
											//En la lista de códigos generada, 
											//verifico si el código de compra actual
											//del estado existe
											this.state.codigosExistentes.some(function(codigo){
												return codigo==this.state.codCompra
											},this)
										
											?
												//Si existe, es una compra existente
												<>
													<h6>Compra existente</h6>
													<h7>Los elementos del detalle de compra se agregarán a le misma</h7>
												</>
											:
												//Si no, es una compra nueva
												<h6>Compra nueva</h6>
										}
									</Label>

									</Col>
								</Row>


							</FormGroup>


							<FormGroup >
								<br/>  
								<h5>  Detalle:  </h5> 
							</FormGroup>

							{ //Aquí implemento el form dinámico

								this.state.detalle.map(
									function (elemento, indice) {
									
									return(

										<FormGroup className="formgroup-detalle">
											<Row className="fila-detalle">
												<Col sm="0">
													<Label className="label-producto">
														Producto:
													</Label>
												</Col>

												<Col sm="0" >
													<Input
														name="idProducto"
														size="sm"
														bsSize="sm"
														type="select" 
														required
														className="inp-producto"
														value={elemento.idProducto}
														onChange={evento =>  this.onChangeDetalle(evento, indice)}
													>

														{
															this.state.productos.map(function(producto) {
																	return <option key={producto._id} value={producto._id}>
																		{producto.descripcion}
																	</option>;
															})
														}

													</Input>
												</Col>


												<Col sm="0">
													<Label className="label-cantidad">
														 Cantidad:
													</Label>
												</Col>



													{elemento.producto.unidad=='u' ?
													<Col sm="0">
														<Input
															name="cantidad"
															size="sm"
															type="number"
															min="1"
															step="1" 
															className="inp-cantidad"
															value={elemento.cantidad}
															onChange={evento =>  this.onChangeDetalle(evento, indice)}
														/>

													</Col>	
													
													:
													<Col sm="0">
														<Input
															name="cantidad"
															size="sm"
															type="number"
															min="0.01"
															step="0.01" 
															className="inp-cantidad"
															value={elemento.cantidad}
															onChange={evento =>  this.onChangeDetalle(evento, indice)}
														/>

													</Col>
													
													}
								
													<Col sm="0">
														<Label className="label-unidad">
															<h6>&nbsp;{elemento.producto.unidad} </h6>
														</Label>
													</Col>
								
													<Col sm="0">
														<Label className="label-precio-unitario">
															{'$/' + elemento.producto.unidad+ ' : $' + elemento.producto.precioCompra} 
														</Label>
													</Col>
													
													
													<Col sm="0">
														<Label  className="label-subtotal">
															{'Subtotal: $ ' + elemento.subtotal}
														</Label>
													</Col>

												<Col sm="0">
													<Label  className="label-tipo-pago">
														Forma de pago: 
													</Label>
												</Col>

												<Col sm="0">
													<Input 
														type = "select" 
														required 
														name="formaPago"
														className="inp-tipo-pago"
														value={elemento.formaPago}
														onChange={evento =>  this.onChangeDetalle(evento, indice)}
													>

														<option value="caja">Caja</option>
														<option value="cuentaCorriente">Cuenta Corriente</option>
														<option value="creditoAFavor">Credito a favor</option>
													</Input>
												</Col>


												<Col sm="0">
													<Button
														className="boton-remover"
														onClick={evento =>  this.removerElemento(evento, indice)}
													>
														Remover
													</Button>
												</Col>


											</Row>

											<Row className="fila-stock">

												<Col sm="0">
													<Label className="label-margen-izq"/>
												</Col>
												
												<Col sm="0">
													<Label className="label-stock-actual">
														<>
															Stock Actual:   
															{	
																//Formateo el stock de acuerdo a la unidad del producto correspondiente
																(elemento.producto.unidad=='u') ?
																(' '+ Number(elemento.producto.stock).toFixed(0) + ' ' + elemento.producto.unidad):
																(' '+ Number(elemento.producto.stock).toFixed(2) + ' ' + elemento.producto.unidad)
															} 
														</>
													</Label>
												</Col>

												<Col sm="0">
													<Label className="label-stock-resultante">

		
															<>
															Stock Resultante : 
															{
																//Formateo el stock resultante de acuerdo a la unidad del producto correspondiente	
																(elemento.producto.unidad=='u') ?
																(' '+ Number(elemento.producto.stockResultante).toFixed(0) + ' ' + elemento.producto.unidad):
																(' '+ Number(elemento.producto.stockResultante).toFixed(2) + ' ' + elemento.producto.unidad)
															} 
															</>
			
													</Label>
													<Col sm="0">
														<Label className="label-margen-der"/>
													</Col>
											
												</Col>
											</Row>

										</FormGroup>


									)
							},this)
							}


							<div className="div-resultantes">


								<Row className="fila-resultantes-cuentas">

									{
										mostrarResultantes
											?
												<>
													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															Caja:
														</Label>
													</Col>


													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															{' $ ' + Number(this.state.cuentas.caja).toFixed(2)}
														</Label>
													</Col>
												</>
											
											:
												<>
													<Col sm="0">
														<Label className="label-espaciado-resultantes"/>
													</Col>
												</>
									}



									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Total compras caja:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + Number(this.state.caja).toFixed(2)}
										</Label>
									</Col>

										
									{
										mostrarResultantes
											?
												<>
													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															Resultante:
														</Label>
													</Col>


													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															{' $ ' + Number(this.state.resultanteCaja).toFixed(2)}
														</Label>
													</Col>
												</>
											
											:
												<>
													<Col sm="0">
														<Label className="label-espaciado-resultantes"/>
													</Col>
												</>
									}

									
								</Row>


								<Row className="fila-resultantes-cuentas">


									{
										mostrarResultantes
											?
												<>
													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															Cuenta Corriente:
														</Label>
													</Col>


													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															{' $ ' + Number(this.state.cuentas.cuentaCorriente).toFixed(2)}
														</Label>
													</Col>
												</>
											
											:
												<>
													<Col sm="0">
														<Label className="label-espaciado-resultantes"/>
													</Col>
												</>
									}	
										

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Total compras c/c:
										</Label>
									</Col>

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + Number(this.state.cuentaCorriente).toFixed(2)}
										</Label>
									</Col>


									{
										mostrarResultantes
											?
												<>
													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															Resultante:
														</Label>
													</Col>

													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															{' $ ' + Number(this.state.resultanteCorriente).toFixed(2)}
														</Label>
													</Col>
												</>
											
											:
												<>
													<Col sm="0">
														<Label className="label-espaciado-resultantes"/>
													</Col>
												</>
									}	
										

								</Row>

								<Row className="fila-resultantes-cuentas">
										
									{
										mostrarResultantes
											?
												<>
													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															Crédito a favor:
														</Label>
													</Col>


													<Col sm="0">
														<Label className="label-resultantes-cuentas">
															{' $ ' + Number(this.state.cuentas.creditoAFavor).toFixed(2)}
														</Label>
													</Col>
												</>
											
											:
												<>
													<Col sm="0">
														<Label className="label-espaciado-resultantes"/>
													</Col>
												</>
									}	
									



									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Total compras a crédito:
										</Label>
									</Col>

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + Number(this.state.creditoAFavor).toFixed(2)}
										</Label>
									</Col>


								{
									mostrarResultantes
										?
											<>
												<Col sm="0">
													<Label className="label-resultantes-cuentas">
														Resultante:
													</Label>
												</Col>


												<Col sm="0">
													<Label className="label-resultantes-cuentas">
														{' $ ' + Number(this.state.resultanteCredito).toFixed(2)}
													</Label>
												</Col>            
											</>
										
										:
											<>
												<Col sm="0">
													<Label className="label-espaciado-resultantes"/>
												</Col>
											</>
								}	
									
									

								</Row>


								<div><br/></div>      


								<Row className="fila-resultantes-cuentas">

									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											Total:
										</Label>
									</Col>


									<Col sm="0">
										<Label className="label-resultantes-cuentas">
											{' $ ' + Number(this.state.total).toFixed(2)}
										</Label>
								</Col>


								</Row>
								</div>

								<Row className="fila-botones-submit">
									<Col sm="0">
										<Button
											className="boton" 
											onClick={this.agregarElemento} 
										>
											Agregar Producto
										</Button>
									</Col>
									
									
									<Col sm="0">
										<Label className="label-margen"/>
									</Col>

									
									<Col sm="0">

									{
										this.validarCuentas()
							
										?
										
											<Button className="boton" type="submit">
												Realizar compra
											</Button>
										
										:
										
											<Label className="label-stock-insuficiente">
												<h6>Tenemos alguna cuenta</h6>
												<h6>con un resultante no válido</h6>
											</Label>
									}
									</Col>
								</Row>

							</Form> 
						
						:	
						
						<h4>No hay compradores o productos registrados</h4>


						}

						<Label className="label-respuesta-api">
							{this.state.isSubmitted && this.state.resData}
						</Label>	
						
						{modoDebug && this.listaProductos() }
					</center>
				</div>
			)
		}
		catch(error){
				alert(error)
		}
		
	}
}
