import React, { Component, useState } from 'react';
import axios from 'axios';


export default class ConsultarCuentas extends Component {

	constructor(props) {
		super(props);
		this.state = {
			cuentas:{},
			cuentasFormateadas:{}
		}
	}


	componentDidMount() {

		axios.get('http://localhost:5000/cuentas')
			.then(respuesta => {
				try{
					this.setState({
					  cuentas: respuesta.data,
					  cuentasFormateadas: {
						  "caja": respuesta.data["caja"].toFixed(2),
						  "cuentaCorriente": respuesta.data["cuentaCorriente"].toFixed(2),
						  "creditoAFavor": respuesta.data["creditoAFavor"].toFixed(2)
						  }
					})
				}
				catch(error) {
					alert(error)
				}
			})
				   
			.catch((error) => {
				alert(error);
			});
	}

	render() {

		try{
			return (
				<center>
				
					<h3>Estado de cuentas: </h3>
					<br/>

					<table>
						<tbody>
							<tr>

								<th>Caja:   </th>

								<th>
									&nbsp; $ &nbsp;
									{this.state.cuentasFormateadas.caja}

								</th>
							</tr>
						</tbody>


						<tbody>
							<tr>
								<th>Cuenta Corriente:   </th>

								<th>
									&nbsp; $  &nbsp;
									{this.state.cuentasFormateadas.cuentaCorriente}

								</th>
							</tr>
						</tbody>


						<tbody>
							<tr>

								<th>Credito a Favor:   </th>

								<th>
									&nbsp; $  &nbsp;
									{this.state.cuentasFormateadas.creditoAFavor}

								</th>
							</tr>
						
						</tbody>


					</table>
				</center>

			)
		}
		catch(error){
			alert(error)
		}
	}
}
