import React from "react";
import {
	Navbar,
	Nav,
	NavItem,
	NavLink,
} from "reactstrap";

import { Link } from "react-router-dom";

const MenuCuentas  = () =>  {

	return (
		<div class="center-menu">

			<Navbar
				color="dark"      
				className="custom-navMenu"
				expand="md"
				container="md"
			>

				<Nav className="mc-auto" navbar>

					<NavItem className="custom-menu">

						<NavLink tag={Link} to={"/cuentas/ingresarRetirar"}>
							<p>Ingresar o retirar</p>
						</NavLink>

					</NavItem>         

					<>&nbsp;&nbsp;&nbsp;</>

					<NavItem className="custom-menu">
						
						<NavLink tag={Link} to={"/cuentas/consultar"}>    
							<p>Consulta de valores</p>
						</NavLink>
						
					</NavItem>

				</Nav>

			</Navbar>
			
		</div>
	);
};

export default MenuCuentas;
