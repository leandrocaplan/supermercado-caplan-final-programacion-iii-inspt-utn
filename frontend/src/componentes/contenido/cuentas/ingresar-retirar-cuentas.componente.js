import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';
//import DatePicker from 'react-datepicker';
//import "react-datepicker/dist/react-datepicker.css";

export default class IngresarRetirar extends Component {
	
	constructor(props) {

		super(props);

		this.state = {
			cuentas:{},
			cuenta:'caja',
			monto:1,
			operacion:'ingresar',
			isSubmitted:false,
			cuentaFormat:{
				"caja":"Caja",
				"cuentaCorriente": "Cuenta  Corriente",
				"creditoAFavor": "Crédito a favor"
			}
		}
	}

	componentDidMount() {

		axios.get('http://localhost:5000/cuentas')
			.then(respuesta => {
				try{
					this.setState({
						cuentas: respuesta.data
					});
				}		
				catch(error){
					alert(error)
				}     
			})

			.catch((error) => {
			alert(error);
			});
      
		console.log('Ejecuto el componentDidMount');
	}

	onChangeCuenta= (e) => {
		try {
			this.setState({
				cuenta: e.target.value,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	
	onChangeMonto = (e) => {
		try {
			this.setState({
				monto: Number(e.target.value),
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}

	onChangeOperacion = (e) => {
		try {
			this.setState({
				operacion: e.target.value,
				isSubmitted:false
			})
		}
		catch(error) {
			alert(error)
		}
	}
	
	onSubmit = (e) => {
		try {
			e.preventDefault();

			const cuentaAct = {
				cuenta: this.state.cuenta,
				monto:(this.state.operacion=='ingresar') ? this.state.monto : -this.state.monto,
				resultante: this.state.cuentas[this.state.cuenta] += (this.state.operacion=='ingresar') ? this.state.monto : -this.state.monto
			}

			this.setState(state =>
				  (
					  state.cuentas[this.state.cuenta]= cuentaAct.resultante,
					  state
				  )
			);

			if(
				(this.state.cuenta=='caja' &&  (cuentaAct.resultante >=0)) ||
				(this.state.cuenta=='cuentaCorriente' &&  (cuentaAct.resultante >=-50000)) ||
				(this.state.cuenta=='creditoAFavor')
			)
			{
				axios.post('http://localhost:5000/cuentas/actualizar', cuentaAct)
					.then(res => {
						this.setState({
							isSubmitted: true,
							resPost:res.data
						});
						console.log(res.data);
					})

					.catch(res => {
						this.setState({resPost: res.data});
						console.log(res.data);
					});
			}
		}
		catch(error) {
			alert(error)
		}
	}
	render() {
		  
		try{
			return (

				<center>
					<div className="center-ingresar-retirar">

						<h3>Ingresar o retirar dinero de cuenta</h3>
						<br/>
						<Form onSubmit={this.onSubmit}>

							<Row className="fila-ingresar-retirar">
								<Col sm="0">
									<Label className="label-ingresar-retirar">
										Cuenta:   
									</Label>
								</Col>

								<Col sm="0">

								<Input
									type="select"
									required className="inp-ingresar-retirar"
									value={this.state.cuenta}
									onChange={this.onChangeCuenta}
								>
									<option value="caja">Caja</option>
									<option value="cuentaCorriente">Cuenta Corriente</option>
									<option value="creditoAFavor">Crédito a favor</option>
								</Input>

								</Col>
							</Row>


							<Row className="fila-ingresar-retirar">
								<Col sm="0">
									<Label className="label-ingresar-retirar">
										Operacion:   
									</Label>
								</Col>

								<Col sm="0">

									<Input
									type="select" 
									required className="inp-ingresar-retirar"
									value={this.state.operacion}
									onChange={this.onChangeOperacion}
									>
										<option value="ingresar">Ingresar</option>
										<option value="retirar">Retirar</option>
									</Input>
								</Col>
							</Row>

							<Row className="fila-ingresar-retirar">
								<Col sm="0">
									<Label className="label-ingresar-retirar">
										Monto: 
									</Label>
								</Col>

								<Col sm="0"> 
									<Input 
									type="number"
									min="0"
									step="0.01"
									className="inp-monto"
									value={this.state.monto}
									onChange={this.onChangeMonto}
									/>

									
								</Col>

							</Row>
							
							<Row className="fila-ingresar-retirar"/>
							{
								(this.state.cuenta=='caja' &&  this.state.operacion=='retirar' &&  this.state.cuentas['caja']-this.state.monto <0) 
								&&
									<Row className="fila-ingresar-retirar">
										<Label className="label-saldo-insuficiente">No hay dinero suficiente en caja para efectuar el retiro</Label>
									</Row>	
							}


							{
								(this.state.cuenta=='cuentaCorriente' &&  this.state.operacion=='retirar' &&  this.state.cuentas['cuentaCorriente']-this.state.monto <-50000) 
								&&
									<Row className="fila-ingresar-retirar">
										<Label className="label-saldo-insuficiente">Se llegó al límite de descubierto</Label>
									</Row>
							}
					
							<Row className="fila-ingresar-retirar">
								<Col>
									<br/>
								</Col>
							</Row>

							<Row className="fila-ingresar-retirar">

								<Col sm="0">
									<Button type="submit" className="boton">
										Realizar Operacion
									</Button>
								</Col>

							</Row>
							
							<br/>
							
						</Form>


						<div class="center-respuesta-api">
						<Label className="label-respuesta-api">
							{this.state.isSubmitted && this.state.resPost}
						</Label>
							{this.state.isSubmitted && 
							<Label className="label-respuesta-api">	
								 { "En " + this.state.cuentaFormat[this.state.cuenta] + " nos quedo: "
									+ ' $ ' + this.state.cuentas[this.state.cuenta].toFixed(2)}
							</Label>		

							}
						</div>
					</div>
				</center> 
			)
		}
		catch(error){
			alert(error)
		}
	}
}
