import React, { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignJustify } from "@fortawesome/free-solid-svg-icons";
import {
	Navbar,
	Button,
	NavbarToggler,
	Collapse,
	Nav,
	NavItem,
	NavLink,
} from "reactstrap";

import { Link } from "react-router-dom";

const Topbar = ({ toggleSidebar }) => {
	const [topbarIsOpen, setTopbarOpen] = useState(true);
	const toggleTopbar = () => setTopbarOpen(!topbarIsOpen);

	return (

		<Navbar
			color="dark"      
			className="custom-navClass"
			expand="md"
			container="md"
		>

			<Button color="info" className="boton-abrir-cerrar" onClick={toggleSidebar}>
				<FontAwesomeIcon icon={faAlignJustify} className="icono-abrir-cerrar" />
			</Button>
			
			<NavbarToggler  onClick={toggleTopbar} />
			
			<Collapse isOpen={topbarIsOpen} navbar>

				<Nav className="mc" navbar>


					<a>&nbsp;&nbsp;</a>
					<NavItem className="topbar-item">
						<NavLink tag={Link} to={"/ventas"}>    
							Ventas
						</NavLink>
					</NavItem>

					<a>&nbsp;&nbsp;</a>

					{
						JSON.parse(sessionStorage.getItem('user')).categoria=='administrador' ?
							<>
								<NavItem className="topbar-item">
									<NavLink tag={Link} to={"/compras"}>
										Compras
									</NavLink>
								</NavItem>

								<a>&nbsp;&nbsp;</a>

								<NavItem className="topbar-item">
									<NavLink tag={Link} to={"/productos"}>
										Productos
									</NavLink>
								</NavItem>

								<a>&nbsp;&nbsp;</a>

								<NavItem className="topbar-item">
									<NavLink tag={Link} to={"/usuarios"}>
										Usuarios
									</NavLink>
								</NavItem>

								<a>&nbsp;&nbsp;</a>

								<NavItem className="topbar-item">
									<NavLink tag={Link} to={"/cuentas"}>
										Cuentas
									</NavLink>
								</NavItem>


								<a>&nbsp;&nbsp;</a>

								<NavItem className="topbar-item">
									<NavLink tag={Link} to={"/resetear"}>
										Resetear
									</NavLink>
								</NavItem>

							</> :
							<></>
					}

					<a>&nbsp;&nbsp;</a>

					<NavItem className="topbar-item">
						<NavLink tag={Link} to={"/logout"}>
							Cerrar sesión
						</NavLink>
					</NavItem>

				</Nav>

			</Collapse>
		</Navbar>

	)
};

export default Topbar;
