import React, { useState,useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';


const Resetear = () => {
	const [resData, setResData] = useState('');

	useEffect(() => {
		const confirmacion = window.confirm('¿Realmente desea resetear la base de datos?');

		if (confirmacion) {
			axios.get('http://localhost:5000/resetear')
				.then((respuesta) => {
				setResData(respuesta.data);
			})
			.catch((res) => {
				if (res.code == 'ERR_BAD_REQUEST') {
					setResData(res.response.data);
				} 
				else {
					setResData(res.message);
				}

			});
		}
	//El array vacío que le paso como parámetro a useEffect,
	//hace que dicha función se comporte de la misma manera
	//que un componentDidMount (se ejecuta una sola vez, al
	//montar el componente)
	}, []);

	return <div>{resData}</div>;
};

export default Resetear;
