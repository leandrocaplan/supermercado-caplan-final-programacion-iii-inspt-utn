import React, { Component } from 'react';
import axios from 'axios';
import { Button, Form, FormGroup, Label, Input, FormText, Col,Row } from 'reactstrap';


export default class EditarUsuario extends Component {

	constructor(props) {
		super(props);

		this.state = {
		  username: '',
		  password: '',
		  nombre: '',
		  apellido: '',
		  categoria: 'vendedor',
		  usernamesExistentes:[],
		  resData:'',
		  isSubmitted: false
		}
	}
	
	componentDidMount() {

		axios.get('http://localhost:5000/usuarios/' + this.props.match.params.id)
		  .then(respuesta => {
				try {
					this.setState({
						username: respuesta.data.usuario.username,
						usernameOriginal: respuesta.data.usuario.username,
						password: respuesta.data.usuario.password,
						nombre: respuesta.data.usuario.nombre,
						apellido: respuesta.data.usuario.apellido,
						categoria: respuesta.data.usuario.categoria,
						usernamesExistentes:respuesta.data.usernamesExistentes
							
					});
				}
				catch(error){
					alert(error)
				}
			 })
			.catch((error) => {
			  console.log(error);
			})           
	  }

	onChangeUsername = (e) => {
		try{
			this.setState({
				username: e.target.value,
				isSubmitted:false
			})
		}			
		catch(error){
			alert(error)
		}
	}
		
	onChangePassword = (e) => {
		try{
			this.setState({
				password: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error);
		}
	}
	
	onChangeNombre = (e) => {
		try{
			this.setState({
				nombre: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
			alert(error);
		}
	}
	
	onChangeApellido = (e) => {
		try{
			this.setState({
				apellido: e.target.value,
				isSubmitted:false
			})
		}
		catch(error){
				alert(error)
			}
		}
	
	onChangeCategoria = (e) => {
		try{
			this.setState({
				categoria: e.target.value,
				isSubmitted:false
			})
		}
		
		catch(error){
			alert(error)
		}		
	}
	
	onSubmit = (e) => {
		try{
			e.preventDefault();

			this.setState({isSubmitted: true})
			const usuario = {
				username: this.state.username,
				password: this.state.password,
				nombre: this.state.nombre,
				apellido: this.state.apellido,
				categoria: this.state.categoria
			}


			let modificarUsername=true;
			let noExiste=true;
			
			if(this.state.username != this.state.usernameOriginal){
				modificarUsername=window.confirm("Al modificar el nombre de usuario de un usuario, puedo obtener resultados inesperados en el sistema\n¿Está seguro de que desea proseguir?");
			}
			
			
			
			if(
				(this.state.usernameOriginal!=this.state.username) &&
				(this.state.usernamesExistentes.some(username => username==this.state.username))
			)
			{
				alert("Ya existe un usuario con ese nombre de usuario");
				noExiste=false;
			}	
						
			if(modificarUsername && noExiste) {
			axios.post('http://localhost:5000/usuarios/actualizar/' + this.props.match.params.id, usuario)
				.then(res => {
					console.log(res.data);
					this.setState({
						usernamesExistentes: this.state.usernamesExistentes
							.filter(u=> u!=this.state.usernameOriginal)
							.concat(this.state.username),
						usernameOriginal:this.state.username,
						resData: res.data,
						isSubmitted:true,	
					});
					
				})
				.catch(res => {
					
					
				   this.setState({
					   isSubmitted:true
					});
				   
				   if(res.code=="ERR_BAD_REQUEST"){	
						this.setState({resData:res.response.data});        
					}
					else {
						this.setState({resData:res.message});

					}
						
					console.log(res);
					}); 
				
			}
		}
		catch(error){
			alert(error);
		}
	}
	
	render() {
		try{
			return (
				<center>
					<div class="center-nuevo-prod-usu">
						<h3>Editar Usuario</h3>
						<br/>
						
						<Form onSubmit={this.onSubmit} className="form-nuevo-prod-usu">
				
						<Row className="fila-nuevo-prod-usu">
				
							<Col sm="0">
								<Label className="label-nuevo-prod-usu">
									Nombre de usuario:
								</Label>
							</Col>
				
							<Col sm="0">
								<Input  type="text"
									required
									className="inp-nuevo-prod-usu"
									value={this.state.username}
									onChange={this.onChangeUsername}
									/>
							</Col>
						</Row>


						<Row className="fila-nuevo-prod-usu">
				
							<Col sm="0">
								<Label className="label-nuevo-prod-usu">
									Contraseña:
								</Label>
							</Col>

							<Col sm="0">
								<Input  type="password"
								required
								className="inp-nuevo-prod-usu"
								value={this.state.password}
								onChange={this.onChangePassword}
								/>
							</Col>                
					
						</Row>


						<Row className="fila-nuevo-prod-usu">
							
							<Col sm="0">
							<Label className="label-nuevo-prod-usu">
								Nombre:
							</Label>

							</Col>
							
							<Col sm="0">
								<Input  type="text"
								required
								className="inp-nuevo-prod-usu"
								value={this.state.nombre}
								onChange={this.onChangeNombre}
								/>
							</Col>                
						</Row>


						<Row className="fila-nuevo-prod-usu">
							<Col sm="0">
								<Label className="label-nuevo-prod-usu">
									Apellido:
								</Label>
							</Col>

							<Col sm="0">
								<Input type="text"
									required
									className="inp-nuevo-prod-usu"
									value={this.state.apellido}
									onChange={this.onChangeApellido}
									/>   
							</Col>             
						</Row>


						<Row className="fila-nuevo-prod-usu">
							<Col sm="0">
								<Label className="label-nuevo-prod-usu">
									Categoría: 
								</Label>
							</Col>

							<Col sm="0">
								<Input
								type="select"
								required
								className="inp-nuevo-prod-usu"
								value={this.state.categoria}
								onChange={this.onChangeCategoria}
								>
									<option value="vendedor">Vendedor/a</option>
									<option value="administrador">Administrador/a</option>
								</Input>                
							</Col>
						</Row>


						<br/>

						<Col sm="0">
							<Button  type="submit" className="boton">Agregar Usuario</Button>
						</Col>

						</Form>
						<Label className="label-respuesta-api">
							{this.state.isSubmitted && this.state.resData}
						</Label>
					</div>
				</center>
			)
			}
		catch(error) {
			alert(error);
		}
	}
}
