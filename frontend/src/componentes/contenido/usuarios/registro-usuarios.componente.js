import React, { Component,useEffect, useState  } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Label} from 'reactstrap';

const Usuario = function(props) {
	

return(
	<tr>
		<td>{props.usuario.username}</td>
		<td>{props.usuario.password}</td>
		<td>{props.usuario.nombre}</td>
		<td>{props.usuario.apellido}</td>
		<td>{props.usuario.categoria}</td>
		<td>
			<Link to={"editar/"+props.usuario._id}>Editar</Link> | <a href="#" onClick={() => { props.eliminarUsuario(props.usuario._id) }}>Eliminar </a>
		</td>   
	</tr>
	);
}

export default class RegistroUsuarios extends Component {

  constructor(props) {

    super(props);
    this.state = {
		usuarios: [], 
     };
  }

	componentDidMount() {

	axios.get('http://localhost:5000/usuarios/')
	  .then(respuesta => {
	   
				try {
					this.setState({
					  usuarios: respuesta.data  
					  .map (
						  function(usuario){
							  if(usuario.categoria=='vendedor') usuario.categoria = 'Vendedor/a';
							  if(usuario.categoria=='administrador') usuario.categoria = 'Administrador/a';
							  return usuario;
							})
					})
				  }
					catch(error){
						alert(error)
					}
				  })
				  .catch((error) => {
					alert(error);
				  });               

  }
  
  
	eliminarUsuario = (id) => {
		try{
			axios.delete('http://localhost:5000/usuarios/'+id)
			  .then(response => { 
					this.setState({
						usuarios: this.state.usuarios.filter(usuarios => usuarios._id !== id),
						isSubmitted:true,
						resData:response.data			
					});
				  })
				  .catch(res => {
					this.setState({
						isSubmitted:true
					});

					if(res.code=="ERR_BAD_REQUEST") {
						this.setState({resData:res.response.data});
					}

					else {
						this.setState({resData:res.message});
					}

					console.log(res);
				});
		}
		
		catch(error){
			alert(error);
		}
	}

	listaUsuarios() {    
		
		try{
			return this.state.usuarios.map(usuarioActual => {
				return <Usuario usuario={usuarioActual} eliminarUsuario={this.eliminarUsuario} key={usuarioActual._id}  />;
			})
		}
		catch(error) {
			alert(error)
		}
	}
	
	render() {
		try{
			return(
			<div>
			
				<h3>Usuarios Registrados</h3>
			
				<table className="table">
					<thead>
						<tr>
							<th>Nombre de usuario</th>
							<th>Contraseña</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Categoría</th>
							<th>Opciones</th>
						</tr>
					</thead>

					<tbody>
						{ this.listaUsuarios() }
					</tbody>
					
				</table>
				<center>
					<Label className="label-respuesta-api">
						{this.state.isSubmitted && this.state.resData}
					</Label>
				</center>
			</div>   
			);
		}
		
		catch(error) {
			alert(error)
		}
	}
}
