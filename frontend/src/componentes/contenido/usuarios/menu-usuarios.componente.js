import React from "react";
import {
  Navbar,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";

const MenuUsuarios  = () =>  {

  return (
		<div class="center-menu">
			<Navbar
				color="dark"      
				className="custom-navMenu"
				expand="md"
				container="md"
			>

				<Nav className="mc-auto" navbar>

					<NavItem className="custom-menu">
						<NavLink tag={Link} to={"/usuarios/nuevo"}>
							<p>Nuevo usuario</p>
						</NavLink>
					</NavItem>         


					<>&nbsp;&nbsp;&nbsp;</>


					<NavItem className="custom-menu">
						<NavLink tag={Link} to={"/usuarios/registro"}>    
							<p>Registro de usuarios</p>
						</NavLink>
					</NavItem>

				</Nav>

			</Navbar>
		</div>
  );
};
export default MenuUsuarios;
