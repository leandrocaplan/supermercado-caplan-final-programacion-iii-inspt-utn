import React, { useState } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import SideBar from "./componentes/sidebar/SideBar";
import Contenido from "./componentes/contenido/Contenido";

const Wrapper = () => {
  const [sidebarIsOpen, setSidebarOpen] = useState(true);
  const toggleSidebar = () => setSidebarOpen(!sidebarIsOpen);

  return (
    <Router>
      <div className="App Wrapper">
        <SideBar toggle={toggleSidebar} isOpen={sidebarIsOpen} />
        <Contenido toggleSidebar={toggleSidebar} sidebarIsOpen={sidebarIsOpen} />
      </div>
    </Router>
  );
};

export default Wrapper;
