import axios from 'axios';
import React, { useState } from "react";
import { BrowserRouter as Router, Route} from "react-router-dom";
import { Link,Switch,Redirect } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css";
import SideBar from "./componentes/sidebar/SideBar";
import Contenido from "./componentes/contenido/Contenido";
import "./App.css";
import Wrapper from "./Wrapper";

async function getUsers() {
	try{  
		let usuarios;
		await axios.get('http://localhost:5000/usuarios/')
			.then(async response  =>  {
				if (response.data.length > 0) {
					usuarios=  await response.data;
				}
			})
			.catch( async(error)  => {
				console.log(error);
			})
		  
		  return usuarios;
  }
  
	catch(error){
		alert(error);
	}
}


const App = () => {
  
	const [sidebarIsOpen, setSidebarOpen] = useState(true);
	const toggleSidebar = () => setSidebarOpen(!sidebarIsOpen);  
	const [errorMessages, setErrorMessages] = useState({});
	const [isSubmitted, setIsSubmitted] = useState(false);


	try{
		const errors = {
			username: "Usuario inválido",
			pass: "Contraseña inválida"
		};

		const onSubmit = async (event) => {
			//Esta linea evita que se recargue la página
			await event.preventDefault();

			const usuarios = await getUsers();
			var { username, pass } = await document.forms[0];


			const userData = await usuarios.find((user) => user.username === username.value);


			await sessionStorage.setItem('user', JSON.stringify(userData));
			if (userData) {
				if (userData.password !== pass.value) {

					await setErrorMessages({ name: "pass", message: errors.pass });
				}
				else {
					await setIsSubmitted(true);
				}
			}
			else {
				await setErrorMessages({ name: "username", message: errors.username });
			}
			await console.log(userData);

			await console.log(sessionStorage);
	  };


	  const renderErrorMessage = (name) =>
		name === errorMessages.name && (
		  <div className="error">{errorMessages.message}</div>
		);

	
	  const renderForm = (
		
		<center>
			<br/>
			<h3>Iniciar sesión</h3>
			<br/>
			<form onSubmit={onSubmit}>

				<center>
					<table> 
						<tbody>
							<tr>
								<th>Nombre de usuario:   </th>
								<th>&emsp;</th>
								<th>
									<input type="text" name="username" required />
									{renderErrorMessage("username")} 
							<	/th>
							</tr>
						</tbody>

						<tbody>
							<tr>
								<th>Contraseña:</th>
								<th>&emsp;</th>
								<th>
									<input type="password" name="pass" required />
									{renderErrorMessage("pass")}
								</th>                
							</tr>
						</tbody>
					</table>
	
					<br/>
					<div className="button-container">
					<input type="submit" value="Ingresar" />
					</div>     
				</center>
			</form>
		</center>
	  );

		return (
			<div className="app">
				<div className="login-form">
					<div className="rotulo">
						<h1 className="rotulo">Sistema de administración del Supermercado Caplan</h1>
					</div>

					{
						isSubmitted 
						
						?
							<Router>
								<Route path="/wrapper" exact component={Wrapper} />
								<Wrapper/>
							</Router>
						: 
						
							renderForm
					}
				</div>
			</div>
		);

	}
	catch(error){
		alert(error);
	}
}
export default App;
