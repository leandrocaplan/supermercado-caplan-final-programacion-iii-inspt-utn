const router = require('express').Router();

let Usuario = require('../modelos/modelo.usuario');

//Devuelve todos los usuarios
router.route('/').get(async(req, res) => {
	try {
		const users=await Usuario.find();
		return res.json(users);
	}
    catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});

//Obtengo un usuario por su ID
router.route('/:id').get(async(req, res) => {
  
  try {
		const user = await Usuario.findById(req.params.id);
		const usernamesExistentes =(await Usuario.find()).map(u=> u.username);
		return res.json({
			usuario:user,
			usernamesExistentes:usernamesExistentes
		});
	}
    catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});

router.route('/agregar').post(async(req, res) => {
	
	try { 
		const username = req.body.username;
		const password =req.body.password;
		const nombre = req.body.nombre;
		const apellido = req.body.apellido;
		const categoria = req.body.categoria;

		const usuarios=await Usuario.find();
		const arrayUsernamesExistentes=usuarios.map(
			function(usuario){
				return (usuario.username);
			}
		);
		
		if(
			(! (arrayUsernamesExistentes.some(usernameExistente => username==usernameExistente)))
		) 
		{
			const nuevoUsuario = new Usuario({username,password,nombre,apellido,categoria});
			await nuevoUsuario.save();
			return res.json('¡Usuario agregado!');
		}
		else {
			return res.json('Validacion fallida');
		}

	}
	catch(err) {
			console.log(err);
			return res.status(400).json('Error: ' + err);
    }
});

router.route('/:id').delete(async(req, res) => {
	try{
		await Usuario.findByIdAndDelete(req.params.id);
		return res.json('Usuario eliminado');
	}
	catch(err) {
			console.log(err);
			return res.status(400).json('Error: ' + err);
    }
});

router.route('/actualizar/:id').post(async(req, res) => {
	try{
		let usuario=await Usuario.findById(req.params.id);

		usuario.username = req.body.username;
		usuario.password = req.body.password;
		usuario.nombre = req.body.nombre;
		usuario.apellido = req.body.apellido;
		usuario.categoria = req.body.categoria; 

		await usuario.save();
		return res.json('¡Usuario actualizado!');
		
	}
	catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
});

module.exports = router;
