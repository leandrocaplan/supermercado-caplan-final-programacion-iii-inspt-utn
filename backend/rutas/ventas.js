const router = require('express').Router();

let Venta = require('../modelos/modelo.venta');
let Producto = require('../modelos/modelo.producto');
let Usuario = require('../modelos/modelo.usuario');
let Cuentas = require('../modelos/modelo.cuentas');

//Esta const identifica al único documento de la colección de cuentas
const unico="000000000000000000000000";

//Recibo una fecha y hora en formato ISO y devuelvo la
//fecha como  un string de la forma "dd/mm/aaaa"
const formatearFecha =function (fecha) {
    const fechaObj=new Date(fecha);
    return (
		fechaObj.getDate().toString().padStart(2, "0") + '/'+
		(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+
		fechaObj.getFullYear()
	);
}

//Recibo una fecha y hora en formato ISO y devuelvo la
//hora como un string de la forma "hh:mm:ss"
const formatearHora =function (fecha) {
    try {
        const fechaObj=new Date(fecha);
        return (
			fechaObj.getHours().toString().padStart(2, "0") + ':'+
			(fechaObj.getMinutes()).toString().padStart(2, "0") + ':'+
			fechaObj.getSeconds().toString().padStart(2, "0")
		);
    }
    catch(error) {
        alert(error)
    }
}

//Asocio cada tipo de pago a cada cuenta contable
const tipoPagoACuenta = {
	'efectivo':'caja',
	'debito':'cuentaCorriente',
	'credito':'creditoAFavor'
};

//Devuelvo todas las ventas existentes
router.route('/').get(async (req, res) => {
    //console.log("entro al get");
    try {
        const ventas= await Venta.find();
        return res.json(ventas);
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});

//Agrego una nueva venta
router.route('/agregar').post( async (req, res) => {
    
    try{
		//Debo validar, elemento por elemento, el detalle.
		//Si hay algun elemento del detalle que no cumple
		//la validación, no puedo guardar la venta

		error=await validarDetalle(req.body.detalle);
		
		//Me traigo el listado de ventas realizadas hasta ahora
		ventasRealizadas = await listarVentas();

		if(!error) {

			const codVenta = await Number(req.body.codVenta);
			const vendedor = await req.body.userVendedor;
			const detalle= await req.body.detalle;

			const fechaHora= await req.body.fechaHora;

			const ventaBuscada= await ventasRealizadas.find (
				function(ventaRealizada) {

					return
					(
						(ventaRealizada.codVenta == codVenta) &&
						(ventaRealizada.fecha == formatearFecha(fechaHora)) &&
						(ventaRealizada.vendedor == vendedor)
					)
				}
			 );
			
			//Me fijo si ya existe una venta con el mismo código, fecha y vendedor
			if(ventaBuscada)
			
			//Si existe, agrego el detalle a la venta existente y la guardo
			{
				let ventaAActualizar=await Venta.findById(ventaBuscada.id);
				ventaAActualizar.detalle= await ventaAActualizar.detalle.concat(detalle);
				await ventaAActualizar.save();
			}

			//Si no, genero una nueva venta y la guardo (debo validar que tenga al menos un elemento en el detalle)
			else if(detalle.length>0){
				const nuevaVenta = await new Venta({
					codVenta,
					vendedor,
					detalle,
					fechaHora
				});

				await nuevaVenta.save();
			}
			for (const elemDetalle of req.body.detalle) {
				await actStockCuentas(elemDetalle,res);

			}
			return res.send("Venta exitosa");

		}
		else{
			return res.send("Validacion fallida");
		}

	}

	catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});

//Valido que el detalle de ventas no me genere un stock negativo
//en ningun producto
const validarDetalle  = async function (detalle) {
	try {
		let error=false;
		
		//Valido primero que no haya cantidades ni subtotales menores o iguales 0
		for (let elemDetalle of detalle) {
			if(Number(elemDetalle.cantidad)<=0 || Number(elemDetalle.subtotal)<=0){
					error=true;
				}
		}
		
		//Hay que hacer primero, un array con cada elemento del detalle asociado a cada producto
		//ya que el detalle puede llegar a tener varios elementos asociados
		//al mismo producto, con cantidades distintas
		//Necesito el total de cada cantidad asociada a cada producto
		
		
		//Podría generar un array con todos los productos incluidos en el detalle,
		//y luego utilizar un reduce para obtener el total de la cantidad vendida
		//de cada uno de ellos.
		//Sin embargo, no me conviene, ya que puedo generar directamente
		//el array con los valores correspondientes al total vendido de
		//cada producto (debería hacer un reduce por cada producto devuelto
		//en un array generado previamente, lo cual no es práctico).

		//Genero un objeto, inicialmente vacio, llamado 'productoCantidad'
		const productoCantidad = {};
		//Este objeto tendrá como clave el ID del producto,
		//y como valor, la cantidad total asociada a dicho producto
		for (let elemDetalle of detalle) {
			//A la cantidad asociada al producto donde estoy parado,
			//le asigno la cantidad que tiene ahora, o 0 si aún no existe
			productoCantidad[elemDetalle.producto] = Number(productoCantidad[elemDetalle.producto]) || 0;
			
			//A la cantidad que obtuve en la linea anterior, le sumo
			//la cantidad correspondiente al elemento del detalle
			//donde estoy parado
			productoCantidad[elemDetalle.producto] += Number(elemDetalle.cantidad);
		}
		
		//Convierto el objeto anterior en un array
		//Deprecacion: podria utilizar directamente el
		//objeto en lugar del array
		const arrayProductosCantidades = [];
		for (let producto in productoCantidad) {
			const cantidad = productoCantidad[producto];
			arrayProductosCantidades.push({
				producto,
				cantidad
			});
		}

		//Una vez generado el array, valido que la cantidad de cada producto vendido
		//no me genere un stock resultante negativo en ningun producto
		for (const elemProdCant of arrayProductosCantidades) {

			const producto = await Number(elemProdCant.producto);
			const cantidad = await Number(elemProdCant.cantidad);

			let productoActualizado= await Producto.findById(elemProdCant.producto);
			
			//Valido stock: Nunca me puede quedar un stock negativo
			//No valido las cuentas, ya que no tienen límite superior
			if(Number(productoActualizado.stock) - Number(cantidad) < 0) {
				error=true;
			}
		}
		
		return error;
	}
	catch(err) {
        console.log(err);
		return err;
    }
}

//Actualizo el stock y las cuentas cada vez que registro un nuevo detalle
const actStockCuentas  = async function (elemDetalle) {
	try {
		//Busco el producto a actualizar por su ID
		let productoActualizado= await Producto.findById(elemDetalle.producto);
		
		//Busco las cuentas contables para actualizarlas
		let cuentas= await Cuentas.findById(unico);

		//Valido que el producto siga existiendo en la base de datos
		if(productoActualizado) {
		
			//Al stock del producto buscado, le resto la cantidad vendida
			productoActualizado.stock = Number(productoActualizado.stock) - Number(elemDetalle.cantidad);
			
			
			//A la cuenta contable asociada al tipo de pago, le sumo el subtotal correspondiente
			//al elemento del detalle.
			cuentas[tipoPagoACuenta[elemDetalle.tipoPago]] = Number(cuentas[tipoPagoACuenta[elemDetalle.tipoPago]]) + Number(elemDetalle.subtotal);
			
			//Guardo el producto con el stock actualizado
			await productoActualizado.save();
			
			//Guardo la cuenta contable correspondiente actualizada
			await cuentas.save();
		}

	}
	catch(err) {
		console.log(err);
		return err;
    }
}

//Devuelvo un registro de venta por su id
router.route('/id/:id').get( async (req, res) => {
    try{
		const venta=await Venta.findById(req.params.id);
		return res.json(venta);
	}	
	catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});

//Busco un detalle de venta por su ID.
//Devuelvo el detalle, la lista completa de ventas,
//y el ID de la venta a la cual está asociada el
//detalle.
const buscarDetalle= async function (idDetalle,idVentaAsociada) {
	try{
		//Me guardo la lista completa de ventas
		const ventas=await Venta.find();

		//Busco la venta cuyo ID es el que recibí por parámetro
		const venta=await Venta.findById(idVentaAsociada);
		//Inicializo la variable 'detalle' con un objeto vacío
		let detalle= {};

	  
		//Si en la venta que busque tengo un elemento de detalle con
		//el ID que recibí por parámetro
		if(venta && venta.detalle.some(det => det._id==idDetalle)) {		
			//En la variable 'detalle' me guardo el elemento
			//del detalle que encontré.
			detalle=venta.detalle.find(det => det._id==idDetalle);
			
		}

		//Luego, devuelvo el detalle completo,
		//la lista de ventas, y el ID de la venta
		//asociada al detalle.
		return {detalle,ventas,idVentaAsociada};
	}
	catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
}

//Devuelvo un registro de venta por su id, con la lista de vendedores completa
router.route('/detalleId/:idDetalle/:idVenta').get( async (req, res) => {      
	try{	
		const detalleVentas= await buscarDetalle(req.params.idDetalle,req.params.idVenta);
		const vendedores= await Usuario.find({categoria: "vendedor"});
		const producto= await Producto.findById(detalleVentas.detalle.producto);
		return res.json({detalleVentas,vendedores,producto});
	}
	catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});

//Devuelvo los elementos necesarios para asociar cada una de las ventas
//con su correspondiente usuario, producto, y cuenta contable asignada

router.route('/asociar').get( async(req, res) => {

    try {
        const ventas=await Venta.find();
        const usuarios= await Usuario.find({categoria: "vendedor"});
        const productos= await Producto.find();
        const cuentas= await Cuentas.findById(unico);
        return res.json({
			"ventas":ventas,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

})

//Recibo por parámetro la lista completa de ventas
//Devuelvo un array plano con cada elemento de detalle
//de venta existente.

//Cada elemento devuelto, contendrá tanto la información
//del elemento del detallle,
//como de la venta a la cual está asociado el mismo.

const aplanarDetalles=function (ventas) {
    try {
		//Declaro un array inicialmente vacío
		let arrayVentasPlano=[];

		//Con un for, recorro la lista de ventas
		//completa recibida por parámetro
		
		//Podriamos utilizar un map en lugar de un for,
		//el cual lo dejamos como comentario:
		//ventas.map(function (venta){
		for (const venta of ventas) {
			
			//Declaro una variable 'detallePlano' a la que le asigno
			//un objeto, el cual contendrá todos los campos
			//que voy a devolver en cada elemento de la lista
			//completa de detalles.
			//Inicialmente, todos estos campos están
			//inicializados con 0 o un string vacío,
			//según corresponda a cada tipo de campo (número o string). 
			let detallePlano= {
				_id:'',
				idVentaAsociada:'',
				codVenta:0,
				vendedor:'',
				producto:0,
				cantidad:0,
				tipoPago:'',
				subtotal:0
			};
			
			//Inicialmente, me guardo en 'detallePlano'
			//todos los valores correspondientes a la venta
			//a la cual está asociada el elemento de detalle
			detallePlano.idVentaAsociada=venta._id;
			detallePlano.codVenta=venta.codVenta;
			detallePlano.vendedor=venta.vendedor;
			detallePlano.fechaHora=venta.fechaHora;
			
			//Luego, en otro for anidado, recorro
			//todos los elementos del detalle correspondientes
			//a la venta en la cual estoy parado
			
			//Podriamos utilizar un map en lugar de un for,
			//el cual lo dejamos como comentario:
			
			//venta.detalle.map(function (elemDetalle){	
			for (const elemDetalle of venta.detalle) {  
			
				//Aqui, en 'detallePlano', me guardo
				//todos los valores correspondientes
				//al elemento del detalle donde estoy
				//parado
				detallePlano._id=elemDetalle._id;
				detallePlano.producto=elemDetalle.producto;
				detallePlano.precioActual=elemDetalle.precioActual;
				detallePlano.unidadActual=elemDetalle.unidadActual;
				detallePlano.cantidad=elemDetalle.cantidad;
				detallePlano.tipoPago=elemDetalle.tipoPago;
				detallePlano.subtotal=elemDetalle.subtotal
				
				//Una vez que tengo cargado en 'detallePlano'
				//todos los valores que necesito, tanto correspondientes
				//a la venta completa como del elemento del detalle
				//en el que estoy parado,
				//concateno el objeto al array declarado fuera
				//de ambos for anidados.
				arrayVentasPlano=arrayVentasPlano.concat({...detallePlano});
			
			//Dejamos comentado lo que correspondería al cierre del map anidado
			//})
			}
		//Dejamos comentado lo que correspondería al cierre del map de nivel superior
		//})
		}

		
		//Una vez que recorrí y agregué al array
		//cada elemento de detalle de cada venta existente,
		//devuelvo el array, en el que incluí todos estos elementos.
		return arrayVentasPlano;
	}
	catch(err) {
        console.log(err);
        return err;
    }
}

//Devuelvo los elementos necesarios para asociar cada una de las ventas
//con su correspondiente usuario, producto, y cuenta contable asignada

router.route('/asociarAplanar').get( async(req, res) => {

    try {
        const ventas=await Venta.find();
        const detallesAplanados= await aplanarDetalles(ventas);
		
        const usuarios= await Usuario.find({categoria: "vendedor"});
        const productos= await Producto.find();
        const cuentas= await Cuentas.findById(unico);
        return res.json({

			"detallesVentas":detallesAplanados,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

})

//Sobrecargo la funcion anterior recibiendo un parámetro
//por URL: en lugar de devolver todos los elementos
//de detalle de todas las ventas, devuelvo solo los
//asociados al ID de venta que recibí por parámetro
router.route('/asociarAplanar/:idVenta').get( async(req, res) => {
//console.log(req.params.idVenta);
    try {
		//Me traigo solo la venta cuyo ID recibí por parámetro
        const venta=await Venta.findById(req.params.idVenta);
        const detallesAplanados= await aplanarDetalles([venta]);
		
        const usuarios= await Usuario.find({categoria: "vendedor"});
        const productos= await Producto.find();
        const cuentas= await Cuentas.findById(unico);
        return res.json({
			"detallesVentas":detallesAplanados,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

})

//Devuelvo los elementos necesarios para asociar una venta por su id,
//con su correspondiente usuario, producto, y cuenta contable asignada

router.route('/asociar/id/:id').get( async (req, res) => {
    //console.log("Entro a asociar");
    //console.log(req.params.id);

    try {
        const venta = await Venta.findById(req.params.id);
        const usuarios= await Usuario.find({categoria: "vendedor"});
        const productos= await Producto.find();
        const cuentas= await Cuentas.findById(unico);

        return res.json({
			"venta":venta,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});

router.route('/asociarDetalle/id/:idDetalle/:idVenta').get( async(req, res) => {
    //console.log("Entro a asociarDetalle");

	try{
		const venta=await Venta.findById(req.params.idVenta);
		const usuarios=await Usuario.find({categoria: "vendedor"} );
		const productos= await Producto.find();
		const cuentas= await Cuentas.findById(unico);
		//console.log(venta);
		const detalle= await venta.detalle.find(elemDetalle => elemDetalle._id==req.params.idDetalle);

		return res.json({
			//Deprecacion: renombrar el campo "venta" por "detalle"
			"venta":detalle,
			//Deprecacion: este campo no lo necesito
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
		});
	}
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});

//Esta función nos sirve para realizar la validación para eliminar varios detalles
//de una sola vez. Si la validación falla, el sistema no nos debería permitir
//eliminar ninguno de los detalles recibidos en el array.
const validarEliminarDetalles  = async function (elementosAEliminar,actStock,actCuentas) {
	try{
		let resultadoValidacionStock=true;
		let resultadoValidacionCuentas=true;
		if(actStock || actCuentas) {
			//Por parámetro solo recibo un array con los IDs de los elementos
			//del detalle que quiero eliminar, y los IDs de la venta a la
			//cual están asignados

			//Para realizar la validacion necesito
			//generarme un array con los datos completos de dichos elementos.

			//Al tener los ID de las ventas completas
			//afectadas, no es necesario traerme todas las ventas existentes
			//en la base de datos para realizar dicha validación,
			//por lo que me traigo solo las ventas correspondientes
			//a los IDs que recibí por parámetro


			//Me genero un array, inicialmente vacío, que contendrá
			//todas las ventas completas afectadas
			let ventas = [];

			//Recorro el listado de elementos a eliminar que recibí por parámetro
			for (let elementoAEliminar of elementosAEliminar) {

				//Si en el array que estoy generando no existe una venta con el ID
				//del listado que recibí por parámero, o bien el array está vacío,
				//le concateno la venta identificada con dicho ID

				if(!ventas.some(

					function(venta) {
						return (venta._id==elementoAEliminar.idVenta)
					})

					||

					ventas.length==0
				)
				{
					ventas=ventas.concat(
						await Venta.findById(elementoAEliminar.idVenta)
					)
				}
			}

			//Una vez que generé el array con las ventas completas,
			//utilizo la funcion aplanarDetalles, para traerme
			//todos los detalles completos existentes en cada una
			//de esas ventas

			//Si bien esta función me traerá infomación redundante, ya que
			//no necesito luego el ID de la venta asociada a cada detalle,
			//es conveniente utilizarla, de manera que reutilizo
			//código ya existente en lugar de generar una funcion nueva
			//solamente para el propósito de esta validación.
			const listaDetallesAplanada= await aplanarDetalles(ventas);


			//De todos los elementos de detalle completos que obtuve, filtro
			//me quedo solamente con los que tienen un ID que se corresponda
			//con la lista de IDs de detalles que recibí por parámetro
			const detallesCompletos=listaDetallesAplanada.filter(function(elemDetalle) {
				if(
					elementosAEliminar.some(
						function(elementoAEliminar) {
							return(elementoAEliminar.idDetalle==elemDetalle._id)
						}
					)
				)
				{
					return elemDetalle
				}
			});
			
			//Una vez que tengo la lista completa de detalles afectados, puedo hacer las
			//validaciones correspondientes de stock y cuentas

			//Hay que hacer primero, un array con cada elemento del detalle asociado a cada producto
			//ya que el detalle puede llegar a tener varios elementos asociados
			//al mismo producto, con cantidades distintas
			//Necesito el total de cada cantidad asociada a cada producto

			if(actStock) {
				
				//Valido primero que no haya cantidades ni subtotales menores a 0
				for (let elemDetalle of detallesCompletos) {
					if(Number(elemDetalle.cantidad)<=0 || Number(elemDetalle.subtotal)<=0){
						resultadoValidacionStock=false;
					}
				}
		
				const productoCantidad = {};
				for (let elemDetalle of detallesCompletos) {
					productoCantidad[elemDetalle.producto] = Number(productoCantidad[elemDetalle.producto]) || 0;
					productoCantidad[elemDetalle.producto] += Number(elemDetalle.cantidad);
				}

				const arrayProductosCantidades = [];
				for (let producto in productoCantidad) {
					const cantidad = productoCantidad[producto];
					arrayProductosCantidades.push({
						producto,
						cantidad
					});
				}

				//Una vez generado el array, valido que la cantidad de cada producto vendido
				//no me genere un stock resultante negativo en ningun producto
				for (const elemProdCant of arrayProductosCantidades) {

					const producto = await Number(elemProdCant.producto);
					const cantidad = await Number(elemProdCant.cantidad);
					let productoActualizado= await Producto.findById(elemProdCant.producto);

					//Valido stock: Nunca me puede quedar un stock superior al original,
					//ni negativo.
					//Valido ademas que el producto siga existiendo en mi base de datos
					if(productoActualizado && Number(productoActualizado.stock) + Number(cantidad) > Number(productoActualizado.stockOriginal)) {
						resultadoValidacionStock=false;
					}
				}
			}

			//Necesito validar el resultante de las cuentas contables,
			//de manera que no pueda tener un resultante inferior al límite
			//de cada cuenta correspondiente

			//Aquí si me conviene, a diferencia de la validación del detalle
			//de ventas, utilizar un reduce, ya que no me interesa controlar
			//el stock de productos vendidos, sinó unicamente el resultante
			//de cada cuenta contable
			if(actCuentas) {
				
				//Valido primero que no haya cantidades ni subtotales menores a 0
				for (let elemDetalle of detallesCompletos) {
					if(Number(elemDetalle.cantidad)<=0 || Number(elemDetalle.subtotal)<=0){
							resultadoValidacionCuentas=false;
						}
				}
				
				const cuentas= await Cuentas.findById(unico);
				const totalCaja=detallesCompletos
					.filter(function(elemDetalle) {
						if(elemDetalle.tipoPago=='efectivo') {
							return elemDetalle;
						}
					})
					.reduce(function(acumulador,elemDetalle) {

						return acumulador + Number(elemDetalle.subtotal);

					}, 0);


				const totalCorriente=detallesCompletos
					.filter(function(elemDetalle) {
						if(elemDetalle.tipoPago=='debito') {
							return elemDetalle;
						}
					})
					.reduce(function(acumulador,elemDetalle) {

						return acumulador + Number(elemDetalle.subtotal);

					}, 0);


				//No necesito hacer el reduce de la cuenta
				//"Crédito a favor", ya que esta no tiene límite
				//inferior ni superior


				//Una vez obtenidos los totales correspondientes a cada forma
				//de pago, valido que los resultantes no resulten inferiores
				//al límite de cada cuenta contable.
				if
				(
					//Si en caja me queda un resultante menor a cero (no puedo tener efectivo negativo)
					(cuentas['caja'] - totalCaja  < 0) ||
					//Si en cuenta corriente me queda un resultante menor al límite de descubierto
					(cuentas['cuentaCorriente'] - totalCorriente  < -50000)
				)
				{

					resultadoValidacionCuentas=false;
				}
			}
		}
		//Para que la validacion devuelva true,
		//tanto el flag de validacion de stock como
		//de cuentas deben valer true
		return (resultadoValidacionStock && resultadoValidacionCuentas);
	}
	catch(err) {
        console.log(err);
        return err;

    }
}


//Elimino un registro de detalle de venta
//Luego, actualizo la venta completa asociada a dicho detalle

//Elimino una lista de elementos de detalles, en un array recibido desde el front
router.route('/eliminarDetalles').post( async (req, res)  => {
    //console.log(req.body);
    try {

        //Esta lista de detalles a eliminar,
        const detallesAEliminar=req.body.listaDetalles;
        
        const actStock=req.body.actStock;
        const actCuentas=req.body.actCuentas;
        const resultadoValidacion= await validarEliminarDetalles(detallesAEliminar,actStock,actCuentas);
		

        if(resultadoValidacion) {
            let resultado;
            for (const detalleAEliminar of detallesAEliminar) {
                resultado= await eliminarDetalle(detalleAEliminar.idDetalle,detalleAEliminar.idVenta,actStock,actCuentas);
            }
            return res.json("Elementos eliminados exitosamente");
        }
        else {
            return res.json("Hubo una falla en la validacion");
        }
    }
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});


router.route('/eliminarDetalle/:idDetalle/:idVenta/:actStock/:actCuentas').delete( async (req, res)  => {
	try{
		const actStock = (req.params.actStock === "true");
		const actCuentas = (req.params.actCuentas === "true");
		const resultado= await eliminarDetalle(req.params.idDetalle,req.params.idVenta,actStock,actCuentas);
		return res.json(resultado);
	}
	catch (err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
	
});

const eliminarDetalle = async function (idDetalle,idVenta,actStock,actCuentas) {
	try{
		//Tenemos cuatro casos posibles, dependiendo de si las opciones
		//de actualizar stock y actualizar cuenta están o no activadas


		//Recibo como parámetro tanto el id del detalle como el de la venta completa

		const ventaAActualizar=await Venta.findById(idVenta);		
		const detalleAEliminar=await ventaAActualizar.detalle.find(elem => elem._id==idDetalle) || null;
		const producto= await Producto.findById(detalleAEliminar.producto);
		const cuentas= await Cuentas.findById(unico);		


		if //Validacion

		(
			
			//Valido que la compra y el detalle sigan existiendo en la base de datos
			(ventaAActualizar && detalleAEliminar) &&
			//Valido  que la cantidad y el subtotal sean mayores a 0
			(Number(detalleAEliminar.cantidad)>0 && Number(detalleAEliminar.subtotal)>0) &&
			
			//Valido el resultante de las cuentas (no me puede quedar ninguna cuenta por debajo de su límite)
			(
				(!actCuentas) ||
				(detalleAEliminar.tipoPago =='efectivo' && (cuentas['caja'] - detalleAEliminar.subtotal >= 0)  ) ||
				(detalleAEliminar.tipoPago =='debito' && (cuentas['cuentaCorriente'] - detalleAEliminar.subtotal >= -50000) ) ||
				(detalleAEliminar.tipoPago =='credito')
			)
			&&
			//Valido el resultante de stock (no me puede quedar ningun producto por encima de su stock original
			//O que el producto no siga existiendo en mi base de datos
			(
				(!actStock)||
				(!producto)||
				( Number(producto.stock)  + Number(detalleAEliminar.cantidad) <= producto.stockOriginal ) 
				
			)

		)
		{
		//Actualizo la venta eliminando el detalle correspondiente
		ventaAActualizar.detalle= await ventaAActualizar.detalle.filter(
			function(elemDetalle){
				if(elemDetalle._id != idDetalle){
					return elemDetalle;
					}
				}
		);

		//Guardo la venta sin el detalle correspondiente

		if(ventaAActualizar.detalle.length>0) {
			await ventaAActualizar.save();
		}

		//Si no me quedaron elementos en el detalle de venta, la elimino
		else {
			await ventaAActualizar.remove();
		}
		//Entro a este if si la opción de actualizar stock está activada,
		//y el producto asociado al detalle de venta aún existe


		if(actStock && producto) {
			producto.stock = Number(producto.stock) +  Number(detalleAEliminar.cantidad);
			await producto.save();
		}
		
		//Entro a este if si la opcion de actualizar cuentas
		//está activada
		if(actCuentas) {

			//A la cuenta contable asociada al tipo de pago, le deduzco el subtotal
			//del elemento del detalle eliminado
			cuentas[tipoPagoACuenta[detalleAEliminar.tipoPago]] -= Number(detalleAEliminar.subtotal);

			//Actualizo la base de datos con el valor resultante al
			//eliminar el detalle de la venta
			await cuentas.save()
		}

		//Devuelvo un string indicando que el detalle fue eliminado
		return 'Detalle eliminado';
		}


		//Si el producto fue dado de baja o está desactivado actSock
		//solo actualizao las cuentas

		//Cierro if de validacion

		//Aqui entro si falló la validación
		else {
			return 'Validacion fallida';
		}
	}
	catch(err) {
		return err;
        console.log(err);
    }
}

//Esta función nos sirve para realizar la validación para eliminar varios detalles
//de una sola vez. Si la validación falla, el sistema no nos debería permitir
//eliminar ninguno de los detalles recibidos en el array.
//Como en este caso, al eliminar una venta, estamos deduciendo el monto
//de cada cuenta contable correspondiente a cada elemento del detalle, tendremos una
//funcion muy similar a 'validarDetalle' del módulo de ventas.

//Actualizo un detalle de venta
router.route('/actualizarDetalle/id/:idDetalle/:idVenta').post( async (req, res) => {
    const actStock=req.body.actStock;
    const actCuentas=req.body.actCuentas;


    try {

        //Declaro variables para guardarme informacion de la venta previa a la actualizacion
        let idProductoAnterior;
        let cantVentaAnterior;
        let tipoPagoAnterior;
        let subtotalAnterior;

		
        let ventaAActualizar=await Venta.findById(req.params.idVenta);
        let detalleAActualizar= await ventaAActualizar.detalle.find(idDetalle => idDetalle._id==req.params.idDetalle) || null;
		

        //En dichas variables, me cargo la informacion de la venta anterior
        //Ademas, me guardo los datos de la venta actualizada
        [idProductoAnterior,detalleAActualizar.producto] = [detalleAActualizar.producto,req.body.idProducto];
        [cantVentaAnterior,detalleAActualizar.cantidad] = [detalleAActualizar.cantidad,Number(req.body.cantidad)];
        [tipoPagoAnterior,detalleAActualizar.tipoPago ] =[detalleAActualizar.tipoPago,req.body.tipoPago];
        [subtotalAnterior, detalleAActualizar.subtotal ] =[detalleAActualizar.subtotal,Number(req.body.subtotal)];
	
		//Actualizo precio y unidad
		detalleAActualizar.precioActual=req.body.precioActual;
		detalleAActualizar.unidadActual=req.body.unidadActual;

		

        let productoActualizado= await Producto.findById(detalleAActualizar.producto);
        let productoAnterior= await Producto.findById(idProductoAnterior);
        let cuentas= await Cuentas.findById(unico);


        //Validación
        if (

			(ventaAActualizar && detalleAActualizar) &&
			//Valido primero que las cantidades y los
			//subtotales sean mayores a 0
			(
				Number(detalleAActualizar.cantidad>0) &&
				Number(cantVentaAnterior>0)  &&
				Number(detalleAActualizar.subtotal>0) &&
				Number(subtotalAnterior>0)
			)
			
			&&
			
            //Validacion de resultados de cuentas
            (
                //Caso 1: actCuentas no está activado
                //(no se actualizará ninguna cuenta)
                !actCuentas

                ||

                //Caso 2:

                //El tipo de pago registrado antes de la actualización es el mismo que
                //el utilizado al hacer la actualización

                (
                    (tipoPagoAnterior==detalleAActualizar.tipoPago) &&
                    (
                        //El tipo de pago es efectivo
                        (detalleAActualizar.tipoPago =='efectivo' &&

                         //El resultante de caja debe ser mayor o igual a 0
                         (cuentas['caja'] - subtotalAnterior + detalleAActualizar.subtotal >= 0)  )

                        ||

                        //El tipo de pago es tarjeta de débito
                        (detalleAActualizar.tipoPago =='debito' &&

                         //El resultante de cuenta corriente es
                         //mayor o igual a -50000 (límite de descubierto)
                         (cuentas['cuentaCorriente'] - subtotalAnterior + detalleAActualizar.subtotal >=  -50000) )

                        ||

                        //El tipo de pago es tarjeta de crédito (no tengo restricciones)
                        (detalleAActualizar.tipoPago =='credito')
                    )
                )


                ||


                //Caso 3:

                //El tipo de pago registrado antes de la actualización es diferente
                //al utilizado al hacer la actualización

                (
                    (tipoPagoAnterior!=detalleAActualizar.tipoPago) &&
                    (
                        //El tipo de pago que tenia antes era en efectivo
                        (tipoPagoAnterior =='efectivo' &&

                         //El resultante de caja debe ser mayor o igual a 0
                         (cuentas['caja'] - subtotalAnterior >=  0)  )

                        ||

                        //El tipo de pago que tenía antes era con tarjeta de débito
                        (tipoPagoAnterior =='debito' &&

                         //El resultante de cuenta corriente debe ser
                         //mayor o igual a -50000 (límite de descubierto)
                         (cuentas['cuentaCorriente'] - subtotalAnterior >=  -50000)  )

                        ||

                        //El tipo de pago que tenia antes era con tarjeta de crédito (no tengo restricciones)
                        (tipoPagoAnterior =='credito')
                    )

                )
                //Termino de validar las cuentas contables
            )


            &&



            //Validacion de stock de productos
            (
                //Caso 1: actStock no está activado
                //(no se actualizará el stock de ningun producto)
                !actStock


                ||

                //Caso 2: El producto asociado al detalle antes de la modificación
                //es el mismo al que hacer la modificacion
                (
                    (productoActualizado) &&
                    (idProductoAnterior == req.body.idProducto) &&
                    //El stock resultante debe ser mayor o igual a 0
                    ((Number(productoActualizado.stock) + Number(cantVentaAnterior) - Number(req.body.cantidad) >=0 )&&
                    (Number(productoActualizado.stock) + Number(cantVentaAnterior) - Number(req.body.cantidad) <= productoActualizado.stockOriginal ))
                    
                )

                ||


                //Caso 3: El producto asociado al detalle antes de la modificación
                //es diferente al que asocio en la modifiacion

                //Como  al producto asociado al detalle antes de la modificación
                //le repongo stock, este no tiene límite superior,
                //por lo cual no necesito hacer una validación para el mismo

                (
                    
                    (productoActualizado) &&
                    (idProductoAnterior!= req.body.idProducto) &&
                    //El stock resultante debe ser mayor o igual a 0
                    (
						(Number(productoActualizado.stock)  - Number(req.body.cantidad) >=0 )&&
						//El stock resultante del producto anterior debe ser menor o igual al original
						(
							(!productoAnterior) ||
							(Number(productoAnterior.stock)  +  Number(cantVentaAnterior) <= productoAnterior.stockOriginal)
						)
                    )
                )


                //Termino de validar el stock resultante
            )

            //Cierro validación
        )



        //Cuerpo del if (lo ejecuto si la validación fue exitosa
        {

           //console.log("Entro a la validación"); 
            //Borro de la venta el detalle anterior
            ventaAActualizar.detalle= await ventaAActualizar.detalle.filter(
				function(elemDetalle){
					if(elemDetalle._id != req.params.idDetalle){
						return elemDetalle;
						}
					}
            );
            
            //Le agrego el detalle actualizado
            ventaAActualizar.detalle= await ventaAActualizar.detalle.concat(detalleAActualizar);
            
            //Guardo la venta con el detalle actualizado
            await ventaAActualizar.save();

            //Si esta activado actualizar stock
            if(actStock) {

                //Si el producto asociado al detalle de venta actualizado es el mismo que el de la venta anterior
                //actualizo el stock con la nueva cantidad

                if(idProductoAnterior==req.body.idProducto) {

                    //Actualizo el stock del producto
                    productoActualizado.stock = Number(productoActualizado.stock) + Number(cantVentaAnterior) - Number(req.body.cantidad);
                    await productoActualizado.save();
                }

                //Si el producto asociado al detalle de venta actualizado NO
                //es el mismo que el de la venta anterior

                else {
                    //Al producto del detalle a actualizar le resto el stock correspondiente al detalle actualizado
                    productoActualizado.stock =  Number(productoActualizado.stock)  - Number(req.body.cantidad);

					//Si es que aún existe en la base de datos,
					//al producto que tenía en el detalle previamente a la actualización,
					//le repongo el stock correspondiente a la cantidad que tenía previamente
					//en el elemento de detalle de venta
                    if(productoAnterior) {
                        productoAnterior.stock= Number(productoAnterior.stock)  +  Number(cantVentaAnterior);
                        await productoAnterior.save();

                    }
                    await productoActualizado.save();
                }
            }


            if(actCuentas) {
                //Si la cuenta contable asociada al detalle actualizado,
                //es la misma que la del detalle previo a actualizar,
                //le deduzco el subtotal anterior y le sumo el nuevo subtotal
                if(tipoPagoAnterior==detalleAActualizar.tipoPago) {

                    cuentas[tipoPagoACuenta[detalleAActualizar.tipoPago]] = 
						Number(cuentas[tipoPagoACuenta[detalleAActualizar.tipoPago]]) -
						Number(subtotalAnterior) +
						Number(detalleAActualizar.subtotal);
                }


                //Si la cuenta contable asociada al detalle actualizado,
                //es diferente a la del detalle previo a actualizar,
                //a esta última le deduzco el subtotal de la venta anterior en la cuenta asociada a la misma
                //y a la cuenta asociada  a la venta actualizada, le sumo el subtotal de la misma
                else {

                    cuentas[tipoPagoACuenta[tipoPagoAnterior]] =
						Number(cuentas[tipoPagoACuenta[tipoPagoAnterior]]) -
						Number(subtotalAnterior);
                    
                    cuentas[tipoPagoACuenta[detalleAActualizar.tipoPago]] = 
						Number(cuentas[tipoPagoACuenta[detalleAActualizar.tipoPago]]) +
						Number(detalleAActualizar.subtotal);
                }

                //Guardo la actualizacion de la cuenta
                await cuentas.save();

            }


			return res.json('¡Detalle de venta actualizado!');
           
        //Cierro if de validación
        }

        //Aqui entro si falló la validación
        else {
            return res.json('Validacion fallida');
        }
    }
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});

//Devuelvo las ventas completas, a traves de su codigo y fecha
router.route('/listaVentas').get( async(req, res) => {

	try{
		const arrayVentas= await listarVentas();
		const vendedores=  await Usuario.find({categoria: "vendedor"});
		return res.json({arrayVentas,vendedores});
	}
	catch (err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
})

router.route('/listaVentas/:idVenta').get( async(req, res) => {

	try {
		const venta = await Venta.findById(req.params.idVenta);
		const vendedores=  await Usuario.find({categoria: "vendedor"});
		const arrayVentas= await listarVentas();
		return res.json({arrayVentas,venta,vendedores});
	}
	catch (err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
})

const listarVentas = async function(){
	try{
		const arrayVentas= await Venta.find();
		return arrayVentas
			.map(function (venta){
				ventaListada={	
					id:venta._id,
					codVenta:venta.codVenta,
					fecha:formatearFecha(venta.fechaHora),
					hora:formatearHora(venta.fechaHora),
					vendedor: venta.vendedor
				}
				return ventaListada;
			}
		); 
	}
	catch (err) {
		console.log(err);
		return err;
	}
	
};

router.route('/reasignar/').post( async (req, res) => {


	try{

		//Me guardo el id de la venta a la que quiero reasignar el detalle
		const idVentaAReasignar=req.body.idVentaAReasignar;
		const ventaExistente=req.body.ventaExistente;
		
		const idDetalle=req.body.idDetalle;
		const idVentaOriginal=req.body.idVentaAsociada;
	
		//Busco el detalle por su id, y me guardo el listado
		//completo de ventas, y el id asociado a la venta original

		const respuestaBuscarDetalle=await buscarDetalle(idDetalle,idVentaOriginal);
		
		//No hago destructuring, ya que necesito castear el id devuelto por buscarDetalle 
		//de objectId a string
		const detalle=respuestaBuscarDetalle.detalle;
		const ventas=respuestaBuscarDetalle.ventas;

		//Si quiero reasignar el detalle a una venta ya existente, entro a este if
		if(ventaExistente){
			
			//Si la venta a la que quiero reasignar el detalle es
			//diferente de la que está actualmente asignada,
			//entro a este if
			if(idVentaAReasignar != idVentaOriginal){
				//Busco por ID la venta a la cual le quiero reasignar el detalle
				let ventaAReasignar= await Venta.findById(idVentaAReasignar);
				//Debug: la muestro por pantalla
				//console.log(ventaAReasignar);
				
				//A dicha venta, le concateno el elemento del detalle que quiero reasignar
				ventaAReasignar.detalle= await ventaAReasignar.detalle.concat(detalle);
				//Guardo la venta con el elemento de detalle añadido
				await ventaAReasignar.save();
				
				//Elimino el detalle en la venta a la cual estaba asignado originalmente
				eliminarDetalle(detalle._id.toHexString(),idVentaOriginal,false,false);
				
				//Devuelvo un JSON indicando que la venta fue reasignada exitosamente
				return res.json({
						id:idVentaAReasignar,
						mensaje:"Venta reasignada exitosamente"
					});
				
			}
			
			//Si la venta a la que quiero reasignar el detalle es
			//la misma en la que está actualmente asignada,
			//entro a este else (no hago nada mas que devolver un mensaje)
			else{

				return res.json({
						id:idVentaOriginal,
						mensaje:"La venta elegida para la reasignacion es la misma que la original"
					});
			}
		}
		
		//Si quiero reasignar el detalle a una nueva venta, entro a este else
		else{
			//Elimino el detalle en la venta a la cual estaba asignado originalmente
			eliminarDetalle(detalle._id.toHexString(),idVentaOriginal,false,false);
				

			const codVenta = await Number(req.body.codVenta);
			const vendedor = await req.body.userVendedor;

			const fechaHora= await req.body.fechaHora;
			const arrayDetalle=[detalle];

			const nuevaVenta = await new Venta({
				codVenta,
				vendedor,
				detalle,
				fechaHora

			});

			await nuevaVenta.save();
			return res.json({
					id:nuevaVenta._id,
					mensaje:"Se generó una nueva venta con el detalle asignado"
				});
		}

	}
		
	catch (err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
});

//Edito el encabezado de una venta
router.route('/editarEncabezado/:idVenta').post( async (req, res) => {

    try {
		//Me guardo en una variable la venta que estoy editando
		let ventaEditada=await Venta.findById(req.params.idVenta);
		//Me guardo en consts los campos que actualizo que
		//recibo del frontend
        const codVenta=req.body.codVenta;
        const vendedor=req.body.userVendedor;
        const fechaHora=req.body.fechaHora;
        const hora=req.body.hora;
        
        //Me guardo en una constante la lista de compras que tengo
        //actualmente en la base de datos
        const listaVentas=await listarVentas();
        
         //Si en dicha lista no tengo una venta con los valores
        //correspondientes al código de venta, vendedor, y fecha
        //que recibo desde el frontend
        if(!listaVentas.some(
			function (venta) {
				return (codVenta==venta.codVenta && vendedor==venta.vendedor && formatearFecha(fechaHora)==venta.fecha)
				}
			)
        )
        //Edito los campos del encabezado con los valores que recibí
        {
            ventaEditada.codVenta=codVenta;
            ventaEditada.vendedor=vendedor;
            ventaEditada.fechaHora=fechaHora;
            await ventaEditada.save();
            return res.json("Encabezado modificado correctamente");
        }
        
        //Si tengo los mismos valores de código de venta, vendedor, y fecha que
        //ya tenía dicha venta, pero con hora diferente,
        else if(
				codVenta==ventaEditada.codVenta &&
				vendedor==ventaEditada.vendedor && 
				formatearFecha(fechaHora)==formatearFecha(ventaEditada.fechaHora) &&
			(
				hora != formatearHora(ventaEditada.fechaHora)
			)
		)
        {
			//Actualizo solamente el campo fechaHora
			//Si bien actualizo el campo fechaHora, validamos
			//que solo se modifique la hora, la cual no me
			//identifica la venta, como tampoco el proveedor.
			ventaEditada.fechaHora=fechaHora;
			await ventaEditada.save();
            return res.json("Encabezado modificado correctamente");
            
        }
        
        //Si ya existe una venta con el mismo código de venta, vendedor, y fecha,
        //devuelvo un mensaje indicando que no puedo editar el encabezado con esos valores
        else
        {
            return res.json("Ya existe una venta con ese encabezado");
        }
    }
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});

module.exports = router;
