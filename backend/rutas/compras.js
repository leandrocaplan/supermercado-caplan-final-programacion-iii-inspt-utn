const router = require('express').Router();

let Compra = require('../modelos/modelo.compra');
let Producto = require('../modelos/modelo.producto');
let Usuario = require('../modelos/modelo.usuario');
let Cuentas = require('../modelos/modelo.cuentas');

const unico="000000000000000000000000";

//Recibo una fecha y hora en formato ISO y devuelvo la
//fecha como un string de la forma "dd/mm/aaaa"
const formatearFecha =function (fecha) {
    const fechaObj=new Date(fecha);
    return (
		fechaObj.getDate().toString().padStart(2, "0") + '/'+
		(fechaObj.getMonth()+1).toString().padStart(2, "0") + '/'+
		fechaObj.getFullYear()
	);
}

//Recibo una fecha y hora en formato ISO y devuelvo la
//hora como un string de la forma "hh:mm:ss"
const formatearHora =function (fecha) {
    try {
        const fechaObj=new Date(fecha);
        return (
			fechaObj.getHours().toString().padStart(2, "0") + ':'+
			(fechaObj.getMinutes()).toString().padStart(2, "0") + ':'+
			fechaObj.getSeconds().toString().padStart(2, "0")
		);
    }
    catch(error) {
        alert(error)
    }
}


/*
 * Algoritmo para calcular stock original

Doy de alta un producto con inicialmente 100 unidades

Stock actual ->100
Stock original->100

Vendo 5:

Stock actual ->95
Stock original ->100

Compro 10:

Stock actual ->105
Stock original ->105

Quiero eliminar las 5 que vendí, actualizando stock->
->No me va a dejar, ya que el resultante
de stock sería 105 + 5 = 110 >105

Necesito una mejor forma de controlar el stock original 
Cada vez que hago una compra, al stock original le sumo el resultante de
todas las ventas existentes de dicho producto
->Para no tener que calcular dicho valor haciendo un reduce de todos los elementos vendidos,
dicho resultante será: el stock original anterior, menos el stock actual anterior

Ej:


Stock actual ->100
Stock original->100

Vendo 5:

Stock actual ->95
Stock original ->100

Diferencia:(100-95): 5 (la calculo antes de actualizar el stock actual)

(Stock original - stock actual)
Compro 10:

Stock actual ->95 + 10= 105
Stock original ->Stock actual + diferencia -> 110
-->Ahora puedo eliminar correctamente lo que vendí
*/

//Devuelvo todos los detalles
router.route('/').get(async (req, res) => {
    //console.log("entro al get");
    try {
        const compras= await Compra.find();
        return res.json(compras);
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});

//Agrego una nueva compra
router.route('/agregar').post( async (req, res) => {
    
    try{

		//Debo validar, elemento por elemento, el detalle.
		//Si hay algun elemento del detalle que no cumple
		//la validación, no puedo guardar la compra

		error=await validarDetalle(req.body.detalle);
		
		//Me traigo el listado de compras realizadas hasta ahora
		//De esta forma, me puedo fijar si ya existe una compra
		//con el mismo código, fecha, comprador y proveedor
		comprasRealizadas = await listarCompras();

		

		if(!error) {

			//Me guardo cada dato de la compra en 'const' correspondientes
			const codCompra = await Number(req.body.codCompra);
			const comprador = await req.body.userComprador;
			const proveedor=await req.body.proveedor;
			const detalle= await req.body.detalle;
			const fechaHora= await req.body.fechaHora;
			
			
			const compraBuscada= await comprasRealizadas.find (
				function(compraRealizada) {

				return(
						  (compraRealizada.codCompra == codCompra) &&
						  (compraRealizada.fecha == formatearFecha(fechaHora)) &&
						  (compraRealizada.comprador == comprador) 
						  
						  //Como criterio, asignar la compra a un proveedor distinto
						  //no resultará en una compra diferente
						  //La línea  '&&(compraRealizada.proveedor == proveedor)'
						  //no la agrego
						  //Entonces, los campos que son únicos para
						  //cada compra son el código, el comprador y la fecha
					  )
				}
			 );
			
			//Me fijo si ya existe una compra con el mismo código, fecha y comprador
			if(compraBuscada)
			
			//Si existe, agrego el detalle a la compra existente y la guardo
			{
						let compraAActualizar=await Compra.findById(compraBuscada.id);
						compraAActualizar.detalle= await compraAActualizar.detalle.concat(detalle);
						await compraAActualizar.save();
			}

			//Si no, genero una nueva compra y la guardo (debo validar que tenga al menos un elemento en el detalle)
			else if(detalle.length>0){
				const nuevaCompra = await new Compra({
					codCompra,
					comprador,
					proveedor,
					detalle,
					fechaHora
				});

				await nuevaCompra.save();
			}
			
			//Por cada elemento del detalle agregado, 
			//actualizo el stock y la cuenta correspondiente
			//a cada elemento del detalle
			for (const elemDetalle of req.body.detalle) {
				await actStockCuentas(elemDetalle,res);
			}
			return res.send("Compra exitosa");

		}
		else{
			return res.send("Validacion fallida");
			}

	}

	catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});



//Valido que el detalle de compras no me genere un resultante
//inválido en ninguna cuenta
const validarDetalle  = async function (detalle) {
	try{	
		//Valido primero que no haya cantidades ni subtotales menores a 0
		//De haberlos, devuelvo true (indicando que hubo un error)
		for (let elemDetalle of detalle) {
			if(Number(elemDetalle.cantidad)<=0 || Number(elemDetalle.subtotal)<=0){
					error=true;
				}
		}
		
		const cuentas= await Cuentas.findById(unico);

		//En el caso de las compras, no necesito validar el stock de ningun
		//producto, ya que este no tiene límite superior
		//Solo necesito validar el resultante de las cuentas contables,
		//de manera que no pueda tener un resultante inferior al límite
		//de cada cuenta correspondiente
		
		//Aquí si me conviene, a diferencia de la validación del detalle
		//de compras, utilizar un reduce, ya que no me interesa controlar
		//el stock de productos vendidos, sinó unicamente el resultante
		//de cada cuenta contable

		const totalCaja=detalle
			.filter(function(elemDetalle){
				if(elemDetalle.formaPago=='caja'){
					return elemDetalle;
					}
				})
			.reduce(function(acumulador,elemDetalle) {
				return acumulador + Number(elemDetalle.subtotal);
			}, 0);


		const totalCorriente=detalle
			.filter(function(elemDetalle){
				if(elemDetalle.formaPago=='cuentaCorriente'){
					return elemDetalle;
					}
				})
			.reduce(function(acumulador,elemDetalle) {
				return acumulador + Number(elemDetalle.subtotal);
				}, 0);

		//No necesito hacer el reduce de la cuenta
		//"Crédito a favor", ya que esta no tiene límite
		//inferior.


		//Una vez obtenidos los totales correspondientes a cada forma
		//de pago, valido que los resultantes no resulten inferiores
		//al límite de cada cuenta contable.
		if
		(
			//Si en caja me queda un resultante menor a cero (no puedo tener efectivo negativo)
			(cuentas['caja'] - totalCaja  < 0) ||
			//O en cuenta corriente me queda un resultante menor al límite de descubierto
			(cuentas['cuentaCorriente'] - totalCorriente  < -50000)
        )
        {
			//Devuelvo true (indicando que hubo un error)
			return true;
		}
    
		else
		{
			//Sino, devuelvo false (indicando que no hubo ningun error)
			return false;
		}
	}
	catch(err) {
        console.log(err);
		return err;
    }
}


//Actualizo el stock de los productos y las cuentas cada vez que registro una nueva
//compra o agrego elementos a una existente, con su respectivo detalle
const actStockCuentas  = async function (elemDetalle) {
	
	try{ 
		//Busco el producto a actualizar por su ID
		let productoActualizado= await Producto.findById(elemDetalle.producto);
		
		//Busco las cuentas contables para actualizarlas
		let cuentas= await Cuentas.findById(unico);
		
		//Valido que el producto siga existiendo en la base de datos
		if(productoActualizado) {
			
			const diferencia=Number(productoActualizado.stockOriginal) - Number(productoActualizado.stock);
			//Al stock del producto buscado, le sumo la cantidad comprada
			productoActualizado.stock = Number(productoActualizado.stock) + Number(elemDetalle.cantidad);

			//Debo actualizar tambien, el parámetro 'stockOriginal', ya que lo necesito
			//para la validación correspondiente el el módulo de ventas        
			productoActualizado.stockOriginal = Number(productoActualizado.stock) + Number(diferencia)
			
			//A la cuenta contable asociada a la forma de pago, le resto el subtotal correspondiente
			//al elemento del detalle.
			cuentas[elemDetalle.formaPago] = Number(cuentas[elemDetalle.formaPago]) - Number(elemDetalle.subtotal);
			
			//Guardo el producto con el stock actualizado
			await productoActualizado.save();
			
			//Guardo la cuenta contable correspondiente actualizada
			await cuentas.save();
		}
	}
	catch(err){
		console.log(err);
		return err;
	}
    
}

//Devuelvo un registro de compra por su id
router.route('/id/:id').get( async (req, res) => {
	try {
		const compra= await Compra.findById(req.params.id);
		return res.json(compra);
	}

	catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});


//Busco un detalle de compra por su ID.
//Devuelvo el detalle, la lista completa de compras,
//y el ID de la compra a la cual está asociada el
//detalle.
const buscarDetalle= async function (idDetalle,idCompraAsociada) {
   try{
		//Me guardo la lista completa de compras
		const compras=await Compra.find();

		//Busco la compra cuyo ID es el que recibí por parámetro
		const compra=await Compra.findById(idCompraAsociada);
		//Inicializo la variable 'detalle' con un objeto vacío
		let detalle= {};

	  
		//Si en la compra que busque sigue existiendo en la
		//base de datos, y tengo un elemento de detalle con
		//el ID que recibí por parámetro

		if(compra && compra.detalle.some(det => det._id==idDetalle)) {
			
			//En la variable 'detalle' me guardo el elemento
			//del detalle que encontré.
			detalle=compra.detalle.find(det => det._id==idDetalle);
			
		}

		
		//Luego, devuelvo el detalle completo,
		//la lista de compras, y el ID de la compra
		//asociada al detalle.
		return {detalle,compras,idCompraAsociada};
	}
	catch(err) {
        console.log(err);
        return err;
    }
	
}


//Devuelvo un registro de compra por su id,con la lista de administradores (compradores) completa
router.route('/detalleId/:idDetalle/:idCompra').get( async (req, res) => {
	try{
		const detalleCompras= await buscarDetalle(req.params.idDetalle,req.params.idCompra);
		const compradores= await Usuario.find({categoria: "administrador"});
		const producto= await Producto.findById(detalleCompras.detalle.producto);
		return res.json({detalleCompras,compradores,producto});
	}
	catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
});


//Devuelvo los elementos necesarios para asociar cada una de las compras
//con su correspondiente usuario, producto, y cuenta contable asignada

router.route('/asociar').get( async(req, res) => {

    try {
        const compras=await Compra.find();
        const usuarios= await Usuario.find({categoria: "administrador"});
        const productos= await Producto.find();
        const cuentas= await Cuentas.findById(unico);
        return res.json({
			"compras":compras,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
})


//Recibo por parámetro una lista de compras
//Devuelvo un array plano con cada elemento de detalle
//de compra existente.

//Cada elemento devuelto, contendrá tanto la información
//del elemento del detallle,
//como de la compra a la cual está asociado el mismo.
const aplanarDetalles=function (compras) {
    try{
		//Declaro un array inicialmente vacío
		let arrayComprasPlano=[];

		//Con un for, recorro la lista de compras
		//completa recibida por parámetro
		
		//Podriamos utilizar un map en lugar de un for,
		//el cual lo dejamos como comentario:
		//compras.map(function (compra){
		for (const compra of compras) {
			
			//Declaro una variable 'detallePlano' a la que le asigno
			//un objeto, el cual contendrá todos los campos
			//que voy a devolver en cada elemento de la lista
			//completa de detalles.
			//Inicialmente, todos estos campos están
			//inicializados con 0 o un string vacío,
			//según corresponda a cada tipo de campo (número o string). 
			let detallePlano= {
				_id:'',
				idCompraAsociada:'',
				codCompra:0,
				comprador:'',
				proveedor:'',
				producto:0,
				cantidad:0,
				formaPago:'',
				subtotal:0
			};
			
			//Inicialmente, me guardo en 'detallePlano'
			//todos los valores correspondientes a la compra
			//a la cual está asociada el elemento de detalle
			detallePlano.idCompraAsociada=compra._id;
			detallePlano.codCompra=compra.codCompra;
			detallePlano.comprador=compra.comprador;
			detallePlano.proveedor=compra.proveedor;
			detallePlano.fechaHora=compra.fechaHora;

			//Luego, en otro for anidado, recorro
			//todos los elementos del detalle correspondientes
			//a la compra en la cual estoy parado
			
			//Podriamos utilizar un map en lugar de un for,
			//el cual lo dejamos como comentario:
			
			//compra.detalle.map(function (elemDetalle){	
			for (const elemDetalle of compra.detalle) {

				//Aqui, en 'detallePlano', me guardo
				//todos los valores correspondientes
				//al elemento del detalle donde estoy
				//parado
				detallePlano._id=elemDetalle._id;
				detallePlano.producto=elemDetalle.producto;
				detallePlano.cantidad=elemDetalle.cantidad;
				detallePlano.precioActual=elemDetalle.precioActual;
				detallePlano.unidadActual=elemDetalle.unidadActual;
				detallePlano.formaPago=elemDetalle.formaPago;
				detallePlano.subtotal=elemDetalle.subtotal;
				
				//Una vez que tengo cargado en 'detallePlano'
				//todos los valores que necesito, tanto correspondientes
				//a la compra completa como del elemento del detalle
				//en el que estoy parado,
				//concateno el objeto al array declarado fuera
				//de ambos for anidados.
				arrayComprasPlano=arrayComprasPlano.concat({...detallePlano});

			//Dejamos comentado lo que correspondería al cierre del map anidado
			//})

			}
		//Dejamos comentado lo que correspondería al cierre del map de nivel superior

		}
		
		//Una vez que recorrí y agregué al array
		//cada elemento de detalle de cada compra existente,
		//devuelvo el array, en el que incluí todos estos elementos.
		return arrayComprasPlano;
	}
	catch(err) {
        console.log(err);
        return err;
    }
}

//Devuelvo los elementos necesarios para asociar cada una de las compras
//con su correspondiente usuario, producto, y cuenta contable asignada

router.route('/asociarAplanar').get( async(req, res) => {
	
    try {
		//Me traigo todas las compras
        const compras=await Compra.find();
		
		//Me traigo todos los detalles de compra aplanados
		//con la funcion definida anteriormente
        const detallesAplanados= await aplanarDetalles(compras);
		
		//Me traigo la lista de compradores (solo pueden ser administradores)
        const usuarios= await Usuario.find({categoria: "administrador"});
        
        //Me traigo la lista de productos
        const productos= await Producto.find();
        
        //Me traigo el estado de las cuentas contables
        const cuentas= await Cuentas.findById(unico);
        
        //En un JSON, devuelvo todo lo que me traje anteriormente
        return res.json({
			"detallesCompras":detallesAplanados,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

})

//Sobrecargo la funcion anterior recibiendo un parámetro
//por URL: en lugar de devolver todos los elementos
//de detalle de todas las compras, devuelvo solo los
//asociados al ID de compra que recibí por parámetro
router.route('/asociarAplanar/:idCompra').get( async(req, res) => {
	
    try {
		//Me traigo la compra cuyo ID recibí por parámetro
        const compra=await Compra.findById(req.params.idCompra);
		
		//Me traigo el detalle de compra aplanado
		//con la funcion definida anteriormente
        const detallesAplanados= await aplanarDetalles([compra]);
		
		//Me traigo la lista de compradores (solo pueden ser administradores)
        const usuarios= await Usuario.find({categoria: "administrador"});
        
        //Me traigo la lista de productos
        const productos= await Producto.find();
        
        //Me traigo el estado de las cuentas contables
        const cuentas= await Cuentas.findById(unico);
        
        //En un JSON, devuelvo todo lo que me traje anteriormente
        return res.json({
			"detallesCompras":detallesAplanados,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

})

//Devuelvo los elementos necesarios para asociar una compra por su ID,
//con su correspondiente usuario, producto, y cuenta contable asignada

router.route('/asociar/id/:id').get( async (req, res) => {

    try {
        const compra = await Compra.findById(req.params.id);
        const usuarios= await Usuario.find({categoria: "administrador"});
        const productos= await Producto.find();
        const cuentas= await Cuentas.findById(unico);

        return res.json({
			"compra":compra,
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
        });
    }
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});


router.route('/asociarDetalle/id/:idDetalle/:idCompra').get( async(req, res) => {

	try{
		const compra=await Compra.findById(req.params.idCompra);
		const usuarios=await Usuario.find({categoria: "administrador"} );
		const productos= await Producto.find();
		const cuentas= await Cuentas.findById(unico);
		const detalle= await compra.detalle.find(elemDetalle => elemDetalle._id==req.params.idDetalle);

		return res.json({
			//Deprecacion: renombrar el campo "compra" por "detalle"
			"compra":detalle,
			//Deprecacion: este campo no lo necesito
			"usuarios":usuarios,
			"productos":productos,
			"cuentas":cuentas
		});
	}
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});


//Recibo por parámetro el ID de un elemento de detalle, el ID de la compra asociada a 
//dicho elemento, y dos opciones booleanas denominadas 'actStock' y 'actCuentas'
//Elimino el elemento de detalle correspondiente a su ID, y la compra asociada correspondiente
//y actualizo tanto el stock del producto correspondiente y la cuenta contable correspondiente,
//dependiendo del valor de verdad de las opciones 'actStock' y 'actCuentas'.

//Como aclaración, podríamos prescindir del parámetro 'idCompra' para implementar
//la función. Sin embargo, nos resulta muy útil,
//ya que optimizará en gran medida el rendimiento de la operación al conocer
//de antemano el ID de la compra completa que vamos a actualizar.

//Nos quedaría sinó, una funcion de tiempo cuadrático en lugar de lineal,
//ya que deberíamos realizar dos ciclos anidados, para recorrer cada compra existente
//y dentro de cada una de ellas, buscar el ID del elemento del detalle correspondiente.
//Como trabajamos con arrays no ordenados, debemos necesariamente utilizar una busqueda lineal,
//utilizando en este caso la función 'find'. 
//De todas maneras, en este caso,una búsqueda lineal nos resulta muy eficiente,
//dado que es poco factible que tengamos mas de cientos elementos en una sola compra.

const eliminarDetalle = async function (idDetalle,idCompra,actStock,actCuentas) {
	try {
		//Tenemos cuatro casos posibles, dependiendo de si las opciones
		//de actualizar stock y actualizar cuenta están o no activadas


		//Buscamos la compra a actualizar de acuerdo al ID de la compra recibido por parámetro,
		//en 'idCompra' y lo guardamos en la variable 'compraAActualizar'
		const compraAActualizar=await Compra.findById(idCompra);
		
		//Dentro de la compra que nos guardamos antes, buscamos el elemento del detalle
		//correspondiente al ID del detalle recibido en 'idDetalle', y lo
		//guardamos en la variable 'detalleAEliminar'
		const detalleAEliminar=await compraAActualizar.detalle.find(elem => elem._id==idDetalle) || null;
		
		//A continuación, buscamos el producto correspondiete al elemento del detalle
		//que queremos eliminar, y lo guardamos en la variable 'producto'
		const producto= await Producto.findById(detalleAEliminar.producto);
        
        //Finalmente, nos traemos las cuentas contables para actualizarlas
        const cuentas= await Cuentas.findById(unico);		

		
		//Al eliminar una compra, si es que estamos actualizando el stock
		//deducimos el stock del producto que nos había aumentado al
		//realizar la compra.

        if //Validacion

        (
			//Valido que la compra y el detalle sigan existiendo en la base de datos
			(compraAActualizar && detalleAEliminar) &&
        	//Valido que la cantidad y el subtotal sean mayores a 0
			(Number(detalleAEliminar.cantidad)>0 && Number(detalleAEliminar.subtotal)>0) &&

        	//Debemos corroborar que nunca nos quede stock negativo
			//(resultaría un valor inválido)
            //Valido stock: Nunca me puede quedar un stock negativo
			//No necesito validar las cuentas, ya que las mismas no tienen límite superior

            (!actStock) || (!producto)||
            (Number(producto.stock) - Number(detalleAEliminar.cantidad) >=0 )

        )
        
        {

			//Actualizo la compra eliminando el detalle correspondiente
            compraAActualizar.detalle= await compraAActualizar.detalle.filter(
				function(elemDetalle){
					if(elemDetalle._id != idDetalle){
						return elemDetalle;
						}
					}
            );
            
            //Guardo la compra actualizada
            //(sin el elemento de detalle recién eliminado)
            
            if(compraAActualizar.detalle.length>0) {
				await compraAActualizar.save();
			}
			
			//Si no me quedaron elementos en el detalle de compra, actualizado,
			//la elimino (no tiene sentido tener registrada una compra sin elementos)
			else {
				await compraAActualizar.remove();
			}

            
            //Entro a este if si la opción de actualizar stock está activada,
            //y el producto asociado al detalle de compra aún existe
           
            if(actStock && producto) {
				//Al stock del producto asociado al elemento del detalle de compra, le deduzco
				//la cantidad que fue comprada previamente
                
                const diferencia=Number(producto.stockOriginal) - Number(producto.stock);
                producto.stock = Number(producto.stock) -  Number(detalleAEliminar.cantidad);
                
                //Debo actualizar tambien el stock original
                producto.stockOriginal = Number(producto.stock) + Number(diferencia);
                //Guardo el producto con el stock actualizado
                await producto.save();

            }
			
			//Si la opción de actualizar cuenta está activada
            if(actCuentas) {

                //A la cuenta contable asociada a la forma de pago de la compra, le repongo
                //el monto correspondiente al cual realize la compra
                cuentas[detalleAEliminar.formaPago] += Number(detalleAEliminar.subtotal);

                //Actualizo la cuenta en la base de datos con el valor resultante al
                //eliminar el detalle de la compra realizado recientemente
                await cuentas.save()

                
            }
			//Devuelvo un string indicando que el detalle fue eliminado
			return 'Detalle eliminado';
        //Cierro if de validacion
        }

        //Aqui entro si falló la validación
        else {
            return 'Validacion fallida';
        }
	}
	catch(err) {
        console.log(err);
		return err;
    }
}


//Esta función nos sirve para realizar la validación para eliminar varios detalles
//de una sola vez. Si la validación falla, el sistema no nos debería permitir
//eliminar ninguno de los detalles recibidos en el array.
//Como en este caso, al eliminar una compra, estamos deduciendo el stock
//de cada producto correspondiente a cada elemento del detalle, tendremos una
//funcion muy similar a 'validarDetalle' del módulo de ventas.

const validarEliminarDetalles  = async function (elementosAEliminar,actStock) {
	try{
		let resultadoValidacion=true;
		
		if(actStock){
			//Por parámetro solo recibo un array con los IDs de los elementos
			//del detalle que quiero eliminar, y los IDs de la compra a la
			//cual están asignados
			
			//Para realizar la validacion necesito
			//generarme un array con los datos completos de dichos elementos.
			
			//Al tener los ID de las compras completas
			//afectadas, no es necesario traerme todas las compras existentes
			//en la base de datos para realizar dicha validación,
			//por lo que me traigo solo las compras correspondientes
			//a los IDs que recibí por parámetro

			
			//Me genero un array, inicialmente vacío, que contendrá
			//todas las compras completas afectadas
			let compras = [];
			
			//Recorro el listado de elementos a eliminar que recibí por parámetro
			for (let elementoAEliminar of elementosAEliminar) {
				
				//Si en el array que estoy generando no existe una compra con el ID
				//del listado que recibí por parámero, o bien el array está vacío,
				//le concateno la compra identificada con dicho ID
				
				if(!compras.some(
					
					function(compra){
							return (compra._id==elementoAEliminar.idCompra)
						})
					
					||
					
					compras.length==0
				)
				{
					compras=compras.concat(
						await Compra.findById(elementoAEliminar.idCompra)
					)
				}
			}
			
			//Una vez que generé el array con las compras completas,
			//utilizo la funcion aplanarDetalles, para traerme
			//todos los detalles completos existentes en cada una
			//de esas compras
			
			//Si bien esta función me traerá infomación redundante, ya que
			//no necesito luego el ID de la compra asociada a cada detalle,
			//es conveniente utilizarla, de manera que reutilizo
			//código ya existente en lugar de generar una funcion nueva
			//solamente para el propósito de esta validación.
			const listaDetallesAplanada= await aplanarDetalles(compras);
			
			
			//De todos los elementos de detalle completos que obtuve, filtro
			//me quedo solamente con los que tienen un ID que se corresponda
			//con la lista de IDs de detalles que recibí por parámetro
			const detallesCompletos=listaDetallesAplanada.filter(function(elemDetalle){
				if(elementosAEliminar.some(
					function(elementoAEliminar){
						return(elementoAEliminar.idDetalle==elemDetalle._id)
					})
				
				)
				{
					return elemDetalle
				}
			});
			
			//Valido primero que no haya cantidades ni subtotales menores a 0
			for (let elemDetalle of detallesCompletos) {
				if(Number(elemDetalle.cantidad)<=0 || Number(elemDetalle.subtotal)<=0){
					resultadoValidacion=false;
				}
			}
			//Hay que hacer primero, un array con cada elemento del detalle asociado a cada producto
			//ya que el detalle puede llegar a tener varios elementos asociados
			//al mismo producto, con cantidades distintas
			//Necesito el total de cada cantidad asociada a cada producto


			const productoCantidad = {};
			for (let elemDetalle of detallesCompletos) {
				productoCantidad[elemDetalle.producto] = Number(productoCantidad[elemDetalle.producto]) || 0;
				productoCantidad[elemDetalle.producto] += Number(elemDetalle.cantidad);
			}

			const arrayProductosCantidades = [];
			for (let producto in productoCantidad) {
				const cantidad = productoCantidad[producto];
				arrayProductosCantidades.push({
					producto,
					cantidad
				});
			}

			
			//Una vez generado el array, valido que la cantidad de cada producto vendido
			//no me genere un stock resultante negativo en ningun producto
			for (const elemProdCant of arrayProductosCantidades) {

				const producto = await Number(elemProdCant.producto);
				const cantidad = await Number(elemProdCant.cantidad);
				let productoActualizado= await Producto.findById(elemProdCant.producto);
				
				//Valido stock: Nunca me puede quedar un stock negativo
				//No valido las cuentas, ya que no tienen límite superior
				//Valido ademas que el producto siga existiendo en mi base de datos
				if(productoActualizado && Number(productoActualizado.stock) - Number(cantidad) < 0) {
					resultadoValidacion=false;
				}
			}

		}
		return resultadoValidacion;
	}
	catch(err) {
        console.log(err);
        return err;
    }
}

//Elimino una lista de elementos de detalles, en un array recibido desde el front
router.route('/eliminarDetalles').post( async (req, res)  => {

    try {
        
		//Esta lista de detalles a eliminar, 
		const detallesAEliminar=req.body.listaDetalles;
		const actStock=req.body.actStock;
		const actCuentas=req.body.actCuentas;
		const resultadoValidacion= await validarEliminarDetalles(detallesAEliminar,actStock);
       
		if(resultadoValidacion){
			let resultado; 
			for (const detalleAEliminar of detallesAEliminar) {
				resultado= await eliminarDetalle(detalleAEliminar.idDetalle,detalleAEliminar.idCompra,actStock,actCuentas);
			}

		return res.json("Elementos eliminados exitosamente");
		}

		else{
			return res.json("Hubo una falla en la validacion");
		}
	}
	catch (err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
});


//Recibo como parámetro, el ID de un detalle de compra, el ID de la compra completa,
//y las opciónes actStock y actCuentas, que me indicarán si debo o no actualizar
//el stock o las cuentas contables.
router.route('/eliminarDetalle/:idDetalle/:idCompra/:actStock/:actCuentas').delete( async (req, res)  => {
	try{
		//Casteamos las opciones 'actStock' y 'actCuentas' de string a booleano
		const actStock = (req.params.actStock === "true");
		const actCuentas = (req.params.actCuentas === "true");

		//Pasamos tanto el ID del elemento del detalle que queremos eliminar,
		//el ID de la compra correspondiente, y las opciones
		//actStock y actCuentas la función 'eliminarDetalle', definida mas arriba.
		//Nos guardamos el resultado de dicho método, que contendrá un 
		//string indicando si la operación fue exitosa, o bien
		//hubo una falla en la validación.
		const resultado= await eliminarDetalle(req.params.idDetalle,req.params.idCompra,actStock,actCuentas);
		return res.json(resultado);
	}
	catch (err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}

});


//Actualizo un elemento del detalle de una compra,
//del cual recibo como parámetro tanto el ID correspondiente
//al elemento del detalle como de la compra completa
router.route('/actualizarDetalle/id/:idDetalle/:idCompra').post( async (req, res) => {
    const actStock=req.body.actStock;
    const actCuentas=req.body.actCuentas;

	try {

		//Declaro variables para guardarme informacion de la compra previa a la actualizacion
		let idProductoAnterior;
		let cantCompraAnterior;
		let formaPagoAnterior;
		let subtotalAnterior;

		//Busco la compra completa por su ID, y me la guardo en la variable 'compraActualizada' 
		let compraAActualizar=await Compra.findById(req.params.idCompra);
		
		//Busco el elemento de detalle a actualizar por su ID, dentro de la compra
		//que busqué arriba, y me la guardo en la variable 'detalleAActualizar'
		let detalleAActualizar=await compraAActualizar.detalle.find(idDetalle => idDetalle._id==req.params.idDetalle) || null;


		//Utilizo destructuring para guardarme, en las variables que declaré arriba,
		//la información del detalle previa a la actualización, y en los campos de
		//'detalleAActualizar', la información nueva con la que quiero actualizar el detalle.
		//De esta forma, me aseguro que la informacion anterior no se pise con la nueva.
		[idProductoAnterior,detalleAActualizar.producto] = [detalleAActualizar.producto,req.body.idProducto];
		[cantCompraAnterior,detalleAActualizar.cantidad] = [detalleAActualizar.cantidad,Number(req.body.cantidad)];
		[formaPagoAnterior,detalleAActualizar.formaPago ] =[detalleAActualizar.formaPago,req.body.formaPago];
		[subtotalAnterior, detalleAActualizar.subtotal ] =[detalleAActualizar.subtotal,Number(req.body.subtotal)];

		
		//Actualizo precio y unidad
		detalleAActualizar.precioActual=req.body.precioActual;
		detalleAActualizar.unidadActual=req.body.unidadActual;
		
		//Me guardo en la variable 'productoActualizado' el producto completo correspondiente
		//al elemento del detalle que estoy actualizando con su información nueva.
		let productoActualizado= await Producto.findById(detalleAActualizar.producto);

		//Me guardo en la variable 'productoAnterior' el producto completo correspondiete
		//asociado al detalle previo a la actualización
		let productoAnterior= await Producto.findById(idProductoAnterior);

		//En la variable 'cuentas', me traigo el estado actual de las cuentas contables
		let cuentas= await Cuentas.findById(unico);


		//Validación
		if (
			
			(compraAActualizar && detalleAActualizar) &&
			//Valido primero que las cantidades y los
			//subtotales sean mayores a 0
			(
				Number(detalleAActualizar.cantidad)>0 &&
				Number(cantCompraAnterior)>0  &&
				Number(detalleAActualizar.subtotal)>0 &&
				Number(subtotalAnterior)>0
			) 
			
			&&
			

			//Validacion de resultados de cuentas
			(
				//Caso 1: actCuentas no está activado
				//(no se actualizará ninguna cuenta)
				!actCuentas

				||

				//Caso 2:

				//La forma de pago registrada antes de la actualización es la misma que
				//la utilizado al hacer la actualización
				
				//El resultante de la cuenta contable correspondiente será:
				//El valor actual de la cuenta +
				//el subtotal que tenía registrado antes de la actualización -
				//el subtotal correspondiente a la actualización
				(
					(formaPagoAnterior==detalleAActualizar.formaPago) &&
					(
						//La forma de pago es caja (efectivo)
						(detalleAActualizar.formaPago =='caja' &&

						 //El resultante de caja debe ser mayor o igual a 0
						 (cuentas['caja'] + subtotalAnterior - detalleAActualizar.subtotal >= 0)  )

						||

						//La forma de pago es con cuenta corriente
						(detalleAActualizar.formaPago =='cuentaCorriente' &&

						 //El resultante de cuenta corriente es
						 //mayor o igual a -50000 (límite de descubierto)
						 (cuentas['cuentaCorriente'] + subtotalAnterior - detalleAActualizar.subtotal >=  -50000) )

						||

						//La forma de pago es crédito a favor (no tengo restricciones)
						(detalleAActualizar.formaPago =='creditoAFavor')
					)
				)


				||


				//Caso 3:

				//La forma de pago registrada antes de la actualización es diferente
				//a la utilizada al hacer la actualización
				
				//El resultante de las cuenta contables correspondientes será:
				
				//En la cuenta contable correspondiente a la que utilizaba
				//previa a la actualización, le sumo el subtotal que tenía
				//registrado previo a la actualización
				
				//En la cuenta contable correspondiente a la que utilizo
				//en el elemento de detalle actualizado, le resto
				//el subtotal del elemento de detalle actualizado
				
				//Como las cuentas solo tienen límite inferior (no superior),
				//debo solo validar el resultante de la cuenta contable asociada
				//a la del elemento actualizado
				(
					(formaPagoAnterior!=detalleAActualizar.formaPago) &&
					(
						//El tipo de pago que tenia antes era en caja
						(detalleAActualizar.formaPago =='caja' &&

						 //El resultante de caja debe ser mayor o igual a 0
						 (cuentas['caja'] - detalleAActualizar.subtotal >=  0)  )

						||

						//El tipo de pago que tenía antes era con tarjeta de débito
						(detalleAActualizar.formaPago =='cuentaCorriente' &&

						 //El resultante de cuenta corriente debe ser
						 //mayor o igual a -50000 (límite de descubierto)
						 (cuentas['cuentaCorriente'] - detalleAActualizar.subtotal >=  -50000)  )

						||

						//El tipo de pago que tenia antes era con tarjeta de crédito (no tengo restricciones)
						(detalleAActualizar.formaPago =='creditoAFavor')
					)

				)
				//Termino de validar las cuentas contables
			)


			&&



			//Validacion de stock de productos
			(
				//Caso 1: actStock no está activado
				//(no se actualizará el stock de ningun producto)
				!actStock


				||

				//Caso 2: El producto asociado al detalle antes de la modificación
				//es el mismo al que hacer la modificacion
				(
					(productoActualizado) &&
					(idProductoAnterior== req.body.idProducto) &&
					//El stock resultante debe ser mayor o igual a 0
					(Number(productoActualizado.stock) - Number(cantCompraAnterior) + Number(detalleAActualizar.cantidad) >=0 )
					
				)

				||


				//Caso 3: El producto asociado al detalle antes de la modificación
				//es diferente al que asocio en la modifiacion

				//Si el producto anterior ya no existe en mi base de datos,
				//no necesito validar su stock
				
				//Como  al producto asociado al detalle en de la modificación
				//le agrego stock, este no tiene límite superior,
				//por lo cual no necesito hacer una validación para el mismo
				
				//Sin embargo, al producto asociado al elemento del detalle antes de
				//la modificacion le deduzco stock, por lo cual debo validar que el mismo nunca sea negativo
				
				

				(

					(productoActualizado) &&
					(idProductoAnterior!= req.body.idProducto) &&
					//El stock resultante debe ser mayor o igual a 0
					(
						(!productoAnterior) ||
						(Number(productoAnterior.stock)  - Number(cantCompraAnterior) >=0 )
					)

				)


				//Termino de validar el stock resultante
			)

			//Cierro validación
		)



			//Cuerpo del if (lo ejecuto si la validación fue exitosa
		{

			
			//Borro de la compra el detalle anterior
			compraAActualizar.detalle= await compraAActualizar.detalle.filter(
				function(elemDetalle){
					if(elemDetalle._id != req.params.idDetalle){
						return elemDetalle;
						}
					}
			);
			
			//Le agrego el detalle actualizado
			compraAActualizar.detalle= await compraAActualizar.detalle.concat(detalleAActualizar);
			
			//Guardo la compra con el detalle actualizado
			await compraAActualizar.save();

			//Si esta activado actualizar stock
			if(actStock) {

				//Si el producto asociado al detalle de compra actualizado es el mismo que el de la compra anterior
				//actualizo el stock con la nueva cantidad

				if(idProductoAnterior== req.body.idProducto) {
					
					const diferencia=Number(productoActualizado.stockOriginal) - Number(productoActualizado.stock);
					//Actualizo el stock del producto
					productoActualizado.stock = Number(productoActualizado.stock) - Number(cantCompraAnterior) + Number(detalleAActualizar.cantidad);
					productoActualizado.stockOriginal = Number(productoActualizado.stock) + Number(diferencia);
					await productoActualizado.save();
				}

				//Si el producto asociado al detalle de compra actualizado NO
				//es el mismo que el de la compra anterior

				else {
					//Al producto del detalle a actualizar le sumo el stock correspondiente al detalle actualizado
					const diferencia=Number(productoActualizado.stockOriginal) - Number(productoActualizado.stock);
					productoActualizado.stock =  Number(productoActualizado.stock)  + Number(detalleAActualizar.cantidad);
					productoActualizado.stockOriginal =  Number(productoActualizado.stock) + Number(diferencia);

					//Si el producto anterior aún existe en la base de datos
					if(productoAnterior) {
						//Le deduzco el stock correspondiente a la compra previo a actualizarla
						const diferencia=Number(productoAnterior.stockOriginal) - Number(productoAnterior.stock);
						productoAnterior.stock= Number(productoAnterior.stock)  -  Number(cantCompraAnterior);
						productoAnterior.stockOriginal= Number(productoAnterior.stock) + Number(diferencia);
						
						//Me guardo la informacion del producto anterior con el stock actualizado
						await productoAnterior.save();

					}
					//Me guardo la informacion del producto nuevo con el stock actualizado
					await productoActualizado.save();

				}
			}

			//Si está activada la opción de actualizar las cuentas
			if(actCuentas) {

				//Si la cuenta contable asociada al detalle actualizado,
				//es la misma que la del detalle previo a actualizar,
				//le repongo el subtotal anterior y le deduzco el nuevo subtotal
				if(formaPagoAnterior==detalleAActualizar.formaPago) {
					cuentas[detalleAActualizar.formaPago] = 
						Number(cuentas[detalleAActualizar.formaPago]) +
						Number(subtotalAnterior) -
						Number(detalleAActualizar.subtotal);
				}


				//Si la cuenta contable asociada al detalle actualizado,
				//es diferente a la del detalle previo a actualizar:
				//A la cuenta correspndiente a la forma de pago del detalle
				//previo a actualizar le repongo el subtotal de la compra previo a la actualización,
				//y a la cuenta correspondiente al detalle actualizado, le resto
				 //el subtotal del elemento del detalle actualizado.
				else {
					cuentas[formaPagoAnterior] =
						Number(cuentas[formaPagoAnterior]) +
						Number(subtotalAnterior);
						
					cuentas[detalleAActualizar.formaPago] = 
						Number(cuentas[detalleAActualizar.formaPago]) -
						Number(detalleAActualizar.subtotal);
				}

				//Guardo la actualizacion de las cuentas en la base de datos
				await cuentas.save();

			}

				//Devuelvo un string indicando que la compra fue actualizada correctamente
				return res.json('¡Detalle de compra actualizado!');
		   
		//Cierro if de validación
		}

		//Aqui entro si falló la validación
		else {
			return res.json('Validacion fallida');
		}
	}

    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

});



//Devuelvo las compras completas, a traves de su codigo y fecha

router.route('/listaCompras').get( async(req, res) => {

	try{
		const arrayCompras= await listarCompras();
		const compradores=  await Usuario.find({categoria: "administrador"});
		return res.json({arrayCompras,compradores});
	}
	catch (err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
})


router.route('/listaCompras/:idCompra').get( async(req, res) => {

	try{
		const compra=await Compra.findById(req.params.idCompra);
		const compradores=  await Usuario.find({categoria: "administrador"});
		const arrayCompras= await listarCompras();
		return res.json({arrayCompras,compra,compradores});
	}
	catch (err) {
			console.log(err);
			return res.status(400).json('Error: ' + err);
		}
})


const listarCompras = async function(){
	try{
		const arrayCompras= await Compra.find();
		return arrayCompras
			.map(function (compra){
				compraListada={	
					id:compra._id,
					codCompra:compra.codCompra,
					fecha:formatearFecha(compra.fechaHora),
					hora:formatearHora(compra.fechaHora),
					comprador: compra.comprador,
					proveedor: compra.proveedor		  
				}
				return compraListada;
			}
		);
	} 
	catch(err) {
        console.log(err);
        return err;
    }
};
   

router.route('/reasignar/').post( async (req, res) => {

    try {

        //console.log(req.body);
        //Me guardo el id de la compra a la que quiero reasignar el detalle
        const idCompraAReasignar=req.body.idCompraAReasignar;
        const compraExistente=req.body.compraExistente;
        
        const idDetalle=req.body.idDetalle;
		const idCompraOriginal=req.body.idCompraAsociada;
        //Busco el detalle por su id, y me guardo el listado
        //completo de compras, y el id asociado a la compra original
        //const {detalle,compras,idCompraOriginal} = await buscarDetalle(req.body.idDetalle);
		const respuestaBuscarDetalle=await buscarDetalle(idDetalle,idCompraOriginal);
	

        //No hago destructuring, ya que necesito castear el id devuelto por buscarDetalle
        //de objectId a string
        const detalle=respuestaBuscarDetalle.detalle;
        const compras=respuestaBuscarDetalle.compras;
  
        //Si quiero reasignar el detalle a una compra ya existente, entro a este if
        if(compraExistente) {

            //Si la compra a la que quiero reasignar el detalle es
            //diferente de la que está actualmente asignada,
            //entro a este if
            if(idCompraAReasignar != idCompraOriginal) {
                //Busco por ID la compra a la cual le quiero reasignar el detalle
                let compraAReasignar= await Compra.findById(idCompraAReasignar);

                //A dicha compra, le concateno el elemento del detalle que quiero reasignar
                compraAReasignar.detalle= await compraAReasignar.detalle.concat(detalle);
                //Guardo la compra con el elemento de detalle añadido
                await compraAReasignar.save();

                //Elimino el detalle en la compra a la cual estaba asignado originalmente
                eliminarDetalle(detalle._id.toHexString(),idCompraOriginal,false,false);

                //Devuelvo un JSON indicando que la compra fue reasignada exitosamente
                return res.json({
						id:idCompraAReasignar,
						mensaje:"Compra reasignada exitosamente"
					});
            }

            //Si la compra a la que quiero reasignar el detalle es
            //la misma en la que está actualmente asignada,
            //entro a este else (no hago nada mas que devolver un mensaje)
            else {
                //console.log("Entro al else");
                return res.json({
					id:idCompraOriginal,
					mensaje:"La compra elegida para la reasignacion es la misma que la original"
                });
            }
        }

        //Si quiero reasignar el detalle a una nueva compra, entro a este else
        else {
			//Elimino el detalle en la compra original (sin actualizar stock ni cuentas)
            eliminarDetalle(detalle._id.toHexString(),idCompraOriginal,false,false);
        
            const codCompra = await Number(req.body.codCompra);
            const comprador = await req.body.userComprador;
            const proveedor = await req.body.proveedor;
            const fechaHora= await req.body.fechaHora;
            const arrayDetalle=[detalle];

			//Genero una nueva compra con los datos
			//de encabezado recibidos desde el front,
			//y el mismo detalle que ya tenía
            const nuevaCompra = await new Compra({

                codCompra,
                comprador,
                proveedor,
                detalle,
                fechaHora

            });

            await nuevaCompra.save();
            
            return res.json({
				id:nuevaCompra._id,
				mensaje:"Se generó una nueva compra con el detalle asignado"
            });
        }


    }
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }

})

//Edito el encabezado de una compra
router.route('/editarEncabezado/:idCompra').post( async (req, res) => {

    try {
		//Me guardo en una variable la compra que estoy editando
		let compraEditada=await Compra.findById(req.params.idCompra);
		
		//Me guardo en consts los campos que actualizo que
		//recibo del frontend
        const codCompra=req.body.codCompra;
        const comprador=req.body.userComprador;
        const fechaHora=req.body.fechaHora;
        const proveedor=req.body.proveedor;
        const hora=req.body.hora;
        
        //Me guardo en una constante la lista de compras que tengo
        //actualmente en la base de datos
        const listaCompras=await listarCompras();
        
        //Si en dicha lista no tengo una compra con los valores
        //correspondientes al código de compra, comprador, y fecha
        //que recibo desde el frontend
        if(!listaCompras.some(
			function (compra) {
				return (codCompra==compra.codCompra && comprador==compra.comprador && formatearFecha(fechaHora)==compra.fecha)

				}
			)
        )
        //Edito los campos del encabezado con los valores que recibí
        {
            
            compraEditada.codCompra=codCompra;
            compraEditada.comprador=comprador;
            compraEditada.fechaHora=fechaHora;
            compraEditada.proveedor=proveedor;
            await compraEditada.save();
            return res.json("Encabezado modificado correctamente");
        }
        //Si tengo los mismos valores de código de compra, comprador, y fecha que
        //ya tenía dicha compra, pero con proveedor y/o hora diferentes,
        else if(
			codCompra==compraEditada.codCompra &&
			comprador==compraEditada.comprador && 
			formatearFecha(fechaHora)==formatearFecha(compraEditada.fechaHora) &&
			(
				proveedor != compraEditada.proveedor ||
				hora != formatearHora(compraEditada.fechaHora)
			)
		)
        {
			//Actualizo solamente los campos 'proveedor' y 'fechaHora'
			//Si bien actualizo el campo fechaHora, validamos
			//que solo se modifique la hora, la cual no me
			//identifica la compra, como tampoco el proveedor.
			compraEditada.proveedor=proveedor;
			compraEditada.fechaHora=fechaHora;
			await compraEditada.save();
            return res.json("Encabezado modificado correctamente");
            
        }
        
        //Si ya existe una compra con el mismo código de compra, comprador, y fecha,
        //y ademas con el mismo proveedot y hora
        //devuelvo un mensaje indicando que no puedo editar el encabezado con esos valores
        else{
			return res.json("Ya existe una compra con ese encabezado");
		}

    }
    catch (err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
})

module.exports = router;
