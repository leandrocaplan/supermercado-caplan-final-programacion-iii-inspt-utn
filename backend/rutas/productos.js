const router = require('express').Router();
let Producto = require('../modelos/modelo.producto');

const mongoose = require('mongoose');

//Obtengo el listado de productos
router.route('/').get(async(req, res) => {	
	try {
		const productos=await Producto.find();
		return res.json(productos);
	}
   catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});


//Agrego un producto
router.route('/agregar').post(async(req, res) => {
	try{
		const _id =   mongoose.Types.ObjectId(req.body._id.padStart(24, '0'));
		const descripcion = req.body.descripcion;
		const unidad = req.body.unidad;
		const precioCompra = req.body.precioCompra;
		const precioVenta = req.body.precioVenta;
		const stock = req.body.stock;
		const stockOriginal = req.body.stock;
		
		const productos=await Producto.find();
		const arrayIdExistentes=productos.map(
			function(producto){
				return (Number(producto._id));
			}
		);
		
		if(
			(! (arrayIdExistentes.some(idExistente => _id==idExistente)))&&
			(Number(precioCompra)>0) &&
			(Number(precioVenta)>0) &&
			(Number(stock)>=0) &&
			(Number(stockOriginal)>=0)
		)
		{
			const newProducto = new Producto({_id,descripcion,unidad,precioCompra,precioVenta,stock,stockOriginal});
			await newProducto.save()	
			return res.json('Producto agregado');
		}
		else
		{
			return res.json('Validacion fallida');
		}
		
	}
    catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});


//Obtengo un producto por su ID
router.route('/:id').get(async(req, res) => {
	try{
		const producto= await Producto.findById(req.params.id);
		return res.json(producto);
	}
    catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});

//Elimino un producto por su id
router.route('/:id').delete(async(req, res) => {
	try{
		await Producto.findByIdAndDelete(req.params.id)
		return res.json('Producto eliminado');
	}
    catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});


//Actualizo un producto por su id
router.route('/actualizar/:id').post(async(req, res) => {

	try{
		let producto=await Producto.findById(req.params.id);
		const diferencia=Number(producto.stockOriginal) - Number(producto.stock);
		
		producto.descripcion = req.body.descripcion;
		producto.unidad=req.body.unidad;
		producto.precioVenta = req.body.precioVenta;
		producto.precioCompra = req.body.precioCompra;
		producto.stock = Number(req.body.stock);
		producto.stockOriginal = Number(req.body.stock) + Number(diferencia); 


		if(
			(Number(producto.precioCompra)>0) &&
			(Number(producto.precioVenta)>0) &&
			(Number(producto.stock)>=0) &&
			(Number(producto.stockOriginal)>=0) 
		)
		{
			await producto.save();
			return res.json('¡Producto Actualizado!');
		}
		else
		{
			return res.json('Validacion fallida');
		}		
	}
	catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
});


//Devuelvo un numero que sea una unidad mayor al máximo ID que tengo registrado
//Genero esta ruta para que no se pise con la ruta '/', definida arriba de todo
router.route('/generar/generarId').get(async(req, res) => {
	try{
		const productos=await Producto.find();

		const arrayIdExistentes=productos.map(
			function(producto){
				return (Number(producto._id));
			}
		);
		const idPred= arrayIdExistentes.length>0 ? Math.max(...arrayIdExistentes) + 1 : 1;

		return res.json({
			idPred:idPred,
			arrayIdExistentes:arrayIdExistentes
		});
	}
	catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});

module.exports = router;
