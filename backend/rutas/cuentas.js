const router = require('express').Router();
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const unico="000000000000000000000000";

let Cuentas = require('../modelos/modelo.cuentas');

//Devuelvo el estado de las cuentas
router.route('/').get(async(req, res) => {
	try {
		const cuentas= await Cuentas.findById(unico);
		return res.json(cuentas);
	}
	
	catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
    }
});


//Ingreso o retiro dinero de una cuenta
router.route('/actualizar/').post(async(req, res) => {
	//Busco el único documento contenido en la colección Cuentas
	try{
		let cuentas= await Cuentas.findById(unico);

		
		if(
			((req.body.cuenta=="caja") && Number(cuentas[req.body.cuenta]) + Number(req.body.monto) >= 0) ||
			((req.body.cuenta=="cuentaCorriente") && Number(cuentas[req.body.cuenta]) + Number(req.body.monto) >= -50000)
		)
		{
			cuentas[req.body.cuenta] +=  Number(req.body.monto);
			await cuentas.save();
			return res.json('¡Cuenta actualizada!');
		}
		else
		{
			return res.json('Error de validacion');
		}
	}
	catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
});

module.exports = router;
