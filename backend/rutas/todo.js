const router = require('express').Router();

let Venta = require('../modelos/modelo.venta');
let Producto = require('../modelos/modelo.producto');
let Usuario = require('../modelos/modelo.usuario');
let Compra = require('../modelos/modelo.compra');
let Cuentas = require('../modelos/modelo.cuentas');

const unico="000000000000000000000000";

//Devuelvo toda la informacion existente
//en la base de datos
router.route('/').get( async(req, res) => {
	try{
		const ventas=await Venta.find();
		const usuarios= await Usuario.find();
		const productos= await Producto.find();
		const compras= await Compra.find();
		const cuentas = await Cuentas.findById(unico);      

		return res.json({
		  "ventas":ventas,
		  "usuarios":usuarios,
		  "productos":productos,
		  "compras": compras,
		  "cuentas": cuentas
		});
	}
	catch(err) {
        console.log(err);
        return res.status(400).json('Error: ' + err);
    }
});


module.exports = router;
