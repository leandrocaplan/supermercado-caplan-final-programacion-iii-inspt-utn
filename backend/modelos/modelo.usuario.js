const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const usuarioSchema = new Schema({
  username: { type: String,required: true, unique: true, trim: true, minlength: 1},
  password: { type: String,required: true, unique: false, trim: true, minlength: 1},
  nombre: { type: String,required: true, unique: false, trim: true, minlength: 1},
  apellido: { type: String,required: true, unique: false, trim: true, minlength: 1},
  categoria: { type: String,required: true, unique: false, trim: true, minlength: 1}
});

const Usuario = mongoose.model('Usuario', usuarioSchema);

module.exports = Usuario;
