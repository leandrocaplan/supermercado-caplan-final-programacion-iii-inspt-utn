const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ObjectId = Schema.Types.ObjectId;
const cuentasSchema = new Schema(
{
    _id: { type: ObjectId, ref:'Cuentas', required: true },
    caja: { type: Number, required: true },
    cuentaCorriente: { type: Number, required: true },
    creditoAFavor: { type: Number, required: true },
});

const Cuentas = mongoose.model('Cuentas', cuentasSchema);

module.exports = Cuentas;

