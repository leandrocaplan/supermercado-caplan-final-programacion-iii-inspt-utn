const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ventaSchema = new Schema({
  codVenta: { type: Number, required: true },
  vendedor: { type: String, required: true },
  
  detalle:[{
	    producto: { type: String, required: true },
	    precioActual:{ type: Number, required: true },
	    unidadActual: { type: String, required: true },
		cantidad:{ type: Number, required: true },
		tipoPago: { type: String, required: true },
		subtotal: { type: Number, required: true }
		}],
	
  fechaHora: { type: Date, required:true}
  
});

const Venta = mongoose.model('Venta', ventaSchema);

module.exports = Venta;
