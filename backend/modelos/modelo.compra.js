const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const compraSchema = new Schema({
	codCompra: { type: Number, required: true },
	comprador: { type: String, required: true },
	proveedor: { type: String, required: true }, 
  
	detalle:[{
	    producto: { type: String, required: true },
	    precioActual:{ type: Number, required: true },
	    unidadActual: { type: String, required: true },
		cantidad:	{ type: Number, required: true },
		formaPago: { type: String, required: true },
		subtotal: { type: Number, required: true }
	}],
	
	fechaHora: { type: Date}
  
});

const Compra = mongoose.model('Compra', compraSchema);

module.exports = Compra;
