const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ObjectId = Schema.Types.ObjectId;
const productoSchema = new Schema({
  _id: { type: ObjectId, ref:'Producto', required: true },
  descripcion: { type: String, required: true },
  unidad:  { type: String, required: true },
  precioCompra: { type: Number, required: true },
  precioVenta: { type: Number, required: true },
  stock: { type: Number, required: true },
  stockOriginal: { type: Number, required: true },
});

const Producto = mongoose.model('Producto', productoSchema);

module.exports = Producto;
