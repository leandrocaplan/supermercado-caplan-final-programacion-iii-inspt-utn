const express = require('express');
const router = require('express').Router();
const cors = require('cors');
const mongoose = require('mongoose');

//Tengo un archivo con funciones para inicializar la base de datos
const inicializarDb = require('./inicializarDb');

require('dotenv').config();

const app = express();
const puerto = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

// const uri = process.env.ATLAS_URI;
const uri = process.env.LOCAL_URI;
	//mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true }
	mongoose.connect(uri, { useNewUrlParser: true,useUnifiedTopology: true  }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("Conexión satisfactoria a MongoDB");
  inicializarDb.primeraEjecucion();
})

//Me traigo todos los routers
const ventasRouter = require('./rutas/ventas');
const comprasRouter = require('./rutas/compras');
const usuariosRouter = require('./rutas/usuarios');
const productosRouter = require('./rutas/productos');
const cuentasRouter = require('./rutas/cuentas');
const todoRouter = require('./rutas/todo');

//Implemento una ruta para resetear la base de datos
app.get('/resetear', function(req, res) {
	try{
		inicializarDb.resetear(connection);
		return res.json("Base de datos reseteada")
	}
	catch(err) {
		console.log(err);
		return res.status(400).json('Error: ' + err);
	}
});

app.use('/', todoRouter);
app.use('/ventas', ventasRouter);
app.use('/compras', comprasRouter);
app.use('/usuarios', usuariosRouter);
app.use('/cuentas', cuentasRouter);
app.use('/productos', productosRouter);

app.listen(puerto, () => {
    console.log(`El servidor está corriendo en el puerto: ${puerto}`);
});
