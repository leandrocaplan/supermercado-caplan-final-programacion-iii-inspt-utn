let Usuario = require('./modelos/modelo.usuario');
let Cuentas = require('./modelos/modelo.cuentas');

const primeraEjecucion = async function(){
	try{
		const usuario = await Usuario.find({categoria:"administrador"});
		const cuentas= await Cuentas.findById("000000000000000000000000");

		if(usuario.length==0){
	
		  const username = "admin";
		  const password = "admin";
		  const nombre = "Primer";
		  const apellido = "Administrador";
		  const categoria = "administrador";

		  const primerUsuario = new Usuario({
		    username,
		    password,
		    nombre,
		    apellido,
		    categoria
		  });
		  console.log("Doy de alta el prmier usuario");
		  primerUsuario.save();
		}
	
		if(!cuentas){
	
		  const _id = "000000000000000000000000";
		  const caja = 0;
		  const cuentaCorriente = 0;
		  const creditoAFavor = 0;
			
		  const inicialCuentas = new Cuentas({
		    _id,
		    caja,
		    cuentaCorriente,
		    creditoAFavor,
		  });
		  console.log("Inicializo las cuentas en 0");
		  console.log(inicialCuentas);
		  inicialCuentas.save();
		}
	}
	catch(err) { 
		console.log(err);
		return res.status(400).json('Error: ' + err)
	}
};

const resetear = async function(connection){
	try{
		await connection.db.dropDatabase(primeraEjecucion());
	}
	 
	 catch(err)  {
		console.log(err);
		return res.status(400).json('Error: ' + err)
	}
}

exports.primeraEjecucion = primeraEjecucion;
exports.resetear = resetear;
