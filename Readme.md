Supermercado Caplan

Este es un sistema desarrollado para la materia Programación III del Instituto Nacinal Superior del Profesorado Técnico, dependiente de la Universidad Tecnológica Nacional.
Utiliza el stack MERN (MongoDB,Express,React y Node.js) para su implementación, como tambien hace uso de la librería de UI Reactstrap para su maquetado.




Descripción de la funcionalidad del proyecto:

Este sistema nos permite llevar el registro de la compra y venta de mercaderías de un supermercado.

Se lleva el registro de los productos existentes, su stock, y su precio de compra y de venta. A la vez, se lleva el registro de la venta (a clientes),
y la compra (a proovedores), de mercaderias.

Cada venta tiene asociada el producto vendido, su vendedor, la cantidad vendida, la forma de pago, y el total de la operación.

Cada compra tiene asociada el producto vendido, el proovedor de la compra, la cantidad comprada, la forma de pago, y el total de la operación.
Las compras deben siempre ser asignadas a un administrador

Una venta nos reduce el stock del producto vendido, a la vez que nos produce un ingreso monetario, mientras que una compra nos incrementa el stock del producto comprado,
a la vez que nos produce un egreso monetario.

Hay dos tipos de usuarios posibles: administradores y vendedores.
Aquellos usuarios registrados como vendedores, solo pueden realizar ventas, consultar el registro de sus propias ventas, y editarlas.

Los administradores, pueden realizar altas y modificaciones de productos, realizar ventas y asignarlas un vendedor, altas y modificaciones de usuarios,
y ademas, realizar compras y editarlas. Ademas, pueden consultar el registro de todas las operaciones descriptas anteriormente.

A la vez, llevamos una contabilidad muy básica del negocio, en donde las formas de pago de las ventas y de las compras, nos producen modificaciones en cada una de las cuentas:

Las ventas en efectivo nos incrementan la cuenta caja, las realizadas con tarjeta de débito nos producen un incremento en la cuenta corriente,
y las realizadas con tarjeta de crédito, un incremento en la cuenta de credito a favor.
En contrapartida, las compras las podemos realizar con cada una de estas cuentas, en las que se nos producirá el egreso en esas mismas correspondiente al importe de la operación.

Podemos desde el sistema, directamente ingresar o retirar los importes deseados de cada una de estas cuentas.
El sistema controla, ademas, los límites de cada una de las mismas, dado que el importe de la cuenta caja no puede ser negativo,
y la cuenta corriente tiene un límite de descubierto, que por el momento está fijado en el valor "-50000".

Es decir que cuando quiera realizar una operación que implique un resultado negativo en la cuenta caja, o un resultado menor a -50000 en la cuenta corriente,
el sistema no me lo va a permitir.

Al eliminar o modificar los datos de un registro de compra o venta, 
el sistema es capaz de corregir automáticamente la actualización de stock y el importe contable asociado a dicha operación en la cuenta correspondiente,
de forma que podemos evitar la necesidad de corregir manualmente dichos valores en caso de que haya existido un error en el registro de la operación,
o bien la misma debió ser cancelada.

Sin embargo, si solo queremos editar o eliminar un registro del detalle de una compra o una venta sin actualizar el stock del producto asociado al mismo,
o la cuenta asociada al mismo, el sistema tambien permite nos hacerlo, teniendo opciones que permiten habilitar o deshabilitar dicha función
 
Si bien para el propósito de este trabajo solo tenemos tres cuentas básicas, este sistema podría ser escalable,
de forma que posteriormente, incluso sea posible integrarlo a un sistema de contabilidad mas sofisticado.
Mientras que aquí solo tenemos tres cuentas básicas, un plan de cuentas de un sistema real puede llegar a tener cientos de cuentas.



Como ejecutar la aplicación localmente:

Este proyecto contiene dos servicios diferentes: la API (backend), y el frontend.
Ambos se ejecutan en un servidor local en puertos diferentes, para lo cual debemos tener instalado, en primer lugar, Node.js, y su manejador de paquetes: npm.
Tambien, debemos tener instalada de forma local el servicio de la base de datos MongoDB, el cual debe estar en ejecución para el funcionamiento de la aplicación.
Al estar el proyecto subido en un repositorio gitLab (o gitHub), debemos primero, clonar el proyecto entero, con ambos módulos (backend y frontend),
a nuestro equipo local a traves del comando "git clone <url del repositorio>".
Luego, debido a que en el repositorio no se encuentran cargados los módulos de Node.js necesarios para el correcto funcionamiento de ambos servicios,
debemos acceder, mediante la línea de comandos, a los subdirectorios "backend" y "frontend", del directorio raíz del proyecto, y ejecutar el comando "npm install"
en cada uno de ellos, de forma que los módulos necesarios para ejecutar ambos servicios se instalen correctamente en nuestro equipo.
A continuación, debemos, en dos consolas separadas, acceder a los subdirectorios "backend" y "frontend", y ejecutar el comando "npm start" en cada una de ellos.

Como para acceder al sistema es necesario tener al menos un usuario registrado, al iniciarse el mismo por primera vez,
dentro del backend se ejecuta una función que, de no existir ningún administrador dado de alta, automáticamente nos genera uno,
cuyas credenciales son "admin" para el nombre de usuario, y también "admin" para la contraseña.
A la vez, nos inicializa el estado de nuestras tres cuentas contables con el valor 0.
De esta forma, podemos ingresar al sistema con dichas credenciales ni bien lo descarguemos y ejecutemos en nuestro equipo local, sin necesidad de ninguna otra herramienta externa.

De todas formas, para una mejor depuración y evaluación de la funcionalidad de la aplicación, lo mas recomendable es tener instalada la aplicación MongoDB Compass,
la cual nos permite acceder directamente a la base de datos de forma visual con una GUI. Desde allí, podremos visualizar, asi como crear y editar manualmente,
todos los documentos que podrán ser generados y modificados desde la aplicacion.
Una alternativa puede ser la utilización de la herramienta MongoDB Shell, cuya funcionalidad es muy similar a MongoDB Compass,
pero su interfaz se basa en una línea de comandos en lugar de ser gráfica.

La URI con la cual haremos la conexión a la base de datos figura en el archivo ".env", situado en el subdirectorio "backend", en la cual figura el nombre de nuestra base de datos.
Por defecto, tenemos una conexión a una base de datos local (en localhost), y una base de datos llamada "mercado".
Si queremos dar de alta nuestro primer usuario de forma manual, debemos, en nuestra base de datos, crear una colección llamada "usuarios",
en la cual debemos ingresar un documento con los datos de nuestro primer usuario.
Los campos que debe tener dicho documento son:
- "_id" (el cual puede ser generado automáticamente por el motor de MongoDB),
- "username" (nombre de usuario), "password" (contraseña),
- "nombre,
- "apellido",
- "categoria" (puede ser "vendedor" o "administrador").

Una vez ingresado nuestro primer usuario, y al tener funcionando los servicios de backend y frontend, debemos acceder, desde nuestro navegador, a la URL "localhost:3000",
donde podremos realizar un login con las credenciales correspondientes y allí, finalmente, podremos acceder plenamente funcionalidad del sistema.




Funcionamiento de los componentes "Editar compra" y "Editar venta":

Cuando editamos un elemento del detalle de una compra o una venta, podemos tener distintas situaciones posibles:

- El producto asociado al elemento del detalle que estamos editando, se corresponde con el mismo que teníamos originalmente en dicho elemento,
  o bien asignamos un producto diferente a dicho elemento del detalle.
  
- El tipo de pago (en el caso de las ventas), o la forma de pago (en el caso de las compras) asociado al elemento del detalle que estamos editando,
  se corresponde con el mismo que teníamos originalmente en dicho elemento, o bien asignamos un tipo o forma de pago diferente a dicho elemento del detalle.

Esto nos presenta un escenario con cuatro situaciones posibles diferentes.

Para las ventas: 

Al editar el registro de un elemento del detalle, si el producto asignado al mismo se corresponde con el que tenía asignado originalmente.
y tengo activada la opción de actualizar stock al editarlo, nuestro stock resultante será:

- El stock que tengo en dicho producto,
- mas la cantidad asignada al elemento del detalle de venta antes de actualizarlo,
- menos la cantidad del detalle de venta actualizado.

De asignarle un producto diferente, se actualizará tanto el stock del producto que tenía asignado previamente como del que le asigno al detalle actualizado.

En este caso, los resultantes serán:

- Al producto correspondiente que asigno al elemento del detalle que actualizo, le resto el stock correspondiente
  a la cantidad que ingreso en el detalle que estoy actualizando.

- Si es que aún existe en la base de datos, al producto que tenía en el detalle previamente a la actualización,
  le repongo el stock correspondiente a la cantidad que tenía previamente en el elemento de detalle de venta.


Para la actualización de las cuentas contables, hago un procedimiento similar pero inverso:

Al editar el registro de un elemento del detalle, si la cuenta contable asignada al mismo se corresponde con el que tenía asignado originalmente.
y tengo activada la opción de actualizar cuentas al editarlo, nuestro resultante de cuenta será:

- El importe que tengo en dicha cuenta,
- menos la el importe asignado al elemento del detalle de venta antes de actualizarlo,
- mas el importe del detalle de venta actualizado.

De asignarle un tipo de pago diferente, se actualizará tanto la cuenta contable que tenía asignada previamente como la que le asigno al detalle actualizado.

En este caso, los resultantes serán:

- A la cuenta contable correspondiente a la que asigno al elemento del detalle que actualizo, le sumo el importe
  correspondiente al que obtengo en el detalle que estoy actualizando.

- A la cuenta contable correspondiente a la que tenía en el detalle previamente a la actualización,
  deduzco el correspondiente al que tenía en el detalle previamente a la actualización.


Para las compras, tengo una situación especular a la de las ventas:

Al editar el registro de un elemento del detalle, si el producto asignado al mismo se corresponde con el que tenía asignado originalmente.
y tengo activada la opción de actualizar stock al editarlo, nuestro stock resultante será:

- El stock que tengo en dicho producto,
- menos la cantidad asignada al elemento del detalle de venta antes de actualizarlo,
- mas la cantidad del detalle de venta actualizado.

De asignarle un producto diferente, se actualizará tanto el stock del producto que tenía asignado previamente como del que le asigno al detalle actualizado.

En este caso, los resultantes serán:

- Al producto correspondiente que asigno al elemento del detalle que actualizo, le sumo el stock correspondiente
  a la cantidad que ingreso en el detalle que estoy actualizando.

- Si es que aún existe en la base de datos, al producto que tenía en el detalle previamente a la actualización,
  le deduzco el stock correspondiente a la cantidad que tenía previamente en el elemento de detalle de venta.


Para la actualización de las cuentas contables, hago un procedimiento similar pero inverso:

Al editar el registro de un elemento del detalle, si la cuenta contable asignada al mismo se corresponde con el que tenía asignado originalmente.
y tengo activada la opción de actualizar cuentas al editarlo, nuestro resultante de cuenta será:

- El importe que tengo en dicha cuenta,
- mas la el importe asignado al elemento del detalle de venta antes de actualizarlo,
- menos el importe del detalle de venta actualizado.

De asignarle una forma de pago diferente, se actualizará tanto la cuenta contable que tenía asignada previamente como la que le asigno al detalle actualizado.

En este caso, los resultantes serán:

- A la cuenta contable correspondiente a la que asigno al elemento del detalle que actualizo, le deduzco el importe
  correspondiente al que obtengo en el detalle que estoy actualizando.

- A la cuenta contable correspondiente a la que tenía en el detalle previamente a la actualización,
  sumo el correspondiente al que tenía en el detalle previamente a la actualización.

Se debe tomar en cuenta que, es probable que antes de hacer la actualización de algún elemento del detalle de ventas o compras,
se actualizen los precios de los productos, por lo que podría obtener un resulante de cuenta distinto al hacer
la edición, sin siquiera editar ningún campo al utilizar dicha función.
